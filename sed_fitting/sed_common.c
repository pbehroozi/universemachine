void gen_sfh_two_c(double total_sm, double plaw, double burst_f, double burst_t) {
  int64_t i;
  double tsm=0, tsm2 = 0;
  for (i=0; i<NUM_SCALES+1; i++) {
    if (stimes[NUM_SCALES]-stimes[i] < burst_t) {
      sfh[i] = pow(stimes[i]/stimes[NUM_SCALES], plaw)*dt[i];
      //sfh[i] = dt[i];
      tsm2 += sfh[i];
    } else {
      sfh[i] = pow(stimes[i]/stimes[NUM_SCALES], plaw)*dt[i];
      tsm += sfh[i];
    }
  }
  if (tsm > 0) tsm = pow(10, total_sm)*(1.0-burst_f)/tsm;
  if (tsm2 > 0) tsm2 = pow(10, total_sm)*burst_f/tsm2;
  for (i=0; i<NUM_SCALES+1; i++) {
    if (stimes[NUM_SCALES]-stimes[i] < burst_t) sfh[i] *= tsm2;
    else sfh[i] *= tsm;
  }
}


void gen_sfh(double total_sm, double plaw) {
  int64_t i;
  double tsm=0;
  for (i=0; i<NUM_SCALES+1; i++) {
    sfh[i] = pow(stimes[i]/stimes[NUM_SCALES], plaw)*dt[i];
    tsm += sfh[i];
  }
  if (tsm > 0) tsm = pow(10, total_sm)/tsm;
  for (i=0; i<NUM_SCALES+1; i++)
    sfh[i] *= tsm;
}



void lums_of_sfh(float **lum_lookup, float *sfh, float *scales, int64_t n, float dust, float dust2, float burst_t, float Z_offset, float *lums, float *auv) {
  int64_t i, j;
  double total_sm = 0;
  double total_l[NUM_FILTERS] = {0};
  double total_l_dust[NUM_FILTERS] = {0};

  double total_l2[NUM_FILTERS] = {0};
  double total_l_dust2[NUM_FILTERS] = {0};
  for (i=0; i<=n; i++) {
    double new_sm = sfh[i];
    //Use instantaneous recycling fraction of 30%
    total_sm += new_sm*0.7;
    if (total_sm < 1) total_sm = 1;
    float z = 1.0/scales[i]-1.0;
    float Z = metallicity(log10f(total_sm), z)+Z_offset;
    for (j=0; j<NUM_FILTERS; j++) {
      float avg_lum_nodust = lookup_lum(lum_lookup[j], Z, n, i, 0);
      float avg_lum_dust = lookup_lum(lum_lookup[j], Z, n, i, 1);
      if (burst_t > 0 && (stimes[i]< stimes[NUM_SCALES]-burst_t)) {
	total_l[j] += avg_lum_nodust*new_sm;
	total_l_dust[j] += avg_lum_dust*new_sm;
      } else {
	total_l2[j] += avg_lum_nodust*new_sm;
	total_l_dust2[j] += avg_lum_dust*new_sm;
      }
    }
  }
  if (dust < 0) dust = 0;
  //  if (dust > 2) dust = 2;
  for (j=0; j<NUM_FILTERS; j++) {
    if (!((total_l[j]+total_l2[j]) > 0 && (total_l_dust[j]+total_l_dust2[j]) > 0)) lums[j] = 1000;
    else {
      if (!(total_l[j] > 0)) { total_l[j] = 1e-100; }
      if (!(total_l2[j] > 0)) { total_l2[j] = 1e-100; }
      lums[j] = log10f(total_l[j]*pow(total_l_dust[j]/total_l[j], dust) +
		       total_l2[j]*pow(total_l_dust2[j]/total_l2[j], dust2))*-2.5;


      //((log10f(total_l[j]) + dust*log10f(total_l_dust[j]/total_l[j]))*-2.5);
      if (j==0) {
	*auv = lums[0] - log10f(total_l[0]+total_l2[0])*-2.5;
      }
    }
  }
}



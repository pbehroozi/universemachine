#ifndef _CONFIG_VARS_H_
#define _CONFIG_VARS_H_
#include <stdint.h>

#define string(a,b)  extern char *  a;
#define real(a,b)    extern double  a;
#define real3(a,b)   extern double  a[3];
#define integer(a,b) extern int64_t a;
//#define real_array(a, __VA_ARGS__) extern double *a; extern int64_t a ## _LENGTH;

#include "config.template.h"

#undef string
#undef integer
#undef real
#undef real3
#undef real_array

#endif /* _CONFIG_VARS_H_ */

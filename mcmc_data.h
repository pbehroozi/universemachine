#ifndef _MCMC_DATA_H_
#define _MCMC_DATA_H_

#include <inttypes.h>
#include "make_sf_catalog.h"
#include "observations.h"


struct mcmc_data {
  void *all;
  int64_t file_length, total_length, num_scales, num_sd, num_obs, index;
  int64_t num_samples;
  struct sf_model_allz *model;

  char *filename;
  float *scales;
  float *shared_data;
  struct observation *obs;
  
#define shared(a,b) float *a;
#include "shared.template.h"
#include "pshared.template.h"
#undef shared
};

struct mcmc_data *load_mcmc_data(char *filename);
void _load_mcmc_data(struct mcmc_data *md, int64_t index);
void free_mcmc_data(struct mcmc_data *md);

#endif  /* _MCMC_DATA_H_ */

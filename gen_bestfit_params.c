#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stringparse.h"
#include "universal_constants.h"
#include "stats.h"

struct sf_model_allz *ms = NULL;
int64_t num_ms = 0;
double *vals = NULL;

int sort_double(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

void gen_correlation_plot(double z, char *filename, struct sf_model_allz b, int64_t type);


int main(int argc, char **argv) {
  int64_t i, j, k;
  char buffer[2048];
  struct sf_model_allz m = {{0}}, b={{0}}, u, d;
  enum parsetype types[NUM_PARAMS+1];
  void *data[NUM_PARAMS+1];
  SHORT_PARSETYPE;
  for (i=0; i<NUM_PARAMS+1; i++) {
    types[i] = F64;
    data[i] = &(m.params[i]);
  }
  data[NUM_PARAMS] = &(CHI2(m));
  while (fgets(buffer, 2048, stdin)) {
    if (stringparse(buffer, data, types, NUM_PARAMS+1)<NUM_PARAMS+1) continue;
#define translate2(a,b) (a)-=(b)
#define translate3(a,b,c) (a)-=((b)+(c))
    translate3(EFF_0_A(m), EFF_0_A2(m), EFF_0_A3(m));
    translate3(V_1_A(m), V_1_A2(m), V_1_A3(m));
    translate3(ALPHA_A(m), ALPHA_A2(m), ALPHA_A3(m));
    translate2(BETA_A(m), BETA_A2(m));
    translate2(GAMMA_A(m), GAMMA_A2(m));
    translate2(Q_LVMP_A(m), Q_LVMP_Z(m));
    translate2(Q_SIG_LVMP_A(m), Q_SIG_LVMP_Z(m));
    DUST_WIDTH(m) = 0.23/DUST_WIDTH(m);
    DUST_M(m) = -20-2.5*(DUST_M(m)-9.2);
    DUST_M_Z(m) = -2.5*DUST_M_Z(m);
    check_realloc_every(ms, sizeof(struct sf_model_allz), num_ms, 1000);
    ms[num_ms] = m;
    num_ms++;
    if (!CHI2(b) || CHI2(m) < CHI2(b)) b=m;
  }

  check_realloc_s(vals, sizeof(double), num_ms);
  for (i=0; i<NUM_PARAMS+2; i++) {
    for (j=0; j<num_ms; j++) vals[j] = ms[j].params[i];
    qsort(vals, num_ms, sizeof(double), sort_double);
    u.params[i] = vals[(int64_t)(num_ms*(1.0+ONE_SIGMA)/2.0)];
    d.params[i] = vals[(int64_t)(num_ms*(1.0-ONE_SIGMA)/2.0)];
    m.params[i] = vals[(int64_t)(num_ms/2.0)];
  }

  check_mkdir("plots", 0755);
  check_mkdir("plots/fits", 0755);

  FILE *out = check_fopen("plots/fits/fit.dat", "w");
  fprintf(out, "#Best Err+ Err- Median\n");
  for (int64_t i=0; i<NUM_PARAMS+2; i++)
    fprintf(out, "%.10e %.10e %.10e %.10e\n", b.params[i], u.params[i]-b.params[i], b.params[i]-d.params[i], m.params[i]);
  fclose(out);

  //Correlation coeffs
  check_mkdir("plots/data", 0755);
  check_mkdir("plots/data/galaxy_halo_correlations", 0755);  
  for (i=0; i<3; i++) {
    snprintf(buffer, 2048, "plots/data/galaxy_halo_correlations/corr_z%"PRId64".dat", i);
    gen_correlation_plot(i, buffer, b, 0);
  }
  gen_correlation_plot(i, "plots/data/orphan_thresh.dat", b, 1);


  check_mkdir("plots/data/parameter_correlations", 0755);
  out = check_fopen("plots/data/parameter_correlations/pcorrs.dat", "w");
  fprintf(out, "#I J Rank_correlation Pearson_correlation\n");
  //int64_t obs_sm_off = &((OBS_SM_SIG(m)))-m.params;
  //int64_t rdecay_off = &((RDECAY(m)))-m.params;
  struct correl_stats *cs = init_correl(1);
  for (i=0; i<NUM_PARAMS+2; i++) {
    //if (i==obs_sm_off) continue;
    //if (i==rdecay_off) continue;
    if (i==NUM_PARAMS) continue;
    for (j=i; j<NUM_PARAMS+2; j++) {
      //if (j==obs_sm_off) continue;
      //if (j==rdecay_off) continue;
      if (j==NUM_PARAMS) continue;

      int64_t ti=i, tj=j;
      if (ti>NUM_PARAMS) ti--;
      if (tj>NUM_PARAMS) tj--;
      //if (ti > obs_sm_off) ti--;
      //if (ti > rdecay_off) ti--;
      //if (tj > obs_sm_off) tj--;
      //if (tj > rdecay_off) tj--;

      if (i==j) {
	fprintf(out, "%"PRId64" %"PRId64" 1 1\n", ti, tj);
	continue;
      }
      clear_correl(cs);
      for (k=0; k<num_ms; k++)
	add_to_correl(cs, ms[k].params[i], ms[k].params[j]);
      calc_rank_correl(cs);
      calc_correl(cs);
      printf("%"PRId64" %"PRId64" %f %f\n", ti, tj, cs->rank_rho, cs->rho);
      fprintf(out, "%"PRId64" %"PRId64" %f %f\n", ti, tj, cs->rank_rho, cs->rho);
      fprintf(out, "%"PRId64" %"PRId64" %f %f\n", tj, ti, cs->rank_rho, cs->rho);
    }
  }
  fclose(out);
  return 0;
}


double mass_to_vmax (double m, double z) {
  double a = 1.0/(1.0+z);
  double a200 = a / 0.3782;
  double m200 = log10((1.0/0.68)*1.115e12 / ((pow(a200, -0.142441) + pow(a200, -1.78959))));
  double v200 = log10(200);
  double exp_vp = (m - m200)/3.0 + v200;
  return exp_vp;
}

double galaxy_halo_correlation(double lvmp, double a, struct sf_model_allz *m) {
  double r_min = R_MIN(*m);
  double r_width = R_WIDTH(*m);
  double r_cen = R_CENTER(*m)+(1.0-a)*R_CENTER_A(*m);
  return (r_min + (1.0-r_min)*(0.5+0.5*erf((r_cen-lvmp)/(r_width*M_SQRT2))));
}


double orphan_threshold(double lvmp, double a, struct sf_model_allz *m) {
  double r_min = ORPHAN_THRESH_V1(*m);
  double r_max = ORPHAN_THRESH_V2(*m);
  double r_width = ORPHAN_LVMP_WIDTH;
  double r_cen = ORPHAN_LVMP;
  return (r_min + (r_max-r_min)*(0.5+0.5*erf((lvmp-r_cen)/(r_width*M_SQRT2))));
}

void gen_correlation_plot(double z, char *filename, struct sf_model_allz b, int64_t type) {
  FILE *out = check_fopen(filename, "w");
  int64_t i, j, nbins = 81;
  double m_min = 8, m_max = 16;
  double a = 1.0/(1.0+z);
  fprintf(out, "#Log10(Mass) %s Err+ Err- LVMP\n", (type) ? "Orphan_thresh" : "R");
  double (*corr_func_ptr)(double, double, struct sf_model_allz *) = (type) ? orphan_threshold : galaxy_halo_correlation;
  for (i=0; i<nbins; i++) {
    double m = m_min + (m_max-m_min)*((double)i/(double)(nbins-1));
    double lvmp = mass_to_vmax(m, z);
    double bf = corr_func_ptr(lvmp, a, &b);
    for (j=0; j<num_ms; j++)
      vals[j] = corr_func_ptr(lvmp, a, ms+j);
    qsort(vals, num_ms, sizeof(double), sort_double);
    double u = vals[(int64_t)(num_ms*(1.0+ONE_SIGMA)/2.0)];
    double d = vals[(int64_t)(num_ms*(1.0-ONE_SIGMA)/2.0)];
    fprintf(out, "%.1f %.6g %.6g %.6g %.3f\n", m, bf, u-bf, bf-d, lvmp);
  }
  fclose(out);
}

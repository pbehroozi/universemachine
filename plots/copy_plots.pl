#!/usr/bin/perl -w
my @plots = qw/smf_comp qf_comp ssfr_comp uvlf_comp uvsm_comp sfr_fsf_db06 sfr_ma_time sfr_all sfr_sf_vmax sfr_fsf_vmax sfr_all_errors sfr_fsf_errors orphan_thresh galaxy_halo_correlations/;
for (@plots) {
    system("cp", "PDF/$_.pdf", "../paper/graphs");
}

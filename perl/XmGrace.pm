package XmGrace;
use Carp;
use ProcessTemplate;
use Universe::Time;
use MultiThread;

our $MultiThread = 0;

our %default_params = (title => "", subtitle => "", xlabel => "", ylabel => "",
		       legendxy => "0.65, 0.8", labelsize => "1.5", lvgap => 1,
		       data => [], 
		       legend2xy => "0.5, 0.3",
		       legend2vg => 0.06,
		       legend3xy => "0.5, 0.3",
		       legend3vg => 0.06,
		       legend4xy => "0.5, 0.3",
		       legend4vg => 0.06,
		       labelcolor => 1,
		       greyscale => 0, page_size => 612, yviewmin => "0.15", xviewmin => "0.25",
		       xviewmax => "1.25", yviewmax => "0.85",
		       xaxis => "on", yaxis => "on",
                       xaxisnumbers => "on", yaxisnumbers => "on", legendfill => "0",
		       altxaxisnumbers => "on", 
		       altyaxisnumbers => "on", 
		       altxaxis => "off",
		       altyaxis => "off",
		       xticklstyle => "1",
		       yticklstyle => "1",
		       xaxistickplace => "both", 
		       yaxistickplace => "both", 
		       altxlabel => "",
		       altylabel => "",
		       altxaxisformat => "general", altyaxisformat => "general",
		       xaxisformat => "general", yaxisformat => "general",
		       xtickspec => "none", ytickspec => "none",
		       altxtickspec => "none", altytickspec => "none",
		       altxmajortick => "10", altxminorticks => 9,
		       altymajortick => "10", altyminorticks => 9,
		       xaxisprec => 5, yaxisprec => 5, 
		       altxaxisprec => 5, altyaxisprec => 5, 
		       fbgcolor => 0,
		       xtickdir => "in", ytickdir => "in",
		       altxtickdir => "in", altytickdir => "in",
		       xticklabelangle => 0,
		       text => [], boxes => [], lines => [], ellipses => []);
our %default_style = (type => "xy", legend => "", legend2 => undef, legend3 => undef, legend4 => undef, data => "", stype => 0, ssize => 0.16, ltype => 1, lstyle => 1, arrowwidth => 1.0, lwidth => 4.0, ftype => 0, fcolor => 1, sskip => 0, errorplace => "both", spattern => 1, slstyle => 0, slwidth => 1);

our %def_text = (text => "", pos => "0.5, 0.5", color => 1, rot => 0, just => 0);

sub import {
    foreach (@_) {
	if (/^gr[ea]yscale/) { $default_params{greyscale} = 1; }
    }
}

sub new {
    my $self = shift;
    bless { @_ }, $self;
}

#sub _err {
#    warn @_;
#    return;
#}

sub to_agr {
    my $self = shift;
    $default_params{$_} = [] for (qw/data text lines boxes/);
    my %params = (%default_params, %$self);
    local $_;
    #The following params will be auto-specified if not given:
    # xmin, ymin, xmax, ymax, xscale, yscale, xmajortick, ymajortick,
    # xminorticks, yminorticks, labelsize

    $params{xticklabelsize} ||= $params{labelsize};
    $params{yticklabelsize} ||= $params{labelsize};
    if ($params{vscale}) { 
	#$params{page_size} = int($params{page_size}*$params{vscale});
	#$params{xview} = 0.25 + 1/$params{vscale};
	$params{yviewmin} = 0.85-0.8*$params{vscale};
	#$params{labelsize} /= $params{vscale};
    }
    my $data = $params{data};
    if (defined($data) and !(ref $data) and length($data)) {
	$data = $params{data} = [{data=>$data}];
    }
    return carp("No graph data!\n") unless (@$data);

    #Calculate automatic variables.
    my ($xmax, $ymax, $xmin, $ymin) = (-1e300, -1e300, 1e300, 1e300);
    my ($noauto) = ((grep { exists $params{$_} }
		     qw(xmax xmin ymax ymin))==4) ? 1 : 0;
    
    for (my $i = 0; $i < @$data; $i++) {
	my $gdata = $data->[$i];
	if (convert_data($gdata, \%params)<0) {
	    if ($params{ignoremissing}) {
		splice(@$data, $i, 1);
		redo;
	    }
	    return carp("Couldn't open $gdata->{data}!");
	}
	next if $noauto;
	my $do_errors = (exists($gdata->{type}) && ($gdata->{type} =~ 'xydy'));
	foreach my $line (split("\n", $gdata->{data})) {
	    my ($x,$y,$d1, $d2) = split(" ", $line);
	    next unless (defined $y);
	    if ($x<$xmin) { $xmin = $x; }
	    if ($x>$xmax) { $xmax = $x; }
	    if ($do_errors) {
		$d1 = 0 unless (defined $d1);
		$d2 = $d1 unless (defined $d2);
		if ($y-$d2<$ymin) { $ymin = $y-$d2; }
		if ($y+$d1>$ymax) { $ymax = $y+$d1; }
	    } else {
		if ($y<$ymin) { $ymin = $y; }
		if ($y>$ymax) { $ymax = $y; }
	    }
	}
    }

    if (!$noauto) {
	if ($xmax == -1e300 or $xmax == $xmin or 
	    $ymax == -1e300 or $ymax == $ymin)
	{ return carp("Degenerate graph!\n"); }
    } else {
	($xmax, $xmin, $ymax, $ymin) = @params{"xmax", "xmin", "ymax", "ymin"};
    }

    #decide on log vs. lin:
    %params = (axes_setup($xmin, $xmax, $params{xscale}, "x"), %params);
    %params = (axes_setup($ymin, $ymax, $params{yscale}, "y", $params{vscale}), %params);
    if ($params{extra_space}) {
	$params{xmin} -= $params{xmajortick}/8;
	$params{xmax} += $params{xmajortick}/8;
	$params{ymin} -= $params{ymajortick}/8;
	$params{ymax} += $params{ymajortick}/8;
    }

    if ($params{xscale} eq "Logarithmic" and 
	($params{xmin} <= 1e-5 or $params{xmax} >= 1e5)) {
	$params{xaxisformat} = "power";
	$params{xaxisprec} = 0;
    }

    if ($params{yscale} eq "Logarithmic" and 
	($params{ymin} <= 1e-5 or $params{ymax} >= 1e5)) {
	$params{yaxisformat} = "power";
	$params{yaxisprec} = 0; 
    }

    my $agr = $header;
    $agr =~ s/<page_size>/$params{page_size}/g;

    my $gr_def = $graph_defaults;
    my $text = [@{$params{text}}];
    my $graph_text = "";
    if (defined $params{colorstrip}) {
	my @c = @{$params{colorstrip}};
	my @t = @c[2..$#c];
	if (!@t and defined $params{zmin} and defined $params{zmax}) {
	    my $fmt = $params{colorstrip_label_format};
	    $fmt = '%.1f' if (!defined $fmt);
	    push @t, map { 
		if ($params{colorstrip_label_transform}) {
		    $_ = $params{colorstrip_label_transform}->($_);
		}
		sprintf($fmt, $_);
	    } ($params{zmin}, 0.5*($params{zmin}+$params{zmax}), $params{zmax});
	}
	if (@t > 1) {
	    my $boldtext = "";
	    $boldtext = '\f{Times-Bold}' if ($params{colorstrip_bold});
	    for (0..$#t) {
		my $f = $_/$#t;
		my $y = $c[1]-(1-$f)*(0.2);
		my $x = $c[0]+0.06;
		my $just = 0;
		if ($params{colorstrip_horizontal}) {
		    $y = $c[1]-0.07;
		    $x = $c[0]+$f*0.2;
		    $just = 2;
		}
		$text = [@$text, {pos => "$x, $y", color => $params{color_density} ? 0 : 1, text => $boldtext.'\v{-0.3}'.$t[$_], just => $just}];
	    }
	}
	if (defined $params{colorstriplabel}) {
	    my ($x, $y, $rot) = ($c[0], $c[1]-0.2, 90);
	    if ($params{colorstrip_horizontal}) {
		($x, $y, $rot) = ($c[0], $c[1], 0);
	    }
	    push @$text, {pos => "$x,$y", color => $params{color_density} ? 0 : 1, text => $params{colorstriplabel}, rot => $rot};
	}
    }

    my ($l2x, $l2y) = split(/[, ]+/, $params{legend2xy});
    for (@$data) {
	if ($_->{legend2}) {
	    push @$text, {pos => join(",", $l2x+0.105, $l2y-0.045+(1.5-$params{labelsize})*0.01), text => $_->{legend2}};
	    $l2y -= $params{legend2vg} + ($_->{legend2kern}||0);
	}
    }

    my ($l3x, $l3y) = split(/[, ]+/, $params{legend3xy});
    for (@$data) {
	if ($_->{legend3}) {
	    push @$text, {pos => join(",", $l3x+0.105, $l3y-0.045+(1.5-$params{labelsize})*0.01), text => $_->{legend3}};
	    $l3y -= $params{legend3vg} + ($_->{legend3kern}||0);
	}
    }

    my ($l4x, $l4y) = split(/[, ]+/, $params{legend4xy});
    for (@$data) {
	if ($_->{legend4}) {
	    push @$text, {pos => join(",", $l4x+0.105, $l4y-0.045+(1.5-$params{labelsize})*0.01), text => $_->{legend4}};
	    $l4y -= $params{legend4vg} + ($_->{legend4kern}||0);
	}
    }

    
    for (@$text) {
	my %textinfo = (%def_text, size => $params{labelsize}, %$_);
	my $ts = $text_syntax;
	for (qw(text size pos rot just color)) {
	    $ts =~ s/<\Q$_\E>/$textinfo{$_}/g;
	}
	$graph_text .= $ts;
    }
    $gr_def =~ s/<text>/$graph_text/g;

    my $boxes = $params{boxes};
    if ($params{colorstrip}) {
	$boxes = [gen_colorstrip([@{$params{colorstrip}}[0,1], $params{colorstrip_horizontal}, $params{colorstrip_marked}, $params{colorstrip_reverse}], $params{zmin}, $params{zmax}), @$boxes];
    }
    my $graph_boxes = "";
    for (@$boxes) {
	my %boxinfo = (pos => "0.5,0.5,0.6,0.6", color => 0, %$_);
	my $bs = $box_syntax;
	for (qw(pos color)) {
	    $bs =~ s/<\Q$_\E>/$boxinfo{$_}/g;
	}
	$graph_boxes .= $bs;
    }
    $gr_def =~ s/<boxes>/$graph_boxes/g;


    my $colors = ($params{color_density} ? $pretty_hist_colors : ($params{rgb_density}) ? $rgb_hist_colors :  $hist_colors) .
	($params{rainbow} ? $rainbow_colors : 
	 ($params{fading_colors} ? $fading_colors : 
	  ($params{molly} ? $molly_colors : 
	   ($params{reduced_color_errorstrip} ? $reduced_color_errorstrip_colors : 
	    ($params{old_rainbow} ? $rainbow_colors_old : $def_colors)))));
    $colors = $color_errorstrip_colors if ($params{color_errorstrip});
    $colors = $alt_linecolors if ($params{alt_linecolors});
    if (defined $params{remap_colors}) {
	for my $c (keys %{$params{remap_colors}}) {
	    $colors =~ s/color $c to \([0-9, ]*\)/color $c to ($params{remap_colors}{$c})/;
	}
    }

    $agr .= $colors . $gr_def;
    if ($params{color_density}) {
	$params{xtickdir} = "out" unless defined($self->{xtickdir});
	$params{ytickdir} = "out" unless defined($self->{ytickdir});
	$params{fbgcolor} = 1;
    }
    $params{legendcolor} ||= $params{color_density} ? 0 : 1;

    for ("x", "y", "altx", "alty") {
	if ($params{$_."ticklabels"}) {
	    my @labels = sort {$a <=> $b} keys %{$params{$_."ticklabels"}};
	    $params{$_."tickspec"} = "both\n".
		'@    '.$_.'axis  tick spec '.(scalar @labels)."\n";
	    for my $label (0..$#labels) {
		my $ltext = $params{$_."ticklabels"}{$labels[$label]};
		my $major = (length($ltext)>0) ? "major" : "minor";
		$params{$_."tickspec"}.= <<"EOS"
@    ${_}axis  tick $major $label, $labels[$label]
@    ${_}axis  ticklabel $label, "$ltext"
EOS
;
	    }
	}
    }

    if ($params{altxaxis} eq 'on') {
	my $axspec = $altxaxis_spec;
	$params{xaxistickplace} = "normal";
	$axspec =~ s/<([a-z]+)>/defined $params{$1} ? $params{$1} : die $1/eg;
	$params{altxaxis} .= "\n$axspec";
    }

    if ($params{altyaxis} eq 'on') {
	my $ayspec = $altyaxis_spec;
	$params{yaxistickplace} = "normal";
	$ayspec =~ s/<([a-z]+)>/defined $params{$1} ? $params{$1} : die $1/eg;
	$params{altyaxis} .= "\n$ayspec";
	$params{xviewmin} -= 0.11;
	$params{xviewmax} -= 0.11;
    }

    my $gs = $graph_setup;
    $gs =~ s/<([a-z]+)>/$params{$1}/g;
    $agr .= $gs;

    my $lines = [@{$params{lines}}];
    my $ellipses = [@{$params{ellipses}}];
    my $skip_style = 0;
    ($l2x, $l2y) = split(/[, ]+/, $params{legend2xy});
    ($l3x, $l3y) = split(/[, ]+/, $params{legend3xy});
    ($l4x, $l4y) = split(/[, ]+/, $params{legend4xy});
    foreach my $i (0..(@$data-1)) {
	my $gdata = $data->[$i];
	my $ds = $data_setup;
	$ds =~ s/s0/s$i/g;
	my $metatype = $gdata->{metatype} || "line";
	my $metastyle = $gdata->{metastyle};
	$metastyle = $i-$skip_style unless defined($metastyle);
	my %defaults = (%default_style, scolor => $metastyle+1, sfcolor => ($gdata->{scolor} || ($metastyle+1)), 
			lcolor => $metastyle+1);
	if ($params{greyscale}) {
	    #$defaults{scolor} = $defaults{lcolor} = 1+int($i/8);
	    $defaults{lstyle} = (1,4,6,7,8,3,5,2)[$metastyle%8];
	}
	if ($params{rainbow} or $params{old_rainbow}) {
	    $defaults{lstyle} = 1;
	}
	if ($metatype eq "scatter") {
	    $defaults{stype} = 1;
	    $defaults{ltype} = 0;
	} elsif ($metatype eq "density" or $metatype eq "cdensity" or $metatype eq "xycdensity" or $metatype eq "xydensity") {
	    $defaults{type} = "xycolor";
	    $defaults{ltype} = $defaults{lstyle} = 0;
	    $defaults{stype} = 2;
	    $defaults{ssize} = 0.8;
	    $defaults{scolor} = $defaults{sfcolor} = 1;
	} elsif ($metatype eq "density_size" or $metatype eq "cdensity_size") {
	    $defaults{type} = "xysize";
	    $defaults{ltype} = $defaults{lstyle} = 0;
	    $defaults{ssize} = 0.8;
	    $defaults{stype} = 2;
	} elsif (defined($gdata->{type}) and $gdata->{type} eq "xysize") {
	    $defaults{ltype} = $defaults{lstyle} = 0;
	    $defaults{ssize} = 1;
	    $defaults{stype} = 1;
	} elsif ($metatype eq "errorstrip") {
	    $defaults{ltype} = 1;
	    $defaults{lstyle} = 0;
	    $defaults{lwidth} = 0;
	    $skip_style++;
	    if (! defined $gdata->{fcolor}) {
		$gdata->{fcolor} = 255-70*$skip_style;
		if ($params{color_errorstrip}) {
		    $gdata->{fcolor} = $skip_style + 16;
		}
	    }
	}

	%$gdata = (%defaults, %$gdata);
	$ds =~ s/<\Q$_\E>/$gdata->{$_}/g foreach 
	    (qw(type legend stype ssize scolor sfcolor ltype
                fcolor ftype lstyle lwidth arrowwidth lcolor sskip errorplace
                slwidth slstyle spattern));
	if ($gdata->{legend2}) {
	    if ($gdata->{ltype}) {
		push @$lines, {pos => join(",", $l2x+0.013, $l2y-0.031, $l2x+0.093, $l2y-0.031), color => $gdata->{lcolor}, width => $gdata->{lwidth}, style => $gdata->{lstyle}};
	    }
	    if ($gdata->{stype}) {
		my $esize = $gdata->{ssize}*0.01;
		push @$ellipses, {pos => join(",", $l2x+0.053-$esize, $l2y-0.031-$esize, $l2x+0.053+$esize, $l2y-0.031+$esize), lcolor => $gdata->{lcolor}, lwidth => $gdata->{slwidth}, lstyle => $gdata->{slstyle}, color => $gdata->{scolor}, pattern => $gdata->{spattern}};
	    }
	    $l2y -= $params{legend2vg}+($gdata->{legend2kern}||0);
	}

	if ($gdata->{legend3}) {
	    if ($gdata->{ltype}) {
		push @$lines, {pos => join(",", $l3x+0.013, $l3y-0.031, $l3x+0.093, $l3y-0.031), color => $gdata->{lcolor}, width => $gdata->{lwidth}, style => $gdata->{lstyle}};
	    }
	    if ($gdata->{stype}) {
		my $esize = $gdata->{ssize}*0.01;
		push @$ellipses, {pos => join(",", $l3x+0.053-$esize, $l3y-0.031-$esize, $l3x+0.053+$esize, $l3y-0.031+$esize), lcolor => $gdata->{lcolor}, lwidth => $gdata->{slwidth}, lstyle => $gdata->{slstyle}, color => $gdata->{scolor}, pattern => $gdata->{spattern}};
	    }
	    $l3y -= $params{legend3vg}+($gdata->{legend3kern}||0);
	}


	if ($gdata->{legend4}) {
	    if ($gdata->{ltype}) {
		push @$lines, {pos => join(",", $l4x+0.013, $l4y-0.031, $l4x+0.093, $l4y-0.031), color => $gdata->{lcolor}, width => $gdata->{lwidth}, style => $gdata->{lstyle}};
	    }
	    if ($gdata->{stype}) {
		my $esize = $gdata->{ssize}*0.01;
		push @$ellipses, {pos => join(",", $l4x+0.053-$esize, $l4y-0.031-$esize, $l4x+0.053+$esize, $l4y-0.031+$esize), lcolor => $gdata->{lcolor}, lwidth => $gdata->{slwidth}, lstyle => $gdata->{slstyle}, color => $gdata->{scolor}, pattern => $gdata->{spattern}};
	    }
	    $l4y -= $params{legend4vg}+($gdata->{legend4kern}||0);
	}

	$agr .= $ds;
    }

    foreach my $i (0..(@$data-1)) {
	my $dd = $data_data;
	resort($data->[$i]) if ($data->[$i]->{sort});
	$dd =~ s/S0/S$i/;
	$dd =~ s/<data>/$data->[$i]->{data}/;
	$dd =~ s/<type>/$data->[$i]->{type}/;
	$agr .= $dd;
    }

    if (defined $params{font}) {
	$agr =~ s/font 0/font $params{font}/g;
    }

    for my $line (@$lines) {
	my $ls = $line_syntax;
	$ls =~ s/<\Q$_\E>/$line->{$_}/g for (keys %$line);
	$agr .= $ls;
    }

    for my $ellipse (@$ellipses) {
	my $es = $ellipse_syntax;
	$es =~ s/<\Q$_\E>/$ellipse->{$_}/g for (keys %$ellipse);
	$agr .= $es;
    }

    return $agr;
}

sub write_agr {
    my $self = shift;
    my $output = shift;
    my $agr = $self->to_agr;
    return carp("No graph to output!\n") unless defined($agr);
    open FILE, ">", $output;
    print FILE $agr;
    close FILE;
}

sub write_pdf {
    my $self = shift;
    my $output = shift;
    return if ($MultiThread and !MultiThread::ForkChild());
    removePDF($output);
    $self->write_agr($output);
    convertToImage($output);
    if ($self->{transparent_colors}) {
	convertToTransparent($output, $self->{transparent_colors});
    }
    exit if $MultiThread;
}

sub extract_data {
    my ($data) = shift;
    convert_data($data, {});
    return $data->{data};
}

sub num {
    no warnings 'numeric';
    return $_[0]+0;
}

sub convert_data {
    my ($gdata, $params) = @_;
    if (ref($gdata->{data}) eq 'ARRAY') {
	my $data;
	for (@{$gdata->{data}}) {
	    my $dummy = { %$gdata };
	    $dummy->{data} = $_;
	    return -1 if (convert_data($dummy, $params) < 0);
	    $data .= $dummy->{data}."\n";
	}
	$data =~ s/\n\n+/\n/g;
	$gdata->{data} = $data;
	$gdata->{already_transformed} = 1;
	if ($gdata->{sort}) {
	    my @data = split("\n", $data);
	    @data = sort { num($a) <=> num($b) } @data;
	    $gdata->{data} = join("\n", @data)."\n";
	}
	return 1;
    }
    if ($gdata->{data} =~ /^file(?:\[([0-9,]+)\])?:/) {
	my $range = $1;
	$gdata->{data} =~ s/^file[\[\]0-9,]*://;
	return -1 unless(open DATA, "<", $gdata->{data});

	unless (defined $range) {
	    my $buffer;
	    $gdata->{data} = "";
	    while (read(DATA,$buffer, 1024*1024)) {
		$gdata->{data} .= $buffer;
	    }
	} else {
	    $gdata->{data} = "";
	    local $_;
	    my @range = split(/,/ => $range);
	    my $maxrange = $range[0];
	    for (@range) { $maxrange = $_ if ($maxrange < $_); }
	    while (<DATA>) {
		next if (/^\s*\#/);
		my @results = split(" ", $_);
		next if (@results < $maxrange+1);
		$gdata->{data} .= join(" ", @results[@range])."\n";
	    }
	}
	close DATA;
    }
    $gdata->{data} =~ s/\#[^\n]*\n/\n/g;
    $gdata->{data} =~ s/\n\n+/\n/g;
    $gdata->{data} =~ s/^\n//g;
    return carp("Empty data set!\n") unless length($gdata->{data});
    chomp($gdata->{data});
    $gdata->{data} .= "\n";

    if (exists $gdata->{transform} and ref($gdata->{transform}) eq 'CODE'
	and !$gdata->{already_transformed}) {
	$gdata->{data} =~ s/([^\n]+)\n+/$gdata->{transform}->($1)."\n"/eg;
	$gdata->{data} =~ s/\n\n+/\n/g;
	$gdata->{data} =~ s/^\n//;
	chomp($gdata->{data});
	$gdata->{data}.="\n";
	$gdata->{already_transformed} = 1;
    }

    if ($gdata->{smooth} and (!defined $gdata->{metatype} or 
			      ($gdata->{metatype} !~ /hist/ and
			       $gdata->{metatype} !~ /density/))) {
	split_data_and_smooth($gdata);
    }

    if (exists $gdata->{xmax} or exists $gdata->{ymax} or
	exists $gdata->{xmin} or exists $gdata->{ymin}) {
	split_data_and_apply_limits($gdata);
    }

    print $gdata->{data} if $params{printdata};

    my @lims = map { $params->{$_} } qw(xmin xmax ymin ymax);
    if (exists $gdata->{metatype} and defined($gdata->{metatype})) {
	if ($gdata->{metatype} eq "histogram") {
	    convert_to_hist($gdata, 0, @lims);
	} elsif ($gdata->{metatype} eq "nhistogram") {
	    convert_to_hist($gdata, 1, @lims);
	} elsif ($gdata->{metatype} eq "density") {
	    convert_to_density($gdata, 0, 0, @lims);
	} elsif ($gdata->{metatype} eq "xycdensity") {
	    convert_to_xydensity($gdata, 1, 0, @lims);
	} elsif ($gdata->{metatype} eq "xydensity") {
	    convert_to_xydensity($gdata, 0, 0, @lims);
	} elsif ($gdata->{metatype} eq "cdensity") {
	    convert_to_density($gdata, 1, 0, @lims);
	} elsif ($gdata->{metatype} eq "density_size") {
	    convert_to_density($gdata, 0, 1, @lims);
	} elsif ($gdata->{metatype} eq "cdensity_size") {
	    convert_to_density($gdata, 1, 1, @lims);
	} elsif ($gdata->{metatype} eq "density_contour") {
	    convert_to_density($gdata, 0, 2, @lims);
	    $gdata->{metatype} = "density_size";
	} elsif ($gdata->{metatype} eq "xydensity_contour") {
	    convert_to_xydensity($gdata, 0, 2, @lims);
	    $gdata->{metatype} = "density_size";
	} elsif ($gdata->{metatype} eq "cdensity_contour") {
	    convert_to_density($gdata, 1, 2, @lims);
	    $gdata->{metatype} = "cdensity_size";
	} elsif ($gdata->{metatype} eq "errorstrip") {
	    convert_to_poly($gdata);
	    $gdata->{ftype} = 1 unless defined($gdata->{ftype});
	    $gdata->{type} = 'xy';
	}
	
	if (defined $gdata->{zmax} && !(defined $params->{zmax})) {
	    $params->{zmax} = $gdata->{zmax};
	}
	if (defined $gdata->{zmin} && !(defined $params->{zmin})) {
	    $params->{zmin} = $gdata->{zmin};
	}
    }
    return 1;
}

sub resort {
    my ($data) = shift;    
    $data->{data} = join("\n", map { "@$_" } sort { $a->[0] <=> $b->[0] } map { [split(" ", $_, 2)] } split("\n", $data->{data}));
}

sub to_years {
    my ($x, $y) = split(" ", $_[0],2);
    $x = scale_to_years(1/$x) - scale_to_years(0);
    $x /= 1e9;
    return "$x $y";
}

sub to_lbtime {
    my ($x, $y) = split(" ", $_[0],2);
    $x = scale_to_years(1) - scale_to_years(1/$x);
    $x /= 1e9;
    return "$x $y";
}

sub timespec_gyr {
    my @gyrs = @_;
    my @htimes = map { $_*1e9/$Universe::Time::to_years } @gyrs;
    my @results;
    for (0..$#gyrs) {
	push @results, 1.0/exact_time_to_scale($htimes[$_]), $gyrs[$_];
    } 
    return @results;
}

sub gen_z_timespec {
    my @zs = @_;
    my @out;
    for (@zs) {
        push @out, 1e-9*(scale_to_years(1.0/($_+1)) - scale_to_years(0)), "$_";
    }
    return @out;
}

sub gen_z_timespec_lb {
    my @zs = @_;
    my @out;
    for (@zs) {
        push @out, 1e-9*(scale_to_years(1) - scale_to_years(1.0/($_+1))), "$_";
    }
    return @out;
}

sub upper_line {
    my @a = split(" ", $_[0]);
    my $x = $a[0];
    my $y = $a[1]+$a[2];
    return "$x $y";
}

sub lower_line {
    my @a = split(" ", $_[0]);
    my $x = $a[0];
    my $y = $a[1]-$a[3];
    return "$x $y";
}

sub tolog {
    my @a = @_;
    return sub {
	my (@input) = split(" ", $_[0]);
	for (@a) {
	    if ($input[$_] <= 0) { $input[$_] = -1000; }
	    else { $input[$_] = log($input[$_])/log(10); }
	}
	return "@input";
    }
}

sub tolin {
    my @a = @_;
    return sub {
	my (@input) = split(" ", $_[0]);
	my @output = @input;
	for (@a) {
	    if ($_ < 2) {
		$output[$_] = 10**$input[$_];
	    }
	    elsif (($_ == 2 and @a < 5) || ($_==4)) {
		$output[$_] = 10**($input[1]+$input[$_])-10**($input[1]);
	    } 
	    elsif (($_ == 3 and @a < 5) || ($_==5)) {
		$output[$_] = -(10**($input[1]-$input[$_])-10**($input[1]));
	    }
	    elsif (($_ == 2 and @a > 4)) {
		$output[$_] = 10**($input[0]+$input[$_])-10**($input[0]);
	    }
	    elsif (($_ == 3 and @a > 4)) {
		$output[$_] = -(10**($input[0]-$input[$_])-10**($input[0]));
	    }
	}
	return "@output";
    }
}

sub multiply {
    my %vals = @_;
    return sub {
	my (@input) = split(" ", $_[0]);
	$input[$_] *= $vals{$_} for (keys %vals);
	return "@input";
    }
}



sub bootstrap {
    my $trials = shift @_;
    my $c = @_;
    my $sum = 0;
    my @trials;
    my $tw = 0;
    $tw += $_->[1] for (@_);
    $sum += $_->[0]*$_->[1] for (@_);
    $sum /= $tw;
    for (1..$trials) {
	my $s = 0;
	my $w = 0;
	while ($w < $tw) {
	    my $i = rand($c);
	    $s+=$_[$i][0]*$_[$i][1];
	    $w+=$_[$i][1];
	}
	push @trials, $s/$w;
    }
    @trials = sort { $a <=> $b } @trials;
    my $up = $trials[0.5*(1+0.68)*$trials];
    my $dn = $trials[0.5*(1-0.68)*$trials];
    $up -= $sum;
    $dn = $sum - $dn;
    return ($sum,$up, $dn);
}

sub bootstrap_median {
    my $trials = shift @_;
    my $c = @_;
    my $sum = 0;
    my @trials;
    my $tw = 0;
    $tw += $_->[1] for (@_);
    $sum += $_->[0]*$_->[1] for (@_);
    $sum /= $tw;
    for (1..$trials) {
	my $s = 0;
	my $w = 0;
	my @sample;
	while ($w < $tw) {
	    my $i = rand($c);
	    push @sample, int($i);
	    $w+=$_[$i][1];
	}
	@sample = sort { $_[$a][0] <=> $_[$b][0] } @sample;
	my $w2 = 0;
	for (0..$#sample) {
	    $w2 += $_[$sample[$_]][1]/2.0;
	    if ($w2 > 0.5*$w) {
		$s = $_[$sample[$_]][0];
		my $wa = $w2;
		my $sa = $s;
		last if (@sample == 1);
		my ($wb, $sb);
		if ($_>0) {
		    $wb = $w2 - $_[$sample[$_]][1]/2.0 - $_[$sample[$_-1]][1]/2.0;
		    $sb = $_[$sample[$_-1]][0];
		} else {
		    last;
		}
		$s += (0.5*$w-$w2)*($sb-$sa)/($wb-$wa);
		last;
	    }
	    $w2 += $_[$sample[$_]][1]/2.0;
	}
	push @trials, $s;
    }
    @trials = sort { $a <=> $b } @trials;
    $sum = $trials[0.5*$trials];
    my $up = $trials[0.5*(1+0.68)*$trials];
    my $dn = $trials[0.5*(1-0.68)*$trials];
    $up -= $sum;
    $dn = $sum - $dn;
    return ($sum,$up, $dn);
}

sub Correlate {
    my ($sx, $sy, $sx2, $sy2, $sxy, $sw) = map { 0 } (1..6);
    for (@_) {
	my ($x, $y, $w) = @$_;
	$sx += $x*$w;
	$sy += $y*$w;
	$sw += $w;
#	print "$w\n";
    }
    return 0 unless ($sw > 0);
    $sx /= $sw;
    $sy /= $sw;
    for (@_) {
	my ($x, $y, $w) = @$_;
	$sx2 += ($x-$sx)*($x-$sx)*$w;
	$sy2 += ($y-$sy)*($y-$sy)*$w;
	$sxy += ($x-$sx)*($y-$sy)*$w;
    }
    $sx2 /= $sw;
    $sy2 /= $sw;
    $sxy /= $sw;
#    print "$sx $sy $sx2 $sy2 $sxy $sw\n";
    return 0 unless ($sx2*$sy2 > 0);
    return ($sxy/(sqrt($sx2*$sy2)));
}

sub Rank_Correlate {
    my $tw = 0;
    my @data = @_;
    $tw += $_->[2] for (@data);
    @data = sort { $a->[0] <=> $b->[0] } @data;
    my @data2;
    my $w = 0;
    for (@data) {
	$w += $_->[2]/2;
	push @data2, [$w/$tw, $_->[1], $_->[2]];
	$w += $_->[2]/2;
    }
    @data2 = sort { $a->[1] <=> $b->[1] } @data2;
    $w = 0;
    for (@data2) {
	$w += $_->[2]/2;
	$_->[1] = $w/$tw;
	$w += $_->[2]/2;
    }
    return Correlate(@data2);
}

sub bootstrap_correlate {
    my $trials = shift @_;
    my $corr_fn = shift @_;
    my $c = @_;
    my $sum = 0;
    my @trials;
    $sum = $corr_fn->(@_);
    my $tw = 0;
    $tw += $_->[2] for (@_);
    for (1..$trials) {
	my @sample;
	my $w = 0;
	while ($w < $tw) {
	    my $i = rand($c);
	    $w+=$_[$i][2];
	    push @sample, $_[$i];
	}
	push @trials, $corr_fn->(@sample);
    }
    @trials = sort { $a <=> $b } @trials;
    my $up = $trials[0.5*(1+0.68)*$trials];
    my $dn = $trials[0.5*(1-0.68)*$trials];
    $up -= $sum;
    $dn = $sum - $dn;
    return ($sum,$up, $dn);
}


sub Bin {
    my %params = @_;
    convert_data(\%params, {});
    my $thresh = $params{thresh};
    my $bpdex = $params{bpdex} || 4;
    my $lin_r = $params{lin_r} || 0;
    my $min_r = $params{min_r};
    my $med = $params{median} || 0;
    my $correlate = $params{correlate} || 0;
    my $rank_correlate = $params{rank_correlate} || 0;
    my $weighted = $params{weighted} || 0;
    my $samples = $params{samples} || 1000;
    my @lines = split(/\n/, $params{data});
    my $data = "";
    my (%bins, %w);
    for (@lines) {
	my @a = split;
	my $rp = $a[0];
	next if (defined($min_r) and $rp < $min_r);
	my $w = ($correlate) ? $a[3] : $a[2];
	$w = 1 unless (defined($w) && $weighted);
	my $val = $a[1];
	my $val2 = $a[2];
	$val = ($val > $thresh) ? 1 : 0 if (defined $thresh);
	my $datum = ($correlate or $rank_correlate) ? [$val, $val2, $w] : [$val, $w];
	my $bin = ($lin_r) ? (int($rp*$bpdex)+0.5) : 
	    int(log($rp)/log(10)*$bpdex)+0.5;
	push @{$bins{$bin}}, $datum;
	$w{$bin} += $w;
    }
    for (sort {$a <=> $b} keys %w) {
	next if (@{$bins{$_}} < 2);
	my $b = 10**($_/$bpdex);
	$b = ($_/$bpdex) if ($lin_r);
	my @extras = ($samples);
	if ($correlate) {
	    push @extras, \&Correlate;
	} elsif ($rank_correlate) {
	    push @extras, \&Rank_Correlate;
	}
	my $fn = ($med) ? \&bootstrap_median : \&bootstrap;
	$fn = \&bootstrap_correlate if ($correlate or $rank_correlate);
	my ($avg, $up, $dn) = $fn->(@extras, @{$bins{$_}});
	$data .= "$b $avg $up $dn\n";
    }
    return $data;
}


sub Differential {
    my %params;
    if (@_==2) { $params{data1} = $_[0]; $params{data2} = $_[1]; $params{type} = "absolute"; }
    elsif (@_==3) { $params{data1} = $_[0]; $params{data2} = $_[1]; $params{type} = $_[2]}
    else { %params = @_; }
    my $gdata1 = {data => $params{data1}, ($params{transform1} ? (transform => $params{transform1}) : ())};
    my $gdata2 = {data => $params{data2}, ($params{transform2} ? (transform => $params{transform2}) : ())};
    return carp("Couldn't open $gdata1->{data}!") if (convert_data($gdata1)<0);
    return carp("Couldn't open $gdata2->{data}!") if (convert_data($gdata2)<0);
    my $data = "";
    my @data = split(/\n/ => $gdata2->{data});
    my %data;
    for (@data) {
	my @a = split;
	$data{$a[0]} = $a[1];
    }
    my @keys = keys %data;
    @data = split(/\n/ => $gdata1->{data});
    for (@data) {
	my @a = split;
	my $best_key = $keys[0];
	for (@keys) {
	    if (abs($best_key-$a[0]) > abs($_-$a[0])) { $best_key = $_; }
	}
	my $best_key2 = $keys[0];
	if ($best_key2 == $best_key) { $best_key2 = $keys[1]; }
	for (@keys) {
	    if ($_ != $best_key and abs($best_key2-$a[0]) > abs($_-$a[0])) 
	    { $best_key2 = $_; }
	}
	my $val;
	if (defined $best_key2) {
	    $val = $data{$best_key} + ($a[0]-$best_key)*($data{$best_key2}-$data{$best_key})/($best_key2-$best_key);
	} else {
	    $val = $data{$best_key};
	}
	if ($params{type} eq 'relative') {
	    $a[$_] /= $val for (1..$#a);
	} else {
	    $a[1] -= $val;
	}
	$data .= "@a\n";
    }
    return $data;
}

sub linscale {
    my ($axis, $min, $max, $majortick, $minorticks) = @_;
    my @info = ($axis.'scale' => "Normal",
		$axis.'min' => $min,
		$axis.'max' => $max);
    push @info, $axis.'majortick', $majortick if (defined $majortick);
    push @info, $axis.'minorticks', $minorticks if (defined $minorticks);
    return @info;
}

sub logscale {
    my ($axis, $min, $max, $majortick, $minorticks) = @_;
    my @info = ($axis.'scale' => "Logarithmic",
		$axis.'min' => $min,
		$axis.'max' => $max);
    push @info, $axis.'majortick', $majortick || 10;
    push @info, $axis.'minorticks', $minorticks || 9;
    return @info;
}


sub axes_setup {
    my ($min, $max, $scale, $id, $vscale) = @_;
    my ($majtick, $minticks);
    $scale ||= "";
    if ($min <= 0 or (($scale eq '' and $max/$min < 20) or $scale eq 'Normal')) {
	$scale = "Normal";
	
	my $digits = log(abs($min)||1)/log(10);
	my $mdigits = log(abs($max)||1)/log(10);
	my $mmdigits = log(abs($max-$min)||1e300)/log(10);
	if ($mdigits > $digits) { $digits = $mdigits; }
	if ($mmdigits < $digits) { $digits = $mmdigits; }
	$digits = int($digits+1.5) - 2;
	my $round = 10**$digits;
	my $imax = ($max < 0)
	    ? int($max / $round - 0.5) : int($max / $round + 0.5);
	$imax *= $round;
	$max = $imax if ($imax>$max);
	my $imin = ($min < 0)
	    ? int($min / $round - 0.5) : int($min / $round + 0.5);
	$imin *= $round;
	$min = $imin if ($imin < $min);
	$majtick = $round;
	$majtick*=2 while (int(($max-$min)/$majtick) > 9);
	$majtick/=$vscale*2 if ($vscale and $vscale < 0.5);
	$minticks = 1;
    } else {
	$scale = "Logarithmic";
	my $imax = log($max)/log(10);
	$imax = ($imax < 0) ? int($imax - 0.5) : int($imax + 0.5);
	$max = 10**$imax if ($imax > log($max)/log(10));
	my $imin = log($min)/log(10);
	$imin = ($imin < 0) ? int($imin - 0.5) : int($imin + 0.5);
	$min = 10**$imin if ($imin < log($min)/log(10));
	$majtick = 10;
	$minticks = 9;
    }
    return ("${id}min" => $min, "${id}max" => $max, "${id}scale" => $scale,
	    "${id}majortick" => $majtick, "${id}minorticks" => $minticks);
}

sub interpolate {
    my ($x, $scale, $yscale, $bins, $key1, $key2) = @_;
    warn "@_\n";
    my $f = ($x-$key1) / ($key2-$key1);
    my $v1 = ($yscale eq "Normal") ? $bins->{$key1} : log($bins->{$key1});
    my $v2 = ($yscale eq "Normal") ? $bins->{$key2} : log($bins->{$key2});
    my $v = $v1 + $f*($v2-$v1);
    $v = exp($v) if ($yscale ne "Normal");
    return $v;
}

sub convert_to_hist {
    my ($graph, $normalize, $pxmin, $pxmax, $pymin, $pymax) = @_;
    return if ($graph->{converted});
    my $num_bins = $graph->{num_bins} || 15;
    my @data = split(/\n/, $graph->{data});
    my ($xmax, $xmin, $scale) = (-1e300, 1e300);
    my $num_data = @data;
    foreach (@data) {
	next unless (length());
	my ($x) = split;
	$xmax = $x if ($x > $xmax);
	$xmin = $x if ($x < $xmin);
    }
    $xmin = $pxmin if (defined($pxmin));
    $xmax = $pxmax if (defined($pxmax));
    $scale = ($xmin <= 0 or $xmax/$xmin < 20) ? "Normal" : "Logarithmic";
    $scale = $graph->{histscale} if (defined $graph->{histscale});
    my $yscale = "Normal";
    $yscale = $graph->{yhistscale} if (defined $graph->{yhistscale});
    if ($scale eq "Normal" and $graph->{bins_per_unit}) {
	$xmax = int($xmax*$graph->{bins_per_unit} + 0.9)/$graph->{bins_per_unit};
	$xmin = int($xmin*$graph->{bins_per_unit})/$graph->{bins_per_unit};
	$num_bins = ($xmax - $xmin)*$graph->{bins_per_unit};
    }
     
    my $bin_function = ($scale eq "Logarithmic")
	? sub { my ($x) = split(" ", $_[0]); return int($num_bins*(log($x/$xmin)/log($xmax/$xmin))); }
        : sub { my ($x) = split(" ", $_[0]); return int($num_bins * ($x-$xmin)/($xmax-$xmin)); };
    my $fbin_function = ($scale eq "Logarithmic")
	? sub { my ($x) = split(" ", $_[0]); return ($num_bins*(log($x/$xmin)/log($xmax/$xmin))); }
        : sub { my ($x) = split(" ", $_[0]); return ($num_bins * ($x-$xmin)/($xmax-$xmin)); };
    my $bin_placement = ($scale eq "Logarithmic")
	? sub { my ($x) = split(" ", $_[0]); return ($xmin*exp(($x+0.5)*log($xmax/$xmin)/$num_bins)); }
        : sub { my ($x) = split(" ", $_[0]); return ($xmin + ($x+0.5)*($xmax-$xmin)/$num_bins) } ;

    my $bin_width = ($scale eq "Logarithmic") ? log($xmax/$xmin)/(log(10)*$num_bins) : ($xmax - $xmin)/$num_bins;
    my (%bins, %counts);
    my ($ppxmin, $ppxmax);
    if ($graph->{percentile}) {
	@data = sort {$a <=> $b} @data;
	$ppxmin = $data[@data*(1-$graph->{percentile})/2];
	$ppxmax = $data[@data*(1+$graph->{percentile})/2];
    }
    $num_data = 0;
    foreach (@data) {
	next unless (length());
	my ($x, $w) = split(" ", $_);
	$w = 1 unless (defined $w and $graph->{weighted});
	my $b = $bin_function->($_);
	$bins{$b} += $w;
	$counts{$b}++;
	$num_data += $w;
    }
    my $output = "";
    my %poisson;
    $poisson{$_} = 1/sqrt($counts{$_}) foreach (keys %bins);
    if ($normalize) { $bins{$_}/=($num_data*$bin_width) foreach (keys %bins); }
    if ($graph->{normalize_to}) {
	my $max = 0;
	foreach (keys %bins) { $max = $bins{$_} if ($bins{$_}>$max); }
	foreach (keys %bins) { $bins{$_}*=$graph->{normalize_to}/$max; }
    }


    if ($graph->{percentile}) {
	my $bin_total = 0;
	my @keys = sort {$a <=> $b} keys %bins;
	for (0..(@keys-2)) {
	    if ($bin_placement->($keys[$_]) < $ppxmin and
		$bin_placement->($keys[$_+1]) > $ppxmin) {
		my $key = $fbin_function->($ppxmin)-0.5;
		$bins{$key} =
		    interpolate($key, $scale, $yscale,
				\%bins, $keys[$_], $keys[$_+1]);
	    }
	    if ($bin_placement->($keys[$_]) < $ppxmax and
		$bin_placement->($keys[$_+1]) > $ppxmax) {
		my $key = $fbin_function->($ppxmax)-0.5;
		$bins{$key} =
		    interpolate($key, $scale, $yscale,
				\%bins, $keys[$_], $keys[$_+1]);
	    }
	}	
    }
    

    foreach my $bin (sort { $a <=> $b } keys %bins) {
	next if ($bin_placement->($bin) > $xmax or $bin_placement->($bin) < $xmin);
	if ($graph->{percentile}) {
	    next if ($bin < $fbin_function->($ppxmin) or $bin > $fbin_function->($ppxmax));
	}
	$output .= $bin_placement->($bin) . " $bins{$bin}";
	if (defined $graph->{type} and $graph->{type} =~ /dy/) {
	    $output.= " ".$poisson{$bin}*$bins{$bin};
	}
	$output .= "\n";
    }
    $graph->{data} = $output;
    $graph->{converted} = 1;
}

sub convert_to_contour {
    my $bins = shift;
    my @vals;
    my $total = 0;
    my $contour = {};
    foreach (values %$bins) {
	foreach (values %$_) {
	    push @vals, $_;
	    $total+=$_;
	}
    }
    @vals = sort { $b <=> $a } @vals;
    my ($sum, $c1, $c2) = (0,0,0);
    for (@vals) {
	$sum+=$_;
	if (!$c1 and 0.5*$total < $sum) {
	    $c1 = $_;
	}
	if (!$c2 and 0.9*$total < $sum) {
	    $c2 = $_;
	}
    }
    my @x = sort {$a <=> $b} keys %$bins;
    push @x, 0;
    foreach my $x (0..(@x-2)) {
	my @y = sort {$a <=> $b} keys %{$bins->{$x[$x]}};
	push @y, 0;
	foreach my $y (0..(@y-2)) {
	    my $v = $bins->{$x[$x]}{$y[$y]} || 0;
	    next unless $v;
	    my @sides = map { $_ || 0 } ($bins->{$x[$x+1]}{$y[$y]},
					 $bins->{$x[$x-1]}{$y[$y]},
					 $bins->{$x[$x]}{$y[$y+1]},
					 $bins->{$x[$x]}{$y[$y-1]});
	    if ($v > $c1) {
		if (grep { $_ < $c1 } @sides) {
		    $contour->{$x[$x]}{$y[$y]} = 2;
		}
	    } elsif ($v > $c2) {
		if (grep { $_ < $c2 } @sides) {
		    $contour->{$x[$x]}{$y[$y]} = 1;
		}
	    }
	}
    }
    %$bins = %$contour;
}

sub linear_smooth {
    my ($smooth, @list) = @_;
    my @sum;
    my $t = 0;
    for (@list) {
	$t += $_;
	push @sum, $t;
    }
    @list = ();
    unshift @sum, 0;
    for (1..$#sum) {
	my ($min_i, $max_i) =  ($_ > $smooth) ? 
	    (($_ < @sum-$smooth-1) ? ($_ - $smooth, $_ + $smooth) : 
	     ($_ - ($#sum - $_), $#sum)) : (1, 2*$_-1);
	$min_i--;
	push @list, ($sum[$max_i]-$sum[$min_i])/($max_i-$min_i);
    }
    return @list;
}

sub split_data_and_smooth {
    my $graph = shift;
    my @cols;
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	my @a = split(" ", $1);
	push @{$cols[$_]}, $a[$_] for (0..$#a);
    }
    for (1..$#cols) {
	@{$cols[$_]} = linear_smooth($graph->{smooth}, @{$cols[$_]});
    }
    $graph->{data} = "";
    for my $i (0..(@{$cols[0]}-1)) {
	$graph->{data}.= join(" ", map { $cols[$_][$i] } (0..$#cols))."\n";
    }
}

sub split_data_and_apply_limits {
    my $graph = shift;
    my @cols;
    my $newdata = "";
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	my @a = split(" ", $1);
	next if (exists $graph->{xmax} and $a[0] > $graph->{xmax});
	next if (exists $graph->{xmin} and $a[0] < $graph->{xmin});
	next if (exists $graph->{ymax} and $a[1] > $graph->{ymax});
	next if (exists $graph->{ymin} and $a[1] < $graph->{ymin});
	$newdata .= "$1\n";
    }
    $graph->{data} = $newdata;
}

sub smooth {
    my %newbins;
    my ($bins, $xbins, $ybins) = @_;
    for my $x (0..($xbins-1)) {
	for my $y (0..($ybins-1)) {
	    my $val = $bins->{$x}{$y};
	    next unless defined $val;
	    my $counts = 6;
	    if ($x>0) { $newbins{$x-1}{$y}+=$val/6; $counts--; }
	    if ($y>0) { $newbins{$x}{$y-1}+=$val/6; $counts--; }
	    if ($x<$xbins-1) { $newbins{$x+1}{$y}+=$val/6; $counts--; }
	    if ($y<$ybins-1) { $newbins{$x}{$y+1}+=$val/6; $counts--; }
	    $newbins{$x}{$y} += $val*$counts/6;
	}
    }
    %$bins = %newbins;
}

sub smoothxy {
    my %newbins;
    my ($bins, $xbins, $ybins) = @_;
    my @x = sort {$a <=> $b} keys %$bins;
    my @y = sort {$a <=> $b} keys %{$bins->{$x[0]}};
    push @x, $x[0]-10;
    push @y, $y[0]-10;
    for my $x (0..(@x-2)) {
	for my $y (0..(@y-2)) {
	    my $val = $bins->{$x[$x]}{$y[$y]};
	    next unless defined $val;
	    my $counts = 6;
	    if ($x>0) { $newbins{$x[$x-1]}{$y[$y]}+=$val/6; $counts--; }
	    if ($y>0) { $newbins{$x[$x-1]}{$y[$y-1]}+=$val/6; $counts--; }
	    if ($x<@x-2) { $newbins{$x[$x+1]}{$y[$y]}+=$val/6; $counts--; }
	    if ($y<@y-2) { $newbins{$x[$x]}{$y[$y+1]}+=$val/6; $counts--; }
	    $newbins{$x[$x]}{$y[$y]} += $val*$counts/6;
	}
    }
    %$bins = %newbins;
}



sub get_binwidth {
    my $bins = shift;
    my $log = shift;
    my $num_bins;
    my @bins = sort {$a <=> $b} keys %$bins;
    @bins = map { log($_) } @bins if ($log); 
    return 0 unless (@bins > 1);
    my $start = $bins[1];
    my $min_dist = $bins[1]-$bins[0];
    for (2..$#bins) {
	$min_dist = $bins[$_]-$start if ($bins[$_]-$start < $min_dist);
	$start = $bins[$_];
    }
    $num_bins = int(($bins[-1]-$bins[0])/$min_dist+0.5) + 1;
    $min_dist = exp($min_dist) if ($log);
    return ($min_dist, $num_bins);
}

sub convert_to_xydensity {
    my ($graph, $conditional, $plottype, $pxmin, $pxmax, $pymin, $pymax) = @_;
    return if ($graph->{converted});
    #my ($xbins, $ybins);
    #my @data = split(/\n/, $graph->{data});
    my ($xmax, $xmin, $ymax, $ymin) = (-1e300, 1e300, -1e300, 1e300);
    my ($xscale, $yscale, %xcounts, %ycounts);
    local $_;
    #foreach (@data) {
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	$_ = $1;
	next unless (length());
	my ($x, $y, $z) = split();
	#next if ($y < 1e-6);
	$xmax = $x if ($x > $xmax);
	$xmin = $x if ($x < $xmin);
	$ymax = $y if ($y > $ymax);
	$ymin = $y if ($y < $ymin);
	$xcounts{$x}++;
	$ycounts{$y}++;
    }
    $xmax = $pxmax if (defined $pxmax);
    $xmin = $pxmin if (defined $pxmin);
    $ymax = $pymax if (defined $pymax);
    $ymin = $pymin if (defined $pymin);

    $xscale = ($xmin <= 0 or $xmax/$xmin < 20) ? "Normal" : "Logarithmic";
    $yscale = ($ymin <= 0 or $ymax/$ymin < 20) ? "Normal" : "Logarithmic";
    my ($xbinwidth,$xbins) = get_binwidth(\%xcounts, ($xscale eq "Normal") ? 0 : 1);
    my ($ybinwidth,$ybins) = get_binwidth(\%ycounts, ($yscale eq "Normal") ? 0 : 1);

    return if ($xmin == $xmax or $ymin == $ymax);

    my $xbin_function = ($xscale eq "Logarithmic")
	? sub { return int((log($_[0]/$xmin)/log($xbinwidth))+0.5); }
        : sub { return int(($_[0]-$xmin)/($xbinwidth)+0.5); };
    my $xbin_placement = ($xscale eq "Logarithmic")
	? sub { return ($xmin*exp(($_[0])*log($xbinwidth))); }
        : sub { return ($xmin + ($_[0])*$xbinwidth) } ;
    my $ybin_function = ($yscale eq "Logarithmic")
	? sub { return int((log($_[0]/$ymin)/log($ybinwidth))+0.5); }
        : sub { return int(($_[0]-$ymin)/($ybinwidth)+0.5); };
    my $ybin_placement = ($yscale eq "Logarithmic")
	? sub { return ($ymin*exp(($_[0])*log($ybinwidth))) }
        : sub { return ($ymin + ($_[0])*$ybinwidth) } ;

    my %bins;
#    foreach (@data) {
    #print $graph->{data};
    #print "\n\n";
    my ($zmin, $zmax ) = (1e300, -1e300);
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	$_ = $1;
	next unless (length());
	my ($x, $y, $z) = split;
	next if ($y < $ymin || $y > $ymax);
	next if ($x < $xmin || $x > $xmax);
	#$bins{$xbin_function->($x)}{$ybin_function->($y)}=$z;
	$bins{$x}{$y} = $z;
	if ($z < $zmin) { $zmin = $z; }
	if ($z > $zmax) { $zmax = $z; }
    }

    #smooth
    if ($graph->{smooth}) {
	smoothxy(\%bins, $xbins, $ybins) for (1..$graph->{smooth});
    }


    #normalize
    if ($conditional) {
	foreach my $xbin (values %bins) {
	    my $max = 0;
	    foreach (values %$xbin) { $max = $_ if ($_>$max); $_ = 0 if ($_<0);}
	    foreach (keys %$xbin) { $xbin->{$_}/=$max if ($max > 0); }
	}
    } else {
	$zmax = $graph->{zmax} if (defined $graph->{zmax});
	$zmin = $graph->{zmin} if (defined $graph->{zmin});
	if ($zmax != $zmin) {
	    foreach my $xbin (values %bins) {
		foreach (keys %$xbin) { $xbin->{$_}=($xbin->{$_}-$zmin)/($zmax-$zmin); }
	    }
	    $graph->{zmax} = $zmax;
	    $graph->{zmin} = $zmin;
	} else {
	    foreach my $xbin (values %bins) {
		foreach (keys %$xbin) { $xbin->{$_}=1; }
	    }
	}
    }

    if ($plottype and $plottype == 2) {
	convert_to_contour(\%bins);
    }

    #convert to colors
    my $output = "";
    foreach my $x (sort {$a <=> $b } keys %bins) {
	my $loc_x = $x; #$xbin_placement->($x);
	foreach my $y (sort {$a <=> $b} keys %{$bins{$x}}) {
	    my $loc_y = $y; #$ybin_placement->($y);
	    unless ($plottype) {
		my $color = int((1-$bins{$x}{$y})*(256-16))+16;
		if ($color < 16) { $color = 16; } #die "Invalid bin operation---dying ($color<16)!\n"; }
		$color %= 256;
		$output .= "$loc_x $loc_y $color\n" if (int($bins{$x}{$y}*(256-16)));
	    } else {
		my $size = 0.72 * sqrt($bins{$x}{$y});
		$output .= "$loc_x $loc_y $size\n" if ($size > 0.01);
	    }
	}
    }
    
    $graph->{data} = $output;
    $graph->{converted} = 1;
}


#plottype:
# 0: Density (grayscale) plot
# 1: XySize (symbol size) plot
# 2: Contour plot
sub convert_to_density {
    my ($graph, $conditional, $plottype, $pxmin, $pxmax, $pymin, $pymax) = @_;
    return if ($graph->{converted});
    my ($xbins, $ybins) = (80,60);
    #my @data = split(/\n/, $graph->{data});
    my ($xmax, $xmin, $ymax, $ymin) = (-1e300, 1e300, -1e300, 1e300);
    my ($xscale, $yscale);
    local $_;
    #foreach (@data) {
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	$_ = $1;
	next unless (length());
	my ($x, $y) = split();
	#next if ($y < 1e-6);
	$xmax = $x if ($x > $xmax);
	$xmin = $x if ($x < $xmin);
	$ymax = $y if ($y > $ymax);
	$ymin = $y if ($y < $ymin);
    }
    $xmax = $pxmax if (defined $pxmax);
    $xmin = $pxmin if (defined $pxmin);
    $ymax = $pymax if (defined $pymax);
    $ymin = $pymin if (defined $pymin);

    $xscale = ($xmin <= 0 or $xmax/$xmin < 20) ? "Normal" : "Logarithmic";
    $yscale = ($ymin <= 0 or $ymax/$ymin < 20) ? "Normal" : "Logarithmic";
    $xscale = $graph->{xscale} if (defined $graph->{xscale});
    $yscale = $graph->{yscale} if (defined $graph->{yscale});
    return if ($xmin == $xmax or $ymin == $ymax);

    my $xbin_function = ($xscale eq "Logarithmic")
	? sub { return int($xbins*(log($_[0]/$xmin)/log($xmax/$xmin))); }
        : sub { return int($xbins * ($_[0]-$xmin)/($xmax-$xmin)); };
    my $xbin_placement = ($xscale eq "Logarithmic")
	? sub { return ($xmin*exp(($_[0]+0.5)*log($xmax/$xmin)/$xbins)); }
        : sub { return ($xmin + ($_[0]+0.5)*($xmax-$xmin)/$xbins) } ;
    my $ybin_function = ($yscale eq "Logarithmic")
	? sub { return int($ybins*(log($_[0]/$ymin)/log($ymax/$ymin))); }
        : sub { return int($ybins * ($_[0]-$ymin)/($ymax-$ymin)); };
    my $ybin_placement = ($yscale eq "Logarithmic")
	? sub { return ($ymin*exp(($_[0]+0.5)*log($ymax/$ymin)/$ybins)); }
        : sub { return ($ymin + ($_[0]+0.5)*($ymax-$ymin)/$ybins) } ;

    my %bins;
#    foreach (@data) {
    #print $graph->{data} if (defined $foo);
    #print "\n\n";
    while ($graph->{data} =~ /\G([^\n]+)\n+/g) {
	$_ = $1;
	next unless (length());
	my ($x, $y, $w) = split;
	#print "$x $y\n" if (defined $foo);
	$w = 1 unless (defined $w);
	next if ($y < $ymin || $y > $ymax);
	next if ($x < $xmin || $x > $xmax);
	$bins{$xbin_function->($x)}{$ybin_function->($y)}+=$w;
    }

    if ($plottype and $plottype == 2) {
	convert_to_contour(\%bins);
    }

    #smooth
    if ($graph->{smooth}) {
	smooth(\%bins, $xbins, $ybins) for (1..$graph->{smooth});
    }

    if ($graph->{density_transform} and ref($graph->{density_transform}) eq 'CODE') {
	foreach my $xbin (keys %bins) {
	    foreach (keys %{$bins{$xbin}}) { 
		$bins{$xbin}{$_}=
		    $graph->{density_transform}->($bins{$xbin}{$_},
						  $xbin_placement->($xbin),
						  $ybin_placement->($_));
	    }
	}
    }

    #normalize
    if ($conditional) {
	foreach my $xbin (values %bins) {
	    my $max = 0;
	    foreach (values %$xbin) { $max = $_ if ($_>$max); }
	    foreach (keys %$xbin) { $xbin->{$_}/=$max; }
	}
    } else {
	my $max = 0;
	foreach my $xbin (values %bins) {
	    foreach (values %$xbin) { $max = $_ if ($_>$max); }
	}
	$max = 1 unless $max>0;
	foreach my $xbin (values %bins) {
	    foreach (keys %$xbin) { $xbin->{$_}/=$max; }
	}
    }


    #convert to colors
    my $output = "";
    foreach my $x (sort {$a <=> $b } keys %bins) {
	my $loc_x = $xbin_placement->($x);
	foreach my $y (sort {$a <=> $b} keys %{$bins{$x}}) {
	    my $loc_y = $ybin_placement->($y);
	    unless ($plottype) {
		my $color = int((1-$bins{$x}{$y})*(256-16))+16;
		if ($color < 16) { die "Invalid bin operation---dying!\n"; }
		$color %= 256;
		$output .= "$loc_x $loc_y $color\n" if (int($bins{$x}{$y}*(256-16)));
	    } else {
		my $size = 0.72 * sqrt($bins{$x}{$y});
		$output .= "$loc_x $loc_y $size\n" if ($size > 0.01);
	    }
	}
    }
    
    $graph->{data} = $output;
    $graph->{converted} = 1;
}

sub convert_to_poly {
    my ($graph) = @_;
    return if ($graph->{converted});
    my @data = split(/\n/, $graph->{data});
    my (@newdata, @newdata2);
    {
	no warnings "numeric";
	@data = sort { $a <=> $b } @data;
    }
    foreach (@data) {
	my ($x, $y, $dy1, $dy2) = split;
	$dy1 = 0 unless (defined ($dy1));
	$dy2 = $dy1 unless (defined ($dy2));
	push @newdata, "$x ". ($y-$dy2). "\n";
	push @newdata2, "$x ". ($y+$dy1). "\n";
    }
    $graph->{data} = join("", @newdata, reverse @newdata2);
    $graph->{converted} = 1;
}


sub hsv2rgb {
    my ( $h, $s, $v ) = @_;

    if ( $s == 0 ) {
        return $v, $v, $v;
    }

    $h /= 60;
    my $i = int( $h );
    my $f = $h - $i;
    my $p = $v * ( 1 - $s );
    my $q = $v * ( 1 - $s * $f );
    my $t = $v * ( 1 - $s * ( 1 - $f ) );

    if ( $i == 0 ) {
        return $v, $t, $p;
    }
    elsif ( $i == 1 ) {
        return $q, $v, $t;
    }
    elsif ( $i == 2 ) {
        return $p, $v, $t;
    }
    elsif ( $i == 3 ) {
        return $p, $q, $v;
    }
    elsif ( $i == 4 ) {
        return $t, $p, $v;
    }
    else {
        return $v, $p, $q;
    }
}

sub gen_colorstrip {
    my ($params, $zmin, $zmax) = @_;
    my ($x1, $y1, $horiz, $marked, $reverse) = @$params;
    my ($x2, $y2) = ($x1+0.05, $y1-0.2);
    if ($horiz) { ($x2, $y2) = ($x1+0.2, $y1-0.05); }
    my @boxes = ();
    my $val = $zmax;
    for (16..254) {
	my $c = ($reverse) ? (254-$_+16) : $_;
	if ($marked) {
	    my $newval = ($_-16)*($zmin-$zmax)/(254-16) + $zmax;
	    if (int($newval+20-2*($zmax-$zmin)/(254-16)) < int($val+20-2*($zmax-$zmin)/(254-16)) and $newval > $val-0.1 ) {  $c = 1; }
	    else { $val = $newval; }
	}
	my $f = ($_ - 16)/(255-16);
	my $f2 = ($_ - 14)/(255-16);
	unless ($horiz) {
	    push @boxes, { pos => "$x1,".($y1 + $f*($y2-$y1)).",$x2,".($y1+$f2*($y2-$y1)), color => $c };
	} else {
	    push @boxes, { pos => ($x1+(1-$f)*($x2-$x1)).",$y1,".($x1+(1-$f2)*($x2-$x1)).",$y2", color => $c };
	}
    }
    return @boxes;
}

sub pretty_color {
    my $index = shift;
    my $f = 1-($index-16)/(255-16+1);
    my $pow = 1.5;
    if ($f>0.8) {
	$f = $f**$pow;
    } else {
	$f *= 0.8**($pow-1);
    }
    my $r = int(sqrt($f)*255);
    my $g = int($f*$f*$f*255);
    my $b = int(sin(2*3.14159*$f)*255);
    if ($b < 0) { $b = 0; }
#    my $s = 0.85;
#    if ($f < 4/8 and $f > 3/8) { $s += 0.2 * 8*($f-4/8); }
#    if ($f < 3/8) { $s = 0.65; }
#    my $h = (240 + 180*$f)%360;
#    my $v = 254*(sqrt($f)+0.18*exp(-200*($f-0.35)**2)+2*$f*exp(-100*($f-0.125)**2));
#    my ($r, $g, $b) = map { int($_) } hsv2rgb($h, $s, $v);
    return "($r, $g, $b)";
}

sub rgb_color {
    my $index = shift;
    my $f = ($index-16)/(255-16+1)*240;
    my $r = ($f < 60) ? 1 :
	($f < 120) ? (120-$f)/60 : 0;
    my $g = ($f < 60) ? $f/60 :
	($f < 180) ? 1 : (240-$f)/60;
    my $b = ($f < 120) ? 0 :
	($f < 180) ? ($f-120)/60 : 1;
    $r = int($r*255);
    $g = int($g*255);
    $b = int($b*255);
#    print "$index $f $r $g $b\n";
    return "($r, $g, $b)";
}

BEGIN {

    our $header = <<'END_OF_HEADER'
# Grace project file
#
@version 50116
@page size 792, <page_size>
@page scroll 5%
@page inout 5%
@link page off
@map font 0 to "Times-Roman", "Times-Roman"
@map font 1 to "Times-Italic", "Times-Italic"
@map font 2 to "Times-Bold", "Times-Bold"
@map font 3 to "Times-BoldItalic", "Times-BoldItalic"
@map font 4 to "Helvetica", "Helvetica"
@map font 5 to "Helvetica-Oblique", "Helvetica-Oblique"
@map font 6 to "Helvetica-Bold", "Helvetica-Bold"
@map font 7 to "Helvetica-BoldOblique", "Helvetica-BoldOblique"
@map font 8 to "Courier", "Courier"
@map font 9 to "Courier-Oblique", "Courier-Oblique"
@map font 10 to "Courier-Bold", "Courier-Bold"
@map font 11 to "Courier-BoldOblique", "Courier-BoldOblique"
@map font 12 to "Symbol", "Symbol"
@map font 13 to "ZapfDingbats", "ZapfDingbats"
END_OF_HEADER
;

    our $def_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 5 to (188, 143, 143), "brown"
@map color 6 to (220, 220, 100), "sand"
@map color 7 to (148, 0, 211), "violet"
@map color 8 to (0, 255, 255), "cyan"
@map color 9 to (255, 0, 255), "magenta"
@map color 10 to (255, 165, 0), "orange"
@map color 11 to (114, 33, 188), "indigo"
@map color 12 to (103, 7, 72), "maroon"
@map color 13 to (64, 224, 208), "turquoise"
@map color 14 to (150, 150, 150), "grey"
@map color 15 to (255, 255, 0), "yellow"
END_OF_DEF_COLORS
;

    our $molly_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 13 to (188, 143, 143), "brown"
@map color 6 to (220, 220, 100), "sand"
@map color 7 to (148, 0, 211), "violet"
@map color 8 to (0, 255, 255), "cyan"
@map color 9 to (255, 0, 255), "magenta"
@map color 10 to (255, 165, 0), "orange"
@map color 11 to (114, 33, 188), "indigo"
@map color 12 to (103, 7, 72), "maroon"
@map color 5 to (32, 112, 104), "turquoise"
@map color 14 to (150, 150, 150), "grey"
@map color 15 to (255, 255, 0), "yellow"
END_OF_DEF_COLORS
;


our $rainbow_colors_old = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (255, 128, 0), "orange"
@map color 4 to (255, 255, 0), "yellow"
@map color 5 to (0, 255, 0), "green"
@map color 6 to (0, 255, 255), "cyan"
@map color 7 to (0, 128, 255), "cyan2"
@map color 8 to (0, 0, 255), "blue"
@map color 9 to (255, 0, 255), "magenta"
@map color 10 to (255, 0, 128), "violet"
@map color 12 to (188, 143, 143), "brown"
@map color 13 to (220, 220, 100), "sand"
@map color 14 to (103, 7, 72), "maroon"
@map color 15 to (64, 224, 208), "turquoise"
END_OF_DEF_COLORS
;


    our $rainbow_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (255, 128, 0), "orange"
@map color 4 to (240, 255, 0), "yellow1"
@map color 5 to (140, 255, 0), "green1"
@map color 6 to (0, 255, 180), "green2"
@map color 7 to (0, 255, 255), "cyan"
@map color 8 to (0, 128, 255), "cyan2"
@map color 9 to (0, 0, 255), "blue"
@map color 10 to (180, 0, 255), "violet1"
@map color 11 to (255, 0, 255), "magenta"
@map color 12 to (255, 0, 128), "violet"
@map color 13 to (188, 143, 143), "brown"
@map color 14 to (220, 220, 100), "sand"
@map color 15 to (103, 7, 72), "maroon"
@map color 16 to (64, 224, 208), "turquoise"
END_OF_DEF_COLORS
	;


    our $rainbow_colors2 = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (255, 128, 0), "orange"
@map color 4 to (240, 255, 0), "yellow1"
@map color 5 to (140, 255, 0), "green1"
@map color 6 to (0, 255, 180), "green2"
@map color 7 to (0, 255, 255), "cyan"
@map color 8 to (0, 128, 255), "cyan2"
@map color 9 to (0, 0, 255), "blue"
@map color 10 to (180, 0, 255), "violet1"
@map color 11 to (255, 0, 255), "magenta"
@map color 12 to (255, 0, 128), "violet"
@map color 13 to (188, 143, 143), "brown"
@map color 14 to (220, 220, 100), "sand"
@map color 15 to (103, 7, 72), "maroon"
@map color 16 to (64, 224, 208), "turquoise"
END_OF_DEF_COLORS
;


    our $color_errorstrip_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 5 to (188, 143, 143), "brown"
@map color 6 to (220, 220, 100), "sand"
@map color 7 to (148, 0, 211), "violet"
@map color 8 to (0, 255, 255), "cyan"
@map color 9 to (255, 0, 255), "magenta"
@map color 10 to (255, 165, 0), "orange"
@map color 11 to (114, 33, 188), "indigo"
@map color 12 to (103, 7, 72), "maroon"
@map color 13 to (64, 224, 208), "turquoise"
@map color 14 to (0, 139, 0), "green4"
@map color 15 to (255, 255, 0), "yellow"
@map color 16 to (255, 255, 255), "white2"
@map color 17 to (180, 180, 180), "black2"
@map color 18 to (255, 150, 150), "red2"
@map color 19 to (150, 150, 255), "blue2"
@map color 20 to (150, 255, 150), "green2"
@map color 21 to (241, 190, 161), "brown2"
@map color 22 to (230, 230, 150), "sand2"
@map color 23 to (200, 150, 255), "violet2"
@map color 24 to (0, 255, 255), "cyan2"
@map color 25 to (255, 150, 255), "magenta2"
@map color 26 to (255, 165, 0), "orange2"
@map color 27 to (114, 33, 188), "indigo2"
@map color 28 to (103, 7, 72), "maroon2"
@map color 29 to (64, 224, 208), "turquoise2"
@map color 30 to (0, 139, 0), "green42"
@map color 31 to (255, 255, 0), "yellow2"
END_OF_DEF_COLORS
;

    our $fading_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 5 to (130, 100, 100), "brown"
@map color 6 to (255, 0, 255), "magenta"
@map color 7 to (150, 150, 150), "black1"
@map color 8 to (255, 100, 100), "red1"
@map color 9 to (100, 100, 255), "blue1"
@map color 10 to (100, 255, 100), "green1"
@map color 11 to (200, 150, 150), "brown1"
@map color 12 to (255, 100, 255), "magenta1"
@map color 13 to (190, 190, 190), "black2"
@map color 14 to (255, 160, 160), "red2"
@map color 15 to (160, 160, 255), "blue2"
@map color 16 to (160, 255, 160), "green2"
@map color 17 to (220, 180, 180), "brown2"
@map color 18 to (255, 160, 255), "magenta2"
@map color 19 to (220, 220, 220), "black3"
@map color 20 to (255, 200, 200), "red3"
@map color 21 to (200, 200, 255), "blue3"
@map color 22 to (200, 255, 200), "green3"
@map color 23 to (255, 220, 220), "brown3"
@map color 24 to (255, 220, 255), "magenta3"
@map color 25 to (240, 240, 240), "black4"
@map color 26 to (255, 220, 220), "red4"
@map color 27 to (220, 220, 255), "blue4"
@map color 28 to (220, 255, 220), "green4"
@map color 29 to (255, 240, 240), "brown4"
@map color 30 to (255, 240, 255), "magenta4"
@map color 31 to (255, 255, 0), "yellow2"
END_OF_DEF_COLORS
;


    our $reduced_color_errorstrip_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 5 to (188, 143, 143), "brown"
@map color 6 to (220, 220, 100), "sand"
@map color 7 to (148, 0, 211), "violet"
@map color 8 to (0, 255, 255), "cyan"
@map color 9 to (180, 180, 180), "black2"
@map color 10 to (255, 150, 150), "red2"
@map color 11 to (150, 150, 255), "blue2"
@map color 12 to (150, 255, 150), "green2"
@map color 13 to (241, 190, 161), "brown2"
@map color 14 to (220, 220, 100), "sand2"
@map color 15 to (148, 0, 211), "violet2"
END_OF_DEF_COLORS
;

    our $alt_linecolors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
@map color 2 to (255, 0, 0), "red"
@map color 3 to (0, 0, 255), "blue"
@map color 4 to (0, 255, 0), "green"
@map color 5 to (188, 143, 143), "brown"
@map color 6 to (220, 220, 100), "sand"
@map color 7 to (148, 0, 211), "violet"
@map color 8 to (0, 255, 255), "cyan"
@map color 9 to (255, 0, 255), "magenta"
@map color 10 to (255, 165, 0), "orange"
@map color 11 to (114, 33, 188), "indigo"
@map color 12 to (103, 7, 72), "maroon"
@map color 13 to (64, 224, 208), "turquoise"
@map color 14 to (0, 139, 0), "green4"
@map color 15 to (255, 255, 0), "yellow"
@map color 16 to (255, 255, 255), "white2"
@map color 17 to (120, 120, 120), "black2"
@map color 18 to (240, 80, 120), "red2"
@map color 19 to (80, 120, 240), "blue2"
@map color 20 to (120, 240, 80), "green2"
@map color 21 to (241, 190, 161), "brown2"
@map color 22 to (220, 220, 100), "sand2"
@map color 23 to (148, 0, 211), "violet2"
@map color 24 to (0, 255, 255), "cyan2"
@map color 25 to (255, 0, 255), "magenta2"
@map color 26 to (255, 165, 0), "orange2"
@map color 27 to (114, 33, 188), "indigo2"
@map color 28 to (103, 7, 72), "maroon2"
@map color 29 to (64, 224, 208), "turquoise2"
@map color 30 to (0, 139, 0), "green42"
@map color 31 to (255, 255, 0), "yellow2"
END_OF_DEF_COLORS
;

$color_errorstrip_colors .= join("", map { '@map color '."$_ to ($_, $_, $_), \"grey$_\"\n" } (32..255));

our $grey_colors = <<'END_OF_DEF_COLORS'
@map color 0 to (255, 255, 255), "white"
@map color 1 to (0, 0, 0), "black"
END_OF_DEF_COLORS
;
$grey_colors .= join("", map { my $c = int(($_+1)/4*255);
			       '@map color '.($_+1)." to ($c,$c,$c), \"b$_\"\n"}
		     (1..2));

    our $hist_colors = 
join("", map { '@map color '."$_ to ($_, $_, $_), \"grey$_\"\n" } (16..255));

    our $pretty_hist_colors = 
join("", map { '@map color '."$_ to ".pretty_color($_).", \"color$_\"\n" } (16..255));

    our $rgb_hist_colors = 
join("", map { '@map color '."$_ to ".rgb_color($_).", \"color$_\"\n" } (16..255));

    our $graph_defaults = <<'END_OF_GRAPH_DEFAULTS'
@reference date 0
@date wrap off
@date wrap year 1950
@default linewidth 1.0
@default linestyle 1
@default color 1
@default pattern 1
@default font 0
@default char size 1.0
@default symbol size 1.000000
@default sformat "%.8g"
@background color 0
@page background fill off
@timestamp off
@timestamp 0.03, 0.03
@timestamp color 1
@timestamp rot 0
@timestamp font 0
@timestamp char size 1.000000
@timestamp def "Thu Dec 20 15:01:51 2007"
<text>
<boxes>
@r0 off
@link r0 to g0
@r0 type above
@r0 linestyle 1
@r0 linewidth 1.0
@r0 color 1
@r0 line 0, 0, 0, 0
@r1 off
@link r1 to g0
@r1 type above
@r1 linestyle 1
@r1 linewidth 1.0
@r1 color 1
@r1 line 0, 0, 0, 0
@r2 off
@link r2 to g0
@r2 type above
@r2 linestyle 1
@r2 linewidth 1.0
@r2 color 1
@r2 line 0, 0, 0, 0
@r3 off
@link r3 to g0
@r3 type above
@r3 linestyle 1
@r3 linewidth 1.0
@r3 color 1
@r3 line 0, 0, 0, 0
@r4 off
@link r4 to g0
@r4 type above
@r4 linestyle 1
@r4 linewidth 1.0
@r4 color 1
@r4 line 0, 0, 0, 0
@g0 on
@g0 hidden false
@g0 type XY
@g0 stacked false
@g0 bar hgap 0.000000
@g0 fixedpoint off
@g0 fixedpoint type 0
@g0 fixedpoint xy 0.000000, 0.000000
@g0 fixedpoint format general general
@g0 fixedpoint prec 6, 6
END_OF_GRAPH_DEFAULTS
;
    
    our $graph_setup = <<'END_OF_GRAPH_SETUP'
@with g0
@    world <xmin>, <ymin>, <xmax>, <ymax>
@    stack world 0, 0, 0, 0
@    znorm 1
@    view <xviewmin>, <yviewmin>, <xviewmax>, <yviewmax>
@    title "<title>"
@    title font 0
@    title size 1.500000
@    title color 1
@    subtitle "<subtitle>"
@    subtitle font 0
@    subtitle size 1.000000
@    subtitle color 1
@    xaxes scale <xscale>
@    yaxes scale <yscale>
@    xaxes invert off
@    yaxes invert off
@    xaxis  <xaxis>
@    xaxis  type zero false
@    xaxis  offset 0.000000 , 0.000000
@    xaxis  bar on
@    xaxis  bar color <labelcolor>
@    xaxis  bar linestyle 1
@    xaxis  bar linewidth 1.0
@    xaxis  label "<xlabel>"
@    xaxis  label layout para
@    xaxis  label place auto
@    xaxis  label char size <labelsize>
@    xaxis  label font 0
@    xaxis  label color <labelcolor>
@    xaxis  label place normal
@    xaxis  tick on
@    xaxis  tick major <xmajortick>
@    xaxis  tick minor ticks <xminorticks>
@    xaxis  tick default 6
@    xaxis  tick place rounded true
@    xaxis  tick <xtickdir>
@    xaxis  tick major size 1.00000
@    xaxis  tick major color <labelcolor>
@    xaxis  tick major linewidth 1.0
@    xaxis  tick major linestyle <xticklstyle>
@    xaxis  tick major grid off
@    xaxis  tick minor color <labelcolor>
@    xaxis  tick minor linewidth 1.0
@    xaxis  tick minor linestyle <xticklstyle>
@    xaxis  tick minor grid off
@    xaxis  tick minor size 0.500000
@    xaxis  ticklabel <xaxisnumbers>
@    xaxis  ticklabel format <xaxisformat>
@    xaxis  ticklabel prec <xaxisprec>
@    xaxis  ticklabel formula ""
@    xaxis  ticklabel append ""
@    xaxis  ticklabel prepend ""
@    xaxis  ticklabel angle <xticklabelangle>
@    xaxis  ticklabel skip 0
@    xaxis  ticklabel stagger 0
@    xaxis  ticklabel place normal
@    xaxis  ticklabel offset auto
@    xaxis  ticklabel offset 0.000000 , 0.010000
@    xaxis  ticklabel start type auto
@    xaxis  ticklabel start 0.000000
@    xaxis  ticklabel stop type auto
@    xaxis  ticklabel stop 0.000000
@    xaxis  ticklabel char size <xticklabelsize>
@    xaxis  ticklabel font 0
@    xaxis  ticklabel color <labelcolor>
@    xaxis  tick place <xaxistickplace>
@    xaxis  tick spec type <xtickspec>
@    yaxis  <yaxis>
@    yaxis  type zero false
@    yaxis  offset 0.000000 , 0.000000
@    yaxis  bar on
@    yaxis  bar color <labelcolor>
@    yaxis  bar linestyle 1
@    yaxis  bar linewidth 1.0
@    yaxis  label "<ylabel>"
@    yaxis  label layout para
@    yaxis  label place auto
@    yaxis  label char size <labelsize>
@    yaxis  label font 0
@    yaxis  label color <labelcolor>
@    yaxis  label place normal
@    yaxis  tick on
@    yaxis  tick major <ymajortick>
@    yaxis  tick minor ticks <yminorticks>
@    yaxis  tick default 6
@    yaxis  tick place rounded true
@    yaxis  tick <ytickdir>
@    yaxis  tick major size 1.000000
@    yaxis  tick major color <labelcolor>
@    yaxis  tick major linewidth 1.0
@    yaxis  tick major linestyle <yticklstyle>
@    yaxis  tick major grid off
@    yaxis  tick minor color <labelcolor>
@    yaxis  tick minor linewidth 1.0
@    yaxis  tick minor linestyle <yticklstyle>
@    yaxis  tick minor grid off
@    yaxis  tick minor size 0.500000
@    yaxis  ticklabel <yaxisnumbers>
@    yaxis  ticklabel format <yaxisformat>
@    yaxis  ticklabel prec <yaxisprec>
@    yaxis  ticklabel formula ""
@    yaxis  ticklabel append ""
@    yaxis  ticklabel prepend ""
@    yaxis  ticklabel angle 0
@    yaxis  ticklabel skip 0
@    yaxis  ticklabel stagger 0
@    yaxis  ticklabel place normal
@    yaxis  ticklabel offset auto
@    yaxis  ticklabel offset 0.000000 , 0.010000
@    yaxis  ticklabel start type auto
@    yaxis  ticklabel start 0.000000
@    yaxis  ticklabel stop type auto
@    yaxis  ticklabel stop 0.000000
@    yaxis  ticklabel char size <yticklabelsize>
@    yaxis  ticklabel font 0
@    yaxis  ticklabel color <labelcolor>
@    yaxis  tick place <yaxistickplace>
@    yaxis  tick spec type <ytickspec>
@    altxaxis  <altxaxis>
@    altyaxis  <altyaxis>
@    legend on
@    legend loctype view
@    legend <legendxy>
@    legend box color 1
@    legend box pattern 0
@    legend box linewidth 1.0
@    legend box linestyle 0
@    legend box fill color 0
@    legend box fill pattern <legendfill>
@    legend font 0
@    legend char size <labelsize>
@    legend color <legendcolor>
@    legend length 8
@    legend vgap <lvgap>
@    legend hgap 1
@    legend invert false
@    frame type 0
@    frame linestyle 1
@    frame linewidth 1.0
@    frame color <labelcolor>
@    frame pattern 1
@    frame background color <fbgcolor>
@    frame background pattern 1
END_OF_GRAPH_SETUP
;


our $line_syntax = <<'END_OF_LINE_SYNTAX'
@with line
@    line on
@    line loctype view
@    line <pos>
@    line linewidth <width>
@    line linestyle <style>
@    line color <color>
@    line arrow 0
@    line arrow type 0
@    line arrow length 1.000000
@    line arrow layout 1.000000, 1.000000
@line def
END_OF_LINE_SYNTAX
;


our $ellipse_syntax = <<'END_OF_ELLIPSE_SYNTAX'
@with ellipse
@    ellipse on
@    ellipse loctype view
@    ellipse <pos>
@    ellipse linestyle <lstyle>
@    ellipse linewidth <lwidth>
@    ellipse color <lcolor>
@    ellipse fill color <color>
@    ellipse fill pattern <pattern>
@ellipse def
END_OF_ELLIPSE_SYNTAX
;

our $box_syntax = <<'END_OF_BOX_SYNTAX'
@with box
@    box on
@    box loctype view
@    box <pos>
@    box linestyle 0
@    box linewidth 0
@    box color 0
@    box fill color <color>
@    box fill pattern 1
@box def
END_OF_BOX_SYNTAX
;

our $text_syntax = <<'END_OF_TEXT_SYNTAX'
@with string
@    string on
@    string loctype view
@    string <pos>
@    string color <color>
@    string rot <rot>
@    string font 0
@    string just <just>
@    string char size <size>
@    string def "<text>"
END_OF_TEXT_SYNTAX
;

    our $data_setup = <<'END_OF_DATA_SETUP'
@    s0 hidden false
@    s0 type <type>
@    s0 symbol <stype>
@    s0 symbol size <ssize>
@    s0 symbol color <scolor>
@    s0 symbol pattern 1
@    s0 symbol fill color <sfcolor>
@    s0 symbol fill pattern <spattern>
@    s0 symbol linewidth <slwidth>
@    s0 symbol linestyle <slstyle>
@    s0 symbol char 65
@    s0 symbol char font 0
@    s0 symbol skip <sskip>
@    s0 line type <ltype>
@    s0 line linestyle <lstyle>
@    s0 line linewidth <lwidth>
@    s0 line color <lcolor>
@    s0 line pattern 1
@    s0 baseline type 0
@    s0 baseline off
@    s0 dropline off
@    s0 fill type <ftype>
@    s0 fill rule 0
@    s0 fill color <fcolor>
@    s0 fill pattern 1
@    s0 avalue off
@    s0 avalue type 2
@    s0 avalue char size 1.000000
@    s0 avalue font 0
@    s0 avalue color 1
@    s0 avalue rot 0
@    s0 avalue format general
@    s0 avalue prec 3
@    s0 avalue prepend ""
@    s0 avalue append ""
@    s0 avalue offset 0.000000 , 0.000000
@    s0 errorbar on
@    s0 errorbar place <errorplace>
@    s0 errorbar color <lcolor>
@    s0 errorbar pattern 1
@    s0 errorbar size 1.000000
@    s0 errorbar linewidth <arrowwidth>
@    s0 errorbar linestyle 1
@    s0 errorbar riser linewidth <arrowwidth>
@    s0 errorbar riser linestyle 1
@    s0 errorbar riser clip off
@    s0 errorbar riser clip length 0.100000
@    s0 comment ""
@    s0 legend  "<legend>"
END_OF_DATA_SETUP
;

    our $data_data = <<'END_OF_DATA_DATA'
@target G0.S0
@type <type>
<data>
&
END_OF_DATA_DATA
;

our $altxaxis_spec = <<'END_OF_ALT_XAXIS'
@    altxaxis  type zero false
@    altxaxis  offset 0.000000 , 0.000000
@    altxaxis  bar on
@    altxaxis  bar color 1
@    altxaxis  bar linestyle 1
@    altxaxis  bar linewidth 1.0
@    altxaxis  label "<altxlabel>"
@    altxaxis  label layout para
@    altxaxis  label place auto
@    altxaxis  label char size <labelsize>
@    altxaxis  label font 0
@    altxaxis  label color <labelcolor>
@    altxaxis  label place opposite
@    altxaxis  tick on
@    altxaxis  tick major <altxmajortick>
@    altxaxis  tick minor ticks <altxminorticks>
@    altxaxis  tick default 6
@    altxaxis  tick place rounded true
@    altxaxis  tick <altxtickdir>
@    altxaxis  tick major size 1.00000
@    altxaxis  tick major color <labelcolor>
@    altxaxis  tick major linewidth 1.0
@    altxaxis  tick major linestyle 1
@    altxaxis  tick major grid off
@    altxaxis  tick minor color <labelcolor>
@    altxaxis  tick minor linewidth 1.0
@    altxaxis  tick minor linestyle 1
@    altxaxis  tick minor grid off
@    altxaxis  tick minor size 0.500000
@    altxaxis  ticklabel <altxaxisnumbers>
@    altxaxis  ticklabel format <altxaxisformat>
@    altxaxis  ticklabel prec <altxaxisprec>
@    altxaxis  ticklabel formula ""
@    altxaxis  ticklabel append ""
@    altxaxis  ticklabel prepend ""
@    altxaxis  ticklabel angle 0
@    altxaxis  ticklabel skip 0
@    altxaxis  ticklabel stagger 0
@    altxaxis  ticklabel place opposite
@    altxaxis  ticklabel offset auto
@    altxaxis  ticklabel offset 0.000000 , 0.010000
@    altxaxis  ticklabel start type auto
@    altxaxis  ticklabel start 0.000000
@    altxaxis  ticklabel stop type auto
@    altxaxis  ticklabel stop 0.000000
@    altxaxis  ticklabel char size <labelsize>
@    altxaxis  ticklabel font 0
@    altxaxis  ticklabel color <labelcolor>
@    altxaxis  tick place opposite
@    altxaxis  tick spec type <altxtickspec>
END_OF_ALT_XAXIS
;


our $altyaxis_spec = <<'END_OF_ALT_YAXIS'
@    altyaxis  type zero false
@    altyaxis  offset 0.000000 , 0.000000
@    altyaxis  bar on
@    altyaxis  bar color 1
@    altyaxis  bar linestyle 1
@    altyaxis  bar linewidth 1.0
@    altyaxis  label "<altylabel>"
@    altyaxis  label layout para
@    altyaxis  label place auto
@    altyaxis  label char size <labelsize>
@    altyaxis  label font 0
@    altyaxis  label color <labelcolor>
@    altyaxis  label place opposite
@    altyaxis  tick on
@    altyaxis  tick major <altymajortick>
@    altyaxis  tick minor ticks <altyminorticks>
@    altyaxis  tick default 6
@    altyaxis  tick place rounded true
@    altyaxis  tick <altytickdir>
@    altyaxis  tick major size 1.00000
@    altyaxis  tick major color <labelcolor>
@    altyaxis  tick major linewidth 1.0
@    altyaxis  tick major linestyle 1
@    altyaxis  tick major grid off
@    altyaxis  tick minor color <labelcolor>
@    altyaxis  tick minor linewidth 1.0
@    altyaxis  tick minor linestyle 1
@    altyaxis  tick minor grid off
@    altyaxis  tick minor size 0.500000
@    altyaxis  ticklabel <altyaxisnumbers>
@    altyaxis  ticklabel format <altyaxisformat>
@    altyaxis  ticklabel prec <altyaxisprec>
@    altyaxis  ticklabel formula ""
@    altyaxis  ticklabel append ""
@    altyaxis  ticklabel prepend ""
@    altyaxis  ticklabel angle 0
@    altyaxis  ticklabel skip 0
@    altyaxis  ticklabel stagger 0
@    altyaxis  ticklabel place opposite
@    altyaxis  ticklabel offset auto
@    altyaxis  ticklabel offset 0.000000 , 0.010000
@    altyaxis  ticklabel start type auto
@    altyaxis  ticklabel start 0.000000
@    altyaxis  ticklabel stop type auto
@    altyaxis  ticklabel stop 0.000000
@    altyaxis  ticklabel char size <labelsize>
@    altyaxis  ticklabel font 0
@    altyaxis  ticklabel color <labelcolor>
@    altyaxis  tick place opposite
@    altyaxis  tick spec type <altytickspec>
END_OF_ALT_YAXIS
    
}

1;

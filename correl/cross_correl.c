#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include "../check_syscalls.h"
#include "../distance.h"
#include "regions.h"
#include "../mt_rand.h"
#include "correl.h"

double max_pi = 40.0;
double PI_BPUNIT = 0, PI_INV_BPUNIT = 0;
double max_r = 10.0/0.7;
double DIST_BPDEX = 0, DIST_INV_BPDEX = 0;
double min_z = MIN_Z;
double max_z = 0;
double min_sm = 10.0;
double max_sm = 10.5;
double min_sm2 = 10.0;
double max_sm2 = 10.5;
double max_z_sm_mode = 0;
int64_t num_randoms = 1e6;
double rr[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1];
double rr2[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1];
double APP_MAG_LIMIT = 17.77;
//double APP_MAG_LIMIT = 19.1;
#define ZMAX_BINS 1000
double mass_step = 0;
double zmax[ZMAX_BINS]={0};
double inv_vmax[ZMAX_BINS]={0};
double maxdist3[ZMAX_BINS]={0};
#define h0 0.677


void switch_trees(void);
void obs_calc_cross_corr(double *output_corr);

#ifdef CORREL_MAIN
int main(int argc, char **argv)
{
  int64_t i;
  init_cosmology(0.307115, 0.692885, h0);
  if (argc < 7) {
    fprintf(stderr, "Usage: %s dr10_sm_sfr_complete.dat min_sm max_sm dr10_sm_sfr_complete2.dat min_sm max_sm [max_pi max_r min_z max_z num_randoms]\n",
	    argv[0]);
    exit(1);
  }

  if (argc > 2) min_sm = atof(argv[2]);
  if (argc > 3) max_sm = atof(argv[3]);

  if (argc > 5) min_sm2 = atof(argv[5]);
  if (argc > 6) max_sm2 = atof(argv[6]);
  
  if (argc > 7) max_pi = atof(argv[7]);
  if (argc > 8) max_r = atof(argv[8]);
  if (argc > 9) min_z = atof(argv[9]);
  if (argc > 11) num_randoms = atoll(argv[11]);
  
  max_z = max_complete_z((min_sm < min_sm2) ? min_sm : min_sm2);
  if (argc > 10) {
    if (atof(argv[10]) > max_z)
      printf("#Ignored max_z=%s, since it is beyond completeness limit of z=%f.\n", argv[10], max_z);
    else max_z = atof(argv[10]);
  }

  PI_BPUNIT = ((double)NUM_PI_BINS)/(2.0*max_pi);
  PI_INV_BPUNIT = 1.0/PI_BPUNIT;
  DIST_BPDEX = 5;
  DIST_INV_BPDEX = 1.0/DIST_BPDEX;
  max_r = pow(10, MIN_DIST+NUM_DIST_BINS*DIST_INV_BPDEX);

  r250_init(87L);
  load_regions("auto_regions.txt", 0);
  double tmp1 = min_sm;
  double tmp2 = max_sm;
  min_sm = min_sm2;
  max_sm = max_sm2;
  load_hosts(argv[4]);
  switch_trees();
  min_sm = tmp1;
  max_sm = tmp2;
  load_hosts(argv[1]);
  printf("#Area w/in r: %f deg^2\n", area_within_r());
  printf("#Volume: %f Mpc^3\n", area_within_r()*(Vc(max_z)-Vc(min_z))/(4.0*180.0*180.0/M_PI));
  obs_calc_cross_corr(NULL);
  return 0;
}
#endif /*CORREL_MAIN*/


#define SPEED_OF_LIGHT ((double)299792.458) //km/s

#define FAST3TREE_DIM 3
#define FAST3TREE_TYPE struct correl_point
#include "../fast3tree.c"


struct correl_galaxy *hg = NULL;
int64_t num_hg = 0;
struct correl_point *cpoints = NULL;
int64_t num_cpoints = 0;
struct correl_galaxy *hg2 = NULL;
int64_t num_hg2 = 0;
struct correl_point *cpoints2 = NULL;
int64_t num_cpoints2 = 0;



struct correl_point *rpoints = NULL;

struct div_bounds *dbounds = NULL;
int64_t num_dbounds = 0;

struct fast3tree *tree = NULL;
struct fast3tree *tree2 = NULL;
struct fast3tree *rtree = NULL;

int64_t is_complete(float sm, float z) {
  if (z <= 0) return 1;
  float abs_lum = (sm > 0) ? -0.2 - 1.9*sm : sm;
  float ld = Dl(z);
  float rel_lim = abs_lum + log10(ld/10e-6)*5.0;
  if (rel_lim > APP_MAG_LIMIT) return 0;
  return 1;
}

double max_complete_z(float sm) {
  double z1 = 0.0001;
  double z2 = 1.0;
  double z = 0.5*(z1+z2);
  while (z-z1 > 0.00001) {
    if (is_complete(sm, z)) z1 = z;
    else z2 = z;
    z = 0.5*(z1+z2);
  }
  return z;
}

void hg_to_cpoints(void) {
  struct correl_point c;
  memset(&c, 0, sizeof(struct correl_point));
  int64_t i;
  num_cpoints = 0;
  for (i=0; i<num_hg; i++) {
    c.hg = hg+i;
    if (!within_r(c.hg->ra, c.hg->dec)) continue;
    if (c.hg->z < min_z) continue;
    if (c.hg->z > max_z) continue;
    if (min_sm > 0) {
      if (c.hg->sm < min_sm) continue;
      if (c.hg->sm > max_sm) continue;
    } else {
      if (c.hg->amag > min_sm) continue;
      if (c.hg->amag < max_sm) continue;
    }
    if (max_z_sm_mode) {
      double sm = hg[i].sm;
      int64_t smb = (sm-min_sm)/mass_step;
      if (hg[i].z > zmax[smb]) continue;
      c.inv_v = inv_vmax[smb];
    }
    check_realloc_every(cpoints, sizeof(struct correl_point), num_cpoints, 1000);
    calc_pos3d(&c, Dc(c.hg->z));
    cpoints[num_cpoints] = c;
    num_cpoints++;
  }
  if (!tree) tree = fast3tree_init(num_cpoints, cpoints);
  else fast3tree_rebuild(tree, num_cpoints, cpoints);
}


void switch_trees(void) {
  num_cpoints2 = num_cpoints;
  num_cpoints = 0;
  if (cpoints2 && cpoints2!=cpoints) free(cpoints2);
  cpoints2 = cpoints;
  cpoints = NULL;
  if (tree2 && tree2 != tree) fast3tree_free(&tree2);
  tree2 = tree;
  tree = NULL;
  hg2 = hg;
  hg = NULL;
  num_hg2 = num_hg;
  num_hg = 0;
}


void load_hosts(char *filename) {
  int64_t eligible2;
  FILE *input = check_fopen(filename, "r");
  char buffer[1024];
  struct correl_galaxy h = {0};
  h.bptclassp = -1;
  //float cosmo_correction = 0; //log10(0.7/0.68)*2;
  while (fgets(buffer, 1024, input)) {
    int sm_complete = 0;
    if (sscanf(buffer, "%lf %lf %lf %d %lf %lf %lf %lf %lf %lf %d %d %d %"SCNd64" %lf %lf %lf %lf %lf %lf", &(h.ra), &(h.dec), &(h.z), &(h.bptclass), &(h.sm), &(h.sfr), &(h.specsfr), &(h.sm_fib), &(h.sfr_fib), &(h.specsfr_fib), &(h.within_r), &(h.eligible), &sm_complete, &eligible2, h.d4000, h.d4000+1, h.d4000+2, h.d4000+3, &h.pmag, &h.amag) < 18) continue;
    check_realloc_every(hg,sizeof(struct correl_galaxy),num_hg,1000);
    hg[num_hg] = h;
    num_hg++;
  }
  hg_to_cpoints();
}

int64_t set_hg(struct correl_galaxy *hgals, int64_t ngals) {
  hg = hgals;
  num_hg = ngals;
  hg_to_cpoints();
  return num_cpoints;
}

int64_t set_hg2(struct correl_galaxy *hgals, int64_t ngals) {
  hg = hgals;
  num_hg = ngals;
  hg_to_cpoints();
  num_cpoints2 = num_cpoints;
  cpoints2 = cpoints;
  tree2 = tree;
  hg2 = hg;
  num_hg2 = num_hg;
  return num_cpoints;
}

double calc_proj_angle(double ra1, double dec1, double ra2, double dec2) {
  ra1 /= 180.0/M_PI;
  ra2 /= 180.0/M_PI;
  dec1 /= 180.0/M_PI;
  dec2 /= 180.0/M_PI;
  double s1 = sin(0.5*(dec2-dec1));
  double s2 = sin(0.5*(ra2-ra1));
  return 2.0*asin(sqrt(s1*s1 + cos(dec1)*cos(dec2)*s2*s2));
}

void calc_pos3d(struct correl_point *c, double r) {
  c->r = r;
  c->pos[2] = sin(c->hg->dec * M_PI / 180.0)*r;
  r *= cos(c->hg->dec * M_PI / 180.0);
  c->pos[0] = cos(c->hg->ra * M_PI/180.0)*r;
  c->pos[1] = sin(c->hg->ra * M_PI/180.0)*r;
}

int64_t rad_partition(struct correl_point *c, int64_t left, int64_t right, int64_t pivot_ind, int64_t div) {
  struct correl_point pivot = c[pivot_ind], tmp;
  int64_t si, i;
#define SWAP(a,b) { tmp = c[a]; c[a] = c[b]; c[b] = tmp; }
  SWAP(pivot_ind, right-1);
  si = right-2;
  for (i = left; i<si; i++) {
    if (c[i].pos[div] > pivot.pos[div]) { SWAP(i, si); si--; i--; }
  }
  if (c[si].pos[div] < pivot.pos[div]) si++;
  SWAP(right-1, si);
  return si;
#undef SWAP
}

inline float random_unit(void) {
  return(((float)(rand()%(RAND_MAX))/(float)(RAND_MAX)));
}

int64_t sort_median(struct correl_point *c, int64_t num_p, int64_t div) {
  int64_t pivot_index, k = num_p * 0.5;
  int64_t left = 0, right = num_p;
  assert(num_p>0);
  if (num_p < 2) return 0;
  while (1) {
    pivot_index = rad_partition(c, left, right, 
                                left + random_unit()*(right-left), div);
    if (k == pivot_index || (c[left].pos[div]==c[right-1].pos[div])) return k;
    if (k < pivot_index) right = pivot_index;
    else left = pivot_index+1;
  }
  return right-1;
}


void calc_div_bounds(int64_t level, struct div_bounds region, struct correl_point *points, int64_t num_points) {
  double lengths[3] = {0};
  int64_t i, j;

  if (!level) {
    check_realloc_s(dbounds, sizeof(struct div_bounds), TOTAL_DIV);
    num_dbounds = 0;
    if (num_points < 1) return;
    for (j=0; j<6; j++) region.bounds[j] = points[0].pos[j%3];
    for (i=1; i<num_points; i++) {
      for (j=0; j<3; j++) {
	if (region.bounds[j]>points[i].pos[j]) region.bounds[j] = points[i].pos[j];
	if (region.bounds[j+3]<points[i].pos[j]) region.bounds[j+3] = points[i].pos[j];
      }
    }
    for (i=0; i<num_cpoints; i++) {
      for (j=0; j<3; j++) {
	if (region.bounds[j]>cpoints[i].pos[j]) region.bounds[j] = cpoints[i].pos[j];
	if (region.bounds[j+3]<cpoints[i].pos[j]) region.bounds[j+3] = cpoints[i].pos[j];
      }
    }
  }

  if (level==DIV_LOG2) {
    dbounds[num_dbounds] = region;
    num_dbounds++;
    return;
  }

  for (i=0; i<3; i++) lengths[i] = region.bounds[i+3]-region.bounds[i];
  int64_t max_div = 0;
  for (i=1; i<3; i++) if (lengths[i] > lengths[max_div]) max_div = i;
  int64_t left = sort_median(points, num_points, max_div);
  struct div_bounds region2 = region;
  region.bounds[max_div+3]=points[left].pos[max_div];
  calc_div_bounds(level+1, region, points, left);
  region2.bounds[max_div]=points[left].pos[max_div];
  calc_div_bounds(level+1, region2, points+left, num_points-left);
}


void calc_divs(struct correl_point *c, int64_t num_c) {
  int64_t i, j, k;
  for (i=0; i<num_c; i++) {
    for (j=0; j<num_dbounds; j++) {
      for (k=0; k<3; k++) {
	if (c[i].pos[k] < dbounds[j].bounds[k] || c[i].pos[k] > dbounds[j].bounds[k+3]) break;
      }
      if (k==3) break;
    }
    if (j==num_dbounds) {
      printf("%f %f %f\n", c[i].pos[0], c[i].pos[1], c[i].pos[2]);
      for (j=0; j<num_dbounds; j++) {
	printf("%f %f %f - %f %f %f\n", dbounds[j].bounds[0], dbounds[j].bounds[1], dbounds[j].bounds[2], dbounds[j].bounds[3], dbounds[j].bounds[4], dbounds[j].bounds[5]);
      }
      assert(j<num_dbounds);
    }
    c[i].div = j;
  }
}

void gen_randoms(void) {
  int64_t i,j;
  struct correl_galaxy hg = {0};
  check_realloc_s(rpoints, sizeof(struct correl_point), num_randoms);
  memset(rpoints, 0, sizeof(struct correl_point)*num_randoms);
  //double min_dist = Dc(min_z);
  //double max_dist = Dc(max_z);
  double min_dist3 = pow(Dc(min_z), 3);
  double max_dist3 = pow(Dc(max_z), 3);
  //double min_dist3l = pow(Dc(min_z-0.003), 3);
  //double max_dist3h = pow(Dc(max_z+0.003), 3);
  for (i=0; i<num_randoms; i++) {
    rand_within_r(&hg.ra, &hg.dec);
    if (max_z_sm_mode) {
      double sm = cpoints[(int64_t)(dr250()*num_cpoints)].hg->sm;
      int64_t smb = (sm-min_sm)/mass_step;
      max_dist3 = maxdist3[smb];
      rpoints[i].inv_v = inv_vmax[smb];
    }
    double r = cbrt(min_dist3 + dr250()*(max_dist3-min_dist3));
    //double r = cbrt(min_dist3l + dr250()*(max_dist3h-min_dist3l));
    //    r += normal_random(0,290) / (100.0*h0);
    //if (r < min_dist || r > max_dist) { i--; continue; }
    rpoints[i].hg = &hg;
    calc_pos3d(rpoints+i, r);
  }
  struct div_bounds region;
  memset(&region, 0, sizeof(struct div_bounds));
  calc_div_bounds(0, region, rpoints, num_randoms);
  calc_divs(rpoints, num_randoms);
  calc_divs(cpoints, num_cpoints);
  calc_divs(cpoints2, num_cpoints2);
  rtree = fast3tree_init(num_randoms, rpoints);

  FILE *out = check_fopen("data_randoms.dat", "w");
  fprintf(out, "#X Y Z R RA DEC Z DIV Data?\n");
  for (i=0; i<num_cpoints; i++) {
    fprintf(out, "%f %f %f %f %f %f %f %"PRId64" 1\n", cpoints[i].pos[0], cpoints[i].pos[1], cpoints[i].pos[2], cpoints[i].r, cpoints[i].hg->ra, cpoints[i].hg->dec, cpoints[i].hg->z, cpoints[i].div);
  }
  for (i=0; i<num_randoms; i++) {
    double pos[3];
    for (j=0; j<3; j++) pos[j] = rpoints[i].pos[j]/rpoints[i].r;
    double dec = asin(pos[2])*180.0/M_PI;
    double ra = atan2(pos[1], pos[0])*180.0/M_PI;
    if (ra < 0) ra += 360.0;
    double z = comoving_distance_to_redshift(rpoints[i].r);
    fprintf(out, "%f %f %f %f %f %f %f %"PRId64" 0\n", rpoints[i].pos[0], rpoints[i].pos[1], rpoints[i].pos[2], rpoints[i].r, ra, dec, z, rpoints[i].div);
  }
  fclose(out);
}

void print_corr_header(FILE *out) {
  fprintf(out, "#rp wp(rp) (Err)\n"); 
  fprintf(out, "#min_z: %f\n", min_z);
  fprintf(out, "#max_z: %f\n", max_z);
  if (min_sm > 0) {
    fprintf(out, "#min_sm: %f\n", min_sm);
    fprintf(out, "#max_sm: %f\n", max_sm);
    fprintf(out, "#min_sm2: %f\n", min_sm2);
    fprintf(out, "#max_sm2: %f\n", max_sm2);
  } else {
    fprintf(out, "#min_mag: %f\n", min_sm);
    fprintf(out, "#max_mag: %f\n", max_sm);
    fprintf(out, "#min_mag2: %f\n", min_sm2);
    fprintf(out, "#max_mag2: %f\n", max_sm2);
  }
  fprintf(out, "#max_pi: %f\n", max_pi);
  fprintf(out, "#max_r: %f\n", max_r);
  fprintf(out, "#num_randoms: %"PRId64"\n", num_randoms);
}

/*double _calc_log_dist(struct correl_point *c1, struct correl_point *c2, double *pi) {
  double dot = 0;
  double l = 0;
  int64_t i;
  for (i=0; i<3; i++) { dot += c1->pos[i]*c2->pos[i]; }
  *pi = dot/c1->r - c1->r;
  if (fabs(*pi) > max_pi) return -1000;
  dot /= (c1->r*c1->r);
  for (i=0; i<3; i++) { double dx = c2->pos[i] - dot*c1->pos[i]; l+=dx*dx; }
  if (l <= 0) return -1000;
  if (l > max_r*max_r) return -1000;
  return 0.5*log10(l);
  }*/

double _calc_log_dist(struct correl_point *c1, struct correl_point *c2, double *pi) {
  double dot=0, l=0, s[3], r[3];
  int64_t i;
  for (i=0; i<3; i++) {
    s[i] = c2->pos[i] - c1->pos[i];
    r[i] = 0.5*(c1->pos[i]+c2->pos[i]);
    dot += s[i]*r[i];
    l += r[i]*r[i];
    }

  /*for (i=0; i<3; i++) {
    s[i] = c2->pos[i] - c1->pos[i];
    r[i] = c2->pos[i];
    dot += s[i]*r[i];
    l += r[i]*r[i];
    }*/

  *pi = dot / sqrt(l);
  if (fabs(*pi) > max_pi) return -1000;
  for (l=-(*pi)*(*pi),i=0; i<3; i++) l += s[i]*s[i];
  if (l <= 0) return -1000;
  if (l > max_r*max_r) return -1000;
  return 0.5*log10(l);
}

void _count_pairs(struct fast3tree *t1, struct fast3tree *t2, double pairs[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1], double dens, int64_t correct_fiber_coll) {
  int64_t i, j, k, div, div_bin;
  double counts[TOTAL_DIV+1];
  struct fast3tree_results *res = fast3tree_results_init();
  for (div=0; div<TOTAL_DIV+1; div++) {
    counts[div] = 0;
    for (j=0; j<NUM_DIST_BINS; j++) for (k=0; k<NUM_PI_BINS; k++) pairs[j][k][div] = 0;
  }
  
  double dist = sqrt(max_r*max_r+max_pi*max_pi);
  for (i=0; i<t1->num_points; i++) {
    fast3tree_find_sphere(t2, res, t1->root->points[i].pos, dist);
    double tcounts[NUM_DIST_BINS][NUM_PI_BINS] = {{0}};
    double w = 1;
    for (j=0; j<res->num_points; j++) {
      double pi=0;
      double ld = _calc_log_dist(t1->root->points+i, res->points[j], &pi);
      int64_t bin = (ld-MIN_DIST)*DIST_BPDEX;
      if (bin < 0 || bin >= NUM_DIST_BINS) continue;
      int64_t pbin = (pi+max_pi)*PI_BPUNIT;
      if (pbin < 0 || pbin >= NUM_PI_BINS) continue;
      if (max_z_sm_mode) w = t1->root->points[i].inv_v * res->points[j]->inv_v;
      if (correct_fiber_coll) {
	if (calc_proj_angle(t1->root->points[i].hg->ra, t1->root->points[i].hg->dec, res->points[j]->hg->ra, res->points[j]->hg->dec) < 0.000266648) {
	  w += 1.04;
	  tcounts[bin][pbin]+=2.08;
	}
      }
      tcounts[bin][pbin]++;
    }

    for (div = 0; div < TOTAL_DIV; div++) {
      div_bin = (div != t1->root->points[i].div) ? div : TOTAL_DIV;
      if (!max_z_sm_mode)
	counts[div_bin]+=w;
      else
	counts[div_bin]+=t1->root->points[i].inv_v*w;
    }
    for (j=0; j<NUM_DIST_BINS; j++) {
      for (k=0; k<NUM_PI_BINS; k++) {
	if (!tcounts[j][k]) continue;
	for (div = 0; div < TOTAL_DIV; div++) {
	  div_bin = (div != t1->root->points[i].div) ? div : TOTAL_DIV;
	  pairs[j][k][div_bin]+=tcounts[j][k]*w;
	}
      }
    }
  }

  if (max_z_sm_mode) {
    dens = 0;
    for (i=0; i<t2->root->num_points; i++) dens += t2->root->points[i].inv_v;
  }

  for (div = 0; div < TOTAL_DIV+1; div++) {
    if (!counts[div]) continue;
    for (j=0; j<NUM_DIST_BINS; j++)
      for (k=0; k<NUM_PI_BINS; k++)
	pairs[j][k][div] /= dens*((double)counts[div]);
  }
  fast3tree_results_free(res);
}


void _count_random_pairs(struct fast3tree *t, double pairs[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1]) {
  int64_t i, j, k, l, div, div_bin;
  double counts[TOTAL_DIV+1];
  double r2[NUM_DIST_BINS+1];
  for (div=0; div<TOTAL_DIV+1; div++) {
    counts[div] = 0;
    for (j=0; j<NUM_DIST_BINS; j++) for (k=0; k<NUM_PI_BINS; k++) pairs[j][k][div] = 0;
  }
  
  for (i=0; i<NUM_DIST_BINS+1; i++)
    r2[i] = pow(10, 2.0*(MIN_DIST+i*DIST_INV_BPDEX));

  for (i=0; i<t->num_points; i+=10) {
    if (i>0 && !(i%10000)) {
      fprintf(stderr, "#Counted %"PRId64"k RR\n", i/1000);
    }
    double tcounts[NUM_DIST_BINS][NUM_PI_BINS] = {{0}};
    double z[3], x[3], y[3], dot=0;
    for (j=0; j<3; j++) z[j] = t->points[i].pos[j]/t->points[i].r;
    if ((z[0] != z[1]) || (z[1] != z[2])) { x[0] = z[1]; x[1] = z[2]; x[2] = z[0]; }
    else { x[0] = -z[0]; x[1] = z[1]; x[2] = z[2]; }
    for (j=0; j<3; j++) dot += x[j]*z[j];
    assert(dot < 1);
    for (j=0; j<3; j++) x[j] -= dot*z[j];
    for (j=0,dot=0; j<3; j++) dot += x[j]*x[j];
    dot = 1.0/sqrt(dot);
    for (j=0; j<3; j++) x[j] *= dot;
    y[0] = x[1]*z[2] - x[2]*z[1];
    y[1] = x[2]*z[0] - x[0]*z[2];
    y[2] = x[0]*z[1] - x[1]*z[0];

    for (j=0; j<NUM_DIST_BINS; j++) {
      double rmin2 = r2[j];
      double rmax2 = r2[j+1];
      for (k=0; k<NUM_PI_BINS; k++) {
	double pmin = -max_pi + k*PI_INV_BPUNIT;
	double pmax = -max_pi + (k+1)*PI_INV_BPUNIT;
	double rand_angle = dr250()*2.0*M_PI;
	double rand_r2 = rmin2 + dr250()*(rmax2-rmin2);
	double rand_r = sqrt(rand_r2);
	double rand_pi = pmin + dr250()*(pmax-pmin);
	double rand_x = cos(rand_angle)*rand_r;
	double rand_y = sin(rand_angle)*rand_r;
	double r=0;
	double pos[3];
	for (l=0; l<3; l++) pos[l] = t->points[i].pos[l] + rand_pi*z[l] + rand_x*x[l] + rand_y*y[l];
	//Compute RA, DEC, Z:
	for (l=0; l<3; l++) r += pos[l]*pos[l];
	r = sqrt(r);
	double redshift = comoving_distance_to_redshift(r);
	if (redshift < min_z || redshift > max_z) continue;
	for (l=0; l<3; l++) pos[l] /= r;
	double dec = asin(pos[2])*180.0/M_PI;
	double ra = atan2(pos[1],pos[0])*180.0/M_PI;
	if (ra < 0) ra += 360.0;
	if (!within_r(ra, dec)) continue;
	tcounts[j][k]++;
      }
    }

    for (div = 0; div < TOTAL_DIV; div++) {
      div_bin = (div != t->root->points[i].div) ? div : TOTAL_DIV;
      counts[div_bin]++;
    }
    for (j=0; j<NUM_DIST_BINS; j++) {
      for (k=0; k<NUM_PI_BINS; k++) {
	if (!tcounts[j][k]) continue;
	for (div = 0; div < TOTAL_DIV; div++) {
	  div_bin = (div != t->root->points[i].div) ? div : TOTAL_DIV;
	  pairs[j][k][div_bin]+=tcounts[j][k];
	}
      }
    }
  }

  for (div = 0; div < TOTAL_DIV+1; div++) {
    if (!counts[div]) continue;
    for (j=0; j<NUM_DIST_BINS; j++) {
      double area = M_PI*(r2[j+1]-r2[j]);
      double height = PI_INV_BPUNIT;
      double nexp = area*height; //*dens / dens
      for (k=0; k<NUM_PI_BINS; k++)
	pairs[j][k][div] *= nexp/(double)counts[div];
    }
  }
}


void obs_calc_cross_corr(double *output_corr) {
  double dd[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1], corr[NUM_DIST_BINS][TOTAL_DIV+1];
  double dr[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1];
  double dr2[NUM_DIST_BINS][NUM_PI_BINS][TOTAL_DIV+1];
  int64_t j,k,div;

  struct fast3tree *t1 = tree, *t2=tree2;
  if (!output_corr) {
    print_corr_header(stdout);
    fprintf(stdout, "#num_objects(t1): %"PRId64"\n", t1->num_points);
    fprintf(stdout, "#num_objects(t2): %"PRId64"\n", t2->num_points);
    printf("#Generating randoms...\n");
    gen_randoms();
  }
  double area = area_within_r()/(4.0*180.0*180.0/M_PI);
  //double dens1 = t1->num_points / (area*(Vc(max_z) - Vc(min_z)));
  double dens2 = t2->num_points / (area*(Vc(max_z) - Vc(min_z)));
  double dens_r = num_randoms / (area*(Vc(max_z) - Vc(min_z)));

  for (j=0; j<NUM_DIST_BINS; j++) {
    for (div=0; div<TOTAL_DIV+1; div++) {
      for (k=0; k<NUM_PI_BINS; k++) dd[j][k][div] = dr[j][k][div] = dr2[j][k][div] = 0;
      corr[j][div] = 0;
    }
  }

  if (!output_corr) printf("#Counting D1D2...\n");
  _count_pairs(t1,t2,dd,dens2,1);
  if (!output_corr) printf("#Counting D1R...\n");
  _count_pairs(t1,rtree,dr,dens_r,0);
  if (!output_corr) printf("#Counting D2R...\n");
  _count_pairs(t2,rtree,dr2,dens_r,0);
  if (!output_corr) {
    for (j=0; j<NUM_DIST_BINS; j++) {
      for (div=0; div<TOTAL_DIV+1; div++) {
	 for (k=0; k<NUM_PI_BINS; k++) rr[j][k][div] = rr2[j][k][div] = 0;
      }
    }
    printf("#Counting RR...\n");
    if (!max_z_sm_mode)
      _count_random_pairs(rtree,rr);
    else
      _count_pairs(rtree,rtree,rr,dens_r,0);
    printf("#Done.\n");
  }

  if (output_corr) memset(output_corr, 0, sizeof(double)*NUM_DIST_BINS);

  for (j=0; j<NUM_DIST_BINS; j++) {
    for (div=0; div<TOTAL_DIV+1; div++) {
      for (k=0; k<NUM_PI_BINS; k++) {
	if (rr[j][k][div]>0)
	  corr[j][div] += PI_INV_BPUNIT*(dd[j][k][div]-dr[j][k][div]-dr2[j][k][div]+rr[j][k][div])/rr[j][k][div];
	//if (div==TOTAL_DIV) printf("%f %f: %f %f\n", pow(10, MIN_DIST + (j+0.5)*DIST_INV_BPDEX), -max_pi+(k+0.5)*PI_INV_BPUNIT,
	//rr[j][k][div], rr2[j][k][div]);
      }
    }
    double var = 0;
    for (div=0; div<TOTAL_DIV; div++) {
      double dx = corr[j][div]-corr[j][TOTAL_DIV];
      var += dx*dx;
    }
    var *= (double)(TOTAL_DIV - 1)/(double)(TOTAL_DIV);

    double v1 = M_PI * pow(10, 2*(MIN_DIST + j*DIST_INV_BPDEX));
    double v2 = M_PI * pow(10, 2*(MIN_DIST + (j+1)*DIST_INV_BPDEX));
    double ravg = sqrt((v1+v2)/(2.0*M_PI));

    if (corr[j][TOTAL_DIV] > 0) {
      if (output_corr) output_corr[j] = corr[j][TOTAL_DIV]*h0;
      else fprintf(stdout, "%f %f %f %f\n", ravg/1.0, corr[j][TOTAL_DIV]/1.0, sqrt(var)/1.0, pow(10, MIN_DIST + (j+0.5)*DIST_INV_BPDEX));
    }
  }

  if (!output_corr) fprintf(stdout, "\n");
  //fclose(out);
}


void set_sm_limits(double mi_sm, double ma_sm) {
  min_sm = mi_sm;
  max_sm = ma_sm;
}

double cross_correl_library_init(double mi_sm, double ma_sm, double ma_pi, double ma_r, double mi_z, double ma_z, int64_t nr, char *regions_file, double **output_corr) {
  int64_t j,k,div;
  init_cosmology(0.307115, 0.692885, 0.677);
  min_sm = mi_sm;
  max_sm = ma_sm;
  max_pi = ma_pi;
  max_r = ma_r;
  min_z = mi_z;
  num_randoms = nr;
  max_z = ma_z;

  PI_BPUNIT = ((double)NUM_PI_BINS)/(2.0*max_pi);
  PI_INV_BPUNIT = 1.0/PI_BPUNIT;

  //DIST_BPDEX = ((double)NUM_DIST_BINS)/(log10(max_r)-((double)MIN_DIST));
  DIST_BPDEX = 5;
  DIST_INV_BPDEX = 1.0/DIST_BPDEX;
  max_r = pow(10, MIN_DIST+NUM_DIST_BINS*DIST_INV_BPDEX);

  r250_init(87L);
  load_regions(regions_file, 0);
  printf("#Area w/in r: %f deg^2\n", area_within_r());
  double area = area_within_r()/(4.0*180.0*180.0/M_PI);
  printf("#Volume: %f Mpc^3\n", area*(Vc(max_z)-Vc(min_z)));
  gen_randoms();
  for (j=0; j<NUM_DIST_BINS; j++) {
    for (div=0; div<TOTAL_DIV+1; div++) {
      for (k=0; k<NUM_PI_BINS; k++) rr[j][k][div] = rr2[j][k][div] = 0;
    }
  }
  _count_random_pairs(rtree,rr);

  check_realloc_s(*output_corr, sizeof(double), NUM_DIST_BINS);
  return max_z;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"


int main(int argc, char **argv) {
  int64_t i, j, k;
  char buffer[2048];
  
  if (argc < 3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg  /paths/to/model_outputs ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  
  do_config(argv[1]);
  if (!POSTPROCESSING_MODE) {
    fprintf(stderr, "[Error] POSTPROCESSING_MODE must be enabled to extract smhm fits.\n");
    exit(EXIT_FAILURE);
  }

  struct mcmc_data *m = load_mcmc_data(argv[2]);

  
  int64_t num_smhm_params = 0;
  FILE **smhm_param_files = NULL;
  int64_t *smhm_param_offsets = NULL;
#define shared(x,y) check_realloc_s(smhm_param_files, sizeof(FILE *), num_smhm_params+1); \
  check_realloc_s(smhm_param_offsets, sizeof(FILE *), num_smhm_params+1); \
  snprintf(buffer, 2048, "%s/post_%s.txt", OUTBASE, #x); \
  smhm_param_files[num_smhm_params] = check_fopen(buffer, "w"); \
  smhm_param_offsets[num_smhm_params] = m->x - m->shared_data; \
  num_smhm_params++;
  #include "smhm_params.template.h"
#undef shared
  
  for (i=2; i<argc; i++) {
    struct mcmc_data *m = load_mcmc_data(argv[i]);
    double overall_chi2 = m->model->params[NUM_PARAMS+1];
    for (j=0; j<num_smhm_params; j++) {
      if (i==2) fprintf(smhm_param_files[j], "#SMHM_Params SMHM_Fit_Chi2 Overall_Obs_Data_Chi2\n");
      for (k=0; k<SMHM_FIT_PARAMS+1; k++) {
	fprintf(smhm_param_files[j], "%f ", m->shared_data[smhm_param_offsets[j]+k]);
      }
      fprintf(smhm_param_files[j], "%f\n", overall_chi2);
    }
  }

  for (j=0; j<num_smhm_params; j++)
    fclose(smhm_param_files[j]);
  
  return 0;
}

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mf_cache.h"

struct z_mf_cache *all_mf_caches = NULL;

inline double _mf_cache(struct mf_cache *cache, double mass) {
  int i;
  double m, r;
  if (mass >= cache->mass_max) {
    return -1000;
  }
  else {
    m = (mass - cache->mass_min)*cache->inv_mass_spacing;
    if (m < 0) r=((1.0-cache->alpha)*(mass-cache->mass_min) + cache->mf[0]);
    else {
      i = m;
      r = (cache->mf[i] + (cache->mf[i+1]-cache->mf[i])*(m-i));
    }
  }
  return r;
}

inline double mf_cache(double scale, double mass) {
  int i;
  double c1, c2;
  if (scale >= all_mf_caches->scale_max)
    return _mf_cache(all_mf_caches->caches+all_mf_caches->scales-1, mass);
  if (scale < all_mf_caches->scale_min)
    return _mf_cache(all_mf_caches->caches, mass);

  //Find scale
  i = (scale - all_mf_caches->scale_min) * all_mf_caches->avg_inv_scale_spacing;
  while (all_mf_caches->caches[i+1].scale < scale) i++;
  while (all_mf_caches->caches[i].scale > scale) i--;
  c1 = _mf_cache(all_mf_caches->caches + i, mass);
  c2 = _mf_cache(all_mf_caches->caches + i + 1, mass);
  return (c1 + (c2 - c1)*(scale - all_mf_caches->caches[i].scale)*
	  all_mf_caches->caches[i].inv_scale_spacing);
}

float readfloat(FILE *input) {
  char buffer[50] = {0};
  float result;
  fgets(buffer, 50, input);
  sscanf(buffer, "%f", &result);
  return result;
}

void load_mf_cache(char *filename, int argc) {
  FILE *input;
  int i;
  float omega_m, omega_l, h0;
  struct mf_cache *mf;
  if (!(input = fopen(filename, "r"))) {
    printf("Couldn't open MF cache %s!\n", filename);
    exit(6);
  }

  all_mf_caches = (struct z_mf_cache *)malloc(sizeof(struct z_mf_cache));

  omega_m = all_mf_caches->omega_m = readfloat(input);
  omega_l = all_mf_caches->omega_l = readfloat(input);
  h0 = all_mf_caches->h0 = readfloat(input);

  if (argc < 2)
    printf("Cosmology assumed: Omega_M = %f; Omega_Lambda = %f; h = %f\n",
	   omega_m, omega_l, h0);

  all_mf_caches->scale_min = readfloat(input);
  all_mf_caches->scale_max = readfloat(input);
  all_mf_caches->scales = readfloat(input);
  all_mf_caches->avg_inv_scale_spacing = (float)(all_mf_caches->scales-1) / 
    (all_mf_caches->scale_max - all_mf_caches->scale_min);

  if (! (all_mf_caches->scale_min > 0 && all_mf_caches->scale_min < 1.05 &&
	 all_mf_caches->scale_max > all_mf_caches->scale_min &&
	 all_mf_caches->scale_max < 1.05)) {
    printf("Invalid scales in MF cache (%f - %f), expecting 0<a<1.\n",
	   all_mf_caches->scale_min, all_mf_caches->scale_max);
    exit(7);
  }

  all_mf_caches->caches = (struct mf_cache *)
    malloc(sizeof(struct mf_cache)*all_mf_caches->scales);
  for (i=0; i<all_mf_caches->scales; i++) {
    mf = &(all_mf_caches->caches[i]);
    mf->mass_min = readfloat(input);
    mf->mass_max = readfloat(input);
    mf->masses = readfloat(input);
    mf->scale = readfloat(input);
    mf->alpha = fabs(readfloat(input));
    if (! (mf->scale >= all_mf_caches->scale_min &&
	   mf->scale <= all_mf_caches->scale_max)) {
      printf("Invalid scale in MF cache index %d: %f\n", i, mf->scale);
      exit(7);
    }
    if (! (mf->alpha > 1 && mf->alpha < 3)) {
      printf("Invalid |alpha| in MF cache: %f (Expected 1 - 3)\n", mf->alpha);
      exit(7);
    }

    mf->mf = (float *)malloc(sizeof(float)*mf->masses);
    fread(mf->mf, sizeof(float), mf->masses, input);
    mf->inv_mass_spacing = (float)(mf->masses-1)/(mf->mass_max - mf->mass_min);
    mf->inv_m0 = log10(((mf->mass_max - mf->mass_min)*(1.0-mf->alpha)
			+ mf->mf[0] - mf->mf[mf->masses-1])/pow(10, mf->mass_max));
  }
  for (i=0; i<all_mf_caches->scales-1; i++)
    all_mf_caches->caches[i].inv_scale_spacing = 
      1.0 / (all_mf_caches->caches[i+1].scale - all_mf_caches->caches[i].scale);
  fclose(input);
}

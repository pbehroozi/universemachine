#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <inttypes.h>
#include "distance.h"
#include "check_syscalls.h"
#include "lightcone_cache.h"
#include "stringparse.h"
#include "config_vars.h"

struct catalog_halo *halos = NULL;
int64_t num_halos = 0;

void wrap_halos(void) {
  int64_t i,j;
  for (i=0; i<num_halos; i++) {
    for (j=0; j<3; j++) {
      while (halos[i].pos[j] < 0) halos[i].pos[j] += BOX_SIZE;
      while (halos[i].pos[j] > BOX_SIZE) halos[i].pos[j] -= BOX_SIZE;
    }
  }
}


int64_t halo_pos_to_index(const void *h, int64_t *tx, int64_t *ty, int64_t *tz) {
  const struct catalog_halo *th = h;
  int64_t x = th->pos[0]*(float)CACHE_SIZE/(float)BOX_SIZE;
  int64_t y = th->pos[1]*(float)CACHE_SIZE/(float)BOX_SIZE;
  int64_t z = th->pos[2]*(float)CACHE_SIZE/(float)BOX_SIZE;
  if (x==CACHE_SIZE) x--;
  if (y==CACHE_SIZE) y--;
  if (z==CACHE_SIZE) z--;
  if (tx) *tx=x;
  if (ty) *ty=y;
  if (tz) *tz=z;
  return (((x*CACHE_SIZE)+y)*CACHE_SIZE+z);
}

int sort_halo_by_loc(const void *a, const void *b) {
  int i1 = halo_pos_to_index(a, NULL, NULL, NULL);
  int i2 = halo_pos_to_index(b, NULL, NULL, NULL);
  return (i1-i2);
}

void create_halo_caches(struct halo_cat *head) {
  struct halo_cat *cat = head;
  int64_t i, index, last_index=-1, x, y, z;
  int64_t max_i = cat->first_halo_index + cat->num_halos;
  qsort(halos+cat->first_halo_index, cat->num_halos, sizeof(struct catalog_halo), sort_halo_by_loc);
  for (i=cat->first_halo_index; i<max_i; i++) {
    index = halo_pos_to_index(halos+i, &x, &y, &z);
    if (index != last_index) cat->loc[x][y][z] = halos+i;
    last_index = index;
    cat->ncache[x][y][z]++;
  }
}

void clear_halos(void) {
  num_halos = 0;
  free(halos);
  halos = NULL;
}

void load_cat_data(struct halo_cat *cat) {
  char buffer[1024];
  struct stat file_stat;
  snprintf(buffer, 1024, "%s/sfr_catalog_%f.bin", OUTBASE, cat->scale);
  FILE *input = check_fopen(buffer, "rb");
  check_fstat(fileno(input), &file_stat, buffer);

  if (file_stat.st_size % sizeof(struct catalog_halo)) {
    fprintf(stderr, "[Error] Size of file %s not evenly divisible by halo structure size (%"PRId64")\n",
	    buffer, (int64_t)sizeof(struct catalog_halo));
    exit(EXIT_FAILURE);
  }
  cat->num_halos = file_stat.st_size /  sizeof(struct catalog_halo);
  
  check_realloc_s(halos, sizeof(struct catalog_halo), (num_halos+cat->num_halos+1));
  check_fread(&(halos[num_halos]), sizeof(struct catalog_halo), cat->num_halos, input);
  fclose(input);

  num_halos += cat->num_halos;
  wrap_halos();
  create_halo_caches(cat);
}

int sort_scales(const void *a, const void *b) {
  const float *c = a;
  const float *d = b;
  if (*d < *c) return -1;
  if (*c < *d) return 1;
  return 0;
}

void load_halos(struct halo_cat **head) {
  char buffer[1024];
  int64_t snap, num_snaps=0;
  float scale, *scales = NULL;
  
  snprintf(buffer, 1024, "%s/snaps.txt", INBASE);
  FILE *input = check_fopen(buffer, "r");
  *head = NULL;
  while (fgets(buffer, 1024, input)) {
    if (sscanf(buffer, "%"SCNd64" %f", &snap, &scale) != 2) continue;
    check_realloc_every(scales, sizeof(float), num_snaps, 10);
    scales[num_snaps] = scale;
    num_snaps++;
  }
  fclose(input);

  qsort(scales, num_snaps, sizeof(float), sort_scales);
  check_calloc_s(*head, sizeof(struct halo_cat), num_snaps);
  for (snap=0; snap<num_snaps; snap++) {
    head[0][snap].first_halo_index = num_halos;
    head[0][snap].scale = scales[snap];
    head[0][snap].next = head[0]+snap+1;
    head[0][snap].num_snaps = num_snaps;
  }
  head[0][snap-1].next = NULL;
  free(scales);
}

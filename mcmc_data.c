#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "config_vars.h"
#include "mcmc_data.h"
#include "check_syscalls.h"


int64_t get_mcmc_data_sizes(struct mcmc_data *md) {
  int64_t offset = sizeof(struct sf_model_allz);
  memcpy(&md->num_scales, md->all+offset, sizeof(int64_t));
  offset += sizeof(int64_t);  
  memcpy(&md->num_sd, md->all+offset, sizeof(int64_t));
  offset += sizeof(int64_t);
  memcpy(&md->num_obs, md->all+offset, sizeof(int64_t));
  offset += sizeof(int64_t);
  md->num_sd /= sizeof(float);
  md->total_length = offset + sizeof(float)*(md->num_scales+md->num_sd)
    + sizeof(struct observation)*md->num_obs;
  return offset;
}  

void _load_mcmc_data(struct mcmc_data *md, int64_t index) {
  int64_t offset = get_mcmc_data_sizes(md);
  md->index = index;
  offset += md->total_length * index;
  
  md->model = md->all + (md->total_length*index);
  md->scales = md->all+offset;
  offset += sizeof(float)*md->num_scales;
  md->shared_data = md->all+offset;
  offset += sizeof(float)*md->num_sd;
  md->obs = md->all+offset;
  offset += sizeof(struct observation)*md->num_obs;
  if (offset > md->file_length) {
    fprintf(stderr, "[Error] MCMC Datablob %"PRId64" in file %s extends beyond end of file (Expected size: %"PRId64" vs. actual: %"PRId64"\n", index, md->filename, offset, md->file_length);
    exit(EXIT_FAILURE);
  }
  
  int64_t num_scales = md->num_scales;
  offset = 0;
#define shared(a,b) { md->a = md->shared_data + offset; offset += (b); }
#include "shared.template.h"
  if (POSTPROCESSING_MODE) {
#include "pshared.template.h"
  }
#undef shared
}

struct mcmc_data *load_mcmc_data(char *filename) {
  struct mcmc_data *md = NULL;
  check_calloc_s(md, sizeof(struct mcmc_data), 1);
  md->all = check_mmap_file(filename, 'c', &md->file_length);
  md->filename = check_strdup(filename);
  _load_mcmc_data(md, 0);
  md->num_samples = md->file_length / md->total_length;
  return md;
}

void free_mcmc_data(struct mcmc_data *md) {
  munmap(md->all, md->file_length);
  check_free(md->filename);
  check_free(md);
}

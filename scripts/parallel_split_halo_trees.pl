#!/usr/bin/perl -w
use File::Basename;
use Cwd 'abs_path';
BEGIN {
    my $um_directory = abs_path(dirname(__FILE__)."/..");
    push @INC, "$um_directory/perl";
    our $split_halo_trees = "$um_directory/split_halo_trees_phase2";
    if (! -x $split_halo_trees) {
	print "Executable split_halo_trees_phase2 file not found at expected location ($split_halo_trees).\n";
	print "Make sure to run \"make treereg\" to generate.\n";
	exit;
    }
}
use MultiThread;

if (@ARGV < 8) {
    die "Usage: $0 box_size Om h path/to/scales.txt path/to/ranks path/to/boxlist num_parallel_tasks tree.dat...\n";
}

my @common_args = @ARGV[0..5];
my $par_tasks = $ARGV[6];
my @trees = @ARGV[7..$#ARGV];
my $trees_per_task = int(@trees / $par_tasks);
$trees_per_task++ if (@trees % $par_tasks);
my $tree_start = 0;

$MultiThread::threadlimit = $par_tasks;

my $i;
for ($i=0; $i<$par_tasks; $i++) {
    my $tree_end = $tree_start + $trees_per_task - 1;
    if ($tree_start == @trees) {
	$par_tasks = $i;
	last;
    }
    if ($tree_end >= @trees) {
	$par_tasks = $i+1;
	$tree_end = @trees - 1;
    }
    my @this_trees = @trees[$tree_start..$tree_end];
    $tree_start += $trees_per_task;
    next unless MultiThread::ForkChild();
    exec($split_halo_trees, @common_args, $i, "0", @this_trees);
    exit;
}

MultiThread::WaitForChildren();
system($split_halo_trees, @common_args, "-1", $par_tasks, @trees);

#!/usr/bin/perl -w
package ProcessTemplate;
use Cwd qw(abs_path);
use MultiThread;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(processTemplate convertToImage convertToTransparent removePDF);

BEGIN { $ENV{PATH}.=":/usr/local/bin:/usr/texbin/:/opt/local/bin"; }
our $xmgrace_vscale = 0;
our $doforks = 1;
our $density = 72;

sub processTemplate {
    my $template = shift;
    my $output = shift;
    my %args = @_;
    open FILE, "<", $template
	or die "Couldn't open file $template!\n";
    my $template_data = join("", <FILE>);
    close FILE;
    foreach (keys %args) {
	if ($args{$_} =~ s/^file://) {
	    open DATA, "<", $args{$_} or
		die "Couldn't open file $args{$_}!\n";
	    $args{$_} = join("", <DATA>);
	    close DATA;
	}
	$template_data =~ s/\<\Q$_\E\>/$args{$_}/g;
    }
    open OUT, ">", $output
	or die "Couldn't write to file $output!\n";
    print OUT $template_data;
    close OUT;
}

sub convertToImage {
    foreach my $file (@_) {
	$file =~ m!^(.*)/?AGR/(.*)\.agr$!;
	my ($dir, $fn) = ($1, $2);
	$dir = "." unless (length $dir);
	mkdir "$dir/$_" for (qw/PDF EPS PNG AGR/);
	my $pid = $$;
	next if ($doforks and !MultiThread::ForkChild());
	system("xmgrace", "-hardcopy", $file, "-printfile", "$dir/EPS/$fn.eps");
	if ($xmgrace_vscale) {
	    open GS, "-|", "gs -sDEVICE=bbox -sOutputFile=- -dNOPAUSE -dBATCH '$dir/EPS/$fn.eps' 2>&1";
	    my @lines = <GS>;
	    close GS;
	    my ($bbtext) = grep { /^%%BoundingBox/ } @lines;
	    if ($bbtext) {
		chomp $bbtext;
		$bbtext =~ s/(\d+) \d+ (\d+) \d+/$1 0 $2 792/;
		system("perl", "-pi", "-e", "s/^%%BoundingBox.*/$bbtext/", 
		       "$dir/EPS/$fn.eps");		
	    }
	    system("epstopdf","$dir/EPS/$fn.eps", "$dir/PDF/$fn.pdf");
	    system("convert", "-density", $density, "-rotate", "90", "$dir/PDF/$fn.pdf", "$dir/PNG/$fn.png");	
	}
	else {
	    system("ps2pdf","$dir/EPS/$fn.eps","$dir/PDF/$fn.pdf");
	    system("convert",  "-density", $density, "-rotate", "90", "$dir/EPS/$fn.eps", "$dir/PNG/$fn.png");	
	}
	exit if ($doforks);
    }
}

sub convertPDFToImage {
    for my $fn (@_) {
	system("convert",  "-density", $density, "-rotate", "90", "$dir/EPS/$fn.pdf", "$dir/PNG/$fn.png");	
    }
}

sub removePDF {
    my $file = shift;
    if ($file =~ m!^(.*)/?AGR/(.*)\.agr$!) {
	my ($dir, $fn) = ($1, $2);
	$fn .= ".pdf";
	$dir = "." unless length $dir;
	$dir .= "/PDF";
	$file = "$dir/$fn";
    }
    unlink $file;
}

sub convertToTransparent {
    my $file = shift;
    my @colors = ();
    @colors = map { s/\s*//g; $_ } @{$_[0]} if (ref($_[0]) eq 'ARRAY');
    my $svgfile = "";
    if ($file =~ m!^(.*)/?AGR/(.*)\.agr$!) {
	my ($dir, $fn) = ($1, $2);
	$dir = "." unless length $dir;
	mkdir "$dir/SVG", 0755;
	$svgfile = "$dir/SVG/$fn.svg";
	$fn .= ".pdf";
	$dir .= "/PDF";
	$file = "$dir/$fn";
    }
    if (! -r $file) {
	MultiThread::WaitForChildren();
    }
    my $newfile = $file.".tmp";
    system("gs", "-q", "-o", $newfile, "-dNoOutputFonts", "-sDEVICE=pdfwrite", $file);

    my $svg = $svgfile;
    if (!length($svg)) {
	$svg = $file;
	$svg =~ s/\.pdf/\.svg/;
    }
    system("pdftocairo", "-origpagesizes", "-svg", $newfile, $svg);
    unlink $newfile;
    local (*IN, *OUT, $_);
    open IN, "<", $svg;
    open OUT, ">", "$svg.tmp";
    while (<IN>) {
	if (/path/ and !/stroke/ and /rgb\(([^)]*)\)/) {
	    my $rgb = $1;
	    $rgb =~ s/\%//g;
	    my (@vals) = split(/,/, $rgb);
	    my $scaled_rgb = join(",", map { int($_*255/100+0.5) } @vals);
	    if ((grep { /\Q$scaled_rgb\E/ } @colors) or
		(!@colors and 
		 (grep { $_ < 100 } @vals) and (grep {$_>0} @vals))) {
		@vals = map { 100*(($_/100)**(1.4)).'%' } @vals;
		if ($scaled_rgb eq "254,254,254") {
		    @vals = (255,255,255);
		}
		$rgb = join(",", @vals);
		s/rgb\([^)]*\)/rgb($rgb)/;
		s/fill-opacity=\"1\"/fill-opacity=\"0.7\"/;
	    }
	}
	print OUT;
    }
    close OUT;
    close IN;
    rename "$svg.tmp", "$svg";
    #my $ipath = "/Applications/Inkscape.app/Contents/Resources/bin/inkscape";
    #$ipath = "inkscape" unless (-x $ipath);
    $file = abs_path($file);
    $svg = abs_path($svg);
#    print join(" ", $ipath, "-C", "--export-pdf=".$file, $svg,"\n");
    #system($ipath, "-C", "--export-filename=".$file, $svg);
    system("svg2pdf", $svg, $file);
}


1;

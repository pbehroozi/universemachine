#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include "check_syscalls.h"

void print_matrix_dim(double *matrix, int64_t dim) {
  int64_t i,j;
  for (i=0; i<dim; i++) {
    for (j=0; j<dim; j++) {
      printf("%+3e ", matrix[i*dim+j]);
    }
    printf("\n");
  }
}

//Algorithm from the Wikipedia entry on Jacobi decomposition
void jacobi_decompose_dim(double *cov_matrix, double *eigenvalues, double *orth_matrix, int64_t dim) {
  int64_t i,j,k,l;
  int64_t *max_col = NULL;
  int64_t *changed = NULL; //To keep track of eigenvalues changing
  int64_t n = dim;
  int64_t max_row;
  int64_t state = 0;
  double max, c, s, t, u, a;
  //int64_t count = 0;

  check_realloc_s(max_col, sizeof(int64_t), dim);
  check_realloc_s(changed, sizeof(int64_t), dim);
  memset(max_col, 0, sizeof(int64_t)*dim);
  memset(changed, 0, sizeof(int64_t)*dim);
  
  //Set up the maximum value cache
  for (i=0; i<n; i++) {
    max=0;
    max_col[i] = i+1;
    for (j=i+1; j<n; j++) {
      if (fabs(cov_matrix[i*dim+j])>max) {
	max_col[i] = j;
	max = fabs(cov_matrix[i*dim+j]);
      }
    }
  }

  //Set up the orthogonal matrix as the identity:
  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      orth_matrix[i*dim+j] = (i==j) ? 1 : 0;

  //Copy the diagonal values to the eigenvalue array:
  for (i=0; i<n; i++) {
    eigenvalues[i] = cov_matrix[i*dim+i];
    changed[i] = 1;
    for (j=0; j<n; j++)
      if (j!=i && cov_matrix[i*dim+j]) break;
    if (j==n) {
      state--;
      changed[i] = 0;
    }
  }

  //Sweep time: iterate until the eigenvalues stop changing.
  state += n;
  while (state) {
    //count++;
    //Find the largest nonzero element in the matrix:
    max = fabs(cov_matrix[0*dim+max_col[0]]);
    max_row = 0;
    for (i=1; i<n-1; i++) {
      if (fabs(cov_matrix[i*dim+max_col[i]])>max) {
	max = fabs(cov_matrix[i*dim+max_col[i]]);
	max_row = i;
      }
    }
    k = max_row; l = max_col[k];
    max = cov_matrix[k*dim+l];
    if (max==0) break;

    //Calculate the Jacobi rotation matrix
    //Tan 2phi = 2S_kl / (S_kk - S_ll)
    a = (eigenvalues[l] - eigenvalues[k]) * 0.5;
    t = fabsl(a) + sqrtl(max*max + a*a);
    s = sqrtl(max*max + t*t);
    c = t/s;
    s = max/s;
    t = max*max / t;
    if (a<0) {
      s = -s; t = -t;
    }

    //Update eigenvalues
#define UPDATE(x,y)							\
    a = eigenvalues[x];							\
    eigenvalues[x] += y;						\
    if (changed[x] && (fabs(y) < 1e-6*fabs(eigenvalues[x]))) { /*Eigenvalue didn't change*/ \
      changed[x]=0;							\
      state--;								\
    } else if (!changed[x] && (fabs(y) > 1e-6*fabs(eigenvalues[x]))) { /*Egval did change*/ \
      changed[x] = 1;							\
      state++;								\
    }
      
    UPDATE(k, -t);
    UPDATE(l, t);

    //Update covariance matrix:
    cov_matrix[k*dim+l] = 0;

#define ROTATE(m,w,x,y,z,r)  /*Perform a Jacobi rotation*/	\
      t = m[w*dim+x]; u = m[y*dim+z];					\
      m[w*dim+x] = t*c - s*u;					\
      m[y*dim+z] = s*t + c*u;				        \
      if (r) {							\
	if (fabs(m[w*dim+x])>fabs(m[w*dim+max_col[w]]))		\
	  max_col[w] = x;					\
	if (fabs(m[y*dim+z])>fabs(m[y*dim+max_col[y]]))		\
	  max_col[y] = z;					\
      }
    

    for (i=0; i<k; i++) { ROTATE(cov_matrix,i,k,i,l,1); }
    for (i=k+1; i<l; i++) { ROTATE(cov_matrix,k,i,i,l,1); }
    for (i=l+1; i<n; i++) { ROTATE(cov_matrix,k,i,l,i,1); }
    for (i=0; i<n; i++) { ROTATE(orth_matrix,k,i,l,i,0); }
    
#undef ROTATE
#undef UPDATE
  }

  // Restore the matrix to its original form:
  for (k=0; k<n-1; k++) {
    for (l=k+1; l<n; l++) {
      cov_matrix[k*dim+l] = cov_matrix[l*dim+k];
    }
  }
  free(max_col);
  free(changed);
}


void matrix_multiply_dim(double *m, double *in, double *out, int64_t dim) {
  int64_t i,j;
  for (i=0; i<dim; i++) out[i] = 0;
  for (i=0; i<dim; i++)
    for (j=0; j<dim; j++)
      out[i] += m[i*dim+j]*in[j];
}

void vector_madd_dim(double *a, double b, double *c, int64_t dim) {
  int64_t i;
  for (i=0; i<dim; i++)
    a[i] += b*c[i];
}

int jacobi_test_dim(void) {
  /*  double S[dim][dim] = {{ 4,   -30,    60,   -35, 0},
				      {-30,  300,  -675,   420, 0},
				      { 60, -675,  1620, -1050, 0},
				      {-35,  420, -1050,   700, 0},
				      {  0,    0,     0,     0, 1}};
  */
#define NDIM 5
  double S2[NDIM*NDIM] = { 5, -1, 0, 0, 0,
			   -1, 2, 0, 0, 0,
			   0, 0, 1, 0, 0,
			   0, 0, 0, 1, 0,
			   0, 0, 0, 0, 1};
  double O[NDIM*NDIM];
  double E[NDIM];
  double IN[NDIM], OUT[NDIM];
  int i, j;
  jacobi_decompose_dim(S2,E,O, NDIM);
  for (i=0; i<NDIM; i++) {
    printf("Eigenvalue[%d]: %f\n", i, E[i]);
    for (j=0; j<NDIM; j++) {
      printf("Eigv[%d]: %f\n", j, O[i*NDIM+j]);
      IN[j] = O[i*NDIM+j];
    }
    matrix_multiply_dim(S2,IN,OUT,NDIM);
    for (j=0; j<NDIM; j++) {
      printf("Eigv test[%d]: %f %f\n", j, OUT[j], O[i*NDIM+j]*E[i]);
    }
    printf("\n");
  }
  return 0;
}


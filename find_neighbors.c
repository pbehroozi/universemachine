#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "config_vars.h"
#include "config.h"
#include "version.h"

#define FAST3TREE_TYPE struct catalog_halo
#define FAST3TREE_DIM 2
#include "fast3tree.c"

int64_t NEIGHBOR_COUNT = 50;
double MIN_STELLAR_MASS=1e9;
double MAX_NEIGHBOR_MASS_OFFSET = 0.0316227766016838; //10^-1.5
double MAX_NEIGHBOR_VELOCITY_OFFSET=1000;
double MAX_TARGET_STELLAR_MASS=1e15;

#define CYLINDER_BINS 4
#define CYLINDER_NUM_RADII 3
double CYLINDER_BIN_SPACING = 500; //km/s
double CYLINDER_RADII[CYLINDER_NUM_RADII] = {0.5, 1.0, 2.0};


double SCALE_FACTOR=1;
double H=100; //in km/s / Mpc/h 

struct neighbor {
  struct catalog_halo *h;
  float r;
};

struct fast3tree *tree=NULL;
struct fast3tree_results *results=NULL;

void find_neighbors(struct catalog_halo *h, struct fast3tree *t, struct fast3tree_results *r);
void count_cylinders(struct catalog_halo *h, struct fast3tree *t, struct fast3tree_results *r);

int main(int argc, char **argv) {
  if (argc<3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s config_file.cfg sfr_catalog.bin [min_stellar_mass max_neighbor_mass_offset max_target_stellar_mass]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  do_config(argv[1]);
  
  if (argc>3) {
    MIN_STELLAR_MASS = atof(argv[3]);
    if (MIN_STELLAR_MASS < 20) MIN_STELLAR_MASS = pow(10, MIN_STELLAR_MASS);
  }

  if (argc>4) {
    MAX_NEIGHBOR_MASS_OFFSET = atof(argv[4]);
    if (MAX_NEIGHBOR_MASS_OFFSET < 0)
      MAX_NEIGHBOR_MASS_OFFSET = pow(10, MAX_NEIGHBOR_MASS_OFFSET);
  }

  if (argc>5) {
    MAX_TARGET_STELLAR_MASS = atof(argv[5]);
    if (MAX_TARGET_STELLAR_MASS<20)
      MAX_TARGET_STELLAR_MASS = pow(10, MAX_TARGET_STELLAR_MASS);
  }
  
  //Set scale factor if sfr_catalog has a standard name
  char *catalog_filename = strstr(argv[2], "sfr_catalog_");
  if (catalog_filename) SCALE_FACTOR = atof(catalog_filename+strlen("sfr_catalog_"));
  H = 100.0*sqrt(Om*pow(1.0/SCALE_FACTOR, 3.0)+Ol);
  
  //Load halos from supplied file
  struct catalog_halo *halos = NULL;
  int64_t file_length = 0, num_halos = 0;
  check_slurp_file(argv[2], (void **)(&halos), &file_length);
  if (file_length % sizeof(struct catalog_halo)) {
    fprintf(stderr, "[Error] Length of file %s is not divisible by halo structure size.\n", argv[2]);
    exit(EXIT_FAILURE);
  }
  num_halos = file_length / sizeof(struct catalog_halo);

  //Filter halos to remove those that should be ignored and those below the stellar mass cut
  for (int64_t i=0; i<num_halos; i++) {
    if ((halos[i].flags & IGNORE_FLAG) || (halos[i].obs_sm < MIN_STELLAR_MASS)) {
      halos[i] = halos[num_halos-1];
      i--;
      num_halos--;
      continue;
    }
    
    //Convert z-position to redshift-space position
    halos[i].pos[2] += halos[i].pos[5]/H;
    if (halos[i].pos[2] < 0) halos[i].pos[2] += BOX_SIZE;
    if (halos[i].pos[2] > BOX_SIZE) halos[i].pos[2] -= BOX_SIZE;
  }

  //build kdtree
  struct fast3tree *tree = fast3tree_init(num_halos, halos);
  struct fast3tree_results *results = fast3tree_results_init();
  _fast3tree_set_minmax(tree, 0, BOX_SIZE);
  
  //print header
  printf("#Neighbors and cylinder counts for %s\n", argv[2]);
  printf("#[Halo_ID Stellar_Mass Distance]x%"PRId64" [Cylinder_Width Cylinder_Velocity_Start Cylinder_Velocity_End Cylinder_Count]x3\n",
	 NEIGHBOR_COUNT+1);

  //find neighbors / counts in cylinders
  for (int64_t i=0; i<num_halos; i++) {
    if (halos[i].obs_sm > MAX_TARGET_STELLAR_MASS) continue;
    find_neighbors(halos+i, tree, results);
    printf(" ");
    count_cylinders(halos+i, tree, results);
    printf("\n");
  }

  return 0;
}

double periodic_distance(struct catalog_halo *h1, struct catalog_halo *h2) {
  double s=0;
  for (int64_t i=0; i<2; i++) {
    double x = fabs(h1->pos[i]-h2->pos[i]);
    if (x>BOX_SIZE/2.0) x = BOX_SIZE-x;
    s += x*x;
  }
  return sqrt(s);
}

double redshift_space_distance(struct catalog_halo *h1, struct catalog_halo *h2) {
  double s = fabs(h1->pos[2]-h2->pos[2]);
  if (s>BOX_SIZE/2.0) s = BOX_SIZE-s;
  return (H*s);
}

int64_t results_to_neighbor_list(struct catalog_halo *h, struct fast3tree_results *r, struct neighbor **n) {
  int64_t i, num_neighbors = 0;
  for (i=0; i<r->num_points; i++) {
    if (redshift_space_distance(h, r->points[i])<MAX_NEIGHBOR_VELOCITY_OFFSET &&
	r->points[i]->obs_sm > h->obs_sm*MAX_NEIGHBOR_MASS_OFFSET) {
      check_realloc_every(*n, sizeof(struct neighbor), num_neighbors, 1000);
      n[0][num_neighbors].h = r->points[i];
      n[0][num_neighbors].r = periodic_distance(h, r->points[i]);
      num_neighbors++;
    }
  }
  return num_neighbors;
}

int neighbor_distance_compare(const void *a, const void *b) {
  const struct neighbor *c = a;
  const struct neighbor *d = b;
  if (c->r < d->r) return -1;
  if (c->r > d->r) return 1;
  return 0;
}

void find_neighbors(struct catalog_halo *h, struct fast3tree *t, struct fast3tree_results *r) {
  int64_t num_neighbors = 0;
  double radius=0.125;
  struct neighbor *n = NULL;
  while (num_neighbors<NEIGHBOR_COUNT+1 && radius<BOX_SIZE/2.0) {
    fast3tree_find_sphere_periodic(t, r, h->pos, radius);
    num_neighbors = results_to_neighbor_list(h, r, &n);
    radius *= 2.0;
  }
  qsort(n, num_neighbors, sizeof(struct neighbor), neighbor_distance_compare);
  for (int64_t i=0; i<NEIGHBOR_COUNT+1; i++) {
    if (i<num_neighbors)
      printf("%"PRId64" %e %e", n[i].h->id, n[i].h->obs_sm, n[i].r);
    else
      printf("-1 -1 -1");
    if (i<=NEIGHBOR_COUNT) printf(" ");
  }
}



void results_to_cylinder(struct catalog_halo *h, struct fast3tree_results *r, int64_t *counts) {
  for (int64_t i=0; i<CYLINDER_BINS; i++) counts[i] = 0;
  for (int64_t i=0; i<r->num_points; i++) {
    if (r->points[i]->obs_sm > h->obs_sm*MAX_NEIGHBOR_MASS_OFFSET) {
      double s = redshift_space_distance(h, r->points[i]);
      int64_t bin = s/CYLINDER_BIN_SPACING;
      if (bin < CYLINDER_BINS) counts[bin]++;
    }
  }
}

void count_cylinders(struct catalog_halo *h, struct fast3tree *t, struct fast3tree_results *r) {
  int64_t counts[CYLINDER_BINS];
  for (int64_t i=0; i<CYLINDER_NUM_RADII; i++) {
    fast3tree_find_sphere_periodic(t, r, h->pos, CYLINDER_RADII[i]);
    results_to_cylinder(h, r, counts);
    for (int64_t j=0; j<CYLINDER_BINS; j++) {
      printf("%g %g %g %"PRId64, CYLINDER_RADII[i], CYLINDER_BIN_SPACING*i, CYLINDER_BIN_SPACING*(i+1),
	     counts[j]);
      if ((j<(CYLINDER_BINS-1)) || (i<(CYLINDER_NUM_RADII-1)))
	printf(" ");
    }
  }
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include "check_syscalls.h"
#include "stringparse.h"
#include "stats.h"
#include "io_helpers.h"
#include "universe_time.h"
#include "smloss.h"

#define REG_HIST_VARS 6
#define EXTRA_HIST_VARS 4
#define NUM_HIST_VARS (REG_HIST_VARS+EXTRA_HIST_VARS)
#define TYPE_MAX_LENGTH 40

#define SFH_OFF    0
#define ICLH_OFF   1
#define SM_MP_OFF  2
#define ICL_MP_OFF 3
#define HM_MP_OFF  4
#define SFR_MP_OFF 5

#define HM_MIN 10
#define HM_MAX 15
#define HM_BPDEX 2
#define HM_BINS ((HM_MAX - HM_MIN)*HM_BPDEX+1)
#define SM_MIN 8
#define SM_MAX 12
#define SM_BPDEX 2
#define SM_BINS ((SM_MAX - SM_MIN)*SM_BPDEX+1)
#define V_MIN 1.7
#define V_MAX 3.5
#define V_BPDEX 6
#define V_BINS 12


double scale = 0;
int64_t num_scales=0;
float *history = NULL;
double *scales = NULL;
double *scale_times = NULL;
double *rem=NULL, *dt=NULL, *dt2 = NULL;
char **names = NULL;
char **bc_names = NULL;

double min_sort = 0, max_sort = 0;
double *bin_edges = NULL;
int64_t _max_bins = 0;

#define HALO_FIELDS 24

struct sfh_halo {
  int64_t id, upid;
  float mp, m, vmp, v, r, t_tdyn, rank1, rarank, sm, icl, sfr, obs_sm, obs_sfr, obs_uv;
  float a_first_infall, a_last_infall;
  float *history;
};

struct sfh_halo *sh = NULL;
int64_t nh=0;

struct binstats *bs = NULL;
struct binned_correl *bc = NULL;

#define NUM_STATS 6
struct binstats *final_stats[NUM_STATS] = {0};

void load_file(char *filename);


void calc_rem_sm(void) {
  int64_t n, i;
  check_realloc_s(dt, sizeof(double), num_scales);
  check_realloc_s(dt2, sizeof(double), num_scales);
  check_realloc_s(rem, sizeof(double), num_scales*num_scales);
  for (n=0; n<num_scales; n++) {
    double a1 = (n>0) ? 0.5*(scales[n]+scales[n-1]) : 0;
    double a2 = (n<num_scales-1) ? 0.5*(scales[n]+scales[n+1]) : scales[n];
    dt[n] = exact_scale_to_years(a2) - exact_scale_to_years(a1);
    dt2[n] = exact_scale_to_years(scales[n]) - exact_scale_to_years(a1);
    for (i=0; i<num_scales; i++) {
      float max_scale = a2;
      if (max_scale > scales[i]) max_scale = scales[i];
      rem[i*num_scales+n] = (i<n) ? 0.0 : calc_sm_loss_int(a1, max_scale, scales[i]);
    }
  }
}


void setup_bins(double min, int64_t bins, double bpdex) {
  int64_t i;
  check_realloc_s(bin_edges, bins+1, sizeof(double));
  for (i=0; i<bins+1; i++)
    bin_edges[i] = pow(10, min+((double)i - 0.5)/bpdex);
  _max_bins = bins;
}


int64_t bin_of(double val) {
  if (val < bin_edges[0] || val >= bin_edges[_max_bins]) return _max_bins;
  int64_t i;
  for (i=1; i<_max_bins; i++) 
    if (val < bin_edges[i]) return i-1;
  return i-1;
}
  
double key_of(int64_t bin, double min, double bpdex) {
  return (min + (double)bin / bpdex);
}

double log_value(double x, int64_t do_logarithm) {
  if (!do_logarithm) return x;
  if (x<1e-20) return -20;
  return log10(x);
}

int sort_by_hm(const void *a, const void *b) {
  const struct sfh_halo *c = a;
  const struct sfh_halo *d = b;
  int64_t e = bin_of(c->mp);
  int64_t f = bin_of(d->mp);
  if (e < f) return -1;
  if (e > f) return 1;
  return 0;
}

int sort_by_sm(const void *a, const void *b) {
  const struct sfh_halo *c = a;
  const struct sfh_halo *d = b;
  int64_t e = bin_of(c->sm);
  int64_t f = bin_of(d->sm);
  if (e < f) return -1;
  if (e > f) return 1;
  return 0;
}

int sort_by_obs_sm(const void *a, const void *b) {
  const struct sfh_halo *c = a;
  const struct sfh_halo *d = b;
  int64_t e = bin_of(c->obs_sm);
  int64_t f = bin_of(d->obs_sm);
  if (e < f) return -1;
  if (e > f) return 1;
  return 0;
}

int sort_by_vmp(const void *a, const void *b) {
  const struct sfh_halo *c = a;
  const struct sfh_halo *d = b;
  int64_t e = bin_of(c->vmp);
  int64_t f = bin_of(d->vmp);
  if (e < f) return -1;
  if (e > f) return 1;
  return 0;
}

//Reduces the number of page faults in different arrays 
void sort_by_mode(struct sfh_halo *sfh_halos, int64_t num_sfh_halos, int64_t mode) {
  double mins[4] = {HM_MIN, SM_MIN, V_MIN, SM_MIN};
  int64_t max_bins[4] = {HM_BINS, SM_BINS, V_BINS, SM_BINS};
  double bpdex[4] = {HM_BPDEX, SM_BPDEX, V_BPDEX, SM_BPDEX};
  setup_bins(mins[mode], max_bins[mode], bpdex[mode]);
  int (*sort_func)(const void *a, const void *b) = sort_by_hm;
  if (mode == 1) sort_func = sort_by_sm;
  if (mode == 2) sort_func = sort_by_vmp;
  if (mode == 3) sort_func = sort_by_obs_sm;
  qsort(sfh_halos, num_sfh_halos, sizeof(struct sfh_halo), sort_func);
}


void _do_bins(struct sfh_halo *sfh_halos, int64_t num_sfh_halos, char *desc, int64_t mode, int64_t normed) {
  int64_t i, j;
  double mins[4] = {HM_MIN, SM_MIN, V_MIN, SM_MIN};
  int64_t max_bins[4] = {HM_BINS, SM_BINS, V_BINS, SM_BINS};
  double bpdex[4] = {HM_BPDEX, SM_BPDEX, V_BPDEX, SM_BPDEX};
  char *bin_names[4] = {"mpeak", "sm", "vmpeak", "obs_sm"};
  char *normed_names[2] = {"absolute", "relative"};
  char *log_names[2] = {"normal", "logarithmic"};
  assert(mode>=0 && mode < 4);

  setup_bins(mins[mode], max_bins[mode], bpdex[mode]);

  //for (i=0; i<nbs; i++) clear_binstats(bs[i]);
  //for (i=0; i<nbs; i++) clear_binned_correl(bc[i]);

  int64_t *full_counts = NULL;
  check_realloc_s(full_counts, sizeof(int64_t), (max_bins[mode]+1));
  memset(full_counts, 0, sizeof(int64_t)*(max_bins[mode]+1));

  for (i=0; i<NUM_STATS; i++) clear_binstats(final_stats[i]);
  for (i=0; i<num_sfh_halos; i++) {
    struct sfh_halo *th = sfh_halos+i;
    int64_t bin = -1;
    if (!mode) bin = bin_of(th->mp);
    else if (mode==1) bin = bin_of(th->sm);
    else if (mode==2) bin = bin_of(th->vmp);
    else if (mode==3) bin = bin_of(th->obs_sm);
    full_counts[bin]++;
    if (bin >= max_bins[mode]) continue;
    if (normed && !(th->sm > 0)) continue;
    add_to_binstats(final_stats[0], bin, th->mp);
    add_to_binstats(final_stats[1], bin, th->sm);
    add_to_binstats(final_stats[2], bin, th->obs_sm);
    add_to_binstats(final_stats[3], bin, th->vmp);
    add_to_binstats(final_stats[4], bin, th->icl);
    add_to_binstats(final_stats[5], bin, (th->obs_sm > 0) ? (th->obs_sfr / th->obs_sm) : 0);
  }

  struct binstats **fs = final_stats;
  for (i=0; i<NUM_STATS; i++) calc_median(fs[i]);

  check_mkdir("sfh_stats", 0755);

  int64_t v_start = 0, v_stop = 0, cbin=0;
  for (cbin=0; cbin < max_bins[mode]; cbin++) v_start += full_counts[cbin];
  for (cbin=max_bins[mode]-1; cbin>=0; cbin--) {
    clear_binstats(bs);
    clear_binned_correl(bc);
    v_stop = v_start;
    v_start -= full_counts[cbin];

    //Prealloc
    for (i=0; i<bs->num_bins; i++) {
      bs->alloced_bvals[i] = full_counts[cbin];
      check_realloc_aligned_s(bs->bvals[i], sizeof(STATS_VAL_TYPE), 0, full_counts[cbin]);
      memset(bs->bvals[i], 0, full_counts[cbin]*sizeof(STATS_VAL_TYPE)); //Actually get memory...
    }
    for (i=0; i<bc->num_bins; i++) {
      bc->cstats[i].alloced = full_counts[cbin];
      check_realloc_aligned_s(bc->cstats[i].cvals, sizeof(struct correlval), 0, full_counts[cbin]);
      memset(bc->cstats[i].cvals, 0, full_counts[cbin]*sizeof(struct correlval)); //Actually get memory...
    }
    
    for (i=v_start; i<v_stop; i++) {
      struct sfh_halo *th = sfh_halos+i;
      int64_t bin = -1;
      if (normed && !(th->sm > 0)) continue;
      if (!mode) bin = bin_of(th->mp);
      else if (mode==1) bin = bin_of(th->sm);
      else if (mode==2) bin = bin_of(th->vmp);
      else if (mode==3) bin = bin_of(th->obs_sm);
      assert(bin==cbin);

      for (j=0; j<num_scales*REG_HIST_VARS; j++) {
	if (!normed) { add_to_binstats(bs, j, th->history[j]); }
	else { add_to_binstats(bs, j, th->history[j]/th->sm); }
      }

      for (j=0; j<num_scales; j++) {
	double sm = th->history[SM_MP_OFF*num_scales+j];
	double sfr = th->history[SFR_MP_OFF*num_scales+j];
	double icl = th->history[ICL_MP_OFF*num_scales+j];
	double hm = th->history[HM_MP_OFF*num_scales+j];
	double ssfr = (sm > 0) ? (sfr / sm) : 0;
	double icl_frac = (sm>0) ? (icl / sm) : 0;
	double sm_hm = (hm>0) ? (sm / hm) : 0;
	double quenched = (ssfr < 1e-11) ? 1 : 0;
	if (sm>0) add_to_binstats(bs, j+REG_HIST_VARS*num_scales, ssfr);
	add_to_binstats(bs, j+(REG_HIST_VARS+1)*num_scales, icl_frac);
	add_to_binstats(bs, j+(REG_HIST_VARS+2)*num_scales, sm_hm);
	if (sm>0) add_to_binstats(bs, j+(REG_HIST_VARS+3)*num_scales, quenched);
      }

      for (j=0; j<num_scales-1; j++) {
	double sm_now = log_value(th->sm, normed);
	double sm_then = log_value(th->history[num_scales*SM_MP_OFF + j], normed);
	double hm_now = log_value(th->mp, normed);
	double hm_then = log_value(th->history[num_scales*HM_MP_OFF + j], normed);
	add_to_binned_correl(bc, j, sm_now-sm_then, hm_now - hm_then);
      }
    }

    char buffer[1024];
    double key = 0;
    if (!mode) key = key_of(cbin, HM_MIN, HM_BPDEX);
    else if (mode==1) key = key_of(cbin, SM_MIN, SM_BPDEX);
    else if (mode==2) key = key_of(cbin, V_MIN, V_BPDEX);
    else if (mode==3) key = key_of(cbin, SM_MIN, SM_BPDEX);
    snprintf(buffer, 1024, "sfh_stats/stats_a%f_%s_%s_%.3f_%s.dat", scale, 
	     normed_names[normed], bin_names[mode], key, desc);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Type(SFH/ICLH/SM_mp/ICL_mp/HM_mp/SFR_mp/SSFR_mp/ICL_ratio_mp/SM_HM_ratio_mp/fq_mp) Scale Median 1Sig+ 1Sig-\n");
    fprintf(out, "#Halos/galaxies at a=%f (z=%f)\n", scale, 1.0/scale-1.0);
    fprintf(out, "#SF Units: %s\n", (normed ? "1/yr" : "Msun/yr"));
    fprintf(out, "#SFH: Star formation history of present-day stellar population (including merged galaxies).\n");
    fprintf(out, "#ICLH: Star formation history of present-day ICL.\n");
    fprintf(out, "#SM_mp: Stellar mass of main progenitor.\n");
    fprintf(out, "#ICL_mp: ICL of main progenitor.\n");
    fprintf(out, "#HM_mp: Peak halo mass of main progenitor [Msun/h].\n");
    fprintf(out, "#SFR_mp: Star formation rate of main progenitor.\n");
    fprintf(out, "#SSFR_mp: Specific star formation rate of main progenitor.\n");
    fprintf(out, "#ICL_ratio_mp: ICL ratio (ICL/SM) of main progenitor.\n");
    fprintf(out, "#SM_HM_ratio_mp: SM/HM ratio of main progenitor.\n");

    if (normed) fprintf(out, "#SF rates divided by final a=%f (z=%f) stellar mass\n", scale, 1.0/scale-1.0);
    fprintf(out, "#%s range: %g - %g\n", bin_names[mode], key-0.5/bpdex[mode], key+0.5/bpdex[mode]);
    fprintf(out, "#HM    Stats: %e (median) + %e  / - %e\n",  fs[0]->med[cbin], fs[0]->stdup[cbin], fs[0]->stddn[cbin]);
    fprintf(out, "#SM    Stats: %e (median) + %e  / - %e\n",  fs[1]->med[cbin], fs[1]->stdup[cbin], fs[1]->stddn[cbin]);
    fprintf(out, "#ObsSM Stats: %e (median) + %e  / - %e\n",  fs[2]->med[cbin], fs[2]->stdup[cbin], fs[2]->stddn[cbin]);
    fprintf(out, "#VMP   Stats: %e (median) + %e  / - %e\n",  fs[3]->med[cbin], fs[3]->stdup[cbin], fs[3]->stddn[cbin]);
    fprintf(out, "#ICL   Stats: %e (median) + %e  / - %e\n",  fs[4]->med[cbin], fs[4]->stdup[cbin], fs[4]->stddn[cbin]);
    fprintf(out, "#SSFR  Stats: %e (median) + %e  / - %e\n",  fs[5]->med[cbin], fs[5]->stdup[cbin], fs[5]->stddn[cbin]);
    print_medians(bs, out);
    fclose(out);

    snprintf(buffer, 1024, "sfh_stats/hmsm_correls_a%f_%s_%s_%.3f_%s.dat", scale, 
	     log_names[normed], bin_names[mode], key, desc);
    
    out = check_fopen(buffer, "w");
    fprintf(out, "#Delta_Time Ranked_Correl Ignore Ignore Ranked_Error Pearson_Correl Ignore Ignore Pearson_Error Counts\n");
    fprintf(out, "#%s range: %g - %g\n", bin_names[mode], key-0.5/bpdex[mode], key+0.5/bpdex[mode]);
    fprintf(out, "#HM    Stats: %e (median) + %e  / - %e\n",  fs[0]->med[cbin], fs[0]->stdup[cbin], fs[0]->stddn[cbin]);
    fprintf(out, "#SM    Stats: %e (median) + %e  / - %e\n",  fs[1]->med[cbin], fs[1]->stdup[cbin], fs[1]->stddn[cbin]);
    fprintf(out, "#ObsSM Stats: %e (median) + %e  / - %e\n",  fs[2]->med[cbin], fs[2]->stdup[cbin], fs[2]->stddn[cbin]);
    fprintf(out, "#VMP   Stats: %e (median) + %e  / - %e\n",  fs[3]->med[cbin], fs[3]->stdup[cbin], fs[3]->stddn[cbin]);
    fprintf(out, "#ICL   Stats: %e (median) + %e  / - %e\n",  fs[4]->med[cbin], fs[4]->stdup[cbin], fs[4]->stddn[cbin]);
    fprintf(out, "#SSFR  Stats: %e (median) + %e  / - %e\n",  fs[5]->med[cbin], fs[5]->stdup[cbin], fs[5]->stddn[cbin]);
    print_binned_correl(bc, out, 1);
    fclose(out);
  }

  free(full_counts);
}

void calc_rejuvenation(struct sfh_halo *sfh_halos, int64_t num_sfh_halos, char *desc, int64_t mode, int64_t mmp) {
  int64_t i, j, k;
  double bpdex[4] = {5, 5, 10, 5};
  double bin_starts[4] = {HM_MIN, SM_MIN, V_MIN, SM_MIN};
  double bin_stops[4] = {HM_MAX, SM_MAX, V_MAX, SM_MAX};
  char *bin_names[4] = {"mpeak", "sm", "vmpeak", "obs_sm"};
  char *mmp_names[2] = {"merged", "mmp"};
  assert(mode>=0 && mode < 4);

  //Stats to calc:
  //Fraction w/ rejuvenation
  //Median rejuvenation episodes
  //Average quenched duty cycle
  //Average time separating most recent 2 SF periods
  
  double start = bin_starts[mode]-0.5/bpdex[mode];
  double end = bin_stops[mode]+0.5/bpdex[mode];
  struct binstats *fr   = init_binstats(start, end, bpdex[mode], 0, 1);
  struct binstats *reps = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *qd   = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *qtime = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *sftime = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *iclstats = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *mergers = init_binstats(start, end, bpdex[mode], 1, 1);

  struct binstats *fq = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *fq_qtime = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *sm10 = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *sm50 = init_binstats(start, end, bpdex[mode], 1, 1);
  struct binstats *sm90 = init_binstats(start, end, bpdex[mode], 1, 1);


  for (i=0; i<num_sfh_halos; i++) {
    struct sfh_halo *th = sfh_halos+i;
    double key = th->mp;
    if (mode==1) key = th->sm;
    else if (mode==2) key = th->vmp;
    else if (mode==3) key = th->obs_sm;
    
    int64_t sf = 1;
    int64_t r = 0;
    double total_sf = 0, total_q = 0, qt=0, sft=0, qs=0, sfs=0;
    double a10=0, a50=0, a90=0;
    //double last_sm = 0;
    double cami_total_sm = 0, last_cami_sm = 0;

    if (!mmp) { for (j=0; j<num_scales; j++) cami_total_sm += dt[j]*sfh_halos[i].history[j]; }
    else cami_total_sm = sfh_halos[i].sm;
    
    for (j=0; j<num_scales; j++) {
      int64_t sfr_off = (mmp) ? SFR_MP_OFF : SFH_OFF;
      double sfr = sfh_halos[i].history[sfr_off*num_scales+j];
      double sm = sfh_halos[i].history[SM_MP_OFF*num_scales+j];
      double cami_sm = sm;
      
      double *remj = rem + j*num_scales;
      if (!mmp) {
	sm = 0;
	for (k=0; k<j; k++) sm += dt[k]*sfh_halos[i].history[k]*remj[k];
	sm += dt2[j]*sfh_halos[i].history[j]*remj[j];
	cami_sm = 0;
	for (k=0; k<j; k++) cami_sm += dt[k]*sfh_halos[i].history[k];
	cami_sm += dt2[j]*sfh_halos[i].history[j];
      }
      if (!sm) continue;

      if ((cami_sm > 0.1*cami_total_sm) && !a10) {
	if (j<1) a10 = (scales[j]>0) ? scales[j] : 0.001;
	else a10 = scales[j] + (0.1*cami_total_sm-cami_sm)*(scales[j]-scales[j-1])/(cami_sm-last_cami_sm);
      }
      if ((cami_sm > 0.5*cami_total_sm) && !a50) {
	if (j<1) a50 = (scales[j]>0) ? scales[j] : 0.001;
	else a50 = scales[j] + (0.5*cami_total_sm-cami_sm)*(scales[j]-scales[j-1])/(cami_sm-last_cami_sm);
      }
      if ((cami_sm > 0.9*cami_total_sm) && !a90) {
	if (j<1) a90 = (scales[j]>0) ? scales[j] : 0.001;
	else a90 = scales[j] + (0.9*cami_total_sm-cami_sm)*(scales[j]-scales[j-1])/(cami_sm-last_cami_sm);
      }

      int64_t cq = (sfr < 1e-11*sm) ? 1 : 0;
      if (cq) { qs += dt[j]; sfs = 0; }
      else { sfs += dt[j]; qs = 0; }

      if (sf && qs > 300e6) {
	sf = 0;
	qt = qs;
	sft -= qs;
      }
      else if (!sf && sfs > 300e6) {
	r++;
	sf = 1;
	sft = sfs;
	qt -= sfs;
      }

      if (sf) sft += dt[j];
      else qt += dt[j];
      
      if (sm>0) {
	if (cq) total_q += dt[j];
	else total_sf += dt[j];
      }
      //last_sm = sm;
      last_cami_sm = cami_sm;
    }

    double mp_sm = 0, all_sm = 0;
    double *remj = rem + (num_scales-1)*num_scales;
    for (k=0; k<num_scales; k++) {
      mp_sm += dt[k]*sfh_halos[i].history[SFR_MP_OFF*num_scales+k]*remj[k];
      all_sm += dt[k]*sfh_halos[i].history[k]*remj[k];
    }
    double from_mergers = (all_sm > 0) ? (all_sm - mp_sm)/all_sm : 0;

    add_to_binstats(mergers, key, from_mergers);
    add_to_binstats(fr, key, (r > 0) ? 1 : 0);
    add_to_binstats(reps, key, r);
    double total_time = total_sf+total_q;
    if (!total_time) total_time = 1;
    add_to_binstats(qd, key, total_q / total_time);
    if ((sfh_halos[i].obs_sfr > sfh_halos[i].obs_sm*1e-11) && (r>0)) {
      add_to_binstats(qtime, key, qt);
      add_to_binstats(sftime, key, sft);
    }
    add_to_binstats(fq, key, (sfh_halos[i].sfr < 1e-11*sfh_halos[i].sm) ? 1 : 0);
    add_to_binstats(fq_qtime, key, qt);
    add_to_binstats(sm10, key, a10);
    add_to_binstats(sm50, key, a50);
    add_to_binstats(sm90, key, a90);
    double icl_frac = (sfh_halos[i].sm > 0) ? (sfh_halos[i].icl / sfh_halos[i].sm) : 0;
    add_to_binstats(iclstats, key, icl_frac);
  }

  check_mkdir("sfh_stats", 0755);
  char buffer[1024];
  snprintf(buffer, 1024, "sfh_stats/rejuv_stats_a%f_%s_%s_%s.dat", scale, 
	   bin_names[mode], mmp_names[mmp], desc);
  FILE *out = check_fopen(buffer, "w");
  calc_median(reps);
  calc_median(qd);
  fprintf(out, "#%s F_rejuv <Rejuv> AvgErr Median_Rejuv 1Sig+ 1Sig- <Q_duty_cycle> AvgErr Median_Q_duty_cycle 1Sig+ 1Sig- Counts\n", bin_names[mode]);
  for (i=0; i<fr->num_bins; i++) {
    if (!fr->counts[i]) continue;
    print_keyname(fr, i, out);
    fprintf(out, " %g", fr->avg[i]);
    print_5stats(reps, i, out);
    print_5stats(qd, i, out);
    fprintf(out, " %"PRId64"\n", fr->counts[i]);
  }
  fclose(out);

  snprintf(buffer, 1024, "sfh_stats/rejuv_times_a%f_%s_%s_%s.dat", scale, 
	   bin_names[mode], mmp_names[mmp], desc);
  out = check_fopen(buffer, "w");
  calc_median(qtime);
  calc_median(sftime);
  fprintf(out, "#Only galaxies that have rejuvenated at least once and are currently obs. star-forming\n");
  fprintf(out, "#%s <QTime> AvgErr Median_QTime 1Sig+ 1Sig- <SFTime> AvgErr Median_SFTime 1Sig+ 1Sig- Counts\n", bin_names[mode]);
  for (i=0; i<qtime->num_bins; i++) {
    if (!qtime->counts[i]) continue;
    print_keyname(qtime, i, out);
    print_5stats(qtime, i, out);
    print_5stats(sftime, i, out);
    fprintf(out, " %"PRId64"\n", qtime->counts[i]);
  }
  fclose(out);

  snprintf(buffer, 1024, "sfh_stats/icl_stats_a%f_%s_%s.dat", scale, 
	   bin_names[mode], desc);
  out = check_fopen(buffer, "w");
  fprintf(out, "#Scale Median(ICL/SM) +1sig -1sig\n");
  print_medians(iclstats, out);
  fclose(out);

  snprintf(buffer, 1024, "sfh_stats/merger_stats_a%f_%s_%s.dat", scale, 
	   bin_names[mode], desc);
  out = check_fopen(buffer, "w");
  fprintf(out, "#%s Median(SM_merged/SM_tot) +1sig -1sig\n", bin_names[mode]);
  print_medians(mergers, out);
  fclose(out);

  snprintf(buffer, 1024, "sfh_stats/formation_stats_a%f_%s_%s_%s.dat", scale, 
	   bin_names[mode], mmp_names[mmp], desc);
  out = check_fopen(buffer, "w");
  calc_median(fq);
  calc_median(fq_qtime);
  calc_median(sm10);
  calc_median(sm50);
  calc_median(sm90);
  fprintf(out, "#For SF galaxies, QTime is the duration of the last period when they were quenched.\n");
  fprintf(out, "#%s <FQ> AvgErr <QTime> AvgErr Median_QTime 1Sig+ 1Sig- <A_SM10> AvgErr Median_A_SM10 1Sig+ 1Sig- <A_SM50> AvgErr Median_A_SM50 1Sig+ 1Sig- <A_SM90> AvgErr Median_A_SM90 1Sig+ 1Sig- Counts\n", bin_names[mode]);
  for (i=0; i<fq->num_bins; i++) {
    if (!fq->counts[i]) continue;
    print_keyname(fq, i, out);
    fprintf(out, " %g %g", fq->avg[i], fq->avgsd[i]);
    print_5stats(fq_qtime, i, out);
    print_5stats(sm10, i, out);
    print_5stats(sm50, i, out);
    print_5stats(sm90, i, out);
    fprintf(out, " %"PRId64"\n", fq->counts[i]);
  }
  fclose(out);

  free_binstats(fr);
  free_binstats(reps);
  free_binstats(qd);
  free_binstats(qtime);
  free_binstats(sftime);
  free_binstats(iclstats);
  free_binstats(mergers);

  free_binstats(fq);
  free_binstats(fq_qtime);
  free_binstats(sm10);
  free_binstats(sm50);
  free_binstats(sm90);
}

void do_bins(struct sfh_halo *sfh_halos, int64_t num_sfh_halos, char *desc) {
  int64_t i, j;
  for (i=0; i<4; i++) {
    sort_by_mode(sfh_halos, num_sfh_halos, i);
    for (j=0; j<2; j++) {
#ifndef SHORT_ANALYZE
      _do_bins(sfh_halos, num_sfh_halos, desc, i, j);
#endif /*SHORT_ANALYZE*/
      calc_rejuvenation(sfh_halos, num_sfh_halos, desc, i, j);
    }
  }
}

int main(int argc, char **argv)
{
  int64_t i;

  if (argc < 2) {
    fprintf(stdout, "Usage: %s sfh_catalog1.dat ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  init_time_table(0.307, 0.68);
  for (i=1; i<argc; i++) load_file(argv[i]);

  assert(num_scales > 0);
  int64_t nbs = HM_BINS;
  if (SM_BINS>HM_BINS) nbs = SM_BINS;
  if (V_BINS>HM_BINS) nbs = V_BINS;
  /*  check_realloc_s(bs, sizeof(struct binstats *), nbs);
  check_realloc_s(bc, sizeof(struct binned_correl *), nbs);
  for (i=0; i<nbs; i++) {*/
  bs = init_binstats(0, num_scales*NUM_HIST_VARS-1, 1, 1, 0);
  if (names) set_bin_names(bs, names);
  bc = init_binned_correl(0, num_scales-2, 1, 0, 0);
  if (bc_names) set_bin_names_correl(bc, bc_names);
  //}
  for (i=0; i<NUM_STATS; i++) final_stats[i] = init_binstats(0, nbs, 1, 1, 0);

  //All Halos
  do_bins(sh, nh, "all");

  //Q, SF
  int64_t nq=nh;
  for (i=0; i<nq; i++) {
    if (sh[i].sfr > sh[i].sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  do_bins(sh, nq, "q");
  do_bins(sh+nq, nh-nq, "sf");


  nq=nh;
  for (i=0; i<nq; i++) {
    if (sh[i].obs_sfr > sh[i].obs_sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  do_bins(sh, nq, "obs_q");
  do_bins(sh+nq, nh-nq, "obs_sf");

  //Cen, Sat
  int64_t ns = nh;
  for (i=0; i<ns; i++) {
    if (sh[i].upid < 0) {
      ns--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[ns];
      sh[ns] = tmp;
      i--;
      continue;
    }
  }
  do_bins(sh, ns, "sat");
  do_bins(sh+ns, nh-ns, "cen");

  //Sat, SF,Q
  nq = ns;
  for (i=0; i<nq; i++) {
    if (sh[i].sfr > sh[i].sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  do_bins(sh, nq, "sat_q");
  do_bins(sh+nq, ns-nq, "sat_sf");

  nq = ns;
  for (i=0; i<nq; i++) {
    if (sh[i].obs_sfr > sh[i].obs_sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  do_bins(sh, nq, "sat_obs_q");
  do_bins(sh+nq, ns-nq, "sat_obs_sf");


  //Cen, SF,Q
  nq = nh;
  for (i=ns; i<nq; i++) {
    if (sh[i].sfr > sh[i].sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  nq -= ns;
  do_bins(sh+ns, nq, "cen_q");
  do_bins(sh+ns+nq, nh-ns-nq, "cen_sf");


  nq = nh;
  for (i=ns; i<nq; i++) {
    if (sh[i].obs_sfr > sh[i].obs_sm*1e-11) {
      nq--;
      struct sfh_halo tmp = sh[i];
      sh[i] = sh[nq];
      sh[nq] = tmp;
      i--;
      continue;
    }
  }
  nq -= ns;
  do_bins(sh+ns, nq, "cen_q");
  do_bins(sh+ns+nq, nh-ns-nq, "cen_sf");
  return 0;
}




void gen_namelist(void) {
  int64_t i;
  char *types[NUM_HIST_VARS] = {"sfh", "iclh", "sm_mp", "icl_mp", "hm_mp", "sfr_mp", "ssfr_mp", "icl_ratio_mp", "sm_hm_ratio_mp", "fq_mp"};
  if (names) return;
  check_realloc_s(names, sizeof(char *), num_scales*NUM_HIST_VARS);
  check_realloc_s(bc_names, sizeof(char *), num_scales);
  names[0] = check_realloc(NULL, sizeof(char*)*(num_scales*NUM_HIST_VARS+1)*TYPE_MAX_LENGTH, "");
  bc_names[0] = check_realloc(NULL, sizeof(char*)*(num_scales+1)*TYPE_MAX_LENGTH, "");
  for (i=0; i<num_scales*NUM_HIST_VARS; i++) {
    names[i] = names[0]+i*TYPE_MAX_LENGTH;
    snprintf(names[i], TYPE_MAX_LENGTH, "%s %g", types[i/num_scales], scales[i%num_scales]);
    if (i<num_scales-1) {
      bc_names[i] = bc_names[0]+i*TYPE_MAX_LENGTH;
      snprintf(bc_names[i], TYPE_MAX_LENGTH, "%g", scale_times[num_scales-1]-scale_times[i]);
    }
  }
  names[num_scales*NUM_HIST_VARS] = NULL;
  bc_names[num_scales] = NULL;
}


void load_file(char *filename) {
  int64_t i;
  FILE *in = check_fopen(filename, "r");
  char *buffer = check_realloc(NULL, 1024*1024, "Line buffer");

  struct sfh_halo h = {0};
  enum parsetype *types = NULL;
  SHORT_PARSETYPE;

  //ID UPID X Y Z VX VY VZ Mpeak Mnow V@Mpeak Vnow Rvir Tidal_Tdyn Rank_DVmax(Z-score) Random_Rank(Z-score) SM ICL SFR Obs_SM Obs_SFR Obs_UV A_first_infall A_last_infall  
  enum parsetype first_types[HALO_FIELDS] = {D64, D64, K, K, K, K, K, K, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F, F};
  void **data = NULL;
  void *first_data[HALO_FIELDS] = {&h.id, &h.upid, NULL, NULL, NULL, NULL, NULL, NULL, &h.mp, &h.m, &h.vmp, &h.v, &h.r, &h.t_tdyn, &h.rank1, &h.rarank, &h.sm, &h.icl, &h.sfr, &h.obs_sm, &h.obs_sfr, &h.obs_uv, &h.a_first_infall, &h.a_last_infall};
  float *th = NULL;
				 
  while(fgets(buffer, 1024*1024, in)) {
    if (buffer[0]=='#') {
      if (buffer[1]=='a') {
	assert(!scale || scale==atof(buffer+5));
	scale = atof(buffer+5);
      }
      else if (buffer[1] == 'n') {
	assert(!num_scales || num_scales==atol(buffer+13));
	num_scales = atol(buffer+13);
	check_realloc_s(types, sizeof(enum parsetype), num_scales*REG_HIST_VARS+HALO_FIELDS);
	check_realloc_s(data, sizeof(void *), num_scales*REG_HIST_VARS+HALO_FIELDS);
	check_realloc_s(th, sizeof(float), num_scales*REG_HIST_VARS);
	memcpy(types, first_types, sizeof(enum parsetype)*HALO_FIELDS);
	memcpy(data, first_data, sizeof(void *)*HALO_FIELDS);
	for (i=0; i<num_scales*REG_HIST_VARS; i++) {
	  types[i+HALO_FIELDS] = F;
	  data[i+HALO_FIELDS]  = th+i;
	}
      }
      else if (buffer[1] == 's') {
	assert(num_scales > 0);
	check_realloc_s(scales, sizeof(double), num_scales);
	int64_t n = read_params(buffer+13, scales, num_scales);
	assert(n == num_scales);
	check_realloc_s(scale_times, sizeof(double), num_scales);
	for (i=0; i<num_scales; i++) scale_times[i] = exact_scale_to_years(scales[i]);
	calc_rem_sm();
	gen_namelist();
      }
      continue;
    }
    assert(num_scales > 0);
    int64_t n = stringparse(buffer, data, types, HALO_FIELDS+(num_scales*REG_HIST_VARS));
    if (n<(HALO_FIELDS+num_scales*REG_HIST_VARS)) continue;
    check_realloc_every(sh, sizeof(struct sfh_halo), nh, 10000);
    check_realloc_every(history, sizeof(float)*REG_HIST_VARS*num_scales, nh, 10000);
    sh[nh] = h;
    memcpy(history+REG_HIST_VARS*num_scales*nh, th, sizeof(float)*REG_HIST_VARS*num_scales);
    nh++;
  }
  fclose(in);
  if (buffer) free(buffer);
  if (types) free(types);
  if (data) free(data);
  if (th) free(th);

  for (i=0; i<nh; i++)
    sh[i].history = history+(i*REG_HIST_VARS*num_scales);
}

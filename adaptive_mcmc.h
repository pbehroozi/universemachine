#ifndef ADAPTIVE_MCMC_H
#define ADAPTIVE_MCMC_H
#include "sf_model.h"

void clear_stats(void);
void add_to_stats(struct sf_model_allz *a);
void stats_to_step(int64_t num_stats);
void random_step(struct sf_model_allz *a);
void init_orth_matrix(void);
void print_orth_matrix(float temp);
int64_t mcmc_fit(int64_t length, int64_t run);
void set_identity_portion(void);
void adaptive_mcmc(double *params, double *steps, void (*c2f)(struct sf_model_allz *), FILE *output);
void assert_model(struct sf_model_allz *a);

#endif /* ADAPTIVE_MCMC_H */

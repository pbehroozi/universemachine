#!/bin/bash
#SBATCH --ntasks=6
#SBATCH --ntasks-per-node=1
#SBATCH --nodes=6
#SBATCH --cpus-per-task=24
#SBATCH --mem=100GB
#SBATCH --time=100:00:00
#SBATCH --job-name=um-test
#SBATCH --qos=standard
#SBATCH --partition=standard

cd /path/to/UniverseMachine/

#Remove old config file
rm /path/to/OUTBASE/auto-um.cfg

#Start master fitting process
python3 minimize.py um_fit.cfg > fit_log.txt &

#Wait for new config file to be generated
perl -e 'sleep 1 while (!(-e "/path/to/OUTBASE/auto-um.cfg"));'

#Start one box_server process on each node.
srun /bin/bash -c './box_server /path/to/OUTBASE/auto-um.cfg >> output.dat 2>&1'


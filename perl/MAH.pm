package MAH;
use Universe::Time;
use Math::Trig;

use constant M13 => 13.2763173435874; #Mass of calibrated MAH, in Msun (no h).

sub m13_at_a {
    my $a = shift;
    my $z = 1.0/$a - 1.0;
    return (M13 + 3.0*log(1.0+$z)/log(10) - 6.11478*log(1.0+0.5*$z)/log(10) - 0.503151*$z/log(10));
}

#In log units
sub m_at_a {
    my ($m, $a) = @_;
    my $a_0 = 0.205268 - log(exp(log(10)*0.18*(9.64921-$m))+1.0)/log(10);
    my $f = ($m - M13)*(1.0+exp(-(1.0-$a_0)/0.215))/(1.0+exp(-($a-$a_0)/0.215));
    return (m13_at_a($a) + $f);
}


sub ma_rate {
    my ($m, $a) = @_;
    my $m1 = 10**(m_at_a($m, $a-0.005));
    my $m2 = 10**(m_at_a($m, $a+0.005));
    my $t1 = Universe::Time::scale_to_years($a-0.005);
    my $t2 = Universe::Time::scale_to_years($a+0.005);
    my $mar = ($m2-$m1) / ($t2-$t1);
    return ($mar);
}

sub pow {
    return ($_[0]**$_[1]);
}

sub ma_rate_avg {
    my ($m, $a) = @_;
    my $mar = ma_rate($m,$a);
    my $da = $a - 0.4;
    return $mar*(0.22*$da*$da+0.85)*pow(pow(10,m_at_a($m,$a)-12.0), $a*0.05)/pow(1.0+pow(10, (11-0.5*$a-m_at_a($m,$a))), 0.04+0.1*$a);
}

sub m_now {
    my ($a, $m0) = @_;
    my $m_at_z0 = $m0;
    for (my $k=0; $k<20; $k++) {
	$m_at_z0 += $m0-m_at_a($m_at_z0, $a);
    }
    return $m_at_z0;
}

sub ma_rate_avg_mnow {
    my ($m, $a) = @_;
    my $mnow = m_now($a, $m);
    return ma_rate_avg($mnow, $a);
}



1;

#!/usr/bin/perl -w
use lib qw(../perl);
use XmGrace;
use Common;

new XmGrace(xlabel => $rp_label,
            ylabel => $wp_label,
            legendxy => "0.8, 0.8",
            ymax => 1000,
            data => [{data => "file:primus/primus_z1.0_q", legend => "Quenched, Obs", scolor => 2, lcolor => 2, metatype => "scatter", ssize => 1, type => "xy"},
                     {data => "file[0,3]:primus/pred_z1.0", legend => "Quenched, Mock", lcolor => 2},
                     {data => "file:primus/pred_z1.0", legend => "All, Mock", lcolor => 1},
                     {data => "file:primus/primus_z1.0_sf", legend => "SF, Obs", lcolor => 3, scolor => 3, metatype => "scatter", ssize => 1, type => "xy"},
                     {data => "file[0,2]:primus/pred_z1.0", legend => "SF, Mock", lcolor => 3}])->write_pdf("AGR/primus_z1.agr");


new XmGrace(xlabel => $rp_label,
            ylabel => $wp_label,
            legendxy => "0.8, 0.8",
            ymax => 1000,
            data => [{data => "file:primus/primus_z0.5_q", legend => "Quenched, Obs", scolor => 2, lcolor => 2, metatype => "scatter", ssize => 1, type => "xy"},
                     {data => "file[0,3]:primus/pred_z0.5", legend => "Quenched, Mock", lcolor => 2},
                     {data => "file:primus/pred_z0.5", legend => "All, Mock", lcolor => 1},
                     {data => "file:primus/primus_z0.5_sf", legend => "SF, Obs", lcolor => 3, scolor => 3, metatype => "scatter", ssize => 1, type => "xy"},
                     {data => "file[0,2]:primus/pred_z0.5", legend => "SF, Mock", lcolor => 3}])->write_pdf("AGR/primus_z0.5.agr");



sub primus {
    my ($x, $y, $dy) = split(" ", $_[0]);
    $x /= 0.68;
    $y /= 0.68;
    $dy /= 0.68 if ($dy);
    return "$x $y".($dy ? " $dy" : "");
}


sub primus_corr_x {
    my ($x, $y, $dy) = split(" ", $_[0]);
    $x /= 0.68;
    my $x1 = $x*10**(-0.1);
    my $x2 = $x*10**(+0.1);
    $x = sqrt(0.5*($x1**2+$x2**2));
    $y /= 0.68;
    $dy /= 0.68 if ($dy);
    return "$x $y".($dy ? " $dy" : "");
}


new XmGrace(xlabel => $rp_label,
            ylabel => $wp_label,
            legendxy => "0.8, 0.8",
            ymax => 1000,
	    ymin => 2,
	    yscale => "Logarithmic",
	    ymajortick => 10,
	    yminorticks => 9,
            data => [{data => "file[0,3,4]:primus/primus_10.5", legend => "Quenched, Obs", scolor => 2, lcolor => 2, metatype => "scatter", ssize => 1, type => "xydy", transform => \&primus},
                     {data => "file[0,3]:primus/pred_10.5_z0.5", legend => "Quenched, Mock", lcolor => 2},
                     {data => "file:primus/pred_10.5_z0.5", legend => "All, Mock", lcolor => 1},
                     {data => "file[0,1,2]:primus/primus_10.5", legend => "SF, Obs", lcolor => 3, scolor => 3, metatype => "scatter", ssize => 1, type => "xydy", transform => \&primus},
                     {data => "file[0,2]:primus/pred_10.5_z0.5", legend => "SF, Mock", lcolor => 3}])->write_pdf("AGR/primus_10.5_z0.5.agr");


new XmGrace(xlabel => $rp_label,
            ylabel => $wp_label,
            legendxy => "0.8, 0.8",
            ymax => 1000,
	    ymin => 2,
	    yscale => "Logarithmic",
	    ymajortick => 10,
	    yminorticks => 9,
            data => [{data => "file[0,3,4]:primus/primus_10.5", legend => "Quenched, Obs", scolor => 2, lcolor => 2, metatype => "scatter", ssize => 1, type => "xydy", transform => \&primus_corr_x},
                     {data => "file[0,3]:primus/pred_newmd", legend => "Quenched, Mock", lcolor => 2, , transform => \&primus},
                     {data => "file:primus/pred_newmd", legend => "All, Mock", lcolor => 1, , transform => \&primus},
                     {data => "file[0,1,2]:primus/primus_10.5", legend => "SF, Obs", lcolor => 3, scolor => 3, metatype => "scatter", ssize => 1, type => "xydy", transform => \&primus_corr_x},
                     {data => "file[0,2]:primus/pred_newmd", legend => "SF, Mock", lcolor => 3, transform => \&primus}])->write_pdf("AGR/primus_newmd_10.5_z0.5.agr");


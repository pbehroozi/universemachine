#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "config_vars.h"
#include "config.h"
#include "version.h"

int main(int argc, char **argv) {
  int64_t i, j, new_length;
  double limits[3] = {0};
  char limit_names[3] = {'X', 'Y', 'Z'};
  if (argc<2) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s sfr_list.bin [config_file skip limit_x limit_y limit_z]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t skip = 0;
  if (argc>2) do_config(argv[2]);
  if (argc>3) skip = atol(argv[3]);
  for (i=4; i<7 && i<argc; i++) limits[i-4] = atol(argv[i]);
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV\n");
  printf("#UniverseMachine Version: %s\n", UMACHINE_VERSION);
  printf("#Columns:\n");
  printf("#ID: Unique halo ID\n");
  printf("#DescID: ID of descendant halo (or -1 at z=0).\n");
  printf("#UPID: -1 for central halos, otherwise, ID of largest parent halo\n");
  printf("#Flags: Ignore\n");
  printf("#Uparent_Dist: Ignore\n");
  printf("#X Y Z: halo position (comoving Mpc/h)\n");
  printf("#VX VY VZ: halo velocity (physical peculiar km/s)\n");
  printf("#M: Halo mass (Bryan & Norman 1998 virial mass, Msun)\n");
  printf("#V: Halo vmax (physical km/s)\n");
  printf("#MP: Halo peak historical mass (BN98 vir, Msun)\n");
  printf("#VMP: Halo vmax at the time when peak mass was reached.\n");
  printf("#R: Halo radius (BN98 vir, comoving kpc/h)\n");
  printf("#Rank1: halo rank in Delta_vmax (see UniverseMachine paper)\n");
  printf("#Rank2, RA, RARank: Ignore\n");
  printf("#SM: True stellar mass (Msun)\n");
  printf("#ICL: True intracluster stellar mass (Msun)\n");
  printf("#SFR: True star formation rate (Msun/yr)\n");
  printf("#Obs_SM: observed stellar mass, including random & systematic errors (Msun)\n");
  printf("#Obs_SFR: observed SFR, including random & systematic errors (Msun/yr)\n");
  printf("#SSFR: observed SFR/observed SM (1/yr)\n");
  printf("#SMHM: SM/HM ratio\n");
  printf("#Obs_UV: Observed UV Magnitude (M_1500 AB)\n");
  if (argc<3) {
    printf("#Assumed h = %f\n", h0);
    printf("#Assumed box size: %f Mpc/h\n", BOX_SIZE);
  } else {
    printf("#h = %f\n", h0);
    printf("#Omega_Matter = %f\n", Om);
    printf("#Omega_Lambda = %f\n", Ol);
    printf("#Box size: %f Mpc/h\n", BOX_SIZE);
    for (i=0; i<3; i++) {
      if (limits[i])
	printf("#%c limit: %g Mpc/h\n", limit_names[i], limits[i]);
    }
  }
  printf("#Note: all halo masses converted to be in Msun, not Msun/h.\n");
  printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV\n");
  for (i=skip; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    float ssfr = 0;
    if (tp->sm > 0)
      ssfr = tp->obs_sfr / tp->obs_sm;
    float smhm = tp->sm*h0 / tp->mp;
    float pos[3];
    memcpy(pos, tp->pos, sizeof(float)*3);
    for (j=0; j<3; j++) {
      while (pos[j]<0) pos[j] += BOX_SIZE;
      while (pos[j]>=BOX_SIZE) pos[j] -= BOX_SIZE;
      if (limits[j] && pos[j]>=limits[j]) break;
    }
    if (j<3) continue; //Skip halos outside limits
    
    
    if (!(tp->flags & IGNORE_FLAG))
      printf("%"PRId64" %"PRId64" %"PRId64" %"PRId32" %f %f %f %f %f %f %f %.3e %f %.3e %f %f %f %f %f %f %.3e %.3e %.3e %.3e %.3e %.3e %.3e %.3f\n",
	     tp->id, tp->descid, tp->upid, tp->flags, tp->uparent_dist, pos[0], pos[1], pos[2], tp->pos[3], tp->pos[4], tp->pos[5], tp->m/h0,
	     tp->v, tp->mp/h0, tp->vmp, tp->r, tp->rank1, tp->rank2, tp->ra, tp->rarank, tp->sm, tp->icl, tp->sfr, tp->obs_sm, tp->obs_sfr,
	     ssfr, smhm, tp->obs_uv);
  }
  munmap(p, new_length);
  return 0;
}

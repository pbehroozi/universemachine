#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <sys/resource.h>
#include "config_vars.h"
#include "check_syscalls.h"
#include "read_config.h"

void setup_config(void) {
  struct rlimit rlp;
  getrlimit(RLIMIT_NOFILE, &rlp);
  rlp.rlim_cur = rlp.rlim_max;
  setrlimit(RLIMIT_NOFILE, &rlp);
  getrlimit(RLIMIT_CORE, &rlp);
  rlp.rlim_cur = rlp.rlim_max;
  setrlimit(RLIMIT_CORE, &rlp);
}

void do_config(char *filename) {
  struct configfile c = {0};
  if (filename && strlen(filename))
    load_config(&c, filename);

#define string(a,b) a = config_to_string(&c,#a,b)
#define real(a,b) a = config_to_real(&c,#a,b)
#define real3(a,b) config_to_real3(&c,#a,a,b)
#define integer(a,b) a = config_to_real(&c,#a,b)
//#define real_array(a,__VA_ARGS__) config_to_real_array(&c,#a,a,&(a ## _LENGTH), __VA_ARGS__)
#include "config.template.h"
#undef string
#undef real
#undef real3
#undef integer
  syntax_check(&c, "[Warning]");
  setup_config();
  free_config(c);
  if (filename && strlen(filename)) {
    free(CONFIG_FILENAME);
    CONFIG_FILENAME = check_strdup(filename);
  }
}

void output_config(char *filename) {
  char buffer[1024];
  FILE *output;
  if (!filename) filename = "um.cfg";
  snprintf(buffer, 1024, "%s/%s", OUTBASE, filename);
  output = check_fopen(buffer, "w");

#define string(a,b) fprintf(output, "%s = \"%s\"\n", #a, a);
#define real(a,b) fprintf(output, "%s = %g\n", #a, a);
#define real3(a,b) fprintf(output, "%s = (%g, %g, %g)\n", #a, a[0], a[1], a[2]);
#define integer(a,b) fprintf(output, "%s = %"PRId64"\n", #a, a);
#include "config.template.h"

  fclose(output);
}

#ifndef _PLOAD_H_
#define _PLOAD_H_
#include <inttypes.h>

void clear_ploads(char *dirname);
void *pload(char *filename, char *inbase, char *outbase, int64_t id, int64_t total_ids, void *data, int64_t length, char *interface, int64_t num_blocks);

#endif /*_PLOAD_H_*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "inet/rsocket.h"
#include "inet/address.h"
#include "inet/pload.h"
#include "config_vars.h"
#include "config.h"
#include "check_syscalls.h"
#include "server.h"
#include "client.h"
#include "box_server.h"
#include "mt_rand.h"
#include "sampler.h"
#include "fitter.h"
#include "adaptive_mcmc.h"
#include "io_helpers.h"
#include "umachine_server.h"
#include "watchdog.h"
#include "jacobi_dim.h"
#include "version.h"

struct client_info *clients = NULL;
time_t time_start = 0;
int64_t s;
struct EnsembleSampler *e = NULL;
FILE *logfile = NULL;
FILE *all_step_file = NULL;
FILE *submitted_step_file = NULL;
struct sf_model_allz bestfit = {{0}};


int main(int argc, char **argv) {
  int64_t i, j;
  char buffer[1024];

  if (argc < 2) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  watchdog_clear();
  watchdog_start("umachine_server");
  clear_ploads(OUTBASE);

  snprintf(buffer, 1024, "%s/umachine_server.log", OUTBASE);
  logfile = check_fopen(buffer, "a");
  snprintf(buffer, 1024, "%s/all_step_file.dat", OUTBASE);
  all_step_file = check_fopen(buffer, "a");
  snprintf(buffer, 1024, "%s/submitted_step_file.dat", OUTBASE);
  submitted_step_file = check_fopen(buffer, "a");
  
  time_start = time(NULL);
  fprintf(logfile, "UMachine server started at %s", ctime(&time_start));
  syscall_logfile = logfile;
  set_rsocket_role("UMachine Server");

  //Calculate basic run geometry.
  if (NUM_BLOCKS < BLOCKS_PER_NODE) {
    fprintf(logfile, "[Error] NUM_BLOCKS must be >= BLOCKS_PER_NODE.\n");
    fprintf(logfile, "If NUM_BLOCKS < # of processors per node, then start multiple box servers on each node.\n");
    exit(EXIT_FAILURE);
  }
  if (NUM_BLOCKS % BLOCKS_PER_NODE) {
    fprintf(logfile, "[Warning] NUM_BLOCKS not evenly divisible by BLOCKS_PER_NODE.  Not all processors will be used.\n");
  }
  int64_t nodes_per_box = ceil(((float)NUM_BLOCKS) / ((float)BLOCKS_PER_NODE));
  if (NUM_NODES < nodes_per_box) {
    fprintf(logfile, "[Error] Not enough nodes to contain one box; make sure that NUM_NODES >= NUM_BLOCKS / BLOCKS_PER_NODE.\n");
    exit(EXIT_FAILURE);
  }
  if (NUM_NODES % nodes_per_box) {
    fprintf(logfile, "[Warning] NUM_NODES not evenly divisible by the number of nodes per box (%"PRId64").  Not all nodes will be used.\n", nodes_per_box);
  }
  TOTAL_BOXES = NUM_NODES / nodes_per_box;

  if (CATALOG_OUTPUT_MODE && TOTAL_BOXES > 1) {
    fprintf(logfile, "[Warning] CATALOG_OUTPUT_MODE set, so only one box will be generated.\n");
    TOTAL_BOXES = 1;
  }
  if ((FITTING_MODE==1 || EXTERNAL_FITTING_MODE) && TOTAL_BOXES > 1) {
    fprintf(logfile, "[Warning] FITTING_MODE=1 or EXTERNAL_FITTING_MODE set, so only one box will be used.\n");
    TOTAL_BOXES = 1;
  }

  //Init walkers
  if (CATALOG_OUTPUT_MODE) {
    NUM_WALKERS = 1;
    snprintf(buffer, 1024, "%s/catalog_output.dat", INBASE);
    e = new_sampler(400, NUM_PARAMS, STRETCH_STEP);
  } else if (FITTING_MODE) {
    NUM_WALKERS = 2;
    snprintf(buffer, 1024, "%s/fitting_position.dat", INBASE);
    e = new_sampler(400, NUM_PARAMS, STRETCH_STEP);
  } else if (POSTPROCESSING_MODE) {
    snprintf(buffer, 1024, "%s/postprocess.dat", INBASE);
    e = new_sampler((NUM_WALKERS>400) ? NUM_WALKERS : 400, NUM_PARAMS, STRETCH_STEP);
  }
  else {
    e = new_sampler(NUM_WALKERS, NUM_PARAMS, STRETCH_STEP);
    snprintf(buffer, 1024, "%s/%s", INBASE, INITIAL_POSITIONS);
  }
  e->mode = MCMC_STEP_TYPE;
  e->static_dim = (NO_SYSTEMATICS) ? 5 : 2;
  FILE *in = check_fopen(buffer, "r");
  int64_t p_read = read_positions(in, 0);
  fclose(in);
  if (p_read < NUM_WALKERS) {
    fprintf(logfile, "[Error] Number of readable lines in input file (%"PRId64") is fewer than number of walkers (%"PRId64").\n", p_read, NUM_WALKERS);
    exit(EXIT_FAILURE);
  }
  timed_output("Loaded initial walker positions.\n");

  s = setup_server_port();

  if (FORK_BOX_FROM_UMACHINE_SERVER) fork_box_from_umachine_server(argv[0]);

  timed_output("Accepting connections...\n");
  check_realloc_s(clients, sizeof(struct client_info), TOTAL_BOXES);
  memset(clients, 0, sizeof(struct client_info)*TOTAL_BOXES);

  //Accept box server connections
  char *port=NULL;
  int64_t *blocknums=NULL;
  check_realloc_s(blocknums, sizeof(int64_t), BLOCKS_PER_NODE);

  for (i=0; i<NUM_NODES; i++) {
    int client_port;
    uint64_t magic = UM_MAGIC;
    char *address=NULL;
    int64_t cs = accept_connection(s, &address, &client_port);
    if (cs < 0) { i--; continue; }
    send_to_socket_noconfirm(cs, &magic, sizeof(uint64_t));
    recv_from_socket(cs, &magic, sizeof(uint64_t));
    if (magic != UM_MAGIC) {
      fprintf(logfile, "[Error] Received invalid box server response.  Check network connectivity.\n");
      exit(EXIT_FAILURE);
    }

    int64_t box_id = i/nodes_per_box;

    if (box_id > TOTAL_BOXES) {
      int64_t type = QUIT_TYPE;
      send_to_socket_noconfirm(cs, &type, sizeof(int64_t));
      close_rsocket(cs);
      continue;
    }

    if (!(i%nodes_per_box)) {
      int64_t type = BOX_SERVER_TYPE;
      send_to_socket_noconfirm(cs, &type, sizeof(int64_t));
      send_to_socket_noconfirm(cs, address, strlen(address)+1);

      clients[box_id].cs = cs;
      clients[box_id].port = client_port;
      clients[box_id].id = box_id;
      clients[box_id].address = address;
      port = recv_msg_nolength(cs, port);
      clients[box_id].serv_port = atoll(port);
      send_to_socket_noconfirm(cs, &box_id, sizeof(int64_t));
      send_to_socket(cs, port, strlen(port)+1);
    }
    else {
      assert(port);
      int64_t type = LAUNCHER_TYPE;
      send_to_socket_noconfirm(cs, &type, sizeof(int64_t));
      send_to_socket_noconfirm(cs, clients[box_id].address, strlen(clients[box_id].address)+1);
      send_to_socket_noconfirm(cs, &box_id, sizeof(int64_t));
      send_to_socket(cs, port, strlen(port)+1);
    }

    for (j=0; j<BLOCKS_PER_NODE; j++) {
      int64_t bid = (i%nodes_per_box)*BLOCKS_PER_NODE + j;
      if (bid >= NUM_BLOCKS) bid = -1;
      blocknums[j] = bid; 
    }
    send_to_socket_noconfirm(cs, blocknums, sizeof(int64_t)*BLOCKS_PER_NODE);

    if (i%nodes_per_box) close_rsocket(cs);
  }

  timed_output("Workers loading data...\n");

  char cmd[5] = {0};
  for (i=0; i<TOTAL_BOXES; i++) {
    recv_from_socket(clients[i].cs, cmd, 4);
    if (strcmp(cmd, "rdy!")!=0) {
      fprintf(logfile, "[Error] Received invalid box server response!\n");
      exit(EXIT_FAILURE);
    }
  }

  timed_output("Ready to generate %"PRId64" universe%s simultaneously (%"PRId64" core%s).\n",
	       TOTAL_BOXES, (TOTAL_BOXES > 1) ? "s" : "", (BLOCKS_PER_NODE*NUM_NODES),
	       (BLOCKS_PER_NODE*NUM_NODES > 1) ? "s" : "");
  
  if (FITTING_MODE) {
    timed_output("Performing fit.\n");
    fitter_loop();
  } else {
    timed_output("Ready to start MCMC.\n");
    umachine_server_loop();
  }
  fclose(logfile);
  return 0;
}


void print_all_step(struct EnsembleSampler *e, struct Walker *w, int64_t step_time) {
  int64_t i;
  fprintf(all_step_file, "%"PRId64, (int64_t)(w-e->w));
  for (i=0; i<e->dim; i++) {
    fprintf(all_step_file, " %.12g", w->params[i]);
  }
  fprintf(all_step_file, " %.12g %.12g %.12g %.12g %.12g %"PRId64" %"PRId64"sec\n", w->chi2, w->accepted_chi2, w->chi2-w->accepted_chi2, w->dlnl_step, w->dlnl_step - 0.5*e->inv_temp*(w->chi2 - w->accepted_chi2), w->step_mode, step_time);
}


void print_time(FILE *output) {
  int64_t time_now = time(NULL);
  fprintf(output, "[%6"PRId64"s] ", time_now-time_start);
}


int64_t setup_server_port(void) {
  int64_t s, tries, addr_found = 0, auto_addr = 0, auto_port = 0;
#define s_address SERVER_ADDRESS
#define s_port    SERVER_PORT
#define s_iface   SERVER_INTERFACE
  if (!strcasecmp(s_address, "auto")) {
    auto_addr = 1;
    if (strlen(s_iface)) {
      s_address = get_interface_address(s_iface);
      if (s_address) addr_found = 1;
    }
    
    if (!addr_found) {
      s_address = check_realloc(NULL, 1024, "Allocating hostname.");
      if (gethostname(s_address, 1023)<0) {
        printf("Unable to get host address!\n");
        exit(1);
      }
    }
  }
  
  if (!strcasecmp(s_port, "auto")) {
    auto_port = 1;
    s_port = check_realloc(NULL, sizeof(char)*10, "Allocating port.");
    for (tries = 0; tries<500; tries++) {
      snprintf(s_port, 10, "%d", (rand()%63000)+2000);
      s = listen_at_addr(s_address, s_port);
      if (s>=0) break;
    }
  }
  else 
    s = listen_at_addr(s_address, s_port);

  if (s<0) {
    printf("Unable to start server on %s!\n", s_address);
    exit(EXIT_FAILURE);
  }
  
  output_config(AUTO_CONFIG_NAME);
  if (auto_addr) {
    free(s_address);
    s_address = "auto";
  }
  if (auto_port) {
    free(s_port);
    s_port = "auto";
  }

  return s;
}  

void output_random_step_file(char *filename) {
  int64_t i;
  FILE *out = check_fopen(filename, "w");
  struct Walker w = {0};
  check_calloc_s(w.params, sizeof(double), (e->dim+1)*2);
  for (i=0; i<100000; i++) {
    sampler_random_step(e, &w);
    for (int64_t j=0; j<e->dim; j++)
      fprintf(out, "%.12e ", w.params[j]);
    fprintf(out, "\n");
  }
  free(w.params);
  fclose(out);
}

int64_t read_positions(FILE *posfile, int64_t reset_after_hash) {
  char buffer[2048];
  rewind(posfile);
  int64_t total_read = 0;
  int64_t i = 0, steps = 0;
  sampler_init_averages(e);
  while (fgets(buffer, 2048, posfile)) {
    if (buffer[0] == '#') {
      total_read = ftello(posfile);
      if (reset_after_hash) {
	sampler_init_averages(e);
	steps = 0;
      }
      continue;
    }
    double params[NUM_PARAMS+2];
    int64_t np = 0;
    np = read_params(buffer, params, NUM_PARAMS+2);
    if (np < (NUM_PARAMS+1)) break;
    if (steps >= NUM_WALKERS) {
      if (POSTPROCESSING_MODE) break;
      for (int64_t j=0; j<e->dim; j++) {
	e->avg[j] += e->w[i].params[j];
	for (int64_t k=0; k<e->dim; k++)
	  e->cov_matrix[j*e->dim + k] += e->w[i].params[j]*e->w[i].params[k];
      }
      e->avg_steps++;
    }
    init_walker_position(e->w+i, params);
    e->w[i].accepted_chi2 = (np==(NUM_PARAMS+1)) ? params[NUM_PARAMS] : params[NUM_PARAMS+1];
    if (e->w[i].accepted_chi2 > -1 && (e->w[i].accepted_chi2 < CHI2(bestfit) || CHI2(bestfit)==0)) {
      memcpy(bestfit.params, params, sizeof(double)*NUM_PARAMS);
      CHI2(bestfit) = e->w[i].accepted_chi2;
    }
    steps++;
    i = (i+1)%e->num_walkers;
    total_read = ftello(posfile);
  }
  check_fseeko(posfile, total_read, 0);
  if (e->avg_steps)
    for (i=0; i<e->dim; i++) e->avg[i] /= (double)e->avg_steps;
  timed_output("Primed MCMC covariance matrix using %"PRId64"/%"PRId64" steps.\n", e->avg_steps, steps);

  sampler_stats_to_step(e);
  snprintf(buffer, 2048, "%s/initial_cov_matrix_steps.dat", OUTBASE);
  output_random_step_file(buffer);
  //printf("Walker 0; params: %f %f ...\n", e->w[0].params[0], e->w[0].params[1]);
  return steps;
}


void get_chi2(struct sf_model_allz *m) {
  int64_t i;
  for (i=0; i<TOTAL_BOXES; i++) {
    INVALID(m[i]) = 0;
    CHI2(m[i]) = -1;
    if (test_invalid(m[i])) { INVALID(m[i]) = 1; CHI2(m[i]) = 1e30; continue; }
    send_to_socket_noconfirm(clients[i].cs, m+i, sizeof(struct sf_model_allz));
  }
  for (i=0; i<TOTAL_BOXES; i++) {
    double chi2 = -1;
    if (INVALID(m[i])) continue;
    recv_from_socket(clients[i].cs, &chi2, sizeof(double));
    if (chi2 < 0 || !isfinite(chi2)) CHI2(m[i]) = 1e30;
    else CHI2(m[i]) = chi2;
  }
}

void recalc_eigenvalues(void) {
  struct sf_model_allz *trials = NULL;
  int64_t trials_size = NUM_PARAMS+1;
  if (trials_size % TOTAL_BOXES)
    trials_size += (TOTAL_BOXES - (trials_size % TOTAL_BOXES));
  check_realloc_s(trials, sizeof(struct sf_model_allz), trials_size);
  
  int64_t i;
  for (i=0; i<trials_size; i++) memcpy(trials[i].params, bestfit.params, sizeof(double)*NUM_PARAMS);
  for (i=0; i<NUM_PARAMS; i++)
    vector_madd_dim(trials[i].params, e->eigenvalues[i], e->orth_matrix+(i*NUM_PARAMS), NUM_PARAMS);

  for (i=0; i<(trials_size / TOTAL_BOXES); i++)
    get_chi2(trials+i*TOTAL_BOXES);
  free(trials);

  for (i=0; i<NUM_PARAMS; i++) {
    if (!(e->eigenvalues[i]>0)) continue;
    if (CHI2(trials[i])<0 || CHI2(trials[i])>=1e20) continue;
    double new_eig = e->eigenvalues[i]/sqrt(fabs(CHI2(trials[i]) - CHI2(trials[NUM_PARAMS])));
    timed_output("Updated eigenvalue %"PRId64" from %g to %g.\n", i, e->eigenvalues[i], new_eig);
    e->eigenvalues[i] = new_eig;
  }

  char buffer[1024];
  snprintf(buffer, 1024, "%s/recalc_cov_matrix_steps.dat", OUTBASE);
  output_random_step_file(buffer);
}


void fitter_loop(void) {
  char buffer[2048];
  FILE *output;
  if (EXTERNAL_FITTING_MODE) {
    while (fgets(buffer, 2048, stdin)) {
      struct sf_model_allz m = {{0}};
      read_params(buffer, m.params, NUM_PARAMS);
      get_chi2(&m);
      fprintf(stdout, "%g\n", CHI2(m));
      fflush(stdout);
    }
  }
  else {
    sprintf(buffer, "%s/initial_fit.dat", OUTBASE);
    output = check_fopen(buffer, "w");
    if (FITTING_MODE==2)
      adaptive_mcmc(e->w[0].params,e->w[1].params,&get_chi2, output);
    else
      initial_fit(e->w[0].params,e->w[1].params,&get_chi2, output);
    fclose(output);
    watchdog_killall();
  }
}



int64_t send_next_walker(int64_t i) {
  int64_t steps = 0;
  struct sf_model_allz m;
  memset(&m, 0, sizeof(struct sf_model_allz));
  while (1) {
    struct Walker *w = get_next_walker(e);
    if (!w) return steps;
    steps++;
    memcpy(m.params, w->params, sizeof(double)*e->dim);
    assert_model(&m);
    memcpy(w->params, m.params, sizeof(double)*e->dim);
    INVALID(m) = 0;
    CHI2(m) = 0;
    //int64_t reason = 0;
    if (test_invalid(m)) { //((reason = test_invalid(m))) {
      //fprintf(stderr, "[Warning] Skipped invalid mcmc step (%f %f...); reason %"PRId64".\n", m.params[0], m.params[1], reason);
      update_walker(e, w, -1);
      print_all_step(e, w, 0);
      continue;
    }
    clients[i].walker = w;
    send_to_socket_noconfirm(clients[i].cs, &m, sizeof(struct sf_model_allz));
    clients[i].sent_time = time(NULL);
    fprintf(submitted_step_file, "%"PRId64" %"PRId64" ", i, clients[i].id);
    for (int64_t i=0; i<NUM_PARAMS; i++)
      fprintf(submitted_step_file, "%lf ", m.params[i]);
    fprintf(submitted_step_file, "\n");
    break;
  }
  return steps;
}


#define BURN_IN_STAGES 5
void umachine_server_loop(void) {
  int64_t i;
  int64_t steps = 0, max_steps = 0, received = 0;
  float inv_temps[BURN_IN_STAGES] = {0.1, 0.2, 0.3, 0.5, 0.8};
  char buffer[1024];

  struct sf_model_allz m;
  memset(&m, 0, sizeof(struct sf_model_allz));

  if (BURN_IN_STAGE < BURN_IN_STAGES)
    snprintf(buffer, 1024, "%s/burn_in_steps.dat", OUTBASE);
  else
    snprintf(buffer, 1024, "%s/mcmc_steps.dat", OUTBASE);

  char *mode = (BURN_IN_STAGE > -1) ? "r+" : "w";
  FILE *out = check_fopen(buffer, mode);

  if (BURN_IN_STAGE > -1) {
    steps = read_positions(out, 1);
    timed_output("Read %"PRId64" steps from previous run...\n", steps);
    int64_t file_offset = ftello(out);
    timed_output("Resuming output at byte %"PRId64" in file %s.\n", file_offset, buffer);
    if (RECALC_EIGENVALUES) {
      recalc_eigenvalues();
      e->freeze_cov_matrix = 1;
    }
  }

  do {
    if (steps >= max_steps) {
      if (BURN_IN_STAGE < BURN_IN_STAGES) {
	if (max_steps != 0 || BURN_IN_STAGE < 0) {
	  BURN_IN_STAGE++;
	  sampler_reset_averages(e);
	  steps = 0;
	}
	if (BURN_IN_STAGE < BURN_IN_STAGES) {
	  e->inv_temp = inv_temps[BURN_IN_STAGE];
	  max_steps = e->inv_temp * BURN_IN_LENGTH;
	  if (steps==0) fprintf(out, "#Temp now: %f\n", 1.0/e->inv_temp);
	}
      }
      if (BURN_IN_STAGE == BURN_IN_STAGES) {
	if (max_steps != 0) {
	  if (steps >= max_steps) break; //Done
	  fclose(out);
	  snprintf(buffer, 1024, "%s/mcmc_steps.dat", OUTBASE);
	  out = check_fopen(buffer, "w");
	}
	e->inv_temp = 1.0;
	max_steps = MCMC_LENGTH;
      }
      timed_output("MCMC Stage %"PRId64"...\n", BURN_IN_STAGE);
      if (!CATALOG_OUTPUT_MODE) output_config("restart.cfg");
    }

    //Blast out trials
    for (i=0; i<TOTAL_BOXES; i++) steps += send_next_walker(i);
    fflush(submitted_step_file);
    if (!CATALOG_OUTPUT_MODE) {
      while (!ensemble_ready(e)) {
	//Wait for box server response
	clear_rsocket_tags();
	for (i=0; i<TOTAL_BOXES; i++) tag_rsocket(clients[i].cs);
	select_rsocket(RSOCKET_READ, 0);
	for (i=0; i<TOTAL_BOXES; i++) {
	  if (!check_rsocket_tag(clients[i].cs)) continue;
	  double chi2 = -1;
	  recv_from_socket(clients[i].cs, &chi2, sizeof(double));
	  received++;
	  if (POSTPROCESSING_MODE && (received==NUM_WALKERS)) watchdog_killall();
	  update_walker(e, (struct Walker *)clients[i].walker, chi2);
	  print_all_step(e, clients[i].walker, time(NULL)-clients[i].sent_time);
	  steps += send_next_walker(i);
	}
      }
      update_ensemble(e);
      write_accepted_steps_to_file(e, out, 0); //ASCII for now
      fflush(all_step_file);
      timed_output("Completed %"PRId64" steps.\n", steps);
      if (PERFORMANCE_TEST) {
	PERFORMANCE_TEST--;
	if (!PERFORMANCE_TEST) watchdog_killall();
      }
    } else {
      double chi2 = -1;
      recv_from_socket(clients[0].cs, &chi2, sizeof(double));
      break;
    }
  } while (1);

  fclose(out);

  watchdog_killall();
}


void fork_box_from_umachine_server(char *umachine_server) {
  char buffer[1024];
  //Attempt to find box_server executable
  char *box_server_path = check_strdup(umachine_server);
  int64_t len = strlen(box_server_path);
  len -= strlen("umachine_server");
  if (len < 0 || strcmp("umachine_server", box_server_path+len)!=0) {
    fprintf(logfile, "[Error] Unable to fork box_server from umachine_server; \"umachine_server\" not found at end of executable name (\"%s\").\n", umachine_server);
    exit(EXIT_FAILURE);
  }

  strcpy(box_server_path+len, "box_server");
  struct stat st = {0};
  if (stat(box_server_path, &st)<0) {
    fprintf(logfile, "[Error] Unable to fork box_server from umachine_server; unable to check permissions of box_server at path \"%s\".\n", box_server_path);
    exit(EXIT_FAILURE);
  }
  
  if (!(st.st_mode & S_IXUSR)) {
    fprintf(logfile, "[Error] Unable to fork box_server from umachine_server; box_server is not executable at path \"%s\".\n", box_server_path);
    exit(EXIT_FAILURE);
  }
  
  pid_t pid = fork();
  if (pid < 0) {
    fprintf(logfile, "[Error] Unable to fork box_server from umachine_server.\n");
    exit(EXIT_FAILURE);
  }
  if (pid > 0) return;

  snprintf(buffer, 1024, "%s/%s", OUTBASE, AUTO_CONFIG_NAME);
  if (execl(box_server_path, box_server_path, buffer, (char *)NULL) < 0) {
    fprintf(logfile, "[Error] Unable to fork box_server from umachine_server.\n");
    exit(EXIT_FAILURE);
  }
}

#ifndef _TRAPEZOID_PRISM_H_
#define _TRAPEZOID_PRISM_H_

void trapezoid_prism(float x[8], float y[8], float z[8], float step,
		     void (*cb)(int, int, int, void*), void *extra);

#endif /* _TRAPEZOID_PRISM_H_ */


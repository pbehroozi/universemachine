#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include "sampler.h"
#include "check_syscalls.h"
#include "mt_rand.h"
#include "fit_smhm.h"
#include "make_sf_catalog.h"

#define TOL 0.03
#define NUM_STEPS 500000


void smhm_run_sampler(struct smhm_data *sd, int64_t num_sd, float *best_fit_params, float *best_fit, float *median);

struct smhm {
  double m_1, alpha, beta, delta, sm_0, gamma;
};

double smhm_exp10(double x) {
  double a = exp(M_LN10*x);
  return a;
}


struct smhm smhm_at_z(double z, double *f) {
  struct smhm c;
  double a = 1.0/(1.0+z);
  double a1 = a-1.0;
  double lna = log(a);

  c.m_1 = SMHM_M_1(f) + a1*SMHM_M_1_A(f) - lna*SMHM_M_1_A2(f) + z*SMHM_M_1_Z(f);
  c.sm_0 = c.m_1 + SMHM_EFF_0(f) + a1*SMHM_EFF_0_A(f) - lna*SMHM_EFF_0_A2(f) + z*SMHM_EFF_0_Z(f);
  c.alpha = SMHM_ALPHA(f) + a1*SMHM_ALPHA_A(f) - lna*SMHM_ALPHA_A2(f) + z*SMHM_ALPHA_Z(f);
  c.beta = SMHM_BETA(f) + a1*SMHM_BETA_A(f) + z*SMHM_BETA_Z(f);
  c.delta = SMHM_DELTA(f);
  c.gamma = pow(10, SMHM_GAMMA(f) + a1*SMHM_GAMMA_A(f) + z*SMHM_GAMMA_Z(f));
  return c;
}

double smhm_calc_sm_at_m(double m, struct smhm c) {
  double dm = m-c.m_1;
  double dm2 = dm/c.delta;
  double sm = c.sm_0 - log10(smhm_exp10(-c.alpha*dm) + smhm_exp10(-c.beta*dm)) + c.gamma*exp(-0.5*(dm2*dm2));
  return sm;
}

double smhm_calc_chi2(double *params, struct smhm_data *sd, int64_t num_sd) {
  int64_t i;
  double last_z = -1, chi2=0;
  struct smhm c = {0};

  for (i=0; i<num_sd; i++) {
    if (sd[i].z != last_z) {
      last_z = sd[i].z;
      c = smhm_at_z(sd[i].z, params);
      if (c.delta < 0) return 1e30;
    }
    double err = smhm_calc_sm_at_m(sd[i].m, c) - sd[i].sm;
    if (fabs(err) < TOL) err = 0;
    else err = fabs(err) - TOL;
    err /= sd[i].err;
    chi2 += err*err;
  }
  return chi2;
}

void save_bestfit(double *params, float *best_fit, float *median) {
  int64_t n, m;
  struct smhm c = {0};

  memset(best_fit, 0, sizeof(float)*SMHM_NUM_ZS*SMHM_FIT_NBINS);
  for (n=0; n<SMHM_NUM_ZS; n++) {
    double z = 1.0/smhm_scales[n]-1.0;
    c = smhm_at_z(z, params);
    for (m=0; m<SMHM_FIT_NBINS; m++) {
      int64_t bin = n*SMHM_FIT_NBINS+m;
      if (!median[bin]) continue;
      double mh = SMHM_HM_MIN+((double)m+0.5) / (double)SMHM_FIT_BPDEX;
      best_fit[bin] = smhm_calc_sm_at_m(mh, c) - mh;
    }
  }
}


/*
Eff: -1.777 + (-0.006*(a-1) + -0.000*z)*nu + -0.119*(a-1)
nu: exp(-4.000*a^2)
M_h: 11.514 + (-1.793*(a-1) + -0.251*z)*nu
Alpha: -1.412 + (0.731*(a-1) + 0.000*z)*nu
Delta: 3.508 + (2.608*(a-1) + -0.043*z)*nu
Gamma: 0.316 + (1.319*(a-1) + 0.279*z)*nu
*/

void smhm_run_sampler(struct smhm_data *sd, int64_t num_sd, float *best_fit_params, float *best_fit, float *median) {
  int64_t i, j;
  double initial_params[SMHM_FIT_PARAMS] = {-1.25109, 0.983814, 0.127493, 0.13463, 12.0271, 2.16933, 1.81275, -0.140366, 2.27961, -0.956498, -0.68459, 0.00603781, 0.387591, -0.140332, -0.0687521, 0.45946, -1.21011, -1.02497, -0.456822};

  double best_params[SMHM_FIT_PARAMS] = {0}, best_chi2 = -1;

  struct EnsembleSampler *e = new_sampler(100, SMHM_FIT_PARAMS, 1.7);

  for (i=0; i<num_sd; i++)
    if (sd[i].err < TOL) sd[i].err = TOL;

  for (i=0; i<e->num_walkers; i++) {
    double pos[SMHM_FIT_PARAMS];
    for (j=0; j<SMHM_FIT_PARAMS; j++) pos[j] = initial_params[j]+dr250()*0.01;
    init_walker_position(e->w+i, pos);
  }
  
  for (i=0; i<NUM_STEPS; i++) {
    struct Walker *w = get_next_walker(e);
    double chi2 = smhm_calc_chi2(w->params, sd, num_sd);
    if (best_chi2 < 0 || chi2 < best_chi2) {
      memcpy(best_params, w->params, sizeof(double)*SMHM_FIT_PARAMS);
      best_chi2 = chi2;
    }
    update_walker(e, w, chi2);
    if (ensemble_ready(e))
      update_ensemble(e);
  }
  free_sampler(e);
  for (i=0; i<SMHM_FIT_PARAMS; i++)
    best_fit_params[i] = best_params[i];
  best_fit_params[i] = best_chi2;
  
  save_bestfit(best_params, best_fit, median);
}

struct background_smhm fork_smhm_sampler(struct smhm_data *sd, int64_t num_sd, float *best_fit_params, float *best_fit, float *median) {
  struct background_smhm bg = {{0}};
  check_pipe(bg.filedes);
  bg.best_fit_params = best_fit_params;
  bg.best_fit = best_fit;
  if (!(bg.pid = check_fork())) { //Child
    close(bg.filedes[0]);
    smhm_run_sampler(sd, num_sd, best_fit_params, best_fit, median);
    FILE *out = check_fdopen(bg.filedes[1], "w");
    check_fwrite(best_fit_params, sizeof(float), (SMHM_FIT_PARAMS+1), out);
    check_fwrite(best_fit, sizeof(float), SMHM_NUM_ZS*SMHM_FIT_NBINS, out);
    fclose(out); //Also closes file descriptor
    exit(EXIT_SUCCESS);
  }
  return bg;
}

void smhm_sampler_result(struct background_smhm bg) {
  close(bg.filedes[1]);
  FILE *in = check_fdopen(bg.filedes[0], "r");
  check_fread(bg.best_fit_params, sizeof(float), (SMHM_FIT_PARAMS+1), in);
  check_fread(bg.best_fit, sizeof(float), SMHM_NUM_ZS*SMHM_FIT_NBINS, in);
  fclose(in);
  check_waitpid(bg.pid);
}

#ifndef _IO_HELPERS_H_
#define _IO_HELPERS_H_
#include <inttypes.h>

int64_t read_params(char *buffer, double *params, int max_n);
int64_t read_params_from_file(char *fn, double *params, int max_n);

#endif /* _IO_HELPERS_H_ */

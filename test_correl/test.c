#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include "../stats.h"
#include "../mt_rand.h"


int main(void) {
  double r, rest;
  int64_t samples, i;
  printf("Enter samples: ");
  scanf("%"SCNd64, &samples);
  printf("Enter rho: ");
  scanf("%lf", &r);

  rest = sqrt(1.0-r*r);

  r250_init(87L);
  struct correl_stats *cs = init_correl(1000);
  for (i=0; i<samples; i++) {
    double x = normal_random(0, 1);
    double y = r*x + rest*normal_random(0, 1);
    add_to_correl(cs, x, y);
  }
  print_rank_correl(cs, stdout);
  return 0;
}

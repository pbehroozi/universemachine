#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/weak_lensing";
our $plotdir = "$dir/graphs/weak_lensing";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0, 0.1, 0.5, 1, 2);
my @scales = get_scales_at_redshifts("$datadir", "wl", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
my @sm = qw(9.80 10.20 10.60);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

for my $a (@scales) {
    for (0..$#sm) {
	my $sm = $sm[$_];
	my $dcol = $_*2+1;
	my $ecol = $_*2+2;

	my $fn = sprintf("wl_sm%.1f_z%g", $sm, $zs{$a});
	$fn =~ tr/./_/;
	my $sm_text = sprintf('M\s*\N > 10\S%g\N M%s', $sm, $sun_txt);
	new XmGrace(xlabel => $rp_label,
		    ylabel => $dsigma_label,
		    xmin => 0.1, xmax => 2,
		    xmajortick => 10,
		    xminorticks => 9,
		    xscale => "Logarithmic",
		    yscale => "Logarithmic",
		    ymin => 0.3,
		    ymax => 50,
		    ymajortick => 10,
		    yminorticks => 9,
		    legendxy => "0.8, 0.8",
		    color_errorstrip => 1,
		    transparent_colors => $hq,
		    text => [{text => "z = $zs{$a}\\n".$sm_text,  pos => "0.3, 0.23"}],
		    data => [{data => "file[0,7,8,9]:$datadir/wl_sm${sm}_a$a.dat", metatype => "errorstrip", fcolor => 18},
			     {data => "file[0,1,2,3]:$datadir/wl_sm${sm}_a$a.dat", metatype => "errorstrip", fcolor => 17},
			     {data => "file[0,4,5,6]:$datadir/wl_sm${sm}_a$a.dat", metatype => "errorstrip", fcolor => 19},
			     {data => "file[0,7,8,9]:$datadir/wl_sm${sm}_a$a.dat", lcolor => 2, legend => "Quenched",}, 
			     {data => "file[0,1,2,3]:$datadir/wl_sm${sm}_a$a.dat", lcolor => 1, legend => "All",}, 
			     {data => "file[0,4,5,6]:$datadir/wl_sm${sm}_a$a.dat", lcolor => 3, legend => "Star-Forming"}])->write_pdf("$plotdir/AGR/$fn.agr");

	if (abs($zs{$a}-0.1)< 0.01) {
	    my $fn = sprintf("wl_obs_sm%.1f_z%g", $sm, $zs{$a});
	    $fn =~ tr/./_/;
	    new XmGrace(xlabel => $rp_label,
			ylabel => $dsigma_label,
			xmin => 0.1, xmax => 2,
			xmajortick => 10,
			xminorticks => 9,
			xscale => "Logarithmic",
			yscale => "Logarithmic",
			ymin => 0.3,
			ymax => 80,
			ymajortick => 10,
			yminorticks => 9,
			legendxy => "0.8, 0.84",
			color_errorstrip => 1,
			transparent_colors => $hq,
			text => [{text => $sm_text,  pos => "0.28, 0.78"}],
			data => [{data => "file[0,7,8,9]:$datadir/wl_sm${sm}_a$a.dat", metatype => "errorstrip", fcolor => 18},
				 {data => "file[0,4,5,6]:$datadir/wl_sm${sm}_a$a.dat", metatype => "errorstrip", fcolor => 19},
				 {data => "file[0,$dcol,$ecol]:$obsdir/lensing/watson15/watson_lensing_q.dat", type => "xydy", metatype => "scatter", ssize => 1, scolor => 2, lcolor => 2, legend => "Quenched", transform => \&transform_doug},
				 {data => "file[0,$dcol,$ecol]:$obsdir/lensing/watson15/watson_lensing_sf.dat", type => "xydy", metatype => "scatter", ssize => 1, scolor => 3, lcolor => 3, legend => "Star-Forming", transform => \&transform_doug},
				 {data => "file[0,7,8,9]:$datadir/wl_sm${sm}_a$a.dat", lcolor => 2}, 
				 {data => "0 0", lcolor => 1, legend => "Model Predictions",}, 
				 {data => "file[0,4,5,6]:$datadir/wl_sm${sm}_a$a.dat", lcolor => 3}])->write_pdf("$plotdir/AGR/$fn.agr");
	}
    }
}

@scales = get_scales_at_redshifts("$datadir", "wl", 0, 1, 2);
@types = qw(all sf q);
@labels = ("All Galaxies", "Star-Forming Galaxies", "Quenched Galaxies");
for (0..$#sm) {
    my $sm = $sm[$_];
    for (0..$#types) {
	my $type = $types[$_];
	my $offset = 1+3*$_;
	my $color = (1,3,2)[$_];
	my $label = '\v{0.5}'.$labels[$_];
	my $columns = join(",", 0, (map {$_+$offset} (0..2)));
	my $fn = sprintf("wl_cross_redshift_sm%.1f_%s", $sm, $type);
	$fn =~ tr/./_/;
	my $sm_text = sprintf('M\s*\N > 10\S%g\N M%s', $sm, $sun_txt);
	new XmGrace(xlabel => $rp_label,
		    ylabel => $dsigma_label,
		    xmin => 0.1, xmax => 2,
		    xmajortick => 10,
		    xminorticks => 9,
		    xscale => "Logarithmic",
		    yscale => "Logarithmic",
		    ymin => 0.3,
		    ymax => 50,
		    ymajortick => 10,
		    yminorticks => 9,
		    legendxy => "0.95, 0.8",
		    transparent_colors => $hq,
		    fading_colors => 1,
		    text => [{text => $label,  pos => "1.05, 0.78", just => 2},
			     {text => $sm_text,  pos => "0.3, 0.2"},
			     {text => "Model Predictions", pos => "0.3, 0.8"}
		    ],
		    data => [(map { {data => "file[$columns]:$datadir/wl_sm${sm}_a$scales[$_].dat", metatype => "errorstrip", fcolor => $color+6*($_+1)} } (0..$#scales)),
			     (map { {data => "file[$columns]:$datadir/wl_sm${sm}_a$scales[$_].dat", lcolor => $color+6*$_, legend => "z = $zs{$scales[$_]}"}}  (0..$#scales))])->write_pdf("$plotdir/AGR/$fn.agr");
    }

    my $fn = sprintf("wl_cross_redshift_ratios_sm%.1f", $sm);
    $fn =~ tr/./_/;
    my $sm_text = sprintf('M\s*\N > 10\S%g\N M%s', $sm, $sun_txt);
    new XmGrace(xlabel => $rp_label,
		ylabel => '\xDS\f{}\sQ|SF\N(R\sp\N) / \xDS\f{}\sAll\N(R\sp\N)',
		legend2xy => "0.7, 0.83",
		l2vgap => 0.045,
		legendxy => "0.95, 0.5",
		xmin => 0.1, xmax => 2,
		xmajortick => 10,
		xminorticks => 9,
		xscale => "Logarithmic",
		XmGrace::linscale('y', 0.3, 2.1, 0.2, 1),
		transparent_colors => $hq,
		fading_colors => 1,
		text => [{text => $sm_text,  pos => "0.3, 0.2"},
			 {text => "Model Predictions", pos => "0.3, 0.8"}
		    ],
		data => [(map { {data => "file[0,7,8,9]:$datadir/wl_ratios_sm${sm}_a$scales[$_].dat", metatype => "errorstrip", fcolor => 2+6*($_+1)} } (0..$#scales)),
			 (map { {data => "file[0,4,5,6]:$datadir/wl_ratios_sm${sm}_a$scales[$_].dat", metatype => "errorstrip", fcolor => 3+6*($_+1)} } (0..$#scales)),
			 (map { {data => "file[0,7,8,9]:$datadir/wl_ratios_sm${sm}_a$scales[$_].dat", lcolor => 2+6*$_}} (0..$#scales)),
			 (map { {data => "file[0,4,5,6]:$datadir/wl_ratios_sm${sm}_a$scales[$_].dat", lcolor => 3+6*$_}} (0..$#scales)),
			 (map { { data => "1e5 1", legend => "z = $zs{$scales[$_]}", lcolor => 1+6*$_ } } (0..$#scales)),
			 (map { { data => "1e5 1", legend2 => $labels[$_], lcolor => (4-$_) } } (2,1)),
			 
		])->write_pdf("$plotdir/AGR/$fn.agr");
}
    
MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/wl_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}



    
sub transform_doug {
    my (@a) = split(" ", $_[0]);
    $a[0]/=1e3;
    $a[2] =~ tr/\(\)//d;
    $a[0] *= 0.7/0.68;
    $a[1] *= 0.7/0.68;
    $a[2] *= 0.7/0.68;

#    $a[0]/=0.68;
#    $a[1]/=0.68;
#    $a[1] *= 0.7;
#    $a[2] *= 0.7;
    return "@a";
}


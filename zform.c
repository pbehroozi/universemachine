#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "inthash.h"

struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused

struct catalog_halo *halos = NULL;
int64_t *lengths = NULL;
int64_t *offsets = NULL;
int64_t num_snaps = 0;
float *scales = NULL;


double zform(float *mah, int64_t max_index) {
  int64_t i=max_index;
  for (; i>=0; i--) {
    if (mah[i]<0.5*mah[max_index]) {
      assert(i<max_index);
      double a_half = scales[i] + (scales[i+1]-scales[i])*(0.5*mah[max_index]-mah[i])/(mah[i+1]-mah[i]);
      return (1.0/a_half-1.0);
    }
  }
  return (1.0/scales[0]-1.0);
}

void translate_descids(void) {
  int64_t i, j, k, lnod; //Last non-orphan descendant
  for (i=0; i<num_snaps-1; i++) {
    k=offsets[i+1];
    for (j=offsets[i]; j<offsets[i+1];) {
      int64_t last_j = j;
      assert(k<offsets[i+2]);

      //MMP is always first
      if (halos[j].descid == halos[k].id) halos[j].flags |= MMP_FLAG;
      lnod = k;
      assert(!(halos[lnod].flags & ORPHAN_FLAG)); //First halo should never be an orphan

      //Next should be orphan->orphan, non-mmp mergers, and non-mmp->orphan
      //Ordering of types is not guaranteed, but ordering of orphan descendants *is* guaranteed
      for (; j<offsets[i+1]; j++) {
	if (halos[j].descid == halos[lnod].id) { //non-mmp or orphan merger
	  halos[j].descid = lnod;
	} else {
	  if (k+1 == offsets[i+2]) break;
	  if (!(halos[k+1].flags & ORPHAN_FLAG)) break;
	  k++;
	  assert(halos[j].descid == halos[k].id); //Make sure orphan descendants are ordered
	  assert(halos[k].upid == halos[lnod].id); //Make sure orphan is attached to right descendant
	  halos[j].descid = k;
	}
      }

      if (last_j==j) halos[k].flags |= NO_PROGENITOR_FLAG;
      //else halos[j-1].flags |= LAST_PROGENITOR_FLAG; //No longer used.
      k++;
    }
    assert(j==offsets[i+1]);
    for (; k<offsets[i+2]; k++)
      halos[k].flags |= NO_PROGENITOR_FLAG;
    assert(k==offsets[i+2]);
  }
  for (j=0; j<offsets[num_snaps]; j++) halos[j].weight=1; //Reset weights
  for (j=offsets[0]; j<offsets[1]; j++) halos[j].flags |= NO_PROGENITOR_FLAG; //Halos at first snap have no progenitor
}

int main(int argc, char **argv)
{
  char buffer[2048];
  int64_t i, j, k;
  FILE *in = NULL;

  FILE *zform_f = NULL;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s config.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  setup_config();

  snprintf(buffer, 2048, "%s/snaps.txt", INBASE);
  in = check_fopen(buffer, "r");
  while (fgets(buffer, 2048, in)) {
    num_snaps++;
  }

  fprintf(stderr, "Found %"PRId64" snaps\n", num_snaps);
  
  int64_t length = 0;
  check_realloc_s(offsets, sizeof(int64_t), (num_snaps+1));
  check_realloc_s(scales, sizeof(double), num_snaps);
  float *mahs = NULL;

  
  for (i=0; i<NUM_BLOCKS; i++) {
    snprintf(buffer, 2048, "%s/cat.box%"PRId64".dat", INBASE, i);
    check_slurp_file(buffer, (void *)&halos, &length);
    snprintf(buffer, 2048, "%s/offsets.box%"PRId64".txt", INBASE, i);
    in = check_fopen(buffer, "r");
    snprintf(buffer, 2048, "%s/zform.box%"PRId64".dat", INBASE, i);
    zform_f = check_fopen(buffer, "w");
    fprintf(zform_f, "#ID Mpeak z_form\n");
    while (fgets(buffer, 2048, in)) {
      double scale;
      int64_t s, o;
      if (sscanf(buffer, "%"SCNd64" %lf %"SCNd64, &s, &scale, &o)==3) {
	offsets[s] = o;
	scales[s] = scale;
      } else if (sscanf(buffer, "#Total halos: %"SCNd64, &o)==1) {
	offsets[num_snaps] = o;
      }
    }

    translate_descids();
    int64_t cur_halos = 0;
   
    struct inthash *id_to_mah = new_inthash();
    ih_prealloc(id_to_mah, offsets[num_snaps]);
    for (j=0; j<num_snaps; j++) {
      for (k=offsets[j]; k<offsets[j+1]; k++) {
	int64_t mah_index = ih_getint64(id_to_mah, halos[k].id);
	if (mah_index==IH_INVALID) {
	  check_realloc_every(mahs, sizeof(float)*num_snaps, cur_halos, 10000);
	  memset(mahs+cur_halos*num_snaps, 0, sizeof(float)*num_snaps);
	  ih_setint64(id_to_mah, halos[k].id, cur_halos);
	  mah_index = cur_halos;
	  cur_halos++;
	}
	mahs[mah_index*num_snaps+j] = halos[k].mp;
	fprintf(zform_f, "%"PRId64" %g %f\n", halos[k].id, halos[k].mp, zform(mahs+mah_index*num_snaps, j));
	if ((halos[k].descid > -1) && ((halos[k].flags & MMP_FLAG) || (halos[halos[k].descid].flags & ORPHAN_FLAG)))
	  ih_setint64(id_to_mah, halos[halos[k].descid].id, mah_index);
      }
    }
    free_inthash(id_to_mah);
    
    fclose(zform_f);
    fclose(in);
  }

  return 0;
}


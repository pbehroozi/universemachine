#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "all_luminosities.h"
#include "universe_time.h"

#define NUM_SFH_SCALES 1000

void print_allowable_types(void);

int main(int argc, char ** argv) {
  int64_t i;
  float **lum_lookups = NULL;
  float scales[NUM_SFH_SCALES+1] = {0};
  double *lums = NULL;
  char buffer[1024];

  if (argc < 2) {
    print_allowable_types();
    return 0;
  }

  init_time_table(0.307, 0.678);
  load_luminosities("fsps");
  check_realloc_s(lum_lookups, sizeof(float *), argc-1);
  memset(lum_lookups, 0, sizeof(float *)*(argc-1));
  for (i=0; i<NUM_SFH_SCALES+1; i++)
    scales[i] = (double)i / (double)NUM_SFH_SCALES;
  check_realloc_s(lums, sizeof(double), argc-1);
  memset(lums, 0, sizeof(double)*(argc-1));

  for (i=1; i<argc; i++) {
    enum lum_types lt;
    for (lt = 0; lt<LUM_TYPES; lt++)
      if (strcasecmp(argv[i], lum_names[lt])==0) break;
    if (lt == LUM_TYPES) {
      fprintf(stderr, "Unknown filter type %s!\n", argv[i]);
      print_allowable_types();
      exit(EXIT_FAILURE);
    }
    gen_lum_lookup(lum_lookups+i-1, scales, NUM_SFH_SCALES, lt, 0);
  }

  while (fgets(buffer, 1024, stdin)) {
    double a, sm, Z, dust;
    if (sscanf(buffer, "%lf %lf %lf %lf", &a, &sm, &Z, &dust) != 4) {
      for (i=0; i<argc-1; i++) {
	double lraw = (lums[i] > 0) ? log10(lums[i])*-2.5 : 1000;
	printf("%g ", lraw);
	lums[i] = 0;
      }
      printf("\n");
      fflush(stdout);
      continue;
    }
    int64_t n = a*NUM_SFH_SCALES+0.5;
    int64_t t = NUM_SFH_SCALES;
    if (dust < 0) dust = 0;
    if (dust > 2) dust = 2;
    //    printf("%f %f %f %"PRId64" %"PRId64" %f\n", a, sm, Z, t, n, log10(lookup_lum(lum_lookups[0], Z, t, n, 0))*-2.5);
    for (i=0; i<argc-1; i++) {
      double avg_lum_nodust = lookup_lum(lum_lookups[i], Z, t, n, 0);
      double avg_lum_dust = lookup_lum(lum_lookups[i], Z, t, n, 1);
      if (!(avg_lum_nodust > 0 && avg_lum_nodust > 0)) continue;
      double loglum = log(avg_lum_nodust) + dust*log(avg_lum_dust/avg_lum_nodust);
      lums[i] += exp(loglum)*sm;
    }
  }

  free_luminosities();

  return 0;
}


void print_allowable_types(void) {
  int64_t i;
  fprintf(stderr, "Allowable filter types:");
  for (i=0; i<LUM_TYPES; i++) fprintf(stderr, " %s", lum_names[i]);
  fprintf(stderr, "\n");
}

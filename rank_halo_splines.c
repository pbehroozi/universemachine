#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <gsl/gsl_interp.h>
#include <assert.h>
#include "make_sf_catalog.h"
#include "mt_rand.h"
#include "check_syscalls.h"
#include "stringparse.h"
#ifdef ENABLE_HDF5
#include "io_hdf5.c"
#include "sims/rhs_uchuu.h"
#else
#include "sims/rhs_bolshoi.h"
#endif /*ENABLE_HDF5*/



#define SPOINTS_PER_DEX 5.0
#define FPOINTS_PER_SPOINT 100
#define SUB_PER_FBIN 1000
#define MAX_SPLINE_LINESIZE 100000

int64_t PERIODIC = 1;
int64_t cam_param = 0;


struct halo {
  double properties[NUM_PROPS];
};

#define NORMAL_BINS (1024*8+1)
double p_to_normal[NORMAL_BINS];
int64_t num_halos=0;

struct p_vmp {
  double p, lvmp;
};

struct p_vmp *p_vmps = NULL;

double vec_abs(double *v) {
  int64_t i;
  double s = 0;
  for (i=0; i<3; i++) s+=v[i]*v[i];
  return sqrt(s);
}

double perc_to_normal(double p) {
  double x1 = -14;
  double x2 = 14;
  while (x2-x1 > 1e-7) {
    double half = 0.5*(x1+x2);
    double perc = 0.5*(1.0+erf(half));
    if (perc > p) { x2 = half; }
    else { x1 = half; }    
  }
  return ((x1+x2)*M_SQRT1_2);
}

static inline double spline_offset(gsl_interp *spline, gsl_interp_accel *acc, int64_t fbmin, int64_t fbmax, int64_t *counts, int64_t *perc, double *min_p, double *max_p, double *vmp3, double *lvmp, double *xa, double *ya) {
  int64_t i;
  double tot=0, wsum=0;
  gsl_interp_accel_reset(acc);
  for (i=fbmin; i<fbmax; i++) {
    if (!counts[i]) continue;
    wsum += counts[i]*vmp3[i];
    double pthresh = gsl_interp_eval(spline, xa, ya, lvmp[i], acc);
    if ((max_p[i]<=min_p[i]+0.00001*min_p[i]) || pthresh > max_p[i] || pthresh < min_p[i]) {
      /*if (min_p[i] != 0 && (max_p[i]<=min_p[i]+0.00001*fabs(min_p[i])) && 
	  ((pthresh > min_p[i]-0.01*fabs(min_p[i])) && (pthresh < min_p[i]+0.01*fabs(min_p[i]))))
	tot += counts[i]*vmp3[i]*(pthresh - (min_p[i]-0.01*fabs(min_p[i])))/(0.02*fabs(min_p[i]));
	else*/ if (pthresh > min_p[i]) tot += counts[i]*vmp3[i];
      continue;
    }
    int64_t pbin = i*SUB_PER_FBIN;
    double sf = (pthresh - min_p[i])/(max_p[i]-min_p[i])*(double)(SUB_PER_FBIN);
    int64_t sbin = sf;
    sf -= sbin;
    int64_t pbm1 = 0;
    if (sbin > 0) pbm1 = perc[pbin+sbin-1];
    tot += (pbm1 + sf*(perc[pbin+sbin]-pbm1))*vmp3[i];
  }
  return ((tot / wsum) - 0.5);
}


void fit_splines(FILE *out, double low, double high, struct p_vmp *p, int64_t np)
{
  //Basic sanity checks; min/max calculation
  if ((((high-low) < 0.01) && !(high==1.0 || low==0.0)) || (np < 30)) return;
  if ((high-low) < 0.00001) return; 
  int64_t i, j, error=0;
  int64_t np_low = np;
  double min_lv=p[0].lvmp, max_lv=p[0].lvmp;
  double min_val=p[0].p, max_val = p[0].p;
  for (i=1; i<np; i++) {
    if (min_lv > p[i].lvmp) min_lv = p[i].lvmp;
    if (max_lv < p[i].lvmp) max_lv = p[i].lvmp;
    if (min_val > p[i].p) min_val = p[i].p;
    if (max_val < p[i].p) max_val = p[i].p;
  }
  if ((max_lv-min_lv) < 1e-50 || (max_val - min_val)<1e-50) return;
  if (min_lv>0) min_lv *= 0.9999;
  else min_lv *= 1.0001;
  if (max_lv>0) max_lv *= 1.0001;
  else max_lv *= 0.9999;

  //Renormalize values
  for (i=0; i<np; i++)
    p[i].p = (p[i].p - min_val) / (max_val - min_val);

  //Decide on number of spline points, allocate spline
  int64_t npoints = SPOINTS_PER_DEX*(max_lv - min_lv)+1.5;
  if (npoints < 3) npoints = 3;
  double bpdex = (double)(npoints-1) / (max_lv-min_lv);
  double inv_bpdex = 1.0/bpdex;
  const gsl_interp_type *T = (npoints > 4) ? gsl_interp_akima : gsl_interp_cspline;
  gsl_interp *spline = gsl_interp_alloc(T, npoints);
  gsl_interp_accel *acc = gsl_interp_accel_alloc();
  double *xa=NULL, *ya = NULL;
  check_realloc_s(xa, sizeof(double), npoints);
  check_realloc_s(ya, sizeof(double), npoints);
  for (i=0; i<npoints; i++) { xa[i] = min_lv + inv_bpdex*i; }
  
  //Allocate fine bins
  int64_t fbins = (npoints-1)*FPOINTS_PER_SPOINT;
#define alloc_clear_set(x,t,s) t *x = NULL; check_realloc_s(x, sizeof(t), s); memset(x, 0, sizeof(t)*s);
  alloc_clear_set(counts, int64_t, fbins);
  alloc_clear_set(perc, int64_t, (fbins+1)*(1+SUB_PER_FBIN));
  alloc_clear_set(min_p, double, fbins);
  alloc_clear_set(max_p, double, fbins);
  alloc_clear_set(vmp3, double, fbins);
  alloc_clear_set(lvmp, double, fbins);
  alloc_clear_set(smax, double, npoints);
  alloc_clear_set(smin, double, npoints);
  alloc_clear_set(old_ya, double, npoints);

  //Populate fine bins
  double f_bpdex = (double)fbins/(max_lv-min_lv);
  //double f_inv_bpdex = 1.0/f_bpdex;
  for (i=0; i<np; i++) {
    int64_t fbin = f_bpdex*(p[i].lvmp-min_lv);
    if (!counts[fbin] || p[i].p < min_p[fbin]) min_p[fbin] = p[i].p;
    if (!counts[fbin] || p[i].p > max_p[fbin]) max_p[fbin] = p[i].p;
    lvmp[fbin] += p[i].lvmp;
    vmp3[fbin] += pow(10, 3.0*p[i].lvmp);
    counts[fbin]++;
  }
  for (i=0; i<np; i++) {
    int64_t fbin = f_bpdex*(p[i].lvmp-min_lv);
    int64_t pbin = fbin*SUB_PER_FBIN;
    int64_t sbin = (p[i].p - min_p[fbin])/(max_p[fbin]-min_p[fbin])*(double)(SUB_PER_FBIN-0.01);
    if (min_p[fbin]==max_p[fbin] || sbin < 0 || sbin >= SUB_PER_FBIN) { perc[pbin]++; continue; }
    perc[pbin+sbin]++;
  }
  for (i=0; i<fbins; i++) {
    int64_t pbin = i*SUB_PER_FBIN;
    if (counts[i]) {
      lvmp[i] /= (double)counts[i];
      vmp3[i] /= (double)counts[i];
    }
    for (j=1; j<SUB_PER_FBIN; j++)
      perc[j+pbin] += perc[j+pbin-1];
  }

  //Initial guess for spline based on bin medians
  int64_t zero_range = 0;
  for (i=0; i<npoints; i++) {
    int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
    int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
    if (fbmin < 0) fbmin = 0;
    if (fbmax > fbins) fbmax = fbins;
    double wsum = 0;
    int64_t j=0;
    smin[i] = -1e300;
    smax[i] = 1e300;
    for (j=fbmin; j<fbmax; j++) {
      if (!counts[j]) continue;
      wsum += vmp3[j]*counts[j];
      if (smin[i]==-1e300 || smin[i] > min_p[j]) smin[i] = min_p[j];
      if (smax[i]==1e300 || smax[i] < max_p[j]) smax[i] = max_p[j];
    }
    if (smax[i] == 1e300) { 
      if (i>0) { smin[i] = smin[i-1]; smax[i] = smax[i-1]; }
      else { smin[i] = smax[i] = 0.5; }
    }
    if (smax[i] <= smin[i]+0.00001*fabs(smin[i])) { ya[i] = 0.5*(smin[i]+smax[i]); zero_range++; continue; }
    double pthresh=smin[i], dpthresh=(smax[i]-smin[i])/1000.0;
    for (; pthresh < smax[i]; pthresh += dpthresh) {
      double wsum2 = 0;
      for (j=fbmin; j<fbmax; j++) {
	if (!counts[j]) continue;
	if (max_p[j]==min_p[j] || min_p[j]>pthresh || max_p[j]<pthresh) {
	  if (max_p[j] < pthresh) wsum2 += vmp3[j]*counts[j];
	  continue;
	}
	int64_t pbin = j*SUB_PER_FBIN;
	int64_t sbin = (pthresh - min_p[j])/(max_p[j]-min_p[j])*(double)(SUB_PER_FBIN);
	if (sbin > 0)
	  wsum2 += vmp3[j]*perc[pbin+sbin-1];
      }
      if (wsum2 > 0.5*wsum) break;
    }
    ya[i] = pthresh;
  }

  //Can't subdivide in this case
  if (zero_range == npoints) { error=1; goto out; }
  
  
  //Fit spline to fine bins
#define CALC_ERROR spline_offset(spline, acc, fbmin, fbmax, counts, perc, min_p, max_p, vmp3, lvmp, xa, ya)
  double last_error = 0;
  gsl_interp_init(spline, xa, ya, npoints);
  for (i=0; i<npoints; i++) {
    int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
    int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
    if (fbmin < 0) fbmin = 0;
    if (fbmax > fbins) fbmax = fbins;
    double ce = CALC_ERROR;
    last_error += ce*ce;
  }

  double cur_error = last_error;
  last_error *= 1.011;
  while (cur_error*1.0001 < last_error) {
    last_error = cur_error;
    memcpy(old_ya, ya, sizeof(double)*npoints);
    for (i=0; i<npoints; i++) {
      double min = smin[i];
      double max = smax[i];
      int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
      int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
      if (fbmin < 0) fbmin = 0;
      if (fbmax > fbins) { fbmax = fbins; fbmin -= FPOINTS_PER_SPOINT; }
      gsl_interp_init(spline, xa, ya, npoints);
      double ce = CALC_ERROR;
      double ce2 = ce;
      while (max > min+0.00001*fabs(min) && (fabs(min)>1e-100 || max > 1e-9)) {
	ya[i] = 0.5*(max+min);
	gsl_interp_init(spline, xa, ya, npoints);
	ce2 = CALC_ERROR;
	if (ce2 > 0) max = ya[i];
	else min = ya[i];
      }
      if (fabs(ce) < fabs(ce2)) { ya[i] = old_ya[i]; }
    }

    gsl_interp_init(spline, xa, ya, npoints);
    cur_error = 0;
    for (i=0; i<npoints; i++) {
      int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
      int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
      if (fbmin < 0) fbmin = 0;
      if (fbmax > fbins) { fbmax = fbins; fbmin -= FPOINTS_PER_SPOINT; }
      double ce = CALC_ERROR;
      cur_error += ce*ce;
    }
    if (cur_error > last_error) {
      fprintf(stderr, "Warning: %f > %f for %.03f - %.03f of %s\n", cur_error, last_error, low, high, properties[cam_param].label); 
      memcpy(ya, old_ya, sizeof(double)*npoints);
      cur_error = last_error;
    }
  }
#undef CALC_ERROR

  gsl_interp_init(spline, xa, ya, npoints);
  gsl_interp_accel_reset(acc);
  //Final sort by spline
  struct p_vmp tmp;
  for (i=0; i<np_low; i++) {
    double pthresh = gsl_interp_eval(spline, xa, ya, p[i].lvmp, acc);
    if (p[i].p > pthresh) {
      p[i].p = (p[i].p-pthresh)/(1.0-pthresh);
      tmp = p[np_low-1];
      p[np_low-1] = p[i];
      p[i] = tmp;
      np_low--;
      i--;
    } else if (p[i].p==pthresh) { p[i].p = 1; }
    else {
      p[i].p /= pthresh;
    }
  }

  //Print out spline points
  double child_low = perc_to_normal(0.75*low+0.25*high);
  double child_high = perc_to_normal(0.25*low+0.75*high);
  double low_v = perc_to_normal(low);
  double high_v = perc_to_normal(high);
  fprintf(out, "%.12f %"PRId64" %.14e %.14e %.12f %.12f %.12f %.12f", perc_to_normal(0.5*(high+low)), npoints, min_val, max_val, child_low, child_high, low_v, high_v);
  for (i=0; i<npoints; i++) fprintf(out, " %.12e %.12e", xa[i], ya[i]);
  fprintf(out, "\n");

  //Calculate spline errors
  alloc_clear_set(slowc, int64_t, npoints);
  alloc_clear_set(stotc, int64_t, npoints);
  alloc_clear_set(sloww, double, npoints);
  alloc_clear_set(stotw, double, npoints);
  for (i=0; i<np; i++) {
    int64_t sbin = (p[i].lvmp - min_lv)*bpdex + 0.5;
    assert(sbin < npoints);
    double w = pow(10,3.0*p[i].lvmp);
    stotc[sbin]++;
    stotw[sbin]+=w;
    if (i < np_low) { slowc[sbin]++; sloww[sbin]+=w; }
  }
  fprintf(out, "#Spline quality check: terr(%f) ", cur_error);
  for (i=0; i<npoints; i++) {
    double tc = (stotc[i]>0) ? stotc[i] : 1.0;
    double tw = (stotw[i]>0) ? stotw[i] : 1.0;
    fprintf(out, " %.3f=%"PRId64"/%"PRId64" (%.3f=wsum)", (double)slowc[i]/tc, 
	    slowc[i], stotc[i], sloww[i]/tw);
  }
  fprintf(out, "\n\n");
  free(slowc); free(stotc); free(sloww); free(stotw);

  //Free data
 out:
  free(xa); free(ya); free(old_ya); gsl_interp_free(spline); gsl_interp_accel_free(acc); 
  free(counts); free(perc); free(min_p); free(max_p); free(vmp3); free(smax); free(smin); free(lvmp);
  if (error) return;
  if (np_low == 0 || np_low==np) return;

  //Recurse
  fit_splines(out, low, 0.5*(low + high), p, np_low);
  fit_splines(out, 0.5*(low+high), high, p+np_low, np-np_low);
#undef alloc_clear_set
}

struct spline {
  double scale;
  double value;
  double max_val, min_val;
  double child_low, child_high;
  double low_v, high_v;
  int64_t num_points;
  gsl_interp *spline;
  gsl_interp_accel *accel;
  double *x, *y;
  struct spline *low, *high;
};

struct spline *splines = NULL;
int64_t num_splines = 0;
double scaling = 0;

int sort_splines(const void *a, const void *b) {
  const struct spline *c = a;
  const struct spline *d = b;
  if (c->value < d->value) return -1;
  if (c->value > d->value) return 1;
  return 0;
}

struct spline *find_spline(struct spline *s, int64_t ns, double val) {
  int64_t i;
  int64_t best_i = 0;
  for (i=1; i<ns; i++) {
    if (fabs(s[i].value - val) < fabs(s[best_i].value-val))
      best_i = i;
  }
  if (fabs(s[best_i].value-val) > 1e-8) return NULL;
  return s+best_i;
}

void link_splines(struct spline *s, int64_t ns) {
  int64_t i;
  assert(fabs(s[0].value) < 0.001);
  for (i=0; i<ns; i++) {
    s[i].low = find_spline(s, ns, s[i].child_low);
    s[i].high = find_spline(s, ns, s[i].child_high);
  }
}

double rand_rank(double value) {
  double neg_val = copysign(value, -1.0);
  double cumulative = 0.5*(1.0+erf(neg_val * M_SQRT1_2));
  if (!value) cumulative = 1.0;
  double ra_perc = dr250()*cumulative;
  double ra_sd = perc_to_normal(ra_perc);
  return copysign(ra_sd, value);
}

double spline_rank(struct spline *s, double lvmp, double val) {
if (!s || s->x[0] > lvmp || s->x[s->num_points-1] < lvmp) return 0;
#define EVAL(sp) (gsl_interp_eval(sp->spline, sp->x, sp->y, lvmp, sp->accel))
  struct spline *s2 = s;
  int64_t low_flag = 0, high_flag = 0, is_low = 0;
  while (1) {
    val = (val - s->min_val) / (s->max_val - s->min_val);
    double res = EVAL(s);
    if (val > res) {
      s2 = s->high;
      val = (val-res)/(1.0 - res);
      is_low = 0;
      high_flag = 1;
    } else {
      s2 = s->low;
      val /= (res > 0) ? res : 1.0;
      is_low = 1;
      low_flag = 1;
    }
    if (!s2 || s2->x[0] > lvmp || s2->x[s2->num_points-1] < lvmp) break;
    s = s2;
  }

  if (!low_flag && !high_flag) return rand_rank(0);
  if (!low_flag) {
    double low_perc = 0.5*(1.0+erf(s->value*M_SQRT1_2));
    double perc = low_perc + val*(1.0-low_perc);
    return perc_to_normal(perc-1e-8);
  } else if (!high_flag) {
    double high_perc = 0.5*(1.0+erf(s->value*M_SQRT1_2));
    double perc = val*high_perc;
    return perc_to_normal(perc+1e-8);
  }
  if (is_low) return (s->low_v + val*(s->value - s->low_v));
  return (s->value + val*(s->high_v - s->value));
#undef EVAL
}

void clear_ranks(void) {
  int64_t i;
  for (i=0; i<num_splines; i++) {
    gsl_interp_free(splines[i].spline);
    gsl_interp_accel_free(splines[i].accel);
    free(splines[i].x);
    free(splines[i].y);
  }
  free(splines);
  num_splines = 0;
  splines = NULL;
}

void load_ranks(char *path, char *prop, char *scale) {
  int64_t j;
  char *buffer = NULL;
  struct spline s = {0};
  SHORT_PARSETYPE;
  void **data = NULL;
  enum parsetype *pt = NULL;

  check_realloc_s(buffer, sizeof(char), MAX_SPLINE_LINESIZE);
  sprintf(buffer, "%s/%s/splines_%s.list", path, prop, scale);
  FILE *in = check_fopen(buffer, "r");
  while (fgets(buffer, MAX_SPLINE_LINESIZE, in)) {
    if (buffer[0] == '\n' || buffer[0] == ' ') continue;
    if (buffer[0] == '#') {
      if (!strncmp(buffer, "#scaling =", 10))
	sscanf(buffer+11, "%lf", &scaling);
      continue;
    }
    if (sscanf(buffer, "%lf %"SCNd64, &s.value, &s.num_points)!=2) continue;
    s.x = check_realloc(0, sizeof(double)*s.num_points, "Allocating x");
    s.y = check_realloc(0, sizeof(double)*s.num_points, "Allocating y");
    s.scale = atof(scale);
    check_realloc_s(data, sizeof(void *), s.num_points*2+8);
    check_realloc_s(pt, sizeof(enum parsetype), s.num_points*2+8);
    data[0] = &s.value;
    pt[0] = F64;
    data[1] = &s.num_points;
    pt[1] = D64;
    data[2] = &s.min_val;
    pt[2] = F64;
    data[3] = &s.max_val;
    pt[3] = F64;
    data[4] = &s.child_low;
    pt[4] = F64;
    data[5] = &s.child_high;
    pt[5] = F64;
    data[6] = &s.low_v;
    pt[6] = F64;
    data[7] = &s.high_v;
    pt[7] = F64;
    for (j=0; j<s.num_points; j++) {
      data[j*2+8] = s.x+j;
      data[j*2+9] = s.y+j;
      pt[j*2+8] = pt[j*2+9] = F64;
    }
    int64_t n = stringparse(buffer, data, pt, s.num_points*2+8);
    if (n!=(s.num_points*2+8)) continue;
    const gsl_interp_type *T = (s.num_points > 4) ? gsl_interp_akima : gsl_interp_cspline;
    s.spline = gsl_interp_alloc(T, s.num_points);
    s.accel = gsl_interp_accel_alloc();
    gsl_interp_init(s.spline, s.x, s.y, s.num_points);
    check_realloc_every(splines, sizeof(struct spline), num_splines, 1000);
    splines[num_splines] = s;
    num_splines++;
  }
  fclose(in);
  if (num_splines) link_splines(splines, num_splines);
  //qsort(splines, num_splines, sizeof(struct spline), sort_splines);
  free(buffer);
}


int main(int argc, char **argv)
{
  int64_t i;
  char buffer[1024];
#ifndef ENABLE_HDF5
  double Om=0, Ol=0, h0=0, box_size=0;
#else
  double h5_scale = 0;
#endif /*ENABLE_HDF5*/
  
  if (argc < 2) {
#ifndef ENABLE_HDF5
    fprintf(stderr, "Usage: %s scale_factor [catalogs_to_output]\n", argv[0]);
#else
    fprintf(stderr, "Usage: %s catalog1 catalog2 ...\n", argv[0]);
#endif /*ENABLE_HDF5*/
    exit(1);
  }

  char *SCALE_FACTOR = argv[1];
  PERIODIC = 1;
  struct halo d;
  enum parsetype types[NUM_LABELS];
  void *data[NUM_LABELS];
  SHORT_PARSETYPE;
  for (i=0; i<NUM_LABELS; i++) {
    types[i] = F64;
    data[i] = d.properties+i;
  }

  struct halo *halos = NULL;
  r250_init(87L);

#ifndef ENABLE_HDF5
  snprintf(buffer, 1024, "hlist_%s.list", SCALE_FACTOR);
  FILE *in = check_fopen(buffer, "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') {
      double a[3];
      if (sscanf(buffer, "#Omega_M = %lf; Omega_L = %lf; h0 = %lf", a, a+1, a+2)==3) {
	Om = a[0];
	Ol = a[1];
	h0 = a[2];
      }
      if (sscanf(buffer, "#Full box size = %lf", a)==1) {
	box_size = a[0];
      }
      continue;
    }
    int64_t n = stringparse(buffer, data, types, NUM_LABELS);
    if (n<NUM_LABELS) continue;
    d.properties[AX] = vec_abs(d.properties+AX);
    d.properties[AX+1] = d.properties[VX];
    d.properties[VX] = -1.0*vec_abs(d.properties+VX);
    d.properties[JX] = vec_abs(d.properties+JX);
    d.properties[AX500] = vec_abs(d.properties+AX500);
    d.properties[BTOA] = -d.properties[BTOA];
    d.properties[BTOA+1] = -d.properties[BTOA+1];
    d.properties[BTOA500c] = -d.properties[BTOA500c];
    d.properties[BTOA500c+1] = -d.properties[BTOA500c+1];
    d.properties[VMVP] = d.properties[VM]/d.properties[VP];
    for (n=0; n<NUM_PROPS; n++) {
      if (properties[n].important && properties[n].scaling)
	d.properties[n] *= pow(d.properties[VMP], -properties[n].scaling);
    }
    check_realloc_every(halos, sizeof(struct halo), num_halos, 1000);
    halos[num_halos] = d;
    num_halos++;
  }
  fclose(in);
#else /*ENABLE_HDF5*/
  int64_t n=0;
  num_halos = 0;
  double *h5data = NULL;
#define READ_HDF5(h5name, ctype, htype, location) {			\
	hid_t hid = check_H5Dopen(hlist, h5name, "", filename);		\
	check_realloc_s(h5data, sizeof(ctype), dataset_length);		\
	check_H5Dread(hid, htype, h5data, h5name, "", filename);	\
	H5Dclose(hid);							\
	ctype *h5d_type = h5data;					\
	for (i=0; i<dataset_length; i++)				\
	  halos[num_halos+i].location = h5d_type[i];			\
      }

  for (n=1; n<argc; n++) {
    char *filename = argv[n];
    hid_t hlist = check_H5Fopen(filename, H5F_ACC_RDONLY);
    hid_t vmp = check_H5Dopen(hlist, VMP_NAME, "", filename);
    hid_t vmp_space = check_H5Dget_space(vmp);
    hsize_t dataset_length = 0;
    check_H5Sget_simple_extent_dims(vmp_space, &dataset_length);
    H5Sclose(vmp_space);
    H5Dclose(vmp);
    check_realloc_s(halos, sizeof(struct halo), num_halos+dataset_length);
    check_realloc_s(h5data, sizeof(double), dataset_length);
    if (num_halos==0 && dataset_length>0) {
      hid_t scale = check_H5Dopen(hlist, SCALE_NAME, "", filename);
      check_H5Dread(scale, H5T_NATIVE_DOUBLE, h5data, VMP_NAME, "", filename);
      h5_scale = h5data[0];
      H5Dclose(scale);
    }
    READ_HDF5(VMP_NAME, double, H5T_NATIVE_DOUBLE, properties[VMP]);
    READ_HDF5(DLOGVMAX_NAME, double, H5T_NATIVE_DOUBLE, properties[DLOGVMAX]);
    READ_HDF5(TIDAL_FORCE_NAME, double, H5T_NATIVE_DOUBLE, properties[TIDAL_FORCE]);
    H5Fclose(hlist);
    num_halos+=dataset_length;
  }
  if (h5data) free(h5data);
  snprintf(buffer, 1024, "%.5f", h5_scale);
  SCALE_FACTOR = strdup(buffer);
#endif /*ENABLE_HDF5*/

  
  check_realloc_s(p_vmps, sizeof(struct p_vmp), num_halos);
  for (i=0; i<num_halos; i++) p_vmps[i].lvmp = log10(halos[i].properties[VMP]);

  for (cam_param=0; cam_param<NUM_PROPS; cam_param++) {
    if (!properties[cam_param].important) continue;
    snprintf(buffer, 1024, "splines/%s/", properties[cam_param].label);
    mkdir("splines", 0755);
    mkdir(buffer, 0755);
    snprintf(buffer, 1024, "splines/%s/splines_%s.list", properties[cam_param].label, SCALE_FACTOR);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "#Property name: %s\n", properties[cam_param].label);
    fprintf(out, "#a = %s\n", SCALE_FACTOR);
    fprintf(out, "#scaling = %d\n", properties[cam_param].scaling);
    for (i=0; i<num_halos; i++) {
      p_vmps[i].p = halos[i].properties[cam_param];
      p_vmps[i].lvmp = log10(halos[i].properties[VMP]);
    }
    fit_splines(out, 0, 1.0, p_vmps, num_halos);
    fclose(out);
    
#ifndef ENABLE_HDF5
    if (!strcmp(properties[cam_param].label, "VmaxVpeak") || !strcmp(properties[cam_param].label, "mvir") || !strcmp(properties[cam_param].label, "Spin_Bullock") || !strcmp(properties[cam_param].label, "Tidal_Force_Tdyn") || !strcmp(properties[cam_param].label, "DeltaLogVmax")) {
      load_ranks("splines", properties[cam_param].label, SCALE_FACTOR);
      snprintf(buffer, 1024, "splines/%s/ranks_%s.list", properties[cam_param].label, SCALE_FACTOR);
      out = check_fopen(buffer, "w");
      fprintf(out, "#LVMP %s (scaled) Rank\n", properties[cam_param].label);
      for (i=0; i<num_halos; i++) {
	double lvmp = log10(halos[i].properties[VMP]);
	double val = halos[i].properties[cam_param] * pow(halos[i].properties[VMP], scaling);
	fprintf(out, "%f %e %e %.6f\n", lvmp, val, halos[i].properties[cam_param], spline_rank(splines, lvmp, halos[i].properties[cam_param]));
      }
      fclose(out);
      clear_ranks();
    }

    for (i=2; i<argc; i++) if (!strcasecmp(properties[cam_param].label, argv[i])) break;
    if (i<argc) {
      load_ranks("splines", properties[cam_param].label, SCALE_FACTOR);
      snprintf(buffer, 1024, "splines/%s/cat_%s", properties[cam_param].label, SCALE_FACTOR);
      mkdir(buffer, 0755);

      snprintf(buffer, 1024, "splines/%s/cat_%s/cat.box0.dat", properties[cam_param].label, SCALE_FACTOR);
      out = check_fopen(buffer, "w");
      //#scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8) sam_mvir(9) mvir(10) rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19) vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28) Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32) Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Tidal_Force(35) Tidal_ID(36) Rs_Klypin(37) Mmvir_all(38) M200b(39) M200c(40) M500c(41) M2500c(42) Xoff(43) Voff(44) Spin_Bullock(45) b_to_a(46) c_to_a(47) A[x](48) A[y](49) A[z](50) b_to_a(500c)(51) c_to_a(500c)(52) A[x](500c)(53) A[y](500c)(54) A[z](500c)(55) T/|U|(56) M_pe_Behroozi(57) M_pe_Diemer(58) Macc(59) Mpeak(60) Vacc(61) Vpeak(62) Halfmass_Scale(63) Acc_Rate_Inst(64) Acc_Rate_100Myr(65) Acc_Rate_1*Tdyn(66) Acc_Rate_2*Tdyn(67) Acc_Rate_Mpeak(68) Mpeak_Scale(69) Acc_Scale(70) First_Acc_Scale(71) First_Acc_Mvir(72) First_Acc_Vmax(73) Vmax@Mpeak(74) Tidal_Force_Tdyn(75)
      for (i=0; i<num_halos; i++) {
	struct catalog_halo ch = {0};
	struct halo *h = halos+i;
	ch.id = h->properties[1];
	ch.descid = h->properties[3];
	ch.upid = h->properties[6];
	for (int64_t j=0; j<6; j++)
	  ch.pos[j] = h->properties[17+j];
	ch.pos[4] = h->properties[AX+1]; //Because VX was repurposed as |V|
	ch.vmp = h->properties[VMP];
	ch.lvmp = log10(ch.vmp);
	ch.mp = h->properties[60];
	ch.m = h->properties[10];
	ch.v = h->properties[16];
	ch.r = h->properties[11];
	ch.t_tdyn = h->properties[75];
	ch.uparent_dist = -1;
	ch.ra = normal_random(0, 1);

	double lvmp = log10(halos[i].properties[VMP]);
	ch.rank2 = ch.ra;
	ch.rank1 = spline_rank(splines, lvmp, halos[i].properties[cam_param]);
	fwrite(&ch, sizeof(struct catalog_halo), 1, out);
      }
      fclose(out);
      clear_ranks();

      snprintf(buffer, 1024, "splines/%s/cat_%s/offsets.box0.txt", properties[cam_param].label, SCALE_FACTOR);
      out = check_fopen(buffer, "w");
      fprintf(out, "#Output Scale Offset(halos)\n");
      fprintf(out, "0 %s 0\n", SCALE_FACTOR);
      fprintf(out, "#Total halos: %"PRId64"\n", num_halos);
      fclose(out);

      snprintf(buffer, 1024, "splines/%s/cat_%s/snaps.txt", properties[cam_param].label, SCALE_FACTOR);
      out = check_fopen(buffer, "w");
      fprintf(out, "0 %s\n", SCALE_FACTOR);
      fclose(out);

      snprintf(buffer, 1024, "splines/%s/cat_%s/box.cfg", properties[cam_param].label, SCALE_FACTOR);
      out = check_fopen(buffer, "w");
      fprintf(out, "BOX_SIZE = %f\n", box_size);
      fprintf(out, "Om = %f\n", Om);
      fprintf(out, "Ol = %f\n", Ol);
      fprintf(out, "h0 = %f\n", h0);
      fprintf(out, "NUM_BLOCKS = 1\n");
      fclose(out);
    }
#endif /*!defined ENABLE_HDF5*/
  }
  return 0;
}


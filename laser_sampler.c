/* Converted from the Python code of Dan Foreman-Mackey et al. (2012)
   https://github.com/dfm/emcee
   Original License: MIT
   Original copyright: Dan Foreman-Mackey et al. 2010-2013

   Original description:
     An affine invariant Markov chain Monte Carlo (MCMC) sampler.
     Goodman & Weare, Ensemble Samplers With Affine Invariance
     Comm. App. Math. Comp. Sci., Vol. 5 (2010), No. 1, 65–80
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <assert.h>
#include "mt_rand.h"
#include "check_syscalls.h"
#include "sampler.h"
#include "jacobi_dim.h"

void calc_sphere_size(struct EnsembleSampler *e, struct Walker *w, struct Walker *wi);
double add_ln_likelihood(double l1, double l2);

struct EnsembleSampler *new_sampler(int64_t num_walkers, int64_t dim, double a) {
  int64_t i;
  struct EnsembleSampler *e = NULL;

  r250_init(87L);
  check_calloc_s(e, sizeof(struct EnsembleSampler), 1);
  e->num_walkers = num_walkers;
  e->num_half = num_walkers/2;
  e->dim = dim;
  e->num_random_steps = e->dim*10;
  if (e->num_random_steps < 1000) e->num_random_steps = 1000;
  e->a = a;
  e->inv_temp = 1.0;

  if (num_walkers % 2) {
    printf("[Error] Number of walkers in sampler must be even!\n");
    exit(EXIT_FAILURE);
  }
  if (num_walkers < 2*dim) {
    printf("[Error] Too few walkers to probe full dimensionality!\n");
    exit(EXIT_FAILURE);
  }

  check_calloc_s(e->cov_matrix, sizeof(double), e->dim*e->dim);
  check_calloc_s(e->orth_matrix, sizeof(double), e->dim*e->dim);
  check_calloc_s(e->eigenvalues, sizeof(double), e->dim);

  check_calloc_s(e->w, sizeof(struct Walker), num_walkers);
  e->w1 = e->w;
  e->w2 = e->w + e->num_half;

  for (i=0; i<num_walkers; i++) {
    check_calloc_s(e->w[i].params, sizeof(double), dim*2);
    check_calloc_s(e->w[i].avg_pos, sizeof(double), dim);
    e->w[i].dim = dim;
    e->w[i].chi2 = e->w[i].accepted_chi2 = -1;
    e->w[i].z = 1;
  }
  return e;
}

void free_sampler(struct EnsembleSampler *e) {
  int64_t i;
  if (!e) return;
  if (e->w) {
    for (i=0; i<e->num_walkers; i++) if (e->w[i].params) free(e->w[i].params);
    free(e->w);
  }
  if (e->avg) free(e->avg);
  if (e->step) free(e->step);
  free(e->cov_matrix);
  free(e->orth_matrix);
  free(e->eigenvalues);
  free(e);
}

void init_walker_position(struct Walker *w, double *params) {
  memcpy(w->params, params, sizeof(double)*w->dim);
  memcpy(w->params+w->dim, params, sizeof(double)*w->dim);
  w->chi2 = w->accepted_chi2 = -1;
}

void sampler_init_averages(struct EnsembleSampler *e) {
  check_calloc_s(e->avg, sizeof(double), e->dim);
  check_calloc_s(e->best_params, sizeof(double), (e->dim+1));
  check_calloc_s(e->step, sizeof(double), e->dim);
  memset(e->cov_matrix, 0, sizeof(double)*e->dim*e->dim);
  memset(e->orth_matrix, 0, sizeof(double)*e->dim*e->dim);
  memset(e->eigenvalues, 0, sizeof(double)*e->dim);
  e->avg_steps = 0;
}

void sampler_reset_averages(struct EnsembleSampler *e) {
  int64_t i, j, k;
  e->avg_steps = e->num_walkers;
  sampler_init_averages(e);
  for (i=0; i<e->num_walkers; i++) {
    for (j=0; j<e->dim; j++) {
	e->avg[j] += e->w[i].params[j];
	for (k=0; k<e->dim; k++)
	  e->cov_matrix[j*e->dim + k] += e->w[i].params[j]*e->w[i].params[k];
    }
  }
  for (j=0; j<e->dim; j++) e->avg[j] /= (double)e->num_walkers;
  /*for (i=0; i<e->num_half; i++) {
    calc_sphere_size(e, e->w1, e->w1+i);
    calc_sphere_size(e, e->w2, e->w2+i);
    }*/
}

void sampler_stats_to_step(struct EnsembleSampler *e) {
  double mul = 1.0/(double)e->avg_steps;
  int64_t i,j;
  double *input_matrix = NULL;

  double identity_portion = 0;
  check_realloc_s(input_matrix, sizeof(double), e->dim*e->dim);
  for (i=0; i<e->dim; i++) {
    for (j=0; j<e->dim; j++)
      input_matrix[i*e->dim+j] = mul*e->cov_matrix[i*e->dim+j] - e->avg[i]*e->avg[j];
    if (!identity_portion || (identity_portion > input_matrix[i*e->dim+i] && input_matrix[i*e->dim+i]>0))
      identity_portion = input_matrix[i*e->dim+i];
  }

  identity_portion *= 0.05;
  for (i=0; i<e->dim; i++) input_matrix[i*e->dim+i] += identity_portion;

  jacobi_decompose_dim(input_matrix, e->eigenvalues, e->orth_matrix, e->dim);
  for (i=0; i<e->dim; i++) {
    if (e->eigenvalues[i] <=0) e->eigenvalues[i] = 0;
    e->eigenvalues[i] = sqrt(e->eigenvalues[i]);
    //fprintf(stderr, "%.03f ", e->eigenvalues[i]);
  }
  //fprintf(stderr, "\n");
  free(input_matrix);
}


void sampler_random_step(struct EnsembleSampler *e, struct Walker *w) {
  int64_t i;
  for (i=0; i<e->dim; i++)
    e->step[i] = 0; //w->params[i+e->dim] - e->avg[i];

  double theta = M_PI/2.0; //dr250()*2.0*M_PI; //M_PI/2.0;
  double ct = cos(theta);
  double st = sin(theta);
  /*double ct = 0;
    double st = 1;*/

  for (i=0; i<e->dim; i++) {
    if (e->eigenvalues[i]==0) continue;
    double dx = normal_random(0,1);
    vector_madd_dim(e->step, dx*e->eigenvalues[i], e->orth_matrix+(i*e->dim), e->dim);
  }

  for (i=0; i<e->dim; i++)
    w->params[i] = e->avg[i] + ct*(w->params[i]-e->avg[i]) + st*e->step[i];

  w->theta = theta;
  w->step_mode = DJ_SAMPLER;
  w->z = 1;
}

void calc_dlnl_step(struct EnsembleSampler *e, struct Walker *w) {
  int64_t i, j;
  double ct = cos(w->theta);
  double st = sin(w->theta);
  double ds = 0, ds2 = 0;

  for (i=0; i<e->dim; i++)
    e->step[i] = ((w->params[i+e->dim]-e->avg[i]) - (w->params[i] - e->avg[i])*ct) / st;

  for (i=0; i<e->dim; i++) {
    double dx = 0;
    if (e->eigenvalues[i]==0) continue;
    for (j=0; j<e->dim; j++) dx += e->step[j]*e->orth_matrix[i*e->dim+j];
    dx /= e->eigenvalues[i];
    ds2 += dx*dx;
  }

  for (i=0; i<e->dim; i++)
    e->step[i] = ((w->params[i]-e->avg[i]) - (w->params[i+e->dim] - e->avg[i])*ct) / st;

  for (i=0; i<e->dim; i++) {
    double dx = 0;
    if (e->eigenvalues[i]==0) continue;
    for (j=0; j<e->dim; j++) dx += e->step[j]*e->orth_matrix[i*e->dim+j];
    dx /= e->eigenvalues[i];
    ds += dx*dx;
  }
  
  w->dlnl_step = -0.5*(ds2-ds);
}



struct Walker *get_next_walker(struct EnsembleSampler *e) {
  if (e->mode == DJ_SAMPLER && !e->avg) sampler_reset_averages(e);

  if (e->cur_walker < (e->num_half)) {
    e->cur_walker++;
    return (e->w1 + (e->cur_walker-1));
  }
  return NULL;
}

void update_walker(struct EnsembleSampler *e, struct Walker *w, double chi2) {
  int64_t i = w - e->w1;
  assert((i >= 0) && (i < (e->num_half)));
  w->done = 1;
  w->chi2 = chi2;
  if (w->step_mode == DJ_SAMPLER) calc_dlnl_step(e, w);
  while ((e->walkers_done < e->num_half) && (e->w1[e->walkers_done].done))
    e->walkers_done++;
}

int64_t ensemble_ready(struct EnsembleSampler *e) {
  if (e->walkers_done < e->num_half) return 0;
  return 1;
}

void get_accepted_step(struct Walker *w, double *params, double *chi2) {
  memcpy(params, w->params+w->dim, sizeof(double)*w->dim);
  *chi2 = w->accepted_chi2;
}

void write_accepted_steps_to_file(struct EnsembleSampler *e, FILE *out, int64_t binary) {
  int64_t i, j;
  for (i=0; i<e->num_half; i++) {
    if (e->mode == DJ_SAMPLER && e->w1[i].new_dir) continue;
    if (binary) {
      check_fwrite(e->w1[i].params+e->dim, sizeof(double), e->dim, out);
      check_fwrite(&(e->w1[i].accepted_chi2), sizeof(double), 1, out);
    } else {
	for (j=0; j<e->dim; j++)
	  check_fprintf(out, "%.16e ", e->w1[i].params[j+e->dim]);
	check_fprintf(out, "%.16e\n", e->w1[i].accepted_chi2);
    }
  }
  fflush(out);
}

void update_ensemble(struct EnsembleSampler *e) {
  if (e->walkers_done < e->num_half) return;
  int64_t i, j;
  e->walkers_done = e->cur_walker = 0;
  //Test for acceptance:
  for (i=0; i<e->num_half; i++) {
    double lnpdiff = (e->dim - 1.0 - e->static_dim) * log(e->w1[i].z) - 
      0.5*e->inv_temp*(e->w1[i].chi2 - e->w1[i].accepted_chi2);
    if (e->w1[i].step_mode == DJ_SAMPLER) {
      lnpdiff = e->w1[i].dlnl_step - 0.5*e->inv_temp*(e->w1[i].chi2 - e->w1[i].accepted_chi2);
      //fprintf(stderr, "%f %f %f %f\n", e->w1[i].params[0], e->w1[i].params[e->dim], e->w1[i].dlnl_step, -0.5*(e->w1[i].chi2-e->w1[i].accepted_chi2));
    }
#ifdef LNPDIFF_OUTPUT
    fprintf(stderr, "%f\n", lnpdiff);
#endif /*LNPDIFF_OUTPUT*/
    if (e->w1[i].accepted_chi2 < 0 || (isfinite(lnpdiff) && (e->w1[i].chi2 >= 0)
				      && lnpdiff > log(dr250()))) {
      e->w1[i].accepted_chi2 = e->w1[i].chi2;
      memcpy(e->w1[i].params+e->dim, e->w1[i].params, sizeof(double)*e->dim);
      e->w1[i].accepted++;
      e->accepted++;
      e->w1[i].new_dir = 0;
    }
    else {
      e->w1[i].chi2 = e->w1[i].accepted_chi2;
      memcpy(e->w1[i].params, e->w1[i].params+e->dim, sizeof(double)*e->dim);
      e->w1[i].rejected++;
      e->rejected++;
      e->w1[i].new_dir = 1;
    }
  }

  //Update averages:
  if (e->mode == DJ_SAMPLER) {
    int64_t k;
    for (j=0; j<e->dim; j++) {
      double davg = 0;
      for (i=0; i<e->num_half; i++) {
	davg += e->w1[i].params[j];
	for (k=0; k<e->dim; k++)
	  e->cov_matrix[j*e->dim + k] += e->w1[i].params[j]*e->w1[i].params[k];
      }
      e->avg[j] = (e->avg_steps*e->avg[j] + davg)/(double)(e->avg_steps + e->num_half);
    }
    for (i=0; i<e->num_half; i++) {
      //calc_sphere_size(e, e->w1, e->w1+i);
      if (!e->best_params[e->dim] || e->w1[i].chi2 < e->best_params[e->dim]) {
	e->best_params[e->dim] = e->w1[i].chi2;
	for (j=0; j<e->dim; j++) e->best_params[j] = e->w1[i].params[j];
      }
    }
    e->avg_steps += e->num_half;
    if (!e->freeze_cov_matrix)
      sampler_stats_to_step(e);
  }
  
  //Generate new proposals:
  for (i=0; i<e->num_half; i++) {
    e->w1[i].done = 0;
    e->w1[i].z = pow(((e->a - 1.0)*dr250() + 1.0), 2) / e->a;
    int64_t rw = dr250()*e->num_half;
    
    if (e->mode == DJ_SAMPLER && dr250()>0.25) {
      int64_t j, k;
      
      //Random Walk Step:
      if (dr250()>0.0) {
	if (!e->w1[i].step) {
	  check_realloc_s(e->w1[i].step, sizeof(double), e->dim);
	  e->w1[i].new_dir = 1;
	}

	if (e->w1[i].new_dir) {
	  memset(e->w1[i].step, 0, sizeof(double)*e->dim);
	  for (j=0; j<20 || j < e->dim; j++) {
	    rw = dr250()*e->num_half*2.0;
	    double sign = 1;
	    if (rw >= e->num_half) { rw -= e->num_half; sign = -1; }
	    for (k=0; k<e->dim; k++) {
	      e->w1[i].step[k] += sign*(e->w2[rw].params[k+e->dim] - e->best_params[k]);
	    }
	  }
	  for (k=0; k<e->dim; k++) e->w1[i].step[k] /= sqrt(j);
	  double epsilon = 2.4/sqrt(e->dim);
	  epsilon /= 2.0;
	  for (k=0; k<e->dim; k++) e->w1[i].step[k] *= epsilon;
	  e->w1[i].nsteps = 0;
	}

	for (k=0; k<e->dim; k++) e->w1[i].params[k] += e->w1[i].step[k];
	e->w1[i].nsteps++;
	e->w1[i].z = e->w1[i].nsteps;
	e->w1[i].step_mode = GW_SAMPLER;
      } else {
	sampler_random_step(e, e->w1+i);
      }
    } else {
      for (j=0; j<e->dim; j++) {
	double x = e->w2[rw].params[j+e->dim];
	e->w1[i].params[j] = x - e->w1[i].z*(x - e->w1[i].params[j]);
      }
      e->w1[i].step_mode = GW_SAMPLER;
    }
  }

  //Switch proposal halves
  e->w1 = e->w2;
  e->w2 = e->w;
  if (e->w1 == e->w2) e->w2 += e->num_half;
}

double acceptance_fraction(struct EnsembleSampler *e) {
  int64_t t = e->accepted + e->rejected;
  if (!t) return 0;
  return ((double)e->accepted / (double)t);
}

double *autocorrelation(double *params, int64_t nwalkers, int64_t total_steps) {
  int64_t i, ss;
  double a1=0, a2=0, s1=0, s2=0, s12=0;
  int64_t max_step_size = total_steps/nwalkers;
  double *acor = NULL;
  check_realloc_s(acor, sizeof(double), max_step_size);
  acor[0] = 1;
  for (ss=1; ss<max_step_size; ss++) {
    int64_t max_i = total_steps - (ss*nwalkers);
    for (i=0; i<max_i; i++) {
      a1 += params[i];
      a2 += params[i+ss*nwalkers];
    }
    a1 /= (double)max_i;
    a2 /= (double)max_i;
    for (i=0; i<max_i; i++) {
      double d1 = params[i]-a1;
      s1 += d1*d1;
      double d2 = params[i+ss*nwalkers] - a2;
      s2 += d2*d2;
      s12 += d1*d2;
    }
    s1 /= (double)max_i;
    s2 /= (double)max_i;
    s12 /= (double)max_i;
    acor[ss] = (s1 > 0 && s2 > 0) ? s12/sqrt(s1*s2) : 1;
  }
  return acor;
}

void print_autocorrelation(double *params, int64_t nwalkers, int64_t total_steps, FILE *out) {
  int64_t i;
  int64_t max_step_size = total_steps/nwalkers;
  double *acor = autocorrelation(params, nwalkers, total_steps);
  for (i=1; i<max_step_size; i++) {
    fprintf(out, "%"PRId64" %f\n", i, acor[i]);
  }
  fprintf(out, "\n");
  free(acor);
}


#define NN 6
void calc_sphere_size(struct EnsembleSampler *e, struct Walker *w, struct Walker *wi) {
  int64_t i, j;
  double best_r[NN] = {0};
  int64_t best_id[NN] = {-1};
  for (i=0; i<e->num_half; i++) {
    double ds = 0;
    for (j=0; j<e->dim; j++) {
      double dx = w[i].params[j]-wi->params[j];
      ds += dx*dx;
    }
    if (!ds) continue;
    int64_t id = i;
    for (j=0; j<NN; j++) {
      if (ds < best_r[j] || best_r[j]==0) {
	double tmp = best_r[j];
	int64_t tmp_id = best_id[j];
	best_r[j] = ds;
	best_id[j] = id;
	ds = tmp;
	id = tmp_id;
      }
    }
  }
  wi->sphere_size = sqrt(best_r[NN-1])/(sqrt(e->dim));
  memset(wi->avg_pos, 0, sizeof(double)*e->dim);
  for (i=0; i<NN; i++)
    for (j=0; j<e->dim; j++)
      wi->avg_pos[j] += w[best_id[i]].params[j];
  for (j=0; j<e->dim; j++)
    wi->avg_pos[j] += wi->params[j];
  for (j=0; j<e->dim; j++)
    wi->avg_pos[j] /= (double)(NN+1);
  fprintf(stderr, "SS: %f\n", wi->sphere_size);
}


double add_ln_likelihood(double l1, double l2) {
  if (l1 < l2) {
    double tmp = l1;
    l1 = l2;
    l2 = tmp;
  }
  return l1 + log1p(exp(l2-l1));
}

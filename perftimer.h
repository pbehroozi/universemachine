#ifndef _PERFTIMER_H_
#define _PERFTIMER_H_

#ifdef TIME_PROFILING

#include <inttypes.h>

#ifdef __APPLE__
#include <mach/mach.h>
#include <mach/mach_time.h>
#define timer_mach_absolute_time() mach_absolute_time()
#else
int64_t timer_mach_absolute_time(void);
#endif /*__APPLE__*/

void set_timer_role(char *role, int64_t box, int64_t chunk);
extern int64_t timer_t0;
extern int64_t timer_last_write_t;
extern int64_t *timers;
extern int64_t *timer_counts;
extern int64_t num_timers;

int64_t add_timers(char *desc, int64_t num);
int64_t add_timer(char *desc);
void write_timing(int64_t _tnow);

#define timer_init(x,y) int64_t x = add_timer(y);
#define timer_start { timer_t0 = timer_mach_absolute_time(); }
#define timer_inter(x) timers[x] += (timer_mach_absolute_time() - timer_t0)
#define timer_stop(x) { int64_t _tnow = timer_mach_absolute_time(); \
    timers[x]+=(_tnow-timer_t0);	     \
    (timer_counts[x])++; \
  if (_tnow > 1e10+timer_last_write_t) write_timing(_tnow); }

#else /* !TIMER_PROFILING */

#define timer_start
#define timer_init(x,y)
#define timer_inter(x)
#define timer_stop(x)
#define write_timing(x)
#define add_timer(x)
#define add_timers(x,y)
#define set_timer_role(x,y,z)
#define timer_t0 0

#endif /* TIMER_PROFILING */

#endif /* _PERFTIMER_H_ */

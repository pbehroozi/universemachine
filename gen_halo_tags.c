#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "inthash.h"

#define SPEED_OF_LIGHT (299792.458)

struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused

struct catalog_halo **boxes = NULL;
int64_t *lengths = NULL;
int64_t *offsets = NULL;
int64_t num_snaps = 0;


void load_vbins(char *filename);
int sort_by_lvmp(const void *a, const void *b);
void scorr_load_boxes(float target);
double calc_sm(double lvmp);

struct vbin {
  double v, nd, sm, qf;
};
struct vbin *vbins = NULL;
int64_t nvb = 0;


int main(int argc, char **argv)
{
  char buffer[2048];
  int64_t i, j, k;
  FILE *in = NULL;

  FILE **tags = NULL;

  if (argc < 2) {
    fprintf(stderr, "Usage: %s config.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  setup_config();

  snprintf(buffer, 2048, "%s/snaps.txt", INBASE);
  in = check_fopen(buffer, "r");
  while (fgets(buffer, 2048, in)) {
    num_snaps++;
  }

  fprintf(stderr, "Found %"PRId64" snaps\n", num_snaps);
  
  check_realloc_s(boxes, sizeof(struct catalog_halo *), NUM_BLOCKS);
  check_realloc_s(lengths, sizeof(int64_t), NUM_BLOCKS);
  check_realloc_s(offsets, sizeof(int64_t), NUM_BLOCKS*(num_snaps+1));
  check_realloc_s(tags, sizeof(FILE *), NUM_BLOCKS);
  
  for (i=0; i<NUM_BLOCKS; i++) {
    snprintf(buffer, 2048, "%s/cat.box%"PRId64".dat", INBASE, i);
    boxes[i] = check_mmap_file(buffer, 'r', lengths+i);
    snprintf(buffer, 2048, "%s/offsets.box%"PRId64".txt", INBASE, i);
    in = check_fopen(buffer, "r");
    snprintf(buffer, 2048, "%s/tags.box%"PRId64".dat", INBASE, i);
    tags[i] = check_fopen(buffer, "w");
    while (fgets(buffer, 2048, in)) {
      float a;
      int64_t s, o;
      if (sscanf(buffer, "%"SCNd64" %f %"SCNd64, &s, &a, &o)==3) {
	offsets[i*(num_snaps+1)+s] = o;
      } else if (sscanf(buffer, "#Total halos: %"SCNd64, &o)==1) {
	offsets[i*(num_snaps+1)+num_snaps] = o;
      }
    }
    fclose(in);
  }
  
  for (i=0; i<num_snaps; i++) {
    //Build inthash
    struct inthash *ih = new_inthash();
    for (j=0; j<NUM_BLOCKS; j++)
      for (k=offsets[j*(num_snaps+1)+i]; k<offsets[j*(num_snaps+1)+i+1]; k++)
	ih_setval(ih, boxes[j][k].id, boxes[j]+k);

    //Find and print tags
    for (j=0; j<NUM_BLOCKS; j++) {
      for (k=offsets[j*(num_snaps+1)+i]; k<offsets[j*(num_snaps+1)+i+1]; k++) {
	struct extra_halo_tags eh = {0};
	if (boxes[j][k].upid > -1) {
	  struct catalog_halo *ch = ih_getval(ih, boxes[j][k].upid);
	  assert(ch);
	  eh.host_mass = ch->m;
	}
	fwrite(&eh, sizeof(struct extra_halo_tags), 1, tags[j]);
      }
      fflush(tags[j]);
    }
    free_inthash(ih);
    fprintf(stderr, "Snap %"PRId64"\n", i);
  }

  for (i=0; i<NUM_BLOCKS; i++) {
    munmap(boxes[i], lengths[i]);
    fclose(tags[i]);
  }
  return 0;
}

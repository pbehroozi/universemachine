LIB_FLAGS=
CFLAGS=$(LIB_FLAGS) -lm -m64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_DEFAULT_SOURCE -D_POSIX_SOURCE -D_POSIX_C_SOURCE=200809L -D_DARWIN_C_SOURCE -D_BSD_SOURCE -D_SVID_SOURCE -Wall -fno-math-errno -std=c99 
OPT_FLAGS=-O3  #-flto  #-msse3
HDF5_FLAGS=-DH5_USE_16_API -DENABLE_HDF5 -lhdf5 -I/opt/local/include -L/opt/local/lib
#CC=gcc-mp-6
#OPT_FLAGS=-O3 -march=native -mavx2 -ffast-math -Wa,-q

#For recent versions of gcc, use:
#OPT_FLAGS=-O3 -march=native -mavx2 -ffast-math -flto -Wa,-q
#For ICC use
#CC=icc
#OPT_FLAGS=-fast
#For ICC on SLAC computers, use
#OPT_FLAGS=-O3
DEBUG_FLAGS=-g -Wall -O
PROF_FLAGS=-g -pg -ftest-coverage -fprofile-arcs
BASE_FILES=check_syscalls.c stringparse.c mt_rand.c
GSL_FLAGS=-lgsl -lgslcblas -L/opt/local/lib -I/opt/local/include  #-L$(GSL_DIR)/lib -I$(GSL_DIR)/include

all:
	@make reg EXTRA_FLAGS="$(OPT_FLAGS)"

debug:
	@make reg EXTRA_FLAGS="$(DEBUG_FLAGS)"

prof:
	@make reg EXTRA_FLAGS="$(PROF_FLAGS)"

perftime:
	@make reg EXTRA_FLAGS="$(OPT_FLAGS) -lrt -DRSOCKET_PROFILING -DTIME_PROFILING"


treehdf5:
	@make treereg EXTRA_FLAGS="$(OPT_FLAGS) $(HDF5_FLAGS)"

treebuild:
	@make treereg EXTRA_FLAGS="$(OPT_FLAGS)"

treedebug:
	@make treereg EXTRA_FLAGS="$(DEBUG_FLAGS)"

treereg:
	$(CC) zform.c config_vars.c inthash.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o zform
	$(CC) rank_halo_splines.c $(BASE_FILES) $(CFLAGS) $(GSL_FLAGS) $(EXTRA_FLAGS) -o rank_halo_splines
	$(CC) simple_rank_halo_splines.c $(BASE_FILES) $(CFLAGS) $(GSL_FLAGS) $(EXTRA_FLAGS) -o simple_rank_halo_splines
	$(CC) gen_shear_catalog.c  $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_shear_catalog	
	$(CC) split_shear_catalog.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o split_shear_catalog	
	$(CC) shear_catalog.c inthash.c $(BASE_FILES) $(CFLAGS) $(GSL_FLAGS) $(EXTRA_FLAGS) -o shear_catalog	
	$(CC) gen_halo_tags.c config_vars.c inthash.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_halo_tags
	$(CC) shear.c  $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o shear
	$(CC) split_halo_trees_phase2.c cached_io.c inthash.c universe_time.c masses.c orphans.c $(GSL_FLAGS) $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o split_halo_trees_phase2
	$(CC) rank_correlations.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o rank_correlations
	$(CC) split_halo_trees_phase1.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o split_halo_trees_phase1

ic:
	$(CC) test_invalid.c io_helpers.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o test_invalid
	$(CC) fisher.c jacobi_dim.c io_helpers.c sf_model.c  $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o fisher


util:
	@make utilreg EXTRA_FLAGS="$(OPT_FLAGS)"

utildebug:
	@make utilreg EXTRA_FLAGS="$(DEBUG_FLAGS)"

utilreg:
	$(CC) find_neighbors.c config_vars.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o find_neighbors
	$(CC) extract_sfh.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c all_luminosities.c expcache.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_sfh
	$(CC) merger_rates.c config_vars.c config.c read_config.c bin_definitions.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o merger_rates
	$(CC) extract_cmfs.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_cmfs
	$(CC) metallicity_history.c $(BASE_FILES) lightcone_cache.c all_luminosities.c expcache.c inthash.c distance.c config.c read_config.c config_vars.c universe_time.c $(CFLAGS) $(EXTRA_FLAGS) -o metallicity_history
	$(CC) uv_sfr.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o uv_sfr
	$(CC) gen_total_csfr.c sf_model.c mf_cache.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_total_csfr
	$(CC) gen_total_uvlf.c sf_model.c mf_cache.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_total_uvlf
	$(CC) gen_plot_data.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_plot_data
	$(CC) extract_slopes.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_slopes
	$(CC) extract_smhm_params.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_smhm_params
	$(CC) split_samples.c mcmc_data.c config_vars.c config.c read_config.c universe_time.c distance.c bin_definitions.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o split_samples
	$(CC) -DSAMPLE_CORR sample_corr_sm.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c correl/correl.c correl/regions.c correl/matrices.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_corr_sm
	$(CC) -DSAMPLE_CORR sample_corr_sm_regions.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c correl/cross_correl.c correl/regions.c correl/matrices.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_corr_sm_regions
	$(CC) uv_mstar.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o uv_mstar
	$(CC) gen_colorplot_uncertainties.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_colorplot_uncertainties
	$(CC) gen_bestfit_params.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_bestfit_params
	$(CC) match_colors.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o match_colors
	$(CC) extract_orphan_info.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_orphan_info
	$(CC) extract_halo_info.c inthash.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o extract_halo_info
	$(CC) add_dust.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o add_dust
	$(CC) summary_stats.c config_vars.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o summary_stats
	$(CC) corr_catalog_test.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c correl/correl.c correl/regions.c correl/matrices.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o corr_catalog_test
	$(CC) to_binary_sm_catalog.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o to_binary_sm_catalog
	$(CC) -DSTATS_SAVEMEM analyze_sfh.c io_helpers.c smloss.c universe_time.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o analyze_sfh
	$(CC) -DSTATS_SAVEMEM -DSHORT_ANALYZE analyze_sfh.c io_helpers.c smloss.c universe_time.c stats.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o analyze_sfh_short
	$(CC) corr_catalog.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c correl/correl.c correl/regions.c correl/matrices.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o corr_catalog
	$(CC) analysis.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o analysis
	$(CC) -DSAMPLE_CORR sample_dens_sm.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_dens_sm
	$(CC) -DSAMPLE_CORR sample_conf_sm.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_conf_sm
	$(CC) gen_lums.c all_luminosities.c stringparse.c check_syscalls.c universe_time.c expcache.c $(CFLAGS) $(EXTRA_FLAGS) -o gen_lums
	$(CC) vmpf.c config_vars.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o vmpf
	$(CC) sample_corr.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_corr
	$(CC) sample_corr_n2.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c smoothing.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o sample_corr_n2

reg:
	$(CC) make_sf_catalog.c inthash.c muzzin_uvj.c integrate.c smoothing.c sf_model.c io_helpers.c all_luminosities.c expcache.c universe_time.c config_vars.c config.c smloss.c read_config.c perftimer.c vweights.c distance.c watchdog.c bin_definitions.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o make_sf_catalog
	$(CC) lightcone.c $(BASE_FILES) lightcone_cache.c distance.c config.c read_config.c config_vars.c matrices.c trapezoid_prism.c $(CFLAGS) $(EXTRA_FLAGS) -o lightcone
	$(CC) print_sm_catalog.c config_vars.c config.c read_config.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o print_sm_catalog
	$(CC) umachine_server.c sf_model.c io_helpers.c inet/address.c inet/rsocket.c inet/socket.c inet/pload.c config_vars.c config.c read_config.c sampler.c fitter.c adaptive_mcmc.c jacobi_dim.c watchdog.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o umachine_server
	$(CC) gen_obs_comparisons.c config_vars.c config.c read_config.c inline_analysis.c fit_smhm.c sampler.c jacobi_dim.c universe_time.c distance.c bin_definitions.c observations.c vweights.c io_helpers.c watchdog.c sf_model.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -o gen_obs_comparisons
	$(CC) box_server.c sf_model.c all_luminosities.c expcache.c observations.c vweights.c bounds.c io_helpers.c make_sf_catalog.c inthash.c muzzin_uvj.c corr_lib.c integrate.c bin_definitions.c comm_lib.c inet/rsocket.c inet/socket.c inet/pload.c inet/address.c universe_time.c distance.c config_vars.c config.c smloss.c read_config.c watchdog.c perftimer.c inline_analysis.c sampler.c jacobi_dim.c fit_smhm.c smoothing.c $(BASE_FILES) $(CFLAGS) $(EXTRA_FLAGS) -DSF_CATALOG_LIB -o box_server

clean:
	rm -f rank_halo_splines


.REMAKE:

dist: .REMAKE
	@perl -ne 'print "$$1\n" if (/VERSION\s*\"([^\"]+)/)' version.h > VERSION
	cd ../ ; tar -chzvf umachine.tar.gz UniverseMachine/Makefile UniverseMachine/obs UniverseMachine/*.[ch] UniverseMachine/scripts UniverseMachine/perl/*.pm UniverseMachine/perl/*/*.pm UniverseMachine/minimize.py UniverseMachine/examples/loader.py UniverseMachine/python/*.py UniverseMachine/correl/auto_regions.txt UniverseMachine/correl/*.pl UniverseMachine/*/*.[ch] UniverseMachine/*/Makefile UniverseMachine/VERSION UniverseMachine/CHANGELOG UniverseMachine/SCIENCE_CHANGELOG UniverseMachine/LICENSE UniverseMachine/README.pdf; mv umachine.tar.gz UniverseMachine

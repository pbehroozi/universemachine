#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"

extern struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused

#define FAST3TREE_TYPE   struct catalog_galaxy
#define FAST3TREE_PREFIX SAMPLE_CORR
#define FAST3TREE_DIM    2
#include "fast3tree.c"


int sort_by_lvmp(const void *a, const void *b);
void scorr_load_boxes(float target);

int main(int argc, char **argv)
{
  int64_t i, j, k;

  if (argc < 10) {
    fprintf(stderr, "Usage: %s config.cfg scale gdens_low gdens_high f_q f_nr sig_v obs_volume samples...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  do_config(argv[1]);
  setup_config();
  float scale = atof(argv[2]);
  float glow = atof(argv[3]);
  float ghigh = atof(argv[4]);
  float fq = atof(argv[5]);
  float fnr = atof(argv[6]);
  float sig_v = atof(argv[7]);
  float obs_v = atof(argv[8]);
  int64_t samples = atoll(argv[9]);

  r250_init(87L);
  scorr_load_boxes(scale);
  for (i=0; i<nh; i++)
    halos[i].lvmp += normal_random(0, sig_v);
  qsort(halos, nh, sizeof(struct catalog_halo), sort_by_lvmp);
  double volume = pow(BOX_SIZE/h0, 3.0);
  int64_t nlow = glow*volume;
  int64_t nhigh = ghigh*volume;
  int64_t ng = nhigh-nlow;
  check_realloc_s(galaxies, sizeof(struct catalog_galaxy), ng);

  float ra1 = fnr;
  float ra2 = sqrt(1.0-fnr*fnr);
  float hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;

  struct catalog_galaxy *galaxies2 = NULL;
  int64_t num_g2 = 0;

  for (i=nlow; i<nhigh; i++) {
    float ra = ra1*halos[i].rank1 + ra2*halos[i].ra;
    float rank = 0.5*(1.0+erf(ra*M_SQRT1_2));
    struct catalog_galaxy *g = galaxies + (i-nlow);
    g->lsfr = (rank > fq) ? 1 : -1;
    g->pos[0] = halos[i].pos[0];
    g->pos[1] = halos[i].pos[1];
    g->pos[2] = halos[i].pos[2] + halos[i].pos[5]/hz;
    while (g->pos[2]<0) g->pos[2] += BOX_SIZE;
    while (g->pos[2]>BOX_SIZE) g->pos[2] -= BOX_SIZE;
#define ADD_GALAXY { check_realloc_every(galaxies2, sizeof(struct catalog_galaxy), num_g2, 1000); galaxies2[num_g2] = g2; num_g2++; }
    for (j=-1; j<=1; j++) {
      for (k=-1; k<=1; k++) {
	struct catalog_galaxy g2 = *g;
	g2.pos[0] += BOX_SIZE*j;
	g2.pos[1] += BOX_SIZE*k;
	if ((g2.pos[0] > -CORR_LENGTH_RP) && (g2.pos[0] < BOX_SIZE+CORR_LENGTH_RP) &&
	    (g2.pos[1] > -CORR_LENGTH_RP) && (g2.pos[1] < BOX_SIZE+CORR_LENGTH_RP))
	  ADD_GALAXY;
      }
    }
#undef ADD_GALAXY
  }

  struct fast3tree *t = fast3tree_init(ng, galaxies);
  struct fast3tree_results *r = fast3tree_results_init();
  _fast3tree_set_minmax(t, 0, BOX_SIZE);

  init_dists();
  init_corr_tree(galaxies2, num_g2);
  int64_t *bin_counts = NULL;
  check_realloc_s(bin_counts, sizeof(int64_t), NUM_BINS*3*ng);
  for (i=0; i<ng; i++) {
    clear_corrs();
    for (j=0; j<num_g2; j++) {
      double ds=0, dx;
      dx = fabs(galaxies2[j].pos[2] - galaxies[i].pos[2]);
      if (dx > BOX_SIZE/2.0) dx = BOX_SIZE - dx;
      if (dx > CORR_LENGTH_PI) continue;
      for (k=0; k<2; k++) {
	dx = galaxies2[j].pos[k] - galaxies[i].pos[k];
	ds += dx*dx;
      }
      if (!ds) continue;
      float ld = log10f(ds)/2.0;
      float bin = DIST_BPDEX*(ld-MIN_DIST);
      if (bin < 0 || bin >= NUM_BINS) continue;
      int64_t b = bin;
      corr[b]++;
      if (galaxies[i].lsfr < 0 && galaxies2[j].lsfr < 0) corr_q[b]++;
      if (galaxies[i].lsfr > 0 && galaxies2[j].lsfr > 0) corr_sf[b]++;
    }
    memcpy(bin_counts+i*NUM_BINS*3,            corr,    NUM_BINS*sizeof(int64_t));
    memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS,   corr_sf, NUM_BINS*sizeof(int64_t));
    memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS*2, corr_q,  NUM_BINS*sizeof(int64_t));
  }

  obs_v *= pow(h0, 3);
  double rad = sqrt(obs_v/M_PI/BOX_SIZE);

  //Compute correlation function
  for (i=0; i<samples; i++) {
    float c[2];
    clear_corrs();
    printf("#Sample %"PRId64"\n", i);
    c[0] = dr250()*BOX_SIZE;
    c[1] = dr250()*BOX_SIZE;
    if (i==0) fast3tree_find_sphere(t, r, c, BOX_SIZE);
    else fast3tree_find_sphere_periodic(t, r, c, rad);
    int64_t nq=0, na=0, nsf=0;
    for (j=0; j<r->num_points; j++) {
      na++;
      if (r->points[j]->lsfr > 0) nsf++;
      else nq++;
      int64_t *gcorr = bin_counts + (r->points[j]-galaxies)*NUM_BINS*3;
      for (k=0; k<NUM_BINS; k++) {
	corr[k] += gcorr[k];
	corr_sf[k] += gcorr[k+NUM_BINS];
	corr_q[k] += gcorr[k+NUM_BINS*2];
      }
    }
    printf("#NA: %"PRId64"; NSF: %"PRId64"; NQ: %"PRId64"\n", na, nsf, nq);
    float vol = (i==0) ? pow(BOX_SIZE, 3) : obs_v;
    float da = na/vol, dsf = nsf/vol, dq=nq/vol;
    printf("#Vol: %g\n", vol);
    printf("#R C(all-all) C(sf-sf) C(q-q)\n");
    if (na < 1) na = 1;
    if (nq < 1) nq = 1;
    if (nsf < 1) nsf = 1;
    for (j=0; j<NUM_BINS; j++) {
      float a1 = corr_dists[j]*M_PI;
      float a2 = corr_dists[j+1]*M_PI;
      float a = a2-a1;
      float av_r = sqrt(0.5*(a1+a2)/M_PI);
      float raa = 2.0*da*a*CORR_LENGTH_PI;
      float rasf = 2.0*dsf*a*CORR_LENGTH_PI;
      float raq = 2.0*dq*a*CORR_LENGTH_PI;
      printf("%f %e %e %e %e %g %g %g %g\n", av_r, (corr[j]-na*raa)/(na*da*a), (corr_sf[j]-nsf*rasf)/(nsf*dsf*a), (corr_q[j]-nq*raq)/(nq*dq*a), (float)corr[j], da, a, raa, (float)corr_bin_of(av_r*av_r));
    }
  }
  return 0;
}


int sort_by_lvmp(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if (c->lvmp > d->lvmp) return -1;
  if (c->lvmp < d->lvmp) return 1;
  return 0;
}


void scorr_load_boxes(float target) {
  char buffer[1024];
  int64_t i, box_num, min_snap, offset, snap, n;
  float scale;

  for (box_num=0; box_num<NUM_BLOCKS; box_num++) {
    num_scales = 0;
    //First load offsets from file:
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, box_num);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    min_snap=0;
    for (i=1; i<num_scales; i++)
      if (fabs(target-scales[min_snap])>fabs(target-scales[i])) min_snap = i;

    int64_t to_read = offsets[min_snap+1]-offsets[min_snap];

    //Then load halos
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, box_num);
    in = check_fopen(buffer, "r");
    check_fseeko(in, offsets[min_snap]*sizeof(struct catalog_halo), 0);
    check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
    check_fread(halos+nh, sizeof(struct catalog_halo), to_read, in);
    fclose(in);
    nh += to_read;
  }
}

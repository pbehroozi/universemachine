#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"

/* Routine to load box_server.X.samples file and split into one file per sample */
int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg samples_file\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]); //Necessary to check for postprocessing mode

  //Loads data from argv[2], saves to multiple files
  //mcmc_data->total_length is the length of one sample
  char buffer[2048];
  struct mcmc_data *input = load_mcmc_data(argv[2]);
  for (int64_t i=0; i<input->num_samples; i++) {
    snprintf(buffer, 2048, "%s.%"PRId64, argv[2], i);
    FILE *output = check_fopen(buffer, "w");
    fwrite(input->all + input->total_length*i, input->total_length, 1, output);
    fclose(output);
  }
  return 0;
}

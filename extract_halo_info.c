#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "inthash.h"
#include "stringparse.h"
#include "orphans.h"

int main(int argc, char **argv) {
  int64_t i;
  if (argc<2) {
    printf("Usage: %s halo_list.txt hlists...\n", argv[0]);
    exit(1);
  }
  
  char buffer[2048];
  struct inthash *ids_to_find = new_inthash();
  FILE *in = check_fopen(argv[1], "r");
  while (fgets(buffer, 2048, in)) {
    int64_t id=0;
    if (buffer[0]=='#') continue;
    sscanf(buffer, "%"SCNd64, &id);
    if (id<2) //Scale factor is first column, scan second column
      sscanf(buffer, "%*f %"SCNd64, &id);
    if (id<2) continue;
    id %= ORPHAN_ID_OFFSET;
    ih_setval(ids_to_find, id, (void *)1);
  }
  fclose(in);

  SHORT_PARSETYPE;
  enum parsetype t[2] = {K, D64};
  int64_t id;
  void *data[2] = {NULL, &id};
  
  for (i=2; i<argc; i++) {
    FILE *in = check_fopen(argv[i], "r");
    while (fgets(buffer, 2048, in)) {
      if (stringparse(buffer, data, t, 2) != 2) continue;
      if (ih_getval(ids_to_find, id)) printf("%s", buffer);
    }
    fclose(in);
  }
  return 0;
}

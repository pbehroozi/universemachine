#ifndef _HALO_H_
#define _HALO_H_

#define A_UV(x) ((x).t_tdyn)
struct catalog_halo {
  int64_t id, descid, upid;
  int32_t flags; float uparent_dist;
  float pos[6], vmp, lvmp, mp, m, v, r;
  float rank1, rank2, ra, rarank, t_tdyn;
  float sm, icl, sfr, obs_sm, obs_sfr, obs_uv;
  float weight;
};

#define CLEAR_OBSERVABLES(x) { (x).sm = (x).icl = (x).sfr = (x).obs_sm = (x).obs_sfr = (x).obs_uv = 0; }

#define NO_PROGENITOR_FLAG      1  //Halo has no progenitor in the tree
#define MMP_FLAG                2  //Halo is most-massive progenitor of descendant
#define LAST_PROGENITOR_FLAG    4  //Halo is last progenitor (at this timestep) of descendant halo (no longer used)
#define ORPHAN_FLAG             8  //Halo is an orphan
#define IGNORE_FLAG            16  //Halo excluded from analysis (e.g., for orphans below disruption threshold)
#define FOREST_FLAG            32  //Halo is root of a forest
#define FIRST_PROGENITOR_FLAG  64  //Halo is the first progenitor of its descendant
#define EFFECTIVE_MMP_FLAG    128  //Halo is the most-massive progenitor of its descendant

struct reduced_halo {
  //  int32_t descid, flags;
  //Flags can be in bitmask, descid can be evident from ordering
  float uparent_dist, lvmp, rank;
};

struct reduced_halo_outputs {
  float sm, icl, sfr, obs_sm, obs_sfr, obs_uv;
};

struct extra_halo_tags {
  float host_mass;
};

struct extra_halo_info {
  float max_host_lvmp;
};

struct wl_data {
  int64_t id;
  int32_t pcounts[NUM_WL_BINS];
};


struct catalog_galaxy {
  float lsm, lsfr, pos[3], weight;
#ifdef SAMPLE_CORR
  float opos[6];
#endif /*SAMPLE_CORR*/
#ifdef SPLASHBACK
  float hm,rank1,ssfr;
  int64_t upid;
#endif
};

struct catalog_galaxy_weights {
  float sf[NUM_CORR_THRESHES];
  float q[NUM_CORR_THRESHES];
  float max_weight;
};

#endif /* _HALO_H_ */

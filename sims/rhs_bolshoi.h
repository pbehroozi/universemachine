#ifndef _RANK_HALO_SPLINES_H_
#define _RANK_HALO_SPLINES_H_

/*Instructions for modification:
1) Compare the first line of one of your hlist catalogs to the
  #define statements below, and change any column numbers that do not match the
  numbers in parentheses.  E.g., for Vmax, the header for Bolshoi-Planck is
  vmax(16), which matches the #define VM 16 statement below.

2) Modify the contents of the halo_property structure so that the ordering and number of elements matches the columns in the hlist file.  You can remove columns that don't exist; however, at least the columns in the define statements should be present.  You can also reduce runtime by setting the "calculate spline" field to 0 for everything except for DeltaLogVmax.
*/

/*Less likely to change*/
#define SAT 6  /* UPID column */
#define VM 16  /* Vmax column */
#define POS 17 /* X column    */
#define VX 20  /* VX column   */
#define JX 23  /* JX Column   */

/*More likely to change*/
#define NUM_LABELS 79  /* Total number of columns*/

#define AX 48          /* Ax (shape axis) column */
#define AX500 53       /* Ax_500c column         */
#define BTOA 46        /* B/A ratio column       */
#define BTOA500c 51    /* B/A(500c) column       */

#define VMP 74         /* Vmax @ Mpeak column    */
#define VP 62          /* Vpeak column           */
#define VMVP (NUM_LABELS) /* Vmax/Vpeak column; auto-generated*/


#define NUM_PROPS (NUM_LABELS+1)  /* Don't change */

struct halo_property {  /* Don't change */
  char label[50];
  int important;
  int scaling;
};


/*Column descriptors: name, calculate(1)/don't calculate(0) splines, and expected vmax scaling power */

struct halo_property properties[NUM_PROPS] =
  /* Less likely to change */
  {{"scale",0,0}, {"id",0,0}, {"desc_scale",0,0}, {"desc_id",0,0}, {"num_prog",0,0}, 
   {"pid",0,0}, {"upid",0,0}, {"desc_pid",0,0}, {"phantom",0,0}, {"sam_mvir",1,3},
   {"mvir",1,3}, {"rvir",0,0}, {"rs",1,1}, {"vrms",1,1}, {"mmp",0,0}, 
   {"scale_of_last_MM",1,0}, {"vmax",1,1}, {"x",0,0}, {"y",0,0}, {"z",0,0},
   {"-vabs",1,1}, {"vy",0,0}, {"vz",0,0}, {"Jabs",1,5}, {"Jy",0,0}, {"Jz",0,0},
   {"Spin",1,0}, {"Breadth_first_ID",0,0}, {"Depth_first_ID",0,0}, {"Tree_root_ID",0,0},
   {"Orig_halo_ID",0,0}, {"Snap_num",0,0}, {"Next_coprogenitor_depthfirst_ID",0,0},
   {"Last_progenitor_depthfirst_ID",0,0}, {"Last_mainleaf_depthfirst_ID",0,0},
   {"Tidal_Force", 1, 0}, {"Tidal_Force_ID", 0, 0},
					      
   /* More likely to change */
   {"Rs_Klypin",1,1}, {"Mvir_all",1,3}, {"M200b",1,3}, {"M200c",1,3}, {"M500c",1,3},
   {"M2500c",1,3}, {"Xoff",1,1}, {"Voff",1,1}, {"Spin_Bullock",1,0}, {"b_to_a",1,0},
   {"c_to_a",1,0}, {"Aabs",1,1}, {"Ay",0,0}, {"Az",0,0}, {"b_to_a500c",1,0},
   {"c_to_a500c",1,0}, {"A500c_abs",1,1}, {"Ay500c",0,0}, {"Az500c",0,0}, {"TU",1,0},
   {"M_pe_Behroozi",1,3}, {"M_pe_Diemer",1,3}, {"Macc",1,3}, {"Mpeak",1,3}, {"Vacc",1,1},
   {"Vpeak",1,1}, {"Halfmass_Scale",1,0}, {"Acc_Rate_Inst",1,3}, {"Acc_Rate_100Myr",1,3},
   {"Acc_Rate_1Tdyn",1,3}, {"Acc_Rate_2Tdyn",1,3}, {"Acc_Rate_Mpeak",1,3},
   {"Mpeak_Scale",1,0}, {"Acc_Scale",1,0}, {"First_Acc_Scale",1,0}, 
   {"First_Acc_Mvir",1,3}, {"First_Acc_Vmax",1,1}, {"VmaxMpeak",0,0}, 
   {"Tidal_Force_Tdyn",1,0}, {"DeltaLogVmax", 1, 0},
   {"Time_to_future_merger",0,0}, {"Future_merger_MMP_ID",0,0}, 
   {"VmaxVpeak",1,0}};

#endif /* _RANK_HALO_SPLINES_H_ */


/* For reference: Hlist header for Bolshoi-Planck: 
#scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8) sam_mvir(9) mvir(10) rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19) vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28) Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32) Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Tidal_Force(35) Tidal_ID(36) Rs_Klypin(37) Mvir_all(38) M200b(39) M200c(40) M500c(41) M2500c(42) Xoff(43) Voff(44) Spin_Bullock(45) b_to_a(46) c_to_a(47) A[x](48) A[y](49) A[z](50) b_to_a(500c)(51) c_to_a(500c)(52) A[x](500c)(53) A[y](500c)(54) A[z](500c)(55) T/|U|(56) M_pe_Behroozi(57) M_pe_Diemer(58) Macc(59) Mpeak(60) Vacc(61) Vpeak(62) Halfmass_Scale(63) Acc_Rate_Inst(64) Acc_Rate_100Myr(65) Acc_Rate_1*Tdyn(66) Acc_Rate_2*Tdyn(67) Acc_Rate_Mpeak(68) Mpeak_Scale(69) Acc_Scale(70) First_Acc_Scale(71) First_Acc_Mvir(72) First_Acc_Vmax(73) Vmax\@Mpeak(74) Tidal_Force_Tdyn(75) Log_(Vmax/Vmax_max(Tdyn;Tmpeak))(76) Time_to_future_merger(77) Future_merger_MMP_ID(78)
*/



#ifndef _UMACHINE_SERVER_H_
#define _UMACHINE_SERVER_H_

#define AUTO_CONFIG_NAME "auto-um.cfg"

int64_t read_positions(FILE *posfile, int64_t reset_after_hash);
int64_t setup_server_port(void);
void print_time(FILE *output);
void umachine_server_loop(void);
void fitter_loop(void);
void fork_box_from_umachine_server(char *umachine_server);

#endif /* _UMACHINE_SERVER_H_ */

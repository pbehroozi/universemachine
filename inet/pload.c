#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <assert.h>
#include <sys/stat.h>
#include "rsocket.h"
#include "socket.h"
#include "../check_syscalls.h"
#include "address.h"

#define PLOAD_MAX_HOST_LENGTH 400
struct pclient {
  char host[PLOAD_MAX_HOST_LENGTH];
  char port[10];
  int64_t socket;
};

void clear_ploads(char *dirname) {
  DIR *in = opendir(dirname);
  char buffer[1024];
  struct dirent *d;
  if (!in) {
    fprintf(stderr, "Directory: \"%s\"\n", dirname);
    system_error("Could not open!");
  }
  while ((d=readdir(in))) {
    int64_t len = strlen(d->d_name);
    if (len == 0 || (d->d_type & DT_DIR) || d->d_name[len-1]=='~') continue;
    if ((len > 6 && strcmp(d->d_name+len-6, ".pload")==0) ||
	(len > 10 && strcmp(d->d_name+len-10, ".pload.tmp")==0)) {
      snprintf(buffer, 1024, "%s/%s", dirname, d->d_name);
      //printf("Unlinking %s!\n", buffer);
      unlink(buffer);
    }
  }
  closedir(in);
}

void _pblast(void *data, int64_t length, struct pclient *clients, int64_t num_clients) {
  int64_t i;
  for (i=num_clients-1; i>=0; i--) {
    int64_t c = connect_to_addr(clients[i].host, clients[i].port);
    send_to_socket_noconfirm(c, data, length);
    int64_t my_num_clients = (i+1)/2;
    int64_t their_num_clients = i - my_num_clients;
    send_to_socket_noconfirm(c, &their_num_clients, sizeof(int64_t));
    if (their_num_clients)
      send_to_socket(c, clients+my_num_clients, 
		     sizeof(struct pclient)*their_num_clients);
    i = my_num_clients;
    close_rsocket(c);
  }
}

int64_t start_pload_server(char **host, char **port, char *interface) {
  int64_t s, tries, addr_found = 0;
  if (interface && strlen(interface)) {
    *host = get_interface_address(interface);
    if (*host) addr_found = 1;
  }
    
  if (!addr_found) {
    *host = check_realloc(*host, 1024, "Allocating hostname.");
    if (gethostname(*host, 1023)<0) {
        printf("Unable to get host address!\n");
        exit(1);
      }
    }
  
  *port = check_realloc(*port, sizeof(char)*10, "Allocating port.");
  for (tries = 0; tries<500; tries++) {
    snprintf(*port, 10, "%d", (rand()%63000)+2000);
    s = listen_at_addr(*host, *port);
    if (s>=0) break;
  }

  if (s<0) {
    fprintf(stderr, "Unable to start server on %s!\n", *host);
    exit(EXIT_FAILURE);
  }
  return s;
}  


void *_pload_file(char *filename, int64_t length, void *data) {
  FILE *in = check_fopen(filename, "r");
  check_realloc_s(data, length, 1);
  check_fread(data, length, 1, in);
  fclose(in);
  return data;
}

void *pload(char *filename, char *inbase, char *outbase, int64_t id, int64_t total_ids, void *data, int64_t length, char *interface, int64_t num_blocks) {
  int64_t i, num_clients = 0;
  struct pclient *pclients = NULL;
  char buffer[1024];
  char *host=NULL, *port=NULL;

  //if (total_ids <= 1) return data;
  if (total_ids <= 1) {
    snprintf(buffer, 1024, "%s/%s", inbase, filename);
    return _pload_file(buffer, length, data);
  }

  snprintf(buffer, 1024, "%s/%s.pload", outbase, filename);
  int64_t s = start_pload_server(&host, &port, interface);
  if (id==0) {
    snprintf(buffer, 1024, "%s/%s", inbase, filename);
    data = _pload_file(buffer, length, data);
    snprintf(buffer, 1024, "%s/%s.pload", outbase, filename);

    char *fn = check_strdup(buffer);
    snprintf(buffer, 1024, "%s.tmp", fn);
    FILE *out = check_fopen(buffer, "w");
    fprintf(out, "%s\n", host);
    fprintf(out, "%s\n", port);
    fclose(out);
    check_realloc_s(pclients, sizeof(struct pclient), total_ids-1);
    int64_t sleeptime = num_blocks/20;
    if (sleeptime < 5) sleeptime = 5;
    random_sleep(sleeptime);
    //random_sleep(1);
    check_rename(buffer, fn);

    while (num_clients < total_ids-1) {
      pclients[num_clients].socket = accept_connection(s, NULL, NULL);
      num_clients++;
    }
    for (i=0; i<num_clients; i++) {
      int64_t cs = pclients[i].socket;
      recv_from_socket(cs, pclients+i, sizeof(struct pclient));
      close_rsocket(cs);
    }
    unlink(fn);
    free(fn);
  }
  else {
    check_realloc_s(data, sizeof(char), length);
    FILE *in = NULL;
    //Aim for < ~20 conns/sec overall
    int64_t sleeptime = total_ids/20;
    if (sleeptime < 5) sleeptime = 5;
    while (!in) {
      random_sleep(sleeptime);
      in = fopen(buffer, "r");
    }
    check_fgets_and_chomp(buffer, 1024, in);
    char *serv_host = check_strdup(buffer);
    check_fgets_and_chomp(buffer, 1024, in);
    int64_t cm = connect_to_addr(serv_host, buffer);
    fclose(in);

    free(serv_host);

    struct pclient self;
    if (strlen(host) >= PLOAD_MAX_HOST_LENGTH) {
      fprintf(stderr, "[Error] host length >= %d chars (host: %s)\n", PLOAD_MAX_HOST_LENGTH, host);
      exit(EXIT_FAILURE);
    }

    snprintf(self.host, PLOAD_MAX_HOST_LENGTH, "%s", host);
    snprintf(self.port, 10, "%s", port);
    send_to_socket_noconfirm(cm, &self, sizeof(struct pclient));
    close_rsocket(cm);

    int64_t m = accept_connection(s, NULL, NULL);
    recv_from_socket(m, data, length);
    recv_from_socket(m, &num_clients, sizeof(int64_t));
    if (num_clients)
      pclients = recv_and_alloc(m, NULL, sizeof(struct pclient)*num_clients);
    close_rsocket(m);
  }
  if (num_clients)
    _pblast(data, length, pclients, num_clients);
  close_rsocket(s);
  return data;
}

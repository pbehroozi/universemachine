#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>
#include "mt_rand.h"
#include "check_syscalls.h"

#define COUNTS_TYPE double

struct counts_distribution {
  int64_t max_counts, num_stages, num_bins;
  COUNTS_TYPE total_samples, tol;
  int64_t *max, *min;
  COUNTS_TYPE *_dist;
  COUNTS_TYPE *final_dist;
};

void reset_counts_distribution(struct counts_distribution *cd);

void init_counts_distribution(struct counts_distribution *cd, int64_t max_counts, COUNTS_TYPE tol) {
  memset(cd, 0, sizeof(struct counts_distribution));
  cd->max_counts = max_counts;
  cd->num_stages = log(max_counts)/log(2);
  cd->tol = tol;
  cd->num_bins = ((uint64_t)1 << (cd->num_stages))+cd->num_stages
    +1+max_counts;
  check_realloc_s(cd->_dist, sizeof(COUNTS_TYPE), cd->num_bins);
  check_realloc_s(cd->max, sizeof(int64_t), cd->num_stages+1);
  check_realloc_s(cd->min, sizeof(int64_t), cd->num_stages+1);
  cd->final_dist = cd->_dist + cd->num_bins - max_counts - 1;
  reset_counts_distribution(cd);
}

void reset_counts_distribution(struct counts_distribution *cd) {
  int64_t i, j;
  memset(cd->_dist, 0, sizeof(COUNTS_TYPE)*cd->num_bins);
  memset(cd->max, 0, sizeof(int64_t)*(cd->num_stages+1));
  memset(cd->min, 0, sizeof(int64_t)*(cd->num_stages+1));
  cd->final_dist[0] = 1;
  j=0;
  for (i=0; i<cd->num_stages; i++) {
    cd->_dist[j] = 1;
    j += 1 + (1 << i);
  }
}

void free_counts_distribution(struct counts_distribution *cd) {
  if (cd->_dist) {
    free(cd->_dist);
    free(cd->max);
    free(cd->min);
  }
  memset(cd, 0, sizeof(struct counts_distribution));
}

void add_to_counts_distribution(struct counts_distribution *cd, COUNTS_TYPE prob) {
  int64_t i, j, k, max_counts=0;
  if (prob < cd->tol) return;
  assert(prob <= 1.0);
  COUNTS_TYPE inv = 1.0-prob;
  int64_t max = cd->max[cd->num_stages];
  int64_t min = cd->min[cd->num_stages];
  i = (max == cd->max_counts) ? (max-1) : max;
  for (; i >= min; i--) {
    cd->final_dist[i+1] += prob*cd->final_dist[i];
    cd->final_dist[i] *= inv;
  }
  if (min < max && cd->final_dist[min+1] < cd->tol) {
    //cd->final_dist[min] < cd->tol) {
    cd->min[cd->num_stages]++;
    cd->final_dist[min+1] += cd->final_dist[min];
    cd->final_dist[min] = 0;
  }
  if (max < cd->max_counts && cd->final_dist[max+1] > cd->tol)
    cd->max[cd->num_stages]++;

  if (0) { //Structured
    for (i=1; i<cd->num_stages; i++) { //Always ignore first stage
      
    }
  }
}


void finalize_counts_distribution(struct counts_distribution *cd) {
  int64_t i;
  double norm = 0;
  for (i=0; i<=cd->max_counts; i++) norm += cd->final_dist[i];
  norm = 1.0/norm;
  for (i=0; i<=cd->max_counts; i++) cd->final_dist[i]*=norm;
}

//For testing
#include <gsl/gsl_randist.h>
int main(void) {
  int64_t i;
  double ev2 = 0;
  r250_init(87L);
  struct counts_distribution cd;
  init_counts_distribution(&cd, 150, 0);
  for (i=0; i<160; i++) {
    double r = 0.5; //dr250();
    ev2 += r;
    add_to_counts_distribution(&cd, r);
  }
  finalize_counts_distribution(&cd);
  double ev = 0, counts = 0;
  for (i=0; i<151; i++) {
    double exact = gsl_ran_binomial_pdf(i, 0.5, 160);
    printf("%"PRId64" %g %g %g\n", i, cd.final_dist[i], exact, exact-cd.final_dist[i]);
    ev += i*cd.final_dist[i];
    counts += cd.final_dist[i];
  }
  printf("#Exp_value: %g (norm: %g) vs. expected: %g)\n", ev/counts, counts, ev2);
  return 0;
}

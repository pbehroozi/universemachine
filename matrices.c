#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "mt_rand.h"

#define MIN_SEPARATION 10 //Mpc

//Calculates out = in1*in2
void mult_matrix(float in1[3][3], float in2[3][3], float out[3][3]) {
  int i,j,k;
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      out[i][j] = 0;
      for (k=0; k<3; k++) out[i][j] += in1[k][j]*in2[i][k];
    }
  }
}

//Calculates out = mat*in
void mult_vector(float mat[3][3], float in[3], float out[3]) {
  int i,j;
  for (i=0; i<3; i++) {
    out[i] = 0;
    for (j=0; j<3; j++) out[i]+=mat[j][i]*in[j];
  }
}

//Copies one matrix (in) to another (out)
void copy_matrix(float in[3][3], float out[3][3]) {
  int i,j;
  for (i=0; i<3; i++) for (j=0; j<3; j++) out[i][j] = in[i][j];
}

//Inverts any orthogonal matrix (via transpose).
void inv_rot_matrix(float in[3][3], float out[3][3]) {
  int i,j;
  for (i=0; i<3; i++) for (j=0; j<3; j++) out[i][j] = in[j][i];
}

//Generates a 3x3 rotation matrix
//First rotates the x-axis by the polar angle p (-pi/2 to pi/2),
//Then rotates the resulting axis by the azimuthal angle a (0 to 2pi),
//Then rotates around that axis by theta (0 to 2pi).
void rotate3(float p, float a, float theta, float rot[3][3])
{
  //float x[3] = { 1,0,0 };
  //float n[3];
  //float c, s;
  float mul1[3][3] = {{1,0,0},{0,1,0},{0,0,1}};
  float mul2[3][3] = {{1,0,0},{0,1,0},{0,0,1}};
  float mul3[3][3] = {{1,0,0},{0,1,0},{0,0,1}};

  mul3[1][1] = mul3[2][2] = cos(theta);
  mul3[2][1] = sin(theta);
  mul3[1][2] = -mul3[2][1];

  mul1[0][0] = mul1[2][2] = cos(p);
  mul1[2][0] = sin(p);
  mul1[0][2] = -mul1[2][0];

  mul2[0][0] = mul2[1][1] = cos(a);
  mul2[1][0] = -sin(a);
  mul2[0][1] = -mul2[1][0];

  mult_matrix(mul2, mul1, rot);
  copy_matrix(rot, mul1);
  mult_matrix(mul1, mul3, rot);
}

inline float dot_product(float a[3], float b[3]) {
  int i;
  float s = 0;
  for (i=0; i<3; i++) s+=a[i]*b[i];
  return s;
}

//c = a - (a*b)b
void plane_project(float a[3], float b[3], float c[3])
{
  int i;
  float dot = dot_product(a,b);
  for (i=0; i<3; i++) c[i] = a[i] - dot*b[i];
}

//Generates a random rotation matrix
void rand_rotation(float rot[3][3]) {
  float p, a, r, rd;
  r = dr250()*2*M_PI;
  a = dr250()*2*M_PI;
  //The polar angle p must satisfy a distribution such that
  // P(p = theta) = 1/2 * cos(theta)  (-pi/2 < theta < pi/2)
  // Via the jacobian, this is satisfied by
  // theta = asin(2*R - 1) where R is a uniform random number between 0 and 1.
  rd = dr250();
  p = asin(2*rd-1);
  rotate3(p,a,r,rot);
}

int lightcone_test(float v[3], float inv_rot[3][3], 
		   float ytan, float ztan, float d2min, float d2max)
{
  //Determines whether the vector v[3] is within the survey or not.
  //Distance test:
  float v2[3];
  float s = v[0]*v[0] + v[1]*v[1] + v[2]*v[2];
  if (!(s>d2min && s<d2max)) return 0; //Works even if "s" is Nan

  mult_vector(inv_rot, v, v2);
  if (v2[0]*ytan > fabs(v2[1]) && v2[0]*ztan > fabs(v2[2])) return 1;
  return 0;
}

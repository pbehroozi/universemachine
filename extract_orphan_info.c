#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "inthash.h"
#include "stringparse.h"
#include "orphans.h"

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<2) {
    printf("Usage: %s sfr_list.bin hlists...\n", argv[0]);
    exit(1);
  }
  
  char buffer[2048];
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  struct inthash *orphan_ids = new_inthash();

  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    if (tp->flags & ORPHAN_FLAG) {
      int64_t id = tp->id;
      id %= ORPHAN_ID_OFFSET;
      ih_setval(orphan_ids, id, (void *)1);
    }
  }
  munmap(p, new_length);

  SHORT_PARSETYPE;
  enum parsetype t[2] = {K, D64};
  int64_t id;
  void *data[2] = {NULL, &id};
  
  for (i=2; i<argc; i++) {
    FILE *in = check_fopen(argv[i], "r");
    while (fgets(buffer, 2048, in)) {
      if (stringparse(buffer, data, t, 2) != 2) continue;
      if (ih_getval(orphan_ids, id)) printf("%s", buffer);
    }
    fclose(in);
  }
  return 0;
}

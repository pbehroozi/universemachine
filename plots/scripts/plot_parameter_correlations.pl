#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use XmGrace;
use Common;

$ProcessTemplate::density = 200;
my $num_params = 45;
our @excludes = (26, 36);
our %remaps = map { ($_ => $_-1) } (35..41);
$remaps{34} = 41;
my @keys = sort {$a <=> $b } keys %remaps;
@labels[@remaps{@keys}] = @labels[@keys];

my %xticklabels = map {$_ => $labels[$_]} (0..$#labels);
my %yticklabels = map {$num_params-1-$_ => $labels[$_]} (0..$#labels);


sub makeline {
    my ($param) = shift;
    return ({data => "-0.5 ".($num_params-$param-1.5)."\n".($num_params-0.5)." ".($num_params-$param-1.5), lcolor => 1, lwidth => 1},
	    {data => ($param+0.5)." -0.5\n".($param+0.5)." ".($num_params-0.5), lcolor => 1, lwidth => 1});
}

new XmGrace(xlabel => "Parameter Rank Correlations",
	    #ylabel => "\\h{3.5}Rank Correlation of\\nGalaxy SFR to Halo Assembly Rate",
	    #xmin => 1e10, xmax => 1e15,
	    #XmGrace::linscale('y', 0, 1, 0.2, 1),
	    #color_errorstrip => 1,
	    #transparent_colors => 1,
	    #	    xaxistickplace => "off",
	    xticklstyle => 0,
	    yticklstyle => 0,
	    xticklabels => \%xticklabels,
	    yticklabels => \%yticklabels,
	    xticklabelsize => 0.5,
	    yticklabelsize => 0.5,
	    xticklabelangle => 90,
	    xmax => $num_params-0.5,
	    xmin => -0.5,
	    ymax => $num_params-0.5,
	    ymin => -0.5,
	    xviewmax => 0.95,
	    text => [{text => "Median SFRs", pos => "0.98, 0.69"},
		     {text => "Galaxy-Halo\\nCorrelations", pos => "0.98, 0.51"},
		     {text => "Quenching", pos => "0.98, 0.37"},
		     {text => "Systematics", pos => "0.98, 0.255"},
		     {text => "Mergers", pos => "0.98, 0.177"},

],
	    data => [{data => "file:data/parameter_correlations/pcorrs.dat", ssize => 0.97, transform => \&do_abs, metatype => "xydensity"},
		     map { makeline($_) } (18,25,33,40,43)
])->write_pdf("AGR/parameter_correlations.agr");


sub do_abs {
    my ($a, $b, $c) = split(" ", $_[0]);
    my $sub_a=0, $sub_b=0;
    for (@excludes) {
	return "" if ($a==$_);
	return "" if ($b==$_);
	$sub_a++ if ($a>$_);
	$sub_b++ if ($b>$_);
    }
    $a -= $sub_a;
    $b -= $sub_b;
    if ($remaps{$a}) { $a = $remaps{$a}; }
    if ($remaps{$b}) { $b = $remaps{$b}; }
    $b = $num_params-1-$b;
    return "$a $b ".abs($c);
}


BEGIN {
    our @labels = qw(
\xe\f{}\s0\N
\xe\f{}\sa\N
\xe\f{}\sz\N
\xe\f{}\sla\N
V\s0
V\sa
V\sz
V\sla
\xa\f{}\s0
\xa\f{}\sa
\xa\f{}\sz
\xa\f{}\sla
\xb\f{}\s0
\xb\f{}\sa
\xb\f{}\sz
\xd
\xg\f{}\s0
\xg\f{}\sa
\xg\f{}\sz
\xs\f{}\sSF,0
\xs\f{}\sSF,a
f\sshort
V\sR,0
V\sR,a
r\swidth
r\smin
V\sQ,0
V\sQ,a
V\sQ,z
\xs\f{}\sVQ,0
\xs\f{}\sVQ,a
\xs\f{}\sVQ,z
Q\smin,0
Q\smin,a
f\smerge
\xs\f{}\sSM,z
\xm\f{}\s0
\xm\f{}\sa
\xk\f{}
M\sdust,4
M\sdust,z
\xa\f{}\sdust
T\smerge,300
T\smerge,1000
\xc\S\v{-0.3}2
	);
}

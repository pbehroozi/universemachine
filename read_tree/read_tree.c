#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <memory.h>
#include <math.h>
#include <assert.h>
#include "../stringparse.h"
#include "../check_syscalls.h"
#include "read_tree.h"

#define SCALE_FACTOR_MUL 100000
struct halo_tree halo_tree = {0};
struct halo_list all_halos = {0};

int sort_by_location(const void *a, const void *b) {
  const struct halo *c = a;
  const struct halo *d = b;
  int64_t dx=c->pos[0]-d->pos[0],
    dy=c->pos[1]-d->pos[1],
    dz=c->pos[2]-d->pos[2];
  if (dx < 0) return -1;
  if (dx > 0) return 1;
  if (dy < 0) return -1;
  if (dy > 0) return 1;
  if (dz < 0) return -1;
  if (dz > 0) return 1;
  return 0;
}

int sort_by_id(const void *a, const void *b) {
  const struct halo_index_key *c = a;
  const struct halo_index_key *d = b;
  if (c->id < d->id) return -1;
  if (d->id < c->id) return 1;
  return 0;
}

int sort_by_desc(const void *a, const void *b) {
  const struct halo *c = a;
  const struct halo *d = b;
  if (c->desc < d->desc) return -1;
  if (d->desc < c->desc) return 1;
  return 0;
}

struct halo *lookup_halo_in_list(struct halo_list *hl, int64_t id) {
  int64_t i;
  int64_t min=0, max=hl->num_halos-1;
  if (id < 0) return 0;
  while (min < max) {
    i = min + ((max-min)/2);
    if (hl->halo_lookup[i].id < id) min = i+1;
    else max = i;
  }
  if (hl->halo_lookup[max].id == id)
    return (&(hl->halos[hl->halo_lookup[max].index]));
  return 0;
}

struct halo_list *lookup_scale(float scale) {
  int64_t index = scale*SCALE_FACTOR_MUL;
  if (index < 0) return 0;
  if (index>=halo_tree.num_scales) return 0;
  index = halo_tree.scale_factor_conv[index];
  if (index < 0) return 0;
  return &(halo_tree.halo_lists[index]);
}

struct halo_list *find_closest_scale(float scale) {
  int64_t index = scale*SCALE_FACTOR_MUL;
  int64_t i=0, j=0;
  if (!halo_tree.num_scales) return NULL;
  if (index < 0) return &(halo_tree.halo_lists[halo_tree.num_lists-1]);
  if (index>=halo_tree.num_scales-1) return halo_tree.halo_lists;
  //index = halo_tree.scale_factor_conv[index];
  while (index+i < halo_tree.num_scales && halo_tree.scale_factor_conv[index+i]<0) i++;
  while (index+j >= 0 && halo_tree.scale_factor_conv[index+j]<0) j--;
  if (index+i < halo_tree.num_scales) {
    index += i;
    if (index-i+j >= 0 && (llabs(j) < llabs(i))) index += j-i;
  } else if (index+j>=0) index += j;
  index = halo_tree.scale_factor_conv[index];
  return &(halo_tree.halo_lists[index]);
}


struct halo_list *_lookup_or_create_scale(float scale) {
  int64_t index = scale*SCALE_FACTOR_MUL;
  int64_t index2;
  assert(index >= 0);
  if (index>=halo_tree.num_scales) {
    int64_t i;
    halo_tree.scale_factor_conv = check_realloc(halo_tree.scale_factor_conv,
						sizeof(int64_t)*(index+1), "Scale factor conversion");
    for (i=halo_tree.num_scales; i<=index; i++) halo_tree.scale_factor_conv[i] = -1;
    halo_tree.num_scales = index+1;
  }
  index2 = halo_tree.scale_factor_conv[index];
  if (index2 < 0) {
    halo_tree.halo_lists = check_realloc(halo_tree.halo_lists,
					 sizeof(struct halo_list)*(halo_tree.num_lists+1),
					 "Halo Lists");
    index2 = halo_tree.num_lists;
    memset(&(halo_tree.halo_lists[index2]), 0, sizeof(struct halo_list));
    halo_tree.halo_lists[index2].scale = scale;
    halo_tree.scale_factor_conv[index] = index2;
    halo_tree.num_lists++;
  }
  return &(halo_tree.halo_lists[index2]);
}


void build_halo_index(struct halo_list *hl) {
  int64_t i;
  hl->halo_lookup = check_realloc(hl->halo_lookup, 
				  sizeof(struct halo_index_key)*hl->num_halos, "Halo lookup index.");
  for (i=0; i<hl->num_halos; i++) {
    hl->halo_lookup[i].id = hl->halos[i].id;
    hl->halo_lookup[i].index = i;
  }
  qsort(hl->halo_lookup, hl->num_halos, sizeof(struct halo_index_key), sort_by_id);
}



void partition_sort_halos(int64_t min, int64_t max,
		struct halo *halos) {
  float minpivot, maxpivot, pivot;
  int64_t i, si;
  struct halo tmp_h;
  if (max-min < 2) return;

  maxpivot = minpivot = halos[min].scale;
  for (i=min+1; i<max; i++) {
    if (halos[i].scale > maxpivot) maxpivot = halos[i].scale;
    if (halos[i].scale < minpivot) minpivot = halos[i].scale;
  }
  if (minpivot==maxpivot) return;
  pivot = minpivot + (maxpivot-minpivot)/2.0;
  si = max-1;
#define SWAP(a,b) {tmp_h = halos[a]; halos[a] = halos[b]; \
    halos[b] = tmp_h; }
  
  for (i=min; i<si; i++)
    if (halos[i].scale <= pivot) { SWAP(i, si); si--; i--; }
  if (i==si && halos[si].scale>pivot) si++;
#undef SWAP
  partition_sort_halos(min, si, halos);
  partition_sort_halos(si, max, halos);
}


void build_tree() {
  int64_t i, j, start, descid;
  struct halo_list *last_hl = 0, *new_hl;
  struct halo *desc;
  memset(&halo_tree, 0, sizeof(struct halo_tree));
  partition_sort_halos(0, all_halos.num_halos, all_halos.halos);
  for (start=0, i=0; i<all_halos.num_halos; i++) {
    if (i>0) assert(all_halos.halos[i].scale <= all_halos.halos[i-1].scale);
    new_hl = _lookup_or_create_scale(all_halos.halos[i].scale);
    if (new_hl != last_hl) {
      new_hl->halos = &(all_halos.halos[i]);
      if (i) {
	last_hl = lookup_scale(all_halos.halos[start].scale);
	last_hl->num_halos = i-start;
      }
      start = i;
      last_hl = new_hl;
    }
  }
  if (last_hl) last_hl->num_halos = i-start;
  if (!halo_tree.num_lists) return;
  last_hl = halo_tree.halo_lists;
  qsort(last_hl->halos, last_hl->num_halos, sizeof(struct halo), sort_by_location);
  build_halo_index(last_hl);
  for (j=0; j<last_hl->num_halos; j++) {
    last_hl->halos[j].parent = lookup_halo_in_list(last_hl, last_hl->halos[j].pid);
    last_hl->halos[j].uparent = lookup_halo_in_list(last_hl, last_hl->halos[j].upid);
    last_hl->halos[j].desc = 0;
  }

  
  for (i=1; i<halo_tree.num_lists; i++) {
    last_hl = &(halo_tree.halo_lists[i-1]);
    new_hl = &(halo_tree.halo_lists[i]);
    for (j=0; j<new_hl->num_halos; j++) {
      descid = (int64_t)(new_hl->halos[j].desc);
      new_hl->halos[j].desc = lookup_halo_in_list(last_hl, (int64_t)descid);
    }
    qsort(new_hl->halos, new_hl->num_halos, sizeof(struct halo), sort_by_desc);
    build_halo_index(new_hl);
    for (j=0; j<new_hl->num_halos; j++) {
      if ((desc = new_hl->halos[j].desc)) {
	if (desc->prog && desc->prog->mvir > new_hl->halos[j].mvir) {
	  new_hl->halos[j].next_coprog = desc->prog->next_coprog;
	  desc->prog->next_coprog = &(new_hl->halos[j]);
	} else {
	  new_hl->halos[j].next_coprog = desc->prog;
	  desc->prog = &(new_hl->halos[j]);
	}
      }
      new_hl->halos[j].parent = lookup_halo_in_list(new_hl, new_hl->halos[j].pid);
      new_hl->halos[j].uparent = lookup_halo_in_list(new_hl, new_hl->halos[j].upid);
    }
  }
}

#ifndef ENABLE_HDF5
void read_tree(char *filename) {
  int64_t n;
  FILE *input;
  struct halo h = {0};
  int64_t desc_pid, descid, desc_scale;
  char buffer[1024];

  SHORT_PARSETYPE;
  #define NUM_INPUTS 59
  enum short_parsetype stypes[NUM_INPUTS] = 
    { F, D64, F, D64, D64,    //  #scale id desc_scale desc_id num_prog
      D64, D64, D64, D64,       //   pid upid desc_pid phantom 
      F, F, F, F, F,    //mvir orig_mvir rvir rs vrms 
      D64, F, F,          //mmp? scale_of_last_MM vmax 
      F, F, F, F, F, F,    //x y z vx vy vz
      F, F, F, F, //Jx Jy Jz Spin
      D64, D64, D64, D64, D, D64, D64, D64, //bfid, dfid, trid, ohid, snap, ncdfid, lpdfid, mainleafid
      F, D64, //Tidal Force, Tidal ID
      F, F, F, F, F, F, F, F, //Rs_Klypin Mvir_all M200b M200c M500c M2500c Xoff Voff 
      F, F, F, F, F, F, //Spin_Bullock b_to_a c_to_a A[x] A[y] A[z]
      F, F, F, F, F, //b_to_a(500c) c_to_a(500c) A[x](500c) A[y](500c) A[z](500c) 
      F, F, F, //T/|U| M_pe_Behroozi M_pe_Diemer
    };
  enum parsetype types[NUM_INPUTS];
  void *data[NUM_INPUTS] = {&(h.scale), &(h.id), &(desc_scale),
                            &(descid), &(h.num_prog), &(h.pid),
			    &(h.upid), &(desc_pid), &(h.phantom), 
			    &(h.mvir), &(h.orig_mvir), &(h.rvir), &(h.rs), &(h.vrms),
			    &(h.mmp), &(h.scale_of_last_MM), &(h.vmax),
			    &(h.pos[0]), &(h.pos[1]), &(h.pos[2]), 
                            &(h.vel[0]), &(h.vel[1]), &(h.vel[2]),
                            &(h.J[0]), &(h.J[1]), &(h.J[2]), &h.spin,
                            &h.breadth_first_id, &h.depth_first_id, &h.tree_root_id, 
                            &h.orig_halo_id, &h.snap_num, &h.next_coprogenitor_depthfirst_id, 
                            &h.last_progenitor_depthfirst_id, &h.last_mainleaf_depthfirst_id,
                            
                            &h.tidal_force, &h.tidal_id, 
                            &h.rs_klypin, &h.mvir_all, &h.m200b, &h.m200c, &h.m500c, &h.m2500c,
                            &h.xoff, &h.voff, &h.spin_bullock, &h.b_to_a, &h.c_to_a, h.A, h.A+1, h.A+2,
                            &h.b_to_a500c, &h.c_to_a500c, h.A500c, h.A500c+1, h.A500c+2,
                            &h.t_u, &h.m_pe_behroozi, &h.m_pe_diemer,
    };

  for (n=0; n<NUM_INPUTS; n++) types[n] = stypes[n];
  input = check_fopen(filename, "r");
  while (fgets(buffer, 1024, input)) {
    if (buffer[0] == '#') continue;
    n = stringparse(buffer, data, (enum parsetype *)types, NUM_INPUTS);
    if (n<NUM_INPUTS) continue;
    h.desc = (struct halo *)(int64_t)descid;
    h.descid = descid;
    h.parent = (struct halo *)(int64_t)h.pid;
    h.uparent = (struct halo *)(int64_t)h.upid;
    h.mvir = h.orig_mvir;
    if (!(all_halos.num_halos%3000))
      all_halos.halos = check_realloc(all_halos.halos, sizeof(struct halo)*(all_halos.num_halos+3000), "Allocating Halos.");
   
    all_halos.halos[all_halos.num_halos] = h;
    all_halos.num_halos++;
  }
  fclose(input);

  all_halos.halos = check_realloc(all_halos.halos, sizeof(struct halo)*all_halos.num_halos, "Allocating Halos.");
  build_tree();
}
#else
#include "../io_hdf5.c"

void read_tree(char *filename) {
  int64_t i;
  void *h5data = NULL;
  
  hid_t tree = check_H5Fopen(filename, H5F_ACC_RDONLY);
  hid_t id = check_H5Dopen(tree, "/Forests/id", "", filename);
  hid_t id_space = check_H5Dget_space(id);
  hsize_t dataset_length = 0;
  check_H5Sget_simple_extent_dims(id_space, &dataset_length);
  H5Sclose(id_space);
  H5Dclose(id);
  all_halos.num_halos = dataset_length;
  check_realloc_s(all_halos.halos, sizeof(struct halo), (all_halos.num_halos));
  memset(all_halos.halos, 0, sizeof(struct halo)*(all_halos.num_halos));
  
#define READ_HDF5(h5name, ctype, htype, location) {                     \
    hid_t hid = H5Dopen(tree, h5name);					\
    if (hid > -1) {							\
      check_realloc_s(h5data, sizeof(ctype), dataset_length);		\
      check_H5Dread(hid, htype, h5data, h5name, "", filename);		\
      H5Dclose(hid);							\
      ctype *h5d_type = h5data;						\
      for (i=0; i<all_halos.num_halos; i++)				\
	all_halos.halos[i].location = h5d_type[i];			\
    }									\
    else {								\
      fprintf(stderr, "[Warning] Dataset %s not found in file %s, assuming that it is 0.\n", h5name, filename); \
    }									\
  }

  READ_HDF5("/Forests/scale", float, H5T_NATIVE_FLOAT, scale);
  READ_HDF5("/Forests/id", int64_t, H5T_NATIVE_INT64, id);
  READ_HDF5("/Forests/desc_id", int64_t, H5T_NATIVE_INT64, descid);
  READ_HDF5("/Forests/num_prog", int64_t, H5T_NATIVE_INT64, num_prog);
  READ_HDF5("/Forests/pid", int64_t, H5T_NATIVE_INT64, pid);
  READ_HDF5("/Forests/upid", int64_t, H5T_NATIVE_INT64, upid);
  READ_HDF5("/Forests/phantom", int64_t, H5T_NATIVE_INT64, phantom);
  READ_HDF5("/Forests/Mvir", float, H5T_NATIVE_FLOAT, mvir);
  READ_HDF5("/Forests/Mvir", float, H5T_NATIVE_FLOAT, orig_mvir); //On purpose--ignore SAM_Mvir
  READ_HDF5("/Forests/Rvir", float, H5T_NATIVE_FLOAT, rvir);
  READ_HDF5("/Forests/rs", float, H5T_NATIVE_FLOAT, rs);
  READ_HDF5("/Forests/vrms", float, H5T_NATIVE_FLOAT, vrms);
  READ_HDF5("/Forests/mmp", int64_t, H5T_NATIVE_INT64, mmp);
  READ_HDF5("/Forests/scale_of_last_MM", float, H5T_NATIVE_FLOAT, scale_of_last_MM);
  READ_HDF5("/Forests/vmax", float, H5T_NATIVE_FLOAT, vmax);
  READ_HDF5("/Forests/x", float, H5T_NATIVE_FLOAT, pos[0]);
  READ_HDF5("/Forests/y", float, H5T_NATIVE_FLOAT, pos[1]);
  READ_HDF5("/Forests/z", float, H5T_NATIVE_FLOAT, pos[2]);
  READ_HDF5("/Forests/vx", float, H5T_NATIVE_FLOAT, vel[0]);
  READ_HDF5("/Forests/vy", float, H5T_NATIVE_FLOAT, vel[1]);
  READ_HDF5("/Forests/vz", float, H5T_NATIVE_FLOAT, vel[2]);
  READ_HDF5("/Forests/Jx", float, H5T_NATIVE_FLOAT, J[0]);
  READ_HDF5("/Forests/Jy", float, H5T_NATIVE_FLOAT, J[1]);
  READ_HDF5("/Forests/Jz", float, H5T_NATIVE_FLOAT, J[2]);
  READ_HDF5("/Forests/Spin", float, H5T_NATIVE_FLOAT, spin);
  READ_HDF5("/Forests/Breadth_first_ID", int64_t, H5T_NATIVE_INT64, breadth_first_id);
  READ_HDF5("/Forests/Depth_first_ID", int64_t, H5T_NATIVE_INT64, depth_first_id);
  READ_HDF5("/Forests/Tree_root_ID", int64_t, H5T_NATIVE_INT64, tree_root_id);
  READ_HDF5("/Forests/Orig_halo_ID", int64_t, H5T_NATIVE_INT64, orig_halo_id);
  READ_HDF5("/Forests/Snap_idx", int64_t, H5T_NATIVE_INT32, snap_num);
  READ_HDF5("/Forests/Next_coprogenitor_depthfirst_ID", int64_t, H5T_NATIVE_INT64, next_coprogenitor_depthfirst_id);
  READ_HDF5("/Forests/Last_progenitor_depthfirst_ID", int64_t, H5T_NATIVE_INT64, last_progenitor_depthfirst_id);
  READ_HDF5("/Forests/Last_mainleaf_depthfirst_ID", int64_t, H5T_NATIVE_INT64, last_mainleaf_depthfirst_id);
  READ_HDF5("/Forests/Tidal_Force", float, H5T_NATIVE_FLOAT, tidal_force);
  READ_HDF5("/Forests/Tidal_ID", int64_t, H5T_NATIVE_INT64, tidal_id);
  READ_HDF5("/Forests/Rs_Klypin", float, H5T_NATIVE_FLOAT, rs_klypin);
  READ_HDF5("/Forests/Mvir_all", float, H5T_NATIVE_FLOAT, mvir_all);
  READ_HDF5("/Forests/M200b", float, H5T_NATIVE_FLOAT, m200b);
  READ_HDF5("/Forests/M200c", float, H5T_NATIVE_FLOAT, m200c);
  READ_HDF5("/Forests/M500c", float, H5T_NATIVE_FLOAT, m500c);
  READ_HDF5("/Forests/M2500c", float, H5T_NATIVE_FLOAT, m2500c);
  READ_HDF5("/Forests/Xoff", float, H5T_NATIVE_FLOAT, xoff);
  READ_HDF5("/Forests/Voff", float, H5T_NATIVE_FLOAT, voff);
  READ_HDF5("/Forests/Spin_Bullock", float, H5T_NATIVE_FLOAT, spin_bullock);
  READ_HDF5("/Forests/b_to_a", float, H5T_NATIVE_FLOAT, b_to_a);
  READ_HDF5("/Forests/c_to_a", float, H5T_NATIVE_FLOAT, c_to_a);
  READ_HDF5("/Forests/A_x", float, H5T_NATIVE_FLOAT, A[0]);
  READ_HDF5("/Forests/A_y", float, H5T_NATIVE_FLOAT, A[1]);
  READ_HDF5("/Forests/A_z", float, H5T_NATIVE_FLOAT, A[2]);
  READ_HDF5("/Forests/b_to_a_500c", float, H5T_NATIVE_FLOAT, b_to_a500c);
  READ_HDF5("/Forests/c_to_a_500c", float, H5T_NATIVE_FLOAT, c_to_a500c);
  READ_HDF5("/Forests/A_x_500c", float, H5T_NATIVE_FLOAT, A500c[0]);
  READ_HDF5("/Forests/A_y_500c", float, H5T_NATIVE_FLOAT, A500c[1]);
  READ_HDF5("/Forests/A_z_500c", float, H5T_NATIVE_FLOAT, A500c[2]);
  READ_HDF5("/Forests/T_U", float, H5T_NATIVE_FLOAT, t_u);
  READ_HDF5("/Forests/M_pe_Behroozi", float, H5T_NATIVE_FLOAT, m_pe_behroozi);
  READ_HDF5("/Forests/M_pe_Diemer", float, H5T_NATIVE_FLOAT, m_pe_diemer);
  if (h5data) free(h5data);
  H5Fclose(tree);

  for (i=0; i<all_halos.num_halos; i++) {
    struct halo *h = all_halos.halos+i;

    /*Discard zero-filled halos present in the Micro-Uchuu simulation merger trees */
    if ((h->scale==0) && (h->id==0) && (h->mvir==0)) {
      all_halos.halos[i] = all_halos.halos[all_halos.num_halos-1];
      i--;
      all_halos.num_halos--;
      continue;
    }
    
    //Temporary values that are updated after the tree is re-sorted and built.
    h->desc = (struct halo *)(int64_t)h->descid;
    h->parent = (struct halo *)(int64_t)h->pid;
    h->uparent = (struct halo *)(int64_t)h->upid;
  }

  //Shrink all_halos.halos if any halos were removed above.
  check_realloc_s(all_halos.halos, sizeof(struct halo), (all_halos.num_halos));
  build_tree();
}
#endif /*ENABLE_HDF5*/


void delete_tree(void) {
  int64_t i;
  if (!all_halos.num_halos) return;

  for (i=0; i<halo_tree.num_lists; i++)
    free(halo_tree.halo_lists[i].halo_lookup);
  free(halo_tree.halo_lists);
  free(halo_tree.scale_factor_conv);
  memset(&halo_tree, 0, sizeof(struct halo_tree));

  free(all_halos.halos);
  memset(&all_halos, 0, sizeof(struct halo_list));
}


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include "check_syscalls.h"
#include "config_vars.h"

void watchdog_clear(void) {
  char buffer[1024];
  snprintf(buffer, 1024, "%s/done", OUTBASE);
  unlink(buffer);
}

void watchdog_start(char *name) {
  char buffer[1024];
  FILE *wdog;
  snprintf(buffer, 1024, "%s/done", OUTBASE);
  if (check_fork()) return;
  while (1) {
    sleep(10);
    if ((wdog = fopen(buffer, "r"))) {
      fclose(wdog);
      sprintf(buffer, "killall %s", name);
      int res = system(buffer);
      exit((res==0) ? EXIT_SUCCESS : EXIT_FAILURE);
    }
  }
}

void _watchdog_killall(int64_t result, int64_t killself) {
  char buffer[1024];
  snprintf(buffer, 1024, "%s/done", OUTBASE);
  fclose(fopen(buffer, "w"));
  if (killself) exit(result);
}

void watchdog_killall(void) {
  _watchdog_killall(EXIT_SUCCESS, 1);
}

void watchdog_failexit(void) {
  _watchdog_killall(EXIT_FAILURE, 1);
}

void _trigger_watchdog(void) {
  _watchdog_killall(EXIT_FAILURE, 0);
}

#!/usr/bin/perl -w
use Statistics::PCA;
use lib qw(../perl);
use XmGrace;

my ($min_z, $max_z);
my ($corr_q, $corr_sf, $covar, $min_r, $max_r) = @ARGV;
if (!defined ($max_r)) {
    die "Usage: $0 corr_q corr_sf realizations min_r max_r\n";
}

#$min_r = 0.1;
#$max_r = 10;
#@data_a = load_corr($corr_a, "a");
@data_sf = load_corr($corr_sf, "s");
@data_q = load_corr($corr_q, "q");
for (0..$#data_sf) {
    push @obsdata, $data_sf[$_], $data_q[$_];
}
write_corr(@obsdata);


open IN, "<$covar";
while (<IN>) {
    next if (/^#/);
    my ($r, $a, $sf, $q) = split;
    next if ($r < $min_r or $r > $max_r); 
    push @{$sf{$r}}, ($sf > 0) ? log($sf)/log(10) : -1;
    push @{$q{$r}}, ($q>0) ? log($q)/log(10) : -1;
}
close IN;

my @keys = sort {$a <=> $b} keys %sf;
my @full;
for (@keys) {
    push @full, $sf{$_}[0];
    push @full, $q{$_}[0];
}

for my $i (1..(@{$sf{$keys[0]}}-1)) {
    my $arr;
    for (@keys) {
	push @$arr, $sf{$_}[$i];
	push @$arr, $q{$_}[$i];
    }
    for (0..(@$arr-1)) {
	$av[$_]+=$arr->[$_];
	push @{$meds[$_]}, $arr->[$_];
    }
    push @data, $arr;
}

$av[$_] /=@data for (0..$#av);
print "Offsets:\n";
for (0..$#av) {
    print $full[$_]-$av[$_];
    @{$meds[$_]} = sort {$a <=> $b} @{$meds[$_]};
    my $counts = @{$meds[$_]};
    my $err_u = $meds[$_][$counts*(0.5+0.684/2)]-$meds[$_][$counts*0.5];
    my $err_d = -$meds[$_][$counts*(0.5-0.684/2)]+$meds[$_][$counts*0.5];
    push @err_u, $err_u;
    push @err_d, $err_d;
#    print " $err_u $err_d [JK: $obsdata[$_][6]]\n";
}
print "\n";


my $pca = Statistics::PCA->new;
$pca->load_data({format => 'table', data => \@data});
$pca->pca({ eigen => 'M' });
write_pca($pca, @obsdata);

print "#Full-obs comparison:\n";
for (0..$#obsdata) {
    my @od = @{$obsdata[$_]};
    my $r = sqrt($od[3]*$od[4]);
    $r /= 0.677;
    my $ov = 10**($od[5]-log(0.677)/log(10));
    my $mv = 10**($full[$_]-log(0.677)/log(10));
    printf "%s %.3f %.3e %.3e\n", $od[0], $r, $ov, $mv;
    my $err_u = (10**($od[5]+$err_u[$_])-10**$od[5])/0.677;
    my $err_d = (10**$od[5]-10**($od[5]-$err_d[$_]))/0.677;
#    if ($od[0] eq "a") {
#	$aodata .= "$r $ov $err_u $err_d\n";
#	$amdata .= "$r $mv\n";
#    }
    if ($od[0] eq "s") {
	$sodata .= "$r $ov $err_u $err_d\n";
	$smdata .= "$r $mv\n";
    }
    elsif ($od[0] eq "q") {
	$qodata .= "$r $ov $err_u $err_d\n";
	$qmdata .= "$r $mv\n";
    }
}
print "\n";

my $fn = "corr_comp_sm$obsdata[0][1]_sm$obsdata[0][2].agr";
new XmGrace(xlabel => 'R [Mpc]',
	    ylabel => 'w\sp\N [Mpc]',
	    legendxy => "0.8, 0.8",
	    ymax => 4000,
	    data => [{data => $qodata, legend => "Quenched, Obs", scolor => 2, lcolor => 2, metatype => "scatter", ssize => 1, type => "xydydy"},
		     {data => $qmdata, legend => "Quenched, Mock", lcolor => 2},
#		     {data => $aodata, legend => "All, Obs", lcolor => 1, scolor => 1, metatype => "scatter", ssize => 1, type => "xydydy"},
#		     {data => $amdata, legend => "All, Mock", lcolor => 1},
		     {data => $sodata, legend => "SF, Obs", lcolor => 3, scolor => 3, metatype => "scatter", ssize => 1, type => "xydydy"},
		     {data => $smdata, legend => "SF, Mock", lcolor => 3}])->write_pdf("AGR/$fn");


my @labels = map { ("sf.$_", "q.$_") } @keys;

my @important = (0..29);
my @reconstruction;
exit;

my $c = 0;
print "\nPC   Prop.Var.  Cum.Var.  Av. +/- SD;  Sig.Contributors\n";
for my $i ($pca->results('full')) {
#    next unless ($i->[3]<0.85);
    my @sig = map { (abs($i->[5][$_]) > 0.1) ? ([$labels[$_], $i->[5][$_]]) : () } (0..$#important);
    my $d = $data[0];
    my $s = 0;
    for (0..(@$d-1)) { $s += $i->[5][$_]*($d->[$_]-$av[$_]); }
    for (0..(@$d-1)) { $reconstruction[$c][$_] = $i->[5][$_]*$s; }
    $c++;
    next unless @sig;
    my ($av, $sd) = val_sd($i->[5]);
    printf "PC%d:  %.3f   %.3f  %.3e +/- %.3e (%.3e) [%s]", $i->[0], $i->[2], $i->[3], $av, $sd, $s, "@{$i->[5]}";
#    @sig = sort { abs($b->[1]) <=> abs($a->[1]) } @sig;
#    my $tot = 0;
#    for (@sig) {
#        $tot += $_->[1]**2;
        #printf "%s(%.3f) ", $_->[0], $_->[1];
#        last if ($tot > 0.8);
#    }
    print "\n";
}

print "\n";


sub val_sd {
    my $pc = shift;
    my $av=0;
    my $av2=0;
    for (0..$#data) {
	my $s = 0;
	my $d = $data[$_];
	for (0..(@$d-1)) { $s += $pc->[$_]*($d->[$_]-$av[$_]); }
	$av += $s;
	$av2 += $s*$s;
    }
    $av /= @data;
    $av2 /= @data;
    $av2 -= $av*$av;
    my $sd = ($av2>0) ? sqrt($av2) : 0;
    return ($av, $sd);
}



sub write_pca {
    my ($pca, @data) = @_;
    my @first = @{$data[0]};
    my ($sm_min, $sm_max) = @first[1,2];
    $sm_min =~ s/\.$//;
    $sm_max =~ s/\.$//;
    my $factor = ($sm_min >= 11) ? sqrt(2) : 1;
    my $fn = "results/corr_z${min_z}_z${max_z}_sm${sm_min}_sm${sm_max}.dat.pca";
    mkdir "results";
    open OUT, ">$fn";
    print OUT "#type: correlation\n";
    print OUT "#pca: yes\n";
    print OUT "#zlow: $min_z\n";
    print OUT "#zhigh: $max_z\n";
    print OUT "#ref: Behroozi, Wechsler, Hearin, Conroy (2019); https://arxiv.org/abs/1806.07893\n";
    print OUT "#note: Errors increased by sqrt(2) to account for limited volume of Bolshoi-Planck.\n" if ($factor > 1);
    print OUT "#obs: ", scalar(@data), "\n";
    for my $i ($pca->results('full')) {
	my ($av, $sd) = val_sd($i->[5]);
	$sd *= $factor;
	print OUT "$sd @{$i->[5]}\n";
    }
    close OUT;
}

sub write_corr {
    my (@data) = @_;
    my @first = @{$data[0]};
    my ($sm_min, $sm_max) = @first[1,2];
    $sm_min =~ s/\.$//;
    $sm_max =~ s/\.$//;
    my $factor = ($sm_min >= 11) ? sqrt(2) : 1;
    my $fn = "results/corr_z${min_z}_z${max_z}_sm${sm_min}_sm${sm_max}.dat";
    mkdir "results";
    open OUT, ">$fn";
    print OUT "#type: correlation\n";
    print OUT "#pca: yes\n";
    print OUT "#zlow: $min_z\n";
    print OUT "#zhigh: $max_z\n";
    print OUT "#ref: https://arxiv.org/abs/1609.09090\n";
    print OUT "#note: Errors increased by sqrt(2) to account for limited volume of Bolshoi-Planck.\n" if ($factor > 1);
    for (@data) {
	my @d = @$_;
	$d[$_]*=$factor for (6,7);
	print OUT "@d\n";
    }
    close OUT;
}

sub load_corr {
    my ($fn, $type) = @_;
    $fn =~ /_(\d+\.?\d*)_(\d+\.?\d*)/;
    my ($sm_min, $sm_max) = ($1, $2);
    open IN, "<$fn";
    my @data;
    while (<IN>) {
	if (/^#/) {
	    if (/min_z/) {
		/(\d+\.\d+)/;
		$min_z = $1;
	    }
	    if (/max_z/) {
		/(\d+\.\d+)/;
		$max_z = $1;
	    }
	    next;
	}
	my ($r, $v, $err) = split;
	next unless (defined $r);
	$r *= 0.677;
	$v *= 0.677;
	$err *= 0.677;
	next if ($r < $min_r or $r > $max_r);
	my $rbin = (int(100+log($r)*5/log(10))-100);
	my $rmin = sprintf("%.5f", 10**($rbin/5));
	my $rmax = sprintf("%.5f", 10**(($rbin+1)/5));
	$err = sprintf("%.5f", log(1+$err/$v)/log(10));
	$val = sprintf("%.5f", log($v)/log(10));
	push @data, [$type, $sm_min, $sm_max, $rmin, $rmax, $val, $err, $err];
    }
    close IN;
    return @data;
}


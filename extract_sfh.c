#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdarg.h>
#include <unistd.h>
#include "mt_rand.h"
#include "universe_time.h"
#include "distance.h"
#include "check_syscalls.h"
#include "mcmc_data.h"
#include "config.h"
#include "config_vars.h"
#include "version.h"
#include "universal_constants.h"
#include "sf_model.h"
#include "all_luminosities.h"

struct mcmc_data *results=NULL;

void print_model(FILE *out) {
  fprintf(out, "#Model:");
  int64_t i;
  for (i=0; i<NUM_PARAMS; i++)
    fprintf(out, " %.12f", results->model->params[i]);
  fprintf(out, " 0 %.12f\n", CHI2(results->model[0])); 
}

void convert_sfr_to_sfh(float *sfr, float *sfh, int64_t n) {
  for (int64_t k=0; k<n+1; k++) {
    double a1 = (k>0) ? 0.5*(results->scales[k]+results->scales[k-1]) : 0;
    double a2 = (k<n) ? 0.5*(results->scales[k]+results->scales[k+1]) : results->scales[k];
    double dt = scale_to_years(a2) - scale_to_years(a1);
    sfh[k] = sfr[k]*dt;
  }
}

void gen_sfh_data(int64_t index) {
  int64_t i, j, k;
  enum lum_types l;
  char buffer[1024];
  snprintf(buffer, 1024, "sfh_sample.%"PRId64".dat", index);
  FILE *out = check_fopen(buffer, "w");
  print_model(out);

  fprintf(out, "#Num_scales: %"PRId64"\n", results->num_scales);
  fprintf(out, "#Scales:");
  for (i=0; i<results->num_scales; i++) fprintf(out, " %f", results->scales[i]);
  fprintf(out, "\n");

  fprintf(out, "#Num_sm_bins: %"PRId64"\n", (int64_t)(SM_RED_NBINS-1));
  fprintf(out, "#SM_bin_edges:");
  for (j=1; j<SM_RED_NBINS; j++) {
    double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
    fprintf(out, " %g", sm_left);
  }
  double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
  fprintf(out, " %g\n", sm_left);


  float *lum_lookups[LUM_TYPES+3]={0};
  snprintf(buffer, 1024, "lum_sample.%"PRId64".dat", index);
  FILE *lums = check_fopen(buffer, "w");
  print_model(lums);
  fprintf(lums, "#Scale sm_med q/sf/a");
  for (l=0; l<LUM_TYPES; l++) fprintf(lums, " %s(Nodust) %s(Dust)", lum_names[l], lum_names[l]);
  fprintf(lums, " Abs_%s(Nodust) Abs_%s(Dust) Abs_%s(Nodust) Abs_%s(Dust) Abs_%s(Nodust) Abs_%s(Dust)",
	  lum_names[L_m1500], lum_names[L_m1500], lum_names[L_sloan_g], lum_names[L_sloan_g], lum_names[L_sloan_r], lum_names[L_sloan_r]);
  fprintf(lums, "\n");

  float *sfh = NULL;
  check_realloc_s(sfh, sizeof(double), results->num_scales);
  
  for (i=0; i<results->num_scales; i++) {
    struct sf_model c = calc_sf_model(results->model[0], results->scales[i]);
    double conv_factor = pow(10, c.obs_sm_offset);
    fprintf(out, "#Scale_index: %"PRId64"\n", i);
    fprintf(out, "#Scale_value: %f\n", results->scales[i]);
    fprintf(out, "#Quenched threshold: Obs. SFR / Obs. SM < %g / yr\n", QUENCHED_THRESHOLD);
    fprintf(out, "#SFHs in units of observed Msun/yr.\n");
    fprintf(out, "#True SFHs rescaled by average systematic offset of %g dex to be consistent with average observed stellar mass in bin.\n", c.obs_sm_offset);
    fprintf(out, "#See documentation for explanation of true vs. observed SFRs and SMs.\n");
    fprintf(out, "#<SFH_all> <SFH_SF> <SFH_Q>\n");

    //Generate luminosity lookups
    if (results->scales[i]<1) {
      double z_src = 1.0/results->scales[i]-1.0;
      if (z_src<0.05) z_src=0; //all_luminosities.c gives absolute magnitudes when z=0 and apparent when z>=0.05; behavior is undefined for 0<z<0.05.
      for (l=0; l<LUM_TYPES; l++)
	gen_lum_lookup(lum_lookups+l, results->scales, i+1, l, z_src);
      gen_lum_lookup(lum_lookups+LUM_TYPES, results->scales, i+1, L_m1500, 0);
      gen_lum_lookup(lum_lookups+LUM_TYPES+1, results->scales, i+1, L_sloan_g, 0);
      gen_lum_lookup(lum_lookups+LUM_TYPES+2, results->scales, i+1, L_sloan_r, 0);
    }

    for (j=1; j<SM_RED_NBINS; j++) {
      int64_t offset = SM_RED_NBINS*i;
      double sm_left = SM_MIN+j/(double)SM_RED_BPDEX;
      double sm_right = sm_left+1.0/(double)SM_RED_BPDEX;
      if (results->sm_red[offset+j]==0) continue;
      fprintf(out, "#SM_index: %"PRId64"\n", j-1);
      fprintf(out, "#SM_values: %g - %g\n", sm_left, sm_right);
      //double sm = 0.5*(sm_left+sm_right);
      fprintf(out, "#Galaxies in bin: %g\n", results->sm_red[offset+j]);
      fprintf(out, "#SF galaxies in bin: %g\n", results->sm_red[offset+j]-results->sm_red_q2[offset+j]);
      fprintf(out, "#Q galaxies in bin: %g\n", results->sm_red_q2[offset+j]);
      for (k=0; k<results->num_scales; k++) {
	int64_t bin = (offset+j)*results->num_scales+k;
	if (!results->sm_sfh_ratio[bin]) continue;
	fprintf(out, "%"PRId64" %g %g %g\n",
		k, results->sm_sfh_ratio[bin]*conv_factor, results->sm_sfh_sf_ratio[bin]*conv_factor,
		results->sm_sfh_q_ratio[bin]*conv_factor);
      }


#define LUM(x,d) (lum_of_sfh(lum_lookups[x], sfh, results->scales, i, d))
      
      if (results->scales[i]<1) {
	//All galaxies
	fprintf(lums, "%f %g a", results->scales[i], 0.5*(sm_left+sm_right));
	convert_sfr_to_sfh(results->sm_sfh_ratio + (offset+j)*results->num_scales, sfh, i);
	for (l=0; l<LUM_TYPES+3; l++) fprintf(lums, " %g %g", LUM(l,0), LUM(l,1));
	fprintf(lums, "\n");
	
	//SF galaxies
	if (results->sm_red[offset+j]>results->sm_red_q2[offset+j]) {
	  fprintf(lums, "%f %g sf", results->scales[i], 0.5*(sm_left+sm_right));
	  convert_sfr_to_sfh(results->sm_sfh_sf_ratio + (offset+j)*results->num_scales, sfh, i);
	  for (l=0; l<LUM_TYPES+3; l++) fprintf(lums, " %g %g", LUM(l,0), LUM(l,1));
	  fprintf(lums, "\n");
	}

	//Q galaxies
	if (results->sm_red_q2[offset+j]>0) {
	  fprintf(lums, "%f %g q", results->scales[i], 0.5*(sm_left+sm_right));
	  convert_sfr_to_sfh(results->sm_sfh_q_ratio + (offset+j)*results->num_scales, sfh, i);
	  for (l=0; l<LUM_TYPES+3; l++) fprintf(lums, " %g %g", LUM(l,0), LUM(l,1));
	  fprintf(lums, "\n");
	}
      }
    }
  }
  fclose(out);
  fclose(lums);
  free(sfh);
#undef LUM
}


int main(int argc, char **argv) {
  if (argc < 3) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg postprocessed_mcmc_data1 ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  r250_init(87L);
  init_cosmology(Om, Ol, h0);
  init_time_table(Om, h0);

  load_luminosities("fsps");
  
  for (int64_t i=2; i<argc; i++) {
    results = load_mcmc_data(argv[i]);
    gen_sfh_data(i-2);
    free_mcmc_data(results);
  }
  return 0;
}

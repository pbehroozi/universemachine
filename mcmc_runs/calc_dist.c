#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include "../check_syscalls.h"
#include "../sampler.h"
#include "../io_helpers.h"
#include "../make_sf_catalog.h"
#include "../mt_rand.h"

struct sf_model_allz bestfit = {{0}};

int main(int argc, char **argv)
{
  if (argc < 2) {
    fprintf(stderr, "Usage: %s mcmc_file.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  FILE *posfile = check_fopen(argv[1], "r");
  char buffer[2048];
  rewind(posfile);

  //  double new_eigs[NUM_PARAMS] = {0.0427322, 0.105684, 0.0140191, 0.241488, 0.0020954, 0.00621681, 0.000513903, 0.0442832, 0.0505163, 0.316463, 0.0181005, 0.838616, 0.163456, 1.33159, 0.119366, 0.00117525, 0.0173672, 0.154907, 0.0383076, 0.0219435, 0.0667923, 0.0760857, 0.115425, 0.244953, 0.253732, 0.0364125, 0.250008, 0.00212705, 0.0321145, 0.0058682, 0.0082383, 0.0356479, 0.0268349, 0.0152751, 0.52437, 0.0120003, 0.00302572, 0.00413219, 0.0185455, 0.0244624, 0.00714201, 0.0259992, 0.0114779, 0.0423187, 0.0131765};
  
  int64_t total_read = 0;
  int64_t i = 0, steps = 0;
  struct EnsembleSampler *e = new_sampler(684, NUM_PARAMS, 1.4);
  e->mode = DJ_SAMPLER;
  e->static_dim = 2;

  sampler_init_averages(e);
  while (fgets(buffer, 2048, posfile)) {
    double params[NUM_PARAMS+2];
    int64_t np = 0;
    np = read_params(buffer, params, NUM_PARAMS+2);
    if (np < (NUM_PARAMS+1)) break;
    if (params[np-1]>340) continue;
    init_walker_position(e->w+i, params);
    for (int64_t j=0; j<e->dim; j++) {
      e->avg[j] += e->w[i].params[j];
      for (int64_t k=0; k<e->dim; k++)
	e->cov_matrix[j*e->dim + k] += e->w[i].params[j]*e->w[i].params[k];
    }
    e->avg_steps++;
    e->w[i].accepted_chi2 = (np==(NUM_PARAMS+1)) ? params[NUM_PARAMS] : params[NUM_PARAMS+1];
    if (e->w[i].accepted_chi2 > -1 && (e->w[i].accepted_chi2 < CHI2(bestfit) || CHI2(bestfit)==0)) {
      memcpy(bestfit.params, params, sizeof(double)*NUM_PARAMS);
      CHI2(bestfit) = e->w[i].accepted_chi2;
    }
    steps++;
    i = (i+1)%e->num_walkers;
    total_read = ftello(posfile);
  }

  if (e->avg_steps)
    for (i=0; i<e->dim; i++) e->avg[i] /= (double)e->avg_steps;

  sampler_stats_to_step(e);


  FILE *out = check_fopen("random_steps.dat", "w");
  struct Walker w = {0};
  check_calloc_s(w.params, sizeof(double), (e->dim+1)*2);
  for (i=0; i<100000; i++) {
    double ds=0;
    double dist[NUM_PARAMS+2];
    sampler_random_step(e, &w);
    for (int64_t j=0; j<e->dim; j++) {
      dist[j] = e->avg[j]-w.params[j];
    }
    for (int64_t j=0; j<e->dim; j++) {
      double dx = 0;
      if (e->eigenvalues[j]<1e-6) continue;
      for (int64_t k=0; k<e->dim; k++)
	dx += dist[k]*e->orth_matrix[j*e->dim+k];
      dx /= e->eigenvalues[j];
      ds += dx*dx;
    }

    //    fprintf(out, "%.12e ", ds);
    
    for (int64_t j=0; j<e->dim; j++)
      fprintf(out, "%.12e ", w.params[j]);
    fprintf(out, "-1");
    fprintf(out, "\n");
  }
  free(w.params);
  fclose(out);

  for (i=0; i<e->dim; i++) {
    fprintf(stderr, "ev[%"PRId64"]: %g\n", i, e->eigenvalues[i]);
  }
  rewind(posfile);

   while (fgets(buffer, 2048, posfile)) {
    double params[NUM_PARAMS+2];
    int64_t np = 0;
    np = read_params(buffer, params, NUM_PARAMS+2);
    if (np < (NUM_PARAMS+1)) break;
    double ds=0, dsb=0;

    double dist[NUM_PARAMS+2];
    
    for (int64_t i=0; i<e->dim; i++) {
      dist[i] = e->avg[i] - params[i];
    }
    for (int64_t i=0; i<e->dim; i++) {
      double dx = 0;
      if (e->eigenvalues[i]==0) continue;
      for (int64_t j=0; j<e->dim; j++)
	dx += dist[j]*e->orth_matrix[i*e->dim+j];
      dx /= e->eigenvalues[i];
      ds += dx*dx;
    }

    for (int64_t i=0; i<e->dim; i++) {
      dist[i] = bestfit.params[i] - params[i];
    }
    for (int64_t i=0; i<e->dim; i++) {
      double dx = 0;
      if (e->eigenvalues[i]==0) continue;
      for (int64_t j=0; j<e->dim; j++)
	dx += dist[j]*e->orth_matrix[i*e->dim+j];
      dx /= e->eigenvalues[i];
      dsb += dx*dx;
    }

    printf("%f %f", ds, dsb);
    for (i=0; i<np; i++) printf(" %g", params[i]);
    printf("\n");
  }  
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "distance.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "make_sf_catalog.h"

double box_volume = 0;
double *vweights = NULL, *total_volumes = NULL;


void calc_volume_cache(void) {
  int64_t i;
  box_volume = pow(BOX_SIZE/h0, 3);
  check_realloc_s(vweights, sizeof(double), num_scales);
  check_realloc_s(total_volumes, sizeof(double), num_scales);
  for (i=0; i<num_scales; i++) {
    double z1 = (i>0) ? (2.0/(scales[i-1]+scales[i])-1.0) : 1.0/scales[i]-1.0;
    double z2 = (i<(num_scales-1)) ? (2.0/(scales[i+1]+scales[i])-1.0) : 1.0/scales[i]-1.0;
    if (z2 < 0.0001) z2 = 0.0001;
    vweights[i] = (Vc(z1)-Vc(z2))/box_volume;
    total_volumes[i] = Vc(z1);
  }
}

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <assert.h>
#include "observations.h"
#include "distance.h"
#include "universe_time.h"
#include "sf_model.h"
#include "check_syscalls.h"
#include "config_vars.h"
#include "make_sf_catalog.h"
#include "corr_lib.h"
#include "io_helpers.h"
#include "watchdog.h"
#include "vweights.h"

extern FILE *logfile;
double *min_sm_bin = NULL;
double *obs_smf = NULL, *obs_smf_q = NULL, *obs_smf_q_uvj = NULL, *obs_smf_q11 = NULL,  *obs_ssfr = NULL, *obs_uvlf = NULL, *obs_csfr = NULL, *obs_csfr_uv = NULL, *obs_smf_counts = NULL, *obs_ir_excess=NULL, *partials = NULL;
struct pca_errors *pcas = NULL;
int64_t num_pcas = 0;
int64_t num_obs_files_loaded = 0;

char *obs_data_types[NUM_TYPES] = {"smf", "uvlf", "qf", "qf11", "qf_uvj", "ssfr", "cosmic sfr", "cosmic sfr (uv)", "correlation", "cross correlation", "conformity", "lensing", "cdens fsf", "cdens ssfr_sf", "uvsm", "irx"};

void calc_min_sm_bins(void) {
  int64_t i;
  check_realloc_s(min_sm_bin, sizeof(double), num_scales);
  for (i=0; i<num_scales; i++) {
    double z1 = (i>0) ? (2.0/(scales[i-1]+scales[i])-1.0) : 1.0/scales[i]-1.0;
    double z2 = (i<(num_scales-1)) ? (2.0/(scales[i+1]+scales[i])-1.0) : 1.0/scales[i]-1.0;
    if (z2 < 0.0001) z2 = 0.0001;
    double min_sm = SIM_RES_LIMIT_SM0 + SIM_RES_LIMIT_SM1*0.5*(z1+z2);
    min_sm_bin[i] = sm_to_bin(min_sm);
  }
}

void conv_to_cumulative(float *in, double *out, int64_t num_bins, int64_t vweight) {
  int64_t i, j;
  memset(out, 0, sizeof(double)*num_scales*num_bins);
  check_realloc_s(partials, sizeof(double), num_bins);
  memset(partials, 0, sizeof(double)*num_bins);
  for (i=num_scales-1; i>=0; i--) {
    double vw = vweight ? vweights[i] : 1.0;
    for (j=num_bins-1; j>=0; j--) {
      double wn = in[i*num_bins+j]*vw;
      double w = wn;
      if (j<(num_bins-1)) w += out[i*num_bins+j+1];
      w += partials[j];
      out[i*num_bins+j] = w;
      partials[j] += wn;
    }
  }
}



float _calc_median_uvsm(float *data, float bin_floor) {
  int64_t i, j, last_i=-1, counts=0;
  double tot = 0, tot2 = 0, min_nonzero = -1;
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (data[i]>0) {
      counts++;
      if (min_nonzero < 0 || min_nonzero > data[i]) min_nonzero = data[i];
    }
  }

  if (counts < 1) return 0;

  //Smooth over bins w/ zeros
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (!data[i] && last_i > -1) {
      while (!data[i] && i<UVSM_SM_BINS) { i++; }
      if (i < UVSM_SM_BINS) {
	data[last_i] -= min_nonzero / 3.0;
	data[i] -= min_nonzero / 3.0;
	assert(i>last_i+1);
	for (j=last_i+1; j<i; j++) data[j] += 2.0*min_nonzero / (float)(3.0*(i-last_i-1));
      }
    }
    if (i<UVSM_SM_BINS && data[i]) last_i = i;
  }

  for (i=0; i<UVSM_SM_BINS; i++) tot += data[i];
  
  float median = bin_floor;
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (tot2 + data[i] > 0.5*tot) {
      float offset = (0.5*tot - tot2) / data[i];
      //printf("%f %f %f\n", tot, tot2, offset);
      median = bin_floor + ((float)i + offset) / (float)UVSM_SM_BPDEX;
      break;
    }
    tot2 += data[i];
  }
  return pow(10, median);
}

void calc_median_uvsm(void) {
  int64_t i, j;
  for (i=0; i<UVSM_REDSHIFTS; i++) {
    for (j=0; j<UVSM_UV_BINS; j++) {
      float bin_median = ((float)j+0.5) / (float)UVSM_BPMAG + (float)UVSM_MIN;
      float bin_floor = UVSM_SM_NORM + (bin_median - UVSM_SM_NORM_M0)*UVSM_SM_SLOPE;
      median_uvsm[i*UVSM_UV_BINS + j] = _calc_median_uvsm(uvsm+((i*UVSM_UV_BINS)+j)*UVSM_SM_BINS, bin_floor);
      //printf("%"PRId64" %"PRId64": %e %f\n", i, j, median_uvsm[i*UVSM_UV_BINS + j], bin_floor);
      //printf("%f %e %"PRId64" %"PRId64" %f\n", bin_median, median_uvsm[i*UVSM_UV_BINS+j], i, j, bin_floor);
    }
  }
}


void prep_smf_and_ssfr_and_csfr(void) {
  if (!obs_smf) {
    check_realloc_s(obs_smf, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_smf_counts, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_smf_q, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_smf_q_uvj, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_smf_q11, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_ssfr, sizeof(double), num_scales*SM_NBINS);
    check_realloc_s(obs_uvlf, sizeof(double), num_scales*UV_NBINS);
    check_realloc_s(obs_ir_excess, sizeof(double), num_scales*UV_NBINS);
    check_realloc_s(obs_csfr, sizeof(double), num_scales);
    check_realloc_s(obs_csfr_uv, sizeof(double), num_scales);
    calc_volume_cache();
    calc_min_sm_bins();
  }
  conv_to_cumulative(smf, obs_smf_counts, SM_NBINS, 0);
  conv_to_cumulative(smf, obs_smf, SM_NBINS, 1);
  conv_to_cumulative(smf_q, obs_smf_q, SM_NBINS, 1);
  conv_to_cumulative(smf_q2, obs_smf_q11, SM_NBINS, 1);
  conv_to_cumulative(smf_q3, obs_smf_q_uvj, SM_NBINS, 1);
  conv_to_cumulative(ssfr, obs_ssfr, SM_NBINS, 1);
  conv_to_cumulative(uvlf, obs_uvlf, UV_NBINS, 1);
  conv_to_cumulative(ir_excess, obs_ir_excess, UV_NBINS, 1);
  conv_to_cumulative(csfr, obs_csfr, 1, 1);
  conv_to_cumulative(csfr_uv, obs_csfr_uv, 1, 1);
  calc_median_uvsm();
}

int64_t z_to_step(double z) {
  double scale = 1.0/(1.0+z);
  int64_t min = 0, max = num_scales-1;
  while (min < max-1) {
    int64_t i = (max+min)/2;
    if (scales[i]>scale) max = i;
    else min = i;
  }
  if (fabs(scales[min]-scale) < fabs(scales[max]-scale)) return min;
  return max;
}

int64_t sm_to_bin(double sm) {
  if (sm < SM_MIN) return 0;
  if (sm > SM_MAX) sm = SM_MAX;
  return ((sm-SM_MIN)*SM_BPDEX+0.5);
}

int64_t uv_to_bin(double uv) {
  if (uv < UV_MIN) return 0;
  if (uv > UV_MAX) uv = UV_MAX;
  return ((uv-UV_MIN)*UV_BPMAG+0.5);
}

int64_t n_to_dens_bin(double n1, double n2) {
  int64_t b1 = dens_bin_of(n1);
  int64_t b2 = dens_bin_of(n2);
  int64_t b3 = dens_bin_of(n1-1);
  int64_t b4 = dens_bin_of(n2+1);
  if ((b1 != b2) || (b3 == b1) || ((b4==b2) && (b2 != (NUM_DENS_BINS-1)))) return -1;
  if (b1 > NUM_DENS_BINS-1) return -1;
  return b1;
}

int64_t r_to_bin(double r1, double r2) {
  init_dists();
  r1*=r1;
  r2*=r2;
  int64_t b1 = corr_bin_of(r1*1.01);
  int64_t b2 = corr_bin_of(r2*0.99);
  int64_t b3 = corr_bin_of(r1*0.99);
  int64_t b4 = corr_bin_of(r2*1.01);
  if ((b1 != b2) || (b3 == b1) || (b4==b2)) return -1;
  if (b1 > NUM_BINS-1) return -1;
  return b1;
}

int64_t r_to_conf_bin(double r1, double r2) {
  init_conf_dists();
  r1 *= h0;
  r2 *= h0;
  r1*=r1;
  r2*=r2;
  int64_t b1 = conf_bin_of(r1*1.01);
  int64_t b2 = conf_bin_of(r2*0.99);
  int64_t b3 = conf_bin_of(r1*0.99);
  int64_t b4 = conf_bin_of(r2*1.01);
  if ((b1 != b2) || (b3 == b1) || (b4==b2)) return -1;
  if (b1 > NUM_CONF_BINS-1) return -1;
  return b1;
}

int64_t _sm_to_thresh(double sm, double *threshes) {
  int64_t i;
  for (i=3; i>-1; i--) if (sm > threshes[i]) break;
  return i;
}

int64_t sm_to_thresh(double sm_low, double sm_high) {
  double sm_threshes[4];
  sm_threshes[0] = log10(CORR_MIN_SM);
  sm_threshes[1] = log10(CORR_MIN_SM2);
  sm_threshes[2] = log10(CORR_MIN_SM3);
  sm_threshes[3] = log10(CORR_MIN_SM4);
  int64_t b1 = _sm_to_thresh(sm_low*1.01, sm_threshes);
  int64_t b2 = _sm_to_thresh(sm_high*0.99, sm_threshes);
  int64_t b3 = _sm_to_thresh(sm_low*0.99, sm_threshes);
  int64_t b4 = _sm_to_thresh(sm_high*1.01, sm_threshes);
  if ((b1 != b2) || (b3 == b1) || (b4==b2 && b2!=3)) return -1;
  return b1;
}

int64_t bins_to_xcorr_thresh(int64_t smb1, int64_t smb2) {
  if (smb1 < 0 || smb2 < 0) return -1;
  for (int64_t i=0; i<NUM_XCORR_THRESHES; i++) {
    if (XCORR_BIN1[i]==smb1 && XCORR_BIN2[i]==smb2) return i;
  }
  return -1;
}


double volume_of(int64_t step1, int64_t step2) {
  double v1 = total_volumes[step1], v2=0;
  if (step2 < num_scales-1) v2 = total_volumes[step2+1];
  return (v1-v2);
}

//Requires step1<=step2, sm1<sm2
double integrate_vals(double *array, int64_t smb1, int64_t smb2, int64_t step1, int64_t step2) {
  double v1 = array[step1*SM_NBINS+smb1], v2=0, v3=0, v4=0;
  if (smb2 < SM_NBINS) v2 = array[step1*SM_NBINS+smb2];
  if (step2 < num_scales-1) v3 = array[(step2+1)*SM_NBINS+smb1];
  if (v2 && v3) v4 = array[(step2+1)*SM_NBINS+smb2];
  return ((v1-v2)-(v3-v4));
}

double integrate_vals_uvlf(double *array, int64_t smb1, int64_t smb2, int64_t step1, int64_t step2) {
  double v1 = array[step1*UV_NBINS+smb1], v2=0, v3=0, v4=0;
  if (smb2 < UV_NBINS) v2 = array[step1*UV_NBINS+smb2];
  if (step2 < num_scales-1) v3 = array[(step2+1)*UV_NBINS+smb1];
  if (v2 && v3) v4 = array[(step2+1)*UV_NBINS+smb2];
  return ((v1-v2)-(v3-v4));
}

double integrate_csfr(double *array, int64_t step1, int64_t step2) {
  double v1 = array[step1], v2=0;
  if (step2 < num_scales-1) v2 = array[step2+1];
  return (v1-v2);
}


double calc_ssfr(struct observation *o) {
  assert(o->type == SSFR_TYPE);
  double ossfr = integrate_vals(obs_ssfr, o->smb1, o->smb2, o->step1, o->step2);
  double osmf = integrate_vals(obs_smf, o->smb1, o->smb2, o->step1, o->step2);
  if (osmf > 0) ossfr /= osmf;
  return ossfr;
}

double calc_smf(struct observation *o) {
  assert(o->type == SMF_TYPE);
  double osmf = integrate_vals(obs_smf, o->smb1, o->smb2, o->step1, o->step2);
  double vol = volume_of(o->step1, o->step2);
  assert(vol>0 && o->dsm > 0);
  return osmf/(vol*o->dsm);
}

double calc_uvlf(struct observation *o) {
  assert(o->type == UVLF_TYPE);
  double ouvlf = integrate_vals_uvlf(obs_uvlf, o->smb1, o->smb2, o->step1, o->step2);
  double vol = volume_of(o->step1, o->step2);
  assert(vol>0 && o->dsm > 0);
  return ouvlf/(vol*o->dsm);
}

double calc_irx(struct observation *o) {
  assert(o->type == IRX_TYPE);
  double ouvlf = integrate_vals_uvlf(obs_uvlf, o->smb1, o->smb2, o->step1, o->step2);
  double oirx = integrate_vals_uvlf(obs_ir_excess, o->smb1, o->smb2, o->step1, o->step2);
  if (ouvlf>0) oirx /= ouvlf;
  else oirx = 0;
  return oirx;
}

double calc_qf(struct observation *o) {
  assert(o->type == QF_TYPE);
  double osmf = integrate_vals(obs_smf, o->smb1, o->smb2, o->step1, o->step2);
  double osmf_q = integrate_vals(obs_smf_q, o->smb1, o->smb2, o->step1, o->step2);
  if (osmf > 0) osmf_q /= osmf;
  else osmf_q = -1000;
  return osmf_q;
}

double calc_qf_uvj(struct observation *o) {
  assert(o->type == QF_UVJ_TYPE);
  double osmf = integrate_vals(obs_smf, o->smb1, o->smb2, o->step1, o->step2);
  double osmf_q = integrate_vals(obs_smf_q_uvj, o->smb1, o->smb2, o->step1, o->step2);
  if (osmf > 0) osmf_q /= osmf;
  else osmf_q = -1000;
  return osmf_q;
}

double calc_qf11(struct observation *o) {
  assert(o->type == QF11_TYPE);
  double osmf = integrate_vals(obs_smf, o->smb1, o->smb2, o->step1, o->step2);
  double osmf_q = integrate_vals(obs_smf_q11, o->smb1, o->smb2, o->step1, o->step2);
  if (osmf > 0) osmf_q /= osmf;
  else osmf_q = -1000;
  return osmf_q;
}

double calc_csfr(struct observation *o) {
  assert(o->type == CSFR_TYPE);
  double ocsfr = integrate_csfr(obs_csfr, o->step1, o->step2);
  double vol = volume_of(o->step1, o->step2);
  assert(vol>0);
  return ocsfr / vol;
}

double calc_csfr_uv(struct observation *o) {
  assert(o->type == CSFR_UV_TYPE);
  double ocsfr_uv = integrate_csfr(obs_csfr_uv, o->step1, o->step2);
  double vol = volume_of(o->step1, o->step2);
  assert(vol>0);
  return ocsfr_uv / vol;
}

double calc_uvsm(struct observation *o) {
  assert(o->type == UVSM_TYPE);
  return median_uvsm[o->step1*UVSM_UV_BINS+o->smb1];
}

double calc_cdens_fsf(struct observation *o) {
  int64_t i;
  double c=0, sf=0;
  for (i=o->step1-1; i<o->step2; i++) {
    if (scales[i] < DENS_MIN_SCALE) continue;
    c += fdens_counts[o->smb1 + i*NUM_DENS_BINS]*vweights[i];
    sf += fdens_sf[o->smb1 + i*NUM_DENS_BINS]*vweights[i];
  }
  if (!c) return 0;
  return (sf / c);
}

double calc_cdens_ssfr_sf(struct observation *o) {
  int64_t i;
  double sf=0, ssfr_sf=0;
  for (i=o->step1-1; i<o->step2; i++) {
    if (scales[i] < DENS_MIN_SCALE) continue;
    sf += fdens_sf[o->smb1 + i*NUM_DENS_BINS]*vweights[i];
    ssfr_sf += fdens_sf_ssfr[o->smb1 + i*NUM_DENS_BINS]*vweights[i];
  }
  if (!sf) return 0;
  return (ssfr_sf / sf);
}

double calc_conf(struct observation *o) {
  int64_t i;
  double c=0;
  double s1=0, s2=0, s11=0, s22=0, s12=0;
  for (i=o->step1-1; i<o->step2; i++) {
    if (scales[i] < CONFORMITY_MIN_SCALE) continue;
    float *conf = fconfs + i*NUM_CONF_BINS_PER_STEP;
    c   += conf[o->smb1]*vweights[i];
    s1  += conf[o->smb1+NUM_CONF_BINS]*vweights[i];
    s2  += conf[o->smb1+2*NUM_CONF_BINS]*vweights[i];
    s11 += conf[o->smb1+3*NUM_CONF_BINS]*vweights[i];
    s22 += conf[o->smb1+4*NUM_CONF_BINS]*vweights[i];
    s12 += conf[o->smb1+5*NUM_CONF_BINS]*vweights[i];
  }
  if (!c) return 0;
  s1 /= c;
  s2 /= c;
  s11 /= c;
  s12 /= c;
  s22 /= c;
  double num = s12 - s1*s2;
  double sx = s11-s1*s1;
  double sy = s22-s2*s2;
  if (sx <= 0 || sy <= 0) return 0;
  return (num/sqrt(sx*sy));
}

double calc_corr(struct observation *o) {
  int64_t i, thresh = o->smb1, rbin = o->smb2, subtype = o->subtype;
  double s=0, w=0, c=0;
  
  for (i=o->step1; i<=o->step2; i++) {
    float corr_val = fcorrs[i*NUM_CORR_BINS_PER_STEP + thresh*NUM_CORR_TYPES*NUM_BINS + subtype*NUM_BINS + rbin];
    float counts = fccounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES + subtype];
    s += vweights[i]*corr_val;
    c += vweights[i]*counts;
    w += vweights[i];
  }
  if (!w || !c) return 0;
  s /= c;
  c /= w;
  double d = c; //Density
  if (subtype == 3) { //Cross-correlation, QxSF: average pair count density: (nsf*nq)/box_size
    d = 0;
    for (i=o->step1; i<=o->step2; i++) {
      float *counts = fccounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES;
      d += (counts[1]*counts[2])*vweights[i];
    }
    d /= (w*c);
  }
  
  if (!d) return 0;
  double bin_area = M_PI*(corr_dists[rbin+1]-corr_dists[rbin]);
  double counts_per_length = bin_area * d / (BOX_SIZE*BOX_SIZE*BOX_SIZE);
  double pi_max = CORR_LENGTH_PI;
  if (scales[(int)(0.5*(o->step1+o->step2))] < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  double expected_counts = counts_per_length * 2.0 * pi_max;
  return ((s - expected_counts)/counts_per_length);
}


double calc_xcorr(struct observation *o) {
  int64_t i, thresh = o->smb1, rbin = o->smb2, subtype = o->subtype;
  double s=0, w=0, c=0;
  for (i=o->step1; i<=o->step2; i++) {
    float corr_val = fxcorrs[i*NUM_XCORR_BINS_PER_STEP + thresh*NUM_CORR_TYPES*NUM_BINS + subtype*NUM_BINS + rbin];
    float counts = fccounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN1[thresh]*NUM_CORR_TYPES + XCORR_COUNTS1[subtype]];
    s += vweights[i]*corr_val;
    c += vweights[i]*counts;
    w += vweights[i];
  }
  if (!w || !c) return 0;
  s /= c;
  c /= w;
  double d = 0; //Density
  for (i=o->step1; i<=o->step2; i++) {
    float *counts1 = fccounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN1[thresh]*NUM_CORR_TYPES;
    float *counts2 = fccounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN2[thresh]*NUM_CORR_TYPES;
    d += (counts1[XCORR_COUNTS1[subtype]]*counts2[XCORR_COUNTS2[subtype]])*vweights[i];
  }
  d /= (w*c);
  
  if (!d) return 0;
  double bin_area = M_PI*(corr_dists[rbin+1]-corr_dists[rbin]);
  double counts_per_length = bin_area * d / (BOX_SIZE*BOX_SIZE*BOX_SIZE);
  double pi_max = CORR_LENGTH_PI;
  if (scales[(int)(0.5*(o->step1+o->step2))] < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  double expected_counts = counts_per_length * 2.0 * pi_max;
  return ((s - expected_counts)/counts_per_length);
}




void calc_pca_chi2(struct observation *o, struct pca_errors *p) {
  int64_t i, j;
  assert(o->covariance_id == p->covariance_id);
  assert(o->covariance_idx == 0);
  assert(o[p->num_obs-1].covariance_id == p->covariance_id);
  for (i=0; i<p->num_obs; i++) {
    o[i].chi2 = 0;
    o[i].chi = o[i].model_val - o[i].val;
  }
  for (j=0; j<p->num_pcas; j++) {
    double s = 0;
    double stdev = p->pcas[j*(p->num_obs+1)];
    assert(stdev > 0);
    double *pca = p->pcas + j*(p->num_obs+1) + 1;
    for (i=0; i<p->num_obs; i++) s += pca[i]*o[i].chi;
    if (stdev < CF_TOLERANCE) stdev = CF_TOLERANCE;
    if (s > FIT_TOLERANCE) s -= FIT_TOLERANCE;
    else if (s < -FIT_TOLERANCE) s += FIT_TOLERANCE;
    else s = 0;
    double ds = s/stdev;
    o[j].chi2 = ds*ds;
  }
}

void estimate_counts(struct observation *o) {
  int64_t smb1 = o->smb1, smb2 = o->smb2;
  if (o->type == CSFR_UV_TYPE || o->type == CSFR_TYPE) {
    smb1 = 0; smb2 = SM_NBINS;
  }
  o->model_counts = integrate_vals(obs_smf_counts, smb1, smb2, o->step1, o->step2);
}

void model_chi2(struct observation *o) {
  double err, real_err;
  double fit_error = (o->linear_errors) ? fabs(o->val*(pow(10,FIT_TOLERANCE)-1.0)) : fabs(FIT_TOLERANCE);

  if (o->type == QF_TYPE || o->type == QF11_TYPE || o->type == QF_UVJ_TYPE || o->type == CDENS_FSF_TYPE) {
    fit_error = QF_TOLERANCE;
  }

  if (o->model_val == -1000) { //No galaxies in box
    double bwidth = 0;
    if (o->type == SMF_TYPE) bwidth = (double)SM_BPDEX / (o->smb2 - o->smb1 + 1.0);
    else if (o->type == UVLF_TYPE) bwidth = (double)UV_BPMAG / (o->smb2 - o->smb1 + 1.0);
    if (bwidth) {
      //Treat box volume as upper limit
      double nd = log10f(bwidth/box_volume);
      if (nd > o->val) { o->chi = o->chi2 = 0; return; }
      else o->model_val = nd; //o->val is large enough that there should have been at least one galaxy in the box.
    }
    else {
      //Give fixed chi2 of 2 to encourage solutions that have nonzero values.
      o->chi = M_SQRT2;
      o->chi2 = 2.0;
      return;
    }
  }

  err = o->chi = (o->model_val-o->val);

  if (o->type == SMF_TYPE || o->type == SSFR_TYPE) {
    assert(o->step1 >= 0 && o->step1 < num_scales);
    if (o->smb1 < min_sm_bin[o->step1]) {
      if ((o->type == SMF_TYPE && o->chi < 0) ||
	  (o->type == SSFR_TYPE && o->chi > 0)) {
	//o->chi = o->chi2 = 0;
	fit_error = (o->linear_errors) ? fabs(o->val/2.0) : 0.3;
	return;
      }
    }
  }

  if (fabs(err) <= fit_error) { o->chi = o->chi2 = 0; return; }
  if (err > 0) err -= fit_error;
  else err += fit_error;

  if (err > o->err_h) err/=o->err_h;
  else if (-err > o->err_l) err/=o->err_l;
  else {
    real_err = o->err_l +
      (err+o->err_l)/(o->err_h+o->err_l) *
      (o->err_h - o->err_l);
    err /= real_err;
  }
  if (!isfinite(err)) {
    fprintf(logfile, "[Warning] Infinite error at z=%f, m=%f, type=%"PRId64"\n", 
	    0.5*(o->z_low + o->z_high),
	    0.5*(o->sm_low+o->sm_high), o->type);
    err = 1e15;
  }
  o->chi = err;
  o->chi2 = err*err;
}

//Ignore covariance for now...
double calc_single_chi2(struct observation *o) {
  if      (o->type == SMF_TYPE)             o->model_val = calc_smf(o);
  else if (o->type == UVLF_TYPE)            o->model_val = calc_uvlf(o);
  else if (o->type == SSFR_TYPE)            o->model_val = calc_ssfr(o);
  else if (o->type == QF_TYPE)              o->model_val = calc_qf(o);
  else if (o->type == QF11_TYPE)            o->model_val = calc_qf11(o);
  else if (o->type == QF_UVJ_TYPE)          o->model_val = calc_qf_uvj(o);
  else if (o->type == CSFR_TYPE)            o->model_val = calc_csfr(o);
  else if (o->type == CSFR_UV_TYPE)         o->model_val = calc_csfr_uv(o);
  else if (o->type == CORR_TYPE)            o->model_val = calc_corr(o);
  else if (o->type == XCORR_TYPE)           o->model_val = calc_xcorr(o);
  else if (o->type == CONF_TYPE)            o->model_val = calc_conf(o);
  else if (o->type == CDENS_FSF_TYPE)       o->model_val = calc_cdens_fsf(o);
  else if (o->type == CDENS_SSFR_SF_TYPE)   o->model_val = calc_cdens_ssfr_sf(o);
  else if (o->type == UVSM_TYPE)            o->model_val = calc_uvsm(o);
  else if (o->type == IRX_TYPE)             o->model_val = calc_irx(o);
  else o->model_val = 0;

  if (!o->linear_errors) 
    o->model_val = (o->model_val > 0) ? log10(o->model_val) : -1000;

  model_chi2(o);
  return o->chi2;
}

double calc_chi2(struct observation *obs, int64_t num_obs, struct sf_model_allz m) {
  int64_t i;
  double chi2 = 0;

  prep_smf_and_ssfr_and_csfr();
  //Note: invalidness already pre-checked
  chi2 += pow(MU(m)/MU_PRIOR, 2)
    + pow(MU_A(m)/MU_A_PRIOR, 2) 
    + pow(KAPPA_A(m)/KAPPA_PRIOR, 2)
    + pow((OBS_SM_SIG_Z(m)-SIGMA_Z_CENTER)/SIGMA_Z_PRIOR, 2);

  double corr_highz = (INTR_SCATTER_SF(m) + INTR_SCATTER_SF_A(m)*0.9)/OBS_SCATTER_SFR_SF;
  if (corr_highz < 0.001) chi2 += 1000;
  else { chi2 += 1.0/corr_highz - 1.0; }

  //Ensure that stellar mass increases with halo mass at high redshifts
  struct sf_model z12_sfr = calc_sf_model(m, 1.0/(12.0+1.0));
  if (z12_sfr.beta > 0) chi2 += pow(z12_sfr.beta, 2);
  
  for (i=0; i<num_obs; i++) calc_single_chi2(obs+i);
  for (i=0; i<num_obs; i++) {
    if (obs[i].covariance_id > -1) {
      calc_pca_chi2(obs+i, pcas+obs[i].covariance_id);
      i += pcas[obs[i].covariance_id].num_obs-1;
    }
  }
  for (i=0; i<num_obs; i++) {
    obs[i].chi2 *= obs[i].weight;
    chi2 += obs[i].chi2;
  }
  return chi2;
}

void convert_h(struct observation *o) {
  double offset = log10(h0/0.7);
  if (o->type == SMF_TYPE || o->type == CSFR_TYPE || o->type == CSFR_UV_TYPE) {
    o->val -= 3.0*offset;
  }
}


//Load observations from a given file, automatically loading PCA erors if necessary.
//Note that it is possible to mix observation types in the same file; this is
//necessary if, e.g., different measurement types have covariant errors.
void _load_observations(struct observation **obs, int64_t *num_obs, char *filename)
{
  FILE *data;
  char buffer[1024], c;
  char *error_types[] = {"log", "linear"};
  struct observation o = {0};
  int64_t n, i, loaded=0, obs_start = *num_obs, zperline = 0;

  if (!strcmp(filename+strlen(filename)-4, ".pca")) return;
  snprintf(buffer, 1024, "%s/%s", OBSERVATIONS_DIR, filename);

  data = check_fopen(buffer, "r");
  o.type = SMF_TYPE;
  o.linear_errors = 0;
  o.weight = 1;
  o.z_low = o.z_high = 0;
  o.covariance_id = o.covariance_idx = -1;
  o.file_id = num_obs_files_loaded;
  while (fgets(buffer, 1024, data)) {
    if (o.type != CORR_TYPE && o.type != XCORR_TYPE) {
      if (!zperline)
	n = sscanf(buffer, "%lf %lf %lf %lf %lf", &(o.sm_low), &(o.sm_high), &(o.val), &(o.err_h), &(o.err_l));
      else
	n = sscanf(buffer, "%lf %lf %lf %lf %lf %lf %lf", &(o.sm_low), &(o.sm_high), &(o.val), &(o.err_h), &(o.err_l), &(o.z_low), &(o.z_high));
    }
    else if (o.type == CORR_TYPE) {
      n = sscanf(buffer, "%c %lf %lf %lf %lf %lf %lf %lf", &(o.subtype), &(o.sm_low), &(o.sm_high), &(o.r_low), &(o.r_high), &(o.val), &(o.err_h), &(o.err_l));
    }
    else {
      assert(o.type == XCORR_TYPE); //Should be only remaining option
      n = sscanf(buffer, "%c %lf %lf %lf %lf %lf %lf %lf %lf %lf", &(o.subtype), &(o.sm_low), &(o.sm_high), &(o.sm_low2), &(o.sm_high2), &(o.r_low), &(o.r_high), &(o.val), &(o.err_h), &(o.err_l));
    }
    if (buffer[0] == '#') {
      if (!strncmp(buffer, "#zlow: ", 7)) o.z_low = atof(&(buffer[7]));
      if (!strncmp(buffer, "#zhigh: ", 8)) o.z_high = atof(&(buffer[8]));
      if (!strncmp(buffer, "#zperline: ", 11)) zperline = atof(&(buffer[11]));
      if (!strncmp(buffer, "#weight: ", 9)) o.weight = atof(&(buffer[9]));
      if (!strncmp(buffer, "#errors: ", 9)) {
	if (!strncasecmp(&(buffer[9]), "Linear", 6)) o.linear_errors = 1;
	else  o.linear_errors = 0;
      }
      if (!strncmp(buffer, "#type: ", 7)) {
	do {
	  buffer[strlen(buffer)-1] = 0;
	  c = buffer[strlen(buffer)-1];
	} while (c=='\n' || c==' ');
	for (i=0; i<NUM_TYPES; i++)
	  if (!strcasecmp(&(buffer[7]), obs_data_types[i])) break;
	if (i==NUM_TYPES) {
	  fprintf(stderr, "Invalid type \"%s\" in file %s!\n", buffer+7, filename);
	  watchdog_failexit();
	}
	o.type = i;
      }
      if (!strncmp(buffer, "#pca: ", 6)) {
	o.covariance_id = num_pcas;
	o.covariance_idx = 0;
      }
      continue;
    }
    if (((o.type != CORR_TYPE && o.type != XCORR_TYPE) && ((!zperline && n!=5) || (zperline && n!= 7))) //Check normal lines
	|| (o.type == CORR_TYPE && n!=8) //Check correlation functions
	|| (o.type == XCORR_TYPE && n!=10) //Check cross-correlation functions	
	|| (o.type != QF_TYPE && o.type != QF11_TYPE && o.type != QF_UVJ_TYPE && (!o.err_l || !o.err_h))) { //Make sure there are error bars
      if (strlen(buffer) > 1)
	fprintf(stderr, "Invalid line (skipping): %s; type = %"PRId64"\n", buffer, o.type);
      continue;
    }

    if (o.type == CSFR_TYPE || o.type == CSFR_UV_TYPE) {
      o.z_low = o.sm_low;
      o.z_high = o.sm_high;
      o.sm_low = o.sm_high = 10;
    }


    if ((o.type == SMF_TYPE || o.type == QF_TYPE || o.type == QF11_TYPE || o.type == QF_UVJ_TYPE || o.type == SSFR_TYPE) && 
	(o.sm_high > SM_MAX || o.sm_low < SM_MIN)) {
      fprintf(stderr, "Skipping line %s\n(Out of stellar mass range of %f-%f)\n", buffer, (float)SM_MIN, (float)SM_MAX);
      continue;
    }

    //Convert to bins:
    o.step1 = z_to_step(o.z_high);
    o.step2 = z_to_step(o.z_low);
    o.smb1 = sm_to_bin(o.sm_low);
    o.smb2 = sm_to_bin(o.sm_high);
    o.dsm = (o.smb2-o.smb1)/(double)SM_BPDEX;
    if (o.type == UVLF_TYPE || o.type==IRX_TYPE) {
      o.smb1 = uv_to_bin(o.sm_low);
      o.smb2 = uv_to_bin(o.sm_high);
      o.dsm = (o.smb2-o.smb1)/(double)UV_BPMAG;
    }
    if (o.type == UVSM_TYPE) {
      o.step1 = (int64_t)(o.z_low+1.49);
      o.step2 = (int64_t)(o.z_high+0.49);
      if (o.z_low == 0) o.step1 = 0;
      if ((o.step1 != o.step2) || o.step1 >= UVSM_REDSHIFTS) {
	fprintf(stderr, "Skipping line %s\n(Z range of %f - %f does not correspond to calculated bin)\n",
		buffer, o.z_low, o.z_high);
	continue;
      }
      o.smb1 = (int64_t)((o.sm_low-UVSM_MIN)*UVSM_BPMAG+0.99);
      o.smb2 = (int64_t)((o.sm_high-UVSM_MIN)*UVSM_BPMAG-0.01);
      if ((o.smb1 != o.smb2) || (o.smb1 < 0) || (o.smb1 >= UVSM_UV_BINS)) {
	fprintf(stderr, "Skipping line %s\n(UV range of %f - %f does not correspond to calculated bin)\n",
		buffer, o.sm_low, o.sm_high);
	continue;
      }
    }
    
    if (o.type == CDENS_FSF_TYPE || o.type == CDENS_SSFR_SF_TYPE) {
      o.smb1 = n_to_dens_bin(o.sm_low, o.sm_high);
      if (o.smb1<0) {
	fprintf(logfile, "#Error: Neighbor density bins in file %s don't match config file!\n", filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
    }

    if (o.type == CONF_TYPE) {
      o.smb1 = r_to_conf_bin(o.sm_low, o.sm_high);
      if (o.smb1<0) {
	fprintf(logfile, "#Error: Radial bins in file %s don't match config file!\n", filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
    }

    if (o.type == CORR_TYPE) {
      o.smb1 = sm_to_thresh(o.sm_low, o.sm_high);
      o.smb2 = r_to_bin(o.r_low, o.r_high);
      if (o.smb1 < 0 || o.smb2<0) {
	fprintf(logfile, "#Error: SM or Radial bins in file %s don't match config file!\n", filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
      if (o.subtype == 'a') o.subtype = 0;
      else if (o.subtype == 's') o.subtype = 1;
      else if (o.subtype == 'q') o.subtype = 2;
      else if (o.subtype == 'c') o.subtype = 3;
      else {
	fprintf(logfile, "#Error: Unknown subtype %c in file %s! (Should be one of 'a', 's', 'q', or 'c').\n", 
		o.subtype, filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
    }

    if (o.type == XCORR_TYPE) {
      int64_t smb1 = sm_to_thresh(o.sm_low, o.sm_high);
      int64_t smb2 = sm_to_thresh(o.sm_low2, o.sm_high2);
      o.smb1 = bins_to_xcorr_thresh(smb1, smb2);
      o.smb2 = r_to_bin(o.r_low, o.r_high);
      if (o.smb1 < 0 || o.smb2<0) {
	fprintf(logfile, "#Error: SM or Radial bins in file %s don't match config file!\n", filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
      if (o.subtype == 'a') o.subtype = 0;
      else if (o.subtype == 'k') o.subtype = 1;
      else if (o.subtype == 'q') o.subtype = 2;
      else if (o.subtype == 's') o.subtype = 3;
      else {
	fprintf(logfile, "#Error: Unknown subtype %c in file %s! (Should be one of 'a'=AxA, 'k'=AxQ, 'q'=QxQ, or 's'=SFxSF).\n", 
		o.subtype, filename);
	fprintf(logfile, "#Offending line: %s\n", buffer);
	watchdog_failexit();
      }
    }

    //Add in systematic errors from calculations.
    double syst_err = CALCULATION_TOLERANCE;
    struct observation oc = o;
    if (oc.linear_errors) {
      oc.err_h = log10((oc.err_h + oc.val)/oc.val);
      if (oc.err_l >= oc.val) oc.err_l = 1.0;
      else {
	oc.err_l = -log10((oc.val-oc.err_l)/oc.val);
	if (oc.err_l < 0) oc.err_l = -oc.err_l;
	if (oc.err_l > 1.0) oc.err_l = 1.0;
      }
      oc.val = log10(oc.val);
      oc.linear_errors = 0;
    }
    if (oc.type == QF_TYPE || oc.type == QF11_TYPE ||  oc.type == QF_UVJ_TYPE ||
	oc.type == CONF_TYPE || 
	oc.type == CDENS_FSF_TYPE || oc.type == CDENS_SSFR_SF_TYPE)
      oc.linear_errors = 1;
    oc.orig_err_h = oc.err_h;
    oc.orig_err_l = oc.err_l;
    oc.err_h = sqrt(oc.err_h*oc.err_h + syst_err*syst_err);
    oc.err_l = sqrt(oc.err_l*oc.err_l + syst_err*syst_err);
    check_realloc_every(*obs, sizeof(struct observation), *num_obs, 100);
    convert_h(&oc); //Internal stellar mass units still in Msun/h70^2 units
    obs[0][*num_obs] = oc;
    *num_obs = (*num_obs) + 1;
    loaded++;

    if (o.covariance_idx > -1) o.covariance_idx++;
  }
  fclose(data);

  if (o.type == QF_TYPE || o.type == QF11_TYPE ||  o.type == QF_UVJ_TYPE ||
      o.type == CONF_TYPE || 
      o.type == CDENS_FSF_TYPE || o.type == CDENS_SSFR_SF_TYPE)
    o.linear_errors = 1;
  fprintf(logfile, "#Loaded %"PRId64" points from %s (type: %s; errors: %s; file_id: %"PRId64")\n", loaded, filename, obs_data_types[o.type], error_types[o.linear_errors], o.file_id);
  num_obs_files_loaded++;

  if (o.covariance_id < 0) return;

  snprintf(buffer, 1024, "%s/%s.pca", OBSERVATIONS_DIR, filename);
  data = check_fopen(buffer, "r");
  check_realloc_s(pcas, sizeof(struct pca_errors), num_pcas+1);
  memset(pcas+num_pcas, 0, sizeof(struct pca_errors));
  struct pca_errors *p = pcas+num_pcas;
  p->covariance_id = num_pcas;
  p->obs_start = obs_start;
  p->num_obs = (*num_obs) - obs_start;
  while (fgets(buffer, 1024, data)) {
    if (buffer[0]=='#') {
      if (!strncmp(buffer, "#obs: ", 6)) {
	int64_t nobs = atof(buffer+6);
	if (nobs != p->num_obs) {
	  fprintf(logfile, "#Incorrect number of observations in PCA file %s.pca!  (Got %"PRId64"; expected %"PRId64")\n",
		  filename, nobs, p->num_obs);
	  watchdog_failexit();
	}
      }
      continue;
    }
    check_realloc_s(p->pcas, sizeof(double), (p->num_obs+1)*(p->num_pcas+1));
    double *new_pca = p->pcas+(p->num_obs+1)*(p->num_pcas);
    n = read_params(buffer, new_pca, p->num_obs+1);
    if (n>0 && (n < p->num_obs+1)) {
      fprintf(logfile, "#Invalid PCA line in %s.pca; expected %"PRId64" elements but got only %"PRId64"\n",
	      filename, p->num_obs+1, n);
      watchdog_failexit();
    }
    p->num_pcas++;
  }
  fclose(data);
  num_pcas++;
}

void load_observations(struct observation **obs, int64_t *num_obs) {
  DIR *in = opendir(OBSERVATIONS_DIR);
  struct dirent *d;
  if (!in) {
    fprintf(stderr, "Observations directory: \"%s\"\n", OBSERVATIONS_DIR);
    system_error("Could not open!");
  }
  while ((d=readdir(in))) {
    int64_t len = strlen(d->d_name);
    if (len == 0 || (d->d_type & DT_DIR) || d->d_name[len-1]=='~') continue;
    _load_observations(obs, num_obs, d->d_name);
  }
  closedir(in);
}

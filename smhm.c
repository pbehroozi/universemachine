#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <inttypes.h>
#include "make_sf_catalog.h"

off_t fsize(const char *filename) {
  struct stat st; 

  if (stat(filename, &st) == 0)
    return st.st_size;

  return -1; 
}

int main(int argc, char **argv) {
  int64_t sz = fsize(argv[1]);
  FILE *in = fopen(argv[1], "r");
  if (!in) exit(1);
  struct catalog_halo *p = malloc(sz);
  fread(p, 1, sz, in);
  int64_t i;
  int64_t total_read = (sz/sizeof(struct catalog_halo));
  for (i=0; i<total_read; i++) {
    if (p[i].sm < 1e8) continue;
    printf("%e %e\n", p[i].mp/0.68, p[i].sm*0.68/p[i].mp);
  }
  return 0;
}

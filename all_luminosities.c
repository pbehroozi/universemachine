#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <inttypes.h>
#ifdef __linux__
#include <malloc.h>
#endif /* __linux__ */
#include "all_luminosities.h"
#include "check_syscalls.h"
#include "distance.h"
#include "universe_time.h"
#include "stringparse.h"
#include "expcache.h"
#ifdef USE_GSL
#include <gsl/gsl_interp.h>
#endif /* USE_GSL */


#define MIN_AGE 0
#define L_NUM_CACHE_ZS L_NUM_ZS

#ifdef USE_GSL
gsl_interp *lum_interps[DUST_TYPES][LUM_TYPES][L_NUM_REDSHIFTS][L_NUM_ZS];
gsl_interp_accel *lum_accel[DUST_TYPES][LUM_TYPES][L_NUM_REDSHIFTS][L_NUM_ZS];
#endif /*USE_GSL*/

double max_Z=0,min_Z=0;

struct luminosities *tlum = NULL;
extern int64_t num_outputs;
char *lum_names[LUM_TYPES] = {"sloan_g", "sloan_r", "sloan_i","cousins_i",
			      "f160w","f435w", "f606w", "f775w", "f814w", "f850lp",
			      "2MASS_J", "2MASS_H", "2MASS_Ks", "fors_r",
			      "M1500_UV", "JWST_F200W", "JWST_F277W", "JWST_F356W", "JWST_F444W", "f105w", "f125w",
			      "Johnson_V", "Johnson_U", "GALEX_NUV", "f140w", "f098m", "IRAC_1", "IRAC_2"};



void gen_lum_lookup(float **lum_lookup, float *scales, int64_t num_scales, enum lum_types t, float z_src) {
  int64_t i,j,k;
  alloc_metallicity_cache(scales, num_scales);
  check_realloc_s(*lum_lookup, sizeof(float), (num_scales+1)*DUST_TYPES*L_NUM_CACHE_ZS);
  for (i=0; i<L_NUM_CACHE_ZS; i++) {
    double Z = min_Z + i*(max_Z-min_Z)/(double)(L_NUM_CACHE_ZS-1);
    for (j=0; j<num_scales+1; j++) {
      /*float a1 = (j>0) ? scales[j-1] : 0;
	float a2 = scales[j];*/
      float a1 = (j>0) ? 0.5*(scales[j]+scales[j-1]) : 0;
      float a2 = (j<num_scales) ? 0.5*(scales[j]+scales[j+1]) : scales[j];
      //if (j==num_scales) a1 = 0.75*scales[j]+0.25*scales[j-1];
      float t1 = scale_to_years(scales[num_scales])-scale_to_years(a1);
      float t2 = scale_to_years(scales[num_scales])-scale_to_years(a2);
      for (k=0; k<DUST_TYPES; k++) {
	lum_lookup[0][k*L_NUM_CACHE_ZS*(num_scales+1) + j*L_NUM_CACHE_ZS + i] = 
	  lum_at_age_Z(tlum+k, t, t2, t1, Z, z_src, k);
	//	printf("%f %f %f %"PRId64" %"PRId64" %"PRId64" %f\n", 0.5*(a1+a2), scales[num_scales], Z, i, j, k, log10(lum_lookup[0][i*(num_scales+1)*DUST_TYPES + j*DUST_TYPES + k])*-2.5);
      }
    }
  }
}

void rescale_dust(float **lum_lookup, int64_t num_scales, double mag_ratio) {
  int64_t i, j, k;
  double lum[2];
  for (i=0; i<L_NUM_CACHE_ZS; i++) {
    for (j=0; j<num_scales+1; j++) {
      for (k=0; k<DUST_TYPES; k++) {
	lum[k] = lum_lookup[0][k*L_NUM_CACHE_ZS*(num_scales+1) + j*L_NUM_CACHE_ZS + i];
      }
      lum_lookup[0][1*L_NUM_CACHE_ZS*(num_scales+1) + j*L_NUM_CACHE_ZS + i] = lum[0]*pow(lum[1]/lum[0], mag_ratio);
    }
  }
}


static inline float _lookup_lum(float *lum_lookup, float Z, int64_t n, int64_t i, int64_t dust, int64_t b, float f) {
  /*float l1 = lum_lookup[(b*(n+1)*DUST_TYPES)+i*DUST_TYPES+dust];
    float l2 = lum_lookup[((b+1)*(n+1)*DUST_TYPES)+i*DUST_TYPES+dust];*/

  float l1 = lum_lookup[dust*L_NUM_CACHE_ZS*(n+1) + i*L_NUM_CACHE_ZS + b];
  float l2 = lum_lookup[dust*L_NUM_CACHE_ZS*(n+1) + i*L_NUM_CACHE_ZS + b+1];
  return (l1 + f*(l2-l1));
}


float lookup_lum(float *lum_lookup, float Z, int64_t n, int64_t i, int64_t dust) {
  float f = (Z-min_Z) * (double)(L_NUM_CACHE_ZS-1) / (max_Z-min_Z);
  if (f<0) f=0;
  if (f>=L_NUM_CACHE_ZS-1) f=L_NUM_CACHE_ZS-1.001;
  int64_t b = f;
  f -= b;
  return _lookup_lum(lum_lookup, Z, n, i, dust, b, f);
}


float lum_of_sfh(float *lum_lookup, float *sfh, float *scales, int64_t n, float dust) {
  int64_t i;
  double total_sm = 0;
  double total_l = 0;
  double total_l_dust = 0;
  for (i=0; i<=n; i++) {
    double new_sm = sfh[i];
    //Use instantaneous recycling fraction of 30%
    total_sm += new_sm*0.7;
    if (total_sm < 1) total_sm = 1;
    float z = 1.0/scales[i]-1.0;
    float Z = metallicity(log10fc(total_sm), z);
      //metallicity_cache(total_sm, z, i);
    float avg_lum_nodust = lookup_lum(lum_lookup, Z, n, i, 0);
    float avg_lum_dust = lookup_lum(lum_lookup, Z, n, i, 1);
    total_l += avg_lum_nodust*new_sm;
    total_l_dust += avg_lum_dust*new_sm;
  }
  if (dust < 0) dust = 0;
  if (dust > 20) dust = 20;
  if (!(total_l > 0 && total_l_dust > 0)) return 1000;
  return ((log10f(total_l) + dust*log10f(total_l_dust/total_l))*-2.5);
}

float fake_lum4_of_sfh(float *ll, float *ll2, float *ll3, float *ll4, float *sfh, float *scales, int64_t n, float dust, float *uv, float *vj) {
  *uv = *vj = 0;
  return lum_of_sfh(ll, sfh, scales, n, dust);
}


float lum4_of_sfh(float *ll, float *ll2, float *ll3, float *ll4, float *sfh, float *scales, int64_t n, float dust, float *uv, float *vj) {
  int64_t i;
  double total_sm = 0;
  double total_l = 0;
  //double total_l_dust = 0;
  double tl2 = 0, tl3 = 0, tl4 = 0;
  for (i=0; i<=n; i++) {
    double new_sm = sfh[i];
    if (i==n) new_sm *= 0.5;

    //Use instantaneous recycling fraction of 30%
    if (!new_sm) continue;
    total_sm += new_sm*0.7;
    if (total_sm < 1) total_sm = 1;
    float z = (scales[i] > 0) ? 1.0/scales[i]-1.0 : 20;
    float Z = metallicity_cache(total_sm, z, i);
    float avg_lum_nodust = lookup_lum(ll, Z, n, i, 0);
    //float avg_lum_dust = lookup_lum(ll, Z, n, i, 1);
    total_l += avg_lum_nodust*new_sm;
    //total_l_dust += avg_lum_dust*new_sm;
    tl2 += lookup_lum(ll2, Z, n, i, 1)*new_sm;
    tl3 += lookup_lum(ll3, Z, n, i, 1)*new_sm;
    tl4 += lookup_lum(ll4, Z, n, i, 1)*new_sm;
    /*if (total_sm > 1e11 && n==170 && i==n-100) {
      printf("%e %f %f %f %f %f %f %f %f %f\n", total_sm, (log10f(total_l) + dust*log10f(total_l_dust/total_l))*-2.5,
	     log10f(tl2)*-2.5, log10f(tl3)*-2.5, log10f(tl4)*-2.5, Z,
	     log10f(lookup_lum(ll2, Z, n, i, 1))*-2.5, log10f(lookup_lum(ll3, Z, n, i, 1))*-2.5, log10f(lookup_lum(ll4, Z, n, i, 1))*-2.5, log10f(scale_to_years(scales[n])-scale_to_years(scales[i])));
	     }*/
  }
  /*if (dust < 0) dust = 0;
    if (dust > 20) dust = 20;*/
  *uv = *vj = 0;
  //if (!(total_l > 0 && total_l_dust > 0)) return 1000;
  if (!(total_l > 0)) return 1000;

  if (tl2>0 && tl3>0 && tl4>0) {
    *uv = log10f(tl2/tl3)*-2.5;
    *vj = log10f(tl3/tl4)*-2.5;
  }

  return (log10f(total_l)*-2.5);
  //return ((log10f(total_l) + dust*log10f(total_l_dust/total_l))*-2.5);
}


void _luminosities_init(struct luminosities *lum, int64_t dust) {
  gen_exp10cache();
#ifdef USE_GSL
  int64_t i,j,k;
  for (i=0; i<LUM_TYPES; i++) {
    for (j=0; j<L_NUM_REDSHIFTS; j++) {
      for (k=0; k<L_NUM_ZS; k++) {
	lum_interps[dust][i][j][k] = 
	  gsl_interp_alloc(gsl_interp_akima, L_NUM_AGES);
	lum_accel[dust][i][j][k] = gsl_interp_accel_alloc();
	memset(lum_accel[dust][i][j][k], 0, sizeof(gsl_interp_accel));
	gsl_interp_init(lum_interps[dust][i][j][k], lum->ages, lum->lum[i][j][k], L_NUM_AGES);
      }
    }
  }
#endif /*USE_GSL*/
  min_Z = tlum[dust].Zs[0];
  max_Z = tlum[dust].Zs[L_NUM_ZS-1];
}

//From Maiolino et al. 2008: http://arxiv.org/abs/0806.2410v2
float metallicity(float sm, float z) {
  float M0 = 11.219+0.466*z;
  float K0 = 9.07-0.0701*z;
  if (sm > M0) sm = M0;
  sm -= M0;
  float Z = -0.0864*(sm*sm) + K0; //12 + log(O/H)
  /*double sm95 = 9.5 - M0;
  if (sm < sm95) {
    //Z = -0.0864*(sm95*sm95) + K0 + 0.3*(sm-sm95); //Kirby+ 2013: http://arxiv.org/abs/1310.0814
    }*/
  float Zsol = 8.69; //Asplund+ 2009
  Z -= Zsol;
  if (Z < METALLICITY_LOWER_LIMIT) return METALLICITY_LOWER_LIMIT;
  return Z;
}

float *Z_sm_limits = NULL;
int64_t num_z_sm_limits = 0;

float solve_for_metallicity_limit(float z) {
  float sm_min = 0;
  float sm_max = 13;
  while (sm_max - sm_min > 0.0001) {
    float sm = 0.5*(sm_min+sm_max);
    if (metallicity(sm, z) <= METALLICITY_LOWER_LIMIT+0.001) sm_min = sm;
    else sm_max = sm;
  }
  return pow(10, sm_min);
}

void alloc_metallicity_cache(float *scales, int64_t n) {
  if (n >= num_z_sm_limits) {
    check_realloc_s(Z_sm_limits, sizeof(float), (n+1));
    for (; num_z_sm_limits < (n+1); num_z_sm_limits++) {
      Z_sm_limits[num_z_sm_limits]=solve_for_metallicity_limit(1.0/scales[num_z_sm_limits]-1.0);
    }
  }  
}

float metallicity_cache(float sm, float z, int64_t n) {
  if (sm <= Z_sm_limits[n]) return METALLICITY_LOWER_LIMIT;
  return metallicity(log10fc(sm), z);
}



double metallicity_iron(float sm, float z) {
  return (-0.09 + metallicity(sm, z)*1.18);
}

//From Mannucci et al. 2010: http://arxiv.org/pdf/1005.0006v3.pdf
double metallicity_mannucci(float sm, float sfr) {
  sm -= 10.0;
  double sfr_thresh = (-0.14+0.12*sm)/(2.0*0.054);
  if (sfr < sfr_thresh) sfr = sfr_thresh;
  double Z = 8.90 + 0.37*sm - 0.14*sfr - 0.19*sm*sm
    +0.12*sm*sfr - 0.054*sfr*sfr;
  double Zsol = 8.69; //Asplund+ 2009
  return Z-Zsol;
}

double metallicity_moustakas(float sm, float z) {
  double z0 = 9.115;
  double m0 = 9.0+2.043;
  double gamma = 1.41;
  double Z_01 = z0 - log10(1+pow(10, gamma*(sm-m0)));
  double ddz = -0.16 + 0.058*(sm-10.5);
  double Z = Z_01 + ddz*(z-0.1);
  double Zsol = 8.69;
  return Z-Zsol;
}


#ifdef USE_GSL
double __lum_at_age_Z(struct luminosities *lum, enum lum_types t, float age1, float age2, int64_t Z_bin, int64_t redshift_bin, int64_t dust) {
  if (age1==age2) return gsl_interp_eval(lum_interps[dust][t][redshift_bin][Z_bin], lum->ages, lum->lum[t][redshift_bin][Z_bin], age1, lum_accel[dust][t][redshift_bin][Z_bin]);
  return (gsl_interp_eval_integ(lum_interps[dust][t][redshift_bin][Z_bin], lum->ages, lum->lum[t][redshift_bin][Z_bin], age1, age2, lum_accel[dust][t][redshift_bin][Z_bin])/(age2-age1));
}


double _lum_at_age_Z(struct luminosities *lum, enum lum_types t, float age1, float age2, int64_t Z_bin, float z_src, int64_t dust) {
  double f = z_src * L_NUM_BINS_PER_REDSHIFT;
  int64_t rbin = f;
  if (f <= 0) return __lum_at_age_Z(lum, t, age1, age2, Z_bin, 0, dust);
  if (rbin >= L_NUM_REDSHIFTS-1) 
    return __lum_at_age_Z(lum, t, age1, age2, Z_bin, L_NUM_REDSHIFTS-1, dust);
  f -= rbin;
  double l1 = __lum_at_age_Z(lum, t, age1, age2, Z_bin, rbin, dust);
  double l2 = __lum_at_age_Z(lum, t, age1, age2, Z_bin, rbin+1, dust);
  return (l1 + f*(l2-l1));
}

double lum_at_age_Z(struct luminosities *lum, enum lum_types t, float age1, float age2, float Z, float z_src, int64_t dust) {
  if (age1 <= lum->ages[0]) age1 = lum->ages[0];
  if (age1 < MIN_AGE) age1 = MIN_AGE;
  if (age2 <= age1) age2 = age1;
  if (age2 > lum->ages[L_NUM_AGES-1]) age2 = lum->ages[L_NUM_AGES-1];
  if (age2 <= age1) age1 = age2;
  int64_t Z_bin = Z/0.1+20;
  if (Z_bin < 0) Z_bin=0;
  if (Z_bin >= L_NUM_ZS) Z_bin = L_NUM_ZS - 1;
  while (Z_bin < L_NUM_ZS-1 && lum->Zs[Z_bin] < Z) Z_bin++;
  while (Z_bin > 0 && lum->Zs[Z_bin] > Z) Z_bin--;
  
  if ((lum->Zs[Z_bin] > Z) || (Z_bin==L_NUM_ZS-1)) 
    return _lum_at_age_Z(lum, t, age1, age2, Z_bin, z_src, dust);
  
  double f = (Z-lum->Zs[Z_bin])/(lum->Zs[Z_bin+1]-lum->Zs[Z_bin]);
  double l1 = _lum_at_age_Z(lum, t, age1, age2, Z_bin, z_src, dust);
  double l2 = _lum_at_age_Z(lum, t, age1, age2, Z_bin+1, z_src, dust);
  return (l1 + f*(l2-l1));
}

#else /*No GSL */
void __find_age(struct luminosities *lum, double age, int64_t *n, double *f) {
  int64_t i;
  for (i=L_NUM_AGES-2; i>0; i--)
    if (age > lum->ages[i]) break;
  *n=i;
  *f = 1;
  if (age < lum->ages[i]) *f=0;
  else if (age < lum->ages[i+1])
    *f = (age-lum->ages[i])/(lum->ages[i+1]-lum->ages[i]);
}


double ___lum_at_age_Z(struct luminosities *lum, enum lum_types t, int64_t n, double f, int64_t Z_bin, int64_t redshift_bin, int64_t dust) {
  double v1 = lum->lum[t][redshift_bin][Z_bin][n];
  double v2 = lum->lum[t][redshift_bin][Z_bin][n+1];
  //return (v1 + f*(v2-v1));
  return v1*pow(v2/v1, f);
}

double __lum_at_age_Z(struct luminosities *lum, enum lum_types t, int64_t n1, double f1, int64_t n2, double f2, int64_t Z_bin, int64_t redshift_bin, int64_t dust) {
  if (n1==n2) return ___lum_at_age_Z(lum, t, n1, 0.5*(f1+f2), Z_bin, redshift_bin, dust);

  double total_lum = 0;
  double age1 = lum->ages[n1]+f1*(lum->ages[n1+1]-lum->ages[n1]);
  double age2 = lum->ages[n2]+f2*(lum->ages[n2+1]-lum->ages[n2]);
  total_lum += (lum->ages[n1+1]-age1) * ___lum_at_age_Z(lum, t, n1, 0.5*(f1+1.0), Z_bin, redshift_bin, dust);
  for (int64_t i=n1+1; i<n2; i++)
    total_lum += (lum->ages[i+1]-lum->ages[i])*___lum_at_age_Z(lum, t, i, 0.5, Z_bin, redshift_bin, dust);
  total_lum += (age2-lum->ages[n2]) * ___lum_at_age_Z(lum, t, n2, 0.5*f2, Z_bin, redshift_bin, dust);
  return total_lum/(age2-age1);
}


double _lum_at_age_Z(struct luminosities *lum, enum lum_types t, int64_t n1, double f1, int64_t n2, double f2, int64_t Z_bin, float z_src, int64_t dust) {
  double f = z_src * 20;
  int64_t rbin = f;
  if (f <= 0) return __lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, 0, dust);
  if (rbin >= L_NUM_REDSHIFTS-1) 
    return __lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, L_NUM_REDSHIFTS-1, dust);
  f -= rbin;
  double l1 = __lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, rbin, dust);
  double l2 = __lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, rbin+1, dust);
  return (l1 + f*(l2-l1));
}


double lum_at_age_Z(struct luminosities *lum, enum lum_types t, float age1, float age2, float Z, float z_src, int64_t dust) {
  if (age1 > age2) {
    float tmp = age2;
    age2 = age1;
    age1 = tmp;
  }
  if (age1 <= lum->ages[0]) age1 = lum->ages[0];
  if (age1 < MIN_AGE) age1 = MIN_AGE;
  if (age2 <= age1) age2 = age1;
  if (age2 > lum->ages[L_NUM_AGES-1]) age2 = lum->ages[L_NUM_AGES-1];
  if (age2 <= age1) age1 = age2;
  int64_t Z_bin = Z/0.1+20;
  if (Z_bin < 0) Z_bin=0;
  if (Z_bin >= L_NUM_ZS) Z_bin = L_NUM_ZS - 1;
  while (Z_bin < L_NUM_ZS-1 && lum->Zs[Z_bin] < Z) Z_bin++;
  while (Z_bin > 0 && lum->Zs[Z_bin] > Z) Z_bin--;
  int64_t n1, n2;
  double f1, f2;
  __find_age(lum, age1, &n1, &f1);
  __find_age(lum, age2, &n2, &f2);
  
  if ((lum->Zs[Z_bin] > Z) || (Z_bin==L_NUM_ZS-1)) 
    return _lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, z_src, dust);
  
  double f = (Z-lum->Zs[Z_bin])/(lum->Zs[Z_bin+1]-lum->Zs[Z_bin]);
  double l1 = _lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin, z_src, dust);
  double l2 = _lum_at_age_Z(lum, t, n1, f1, n2, f2, Z_bin+1, z_src, dust);
  return (l1 + f*(l2-l1));
}

#endif /*No GSL */




void load_luminosities(char *directory) {
  char buffer[1024];

  //Default
#ifdef CALZETTI_DUST
  char *names[DUST_TYPES] = {"mags_ab_zall_jwst_calzetti_dust0.dat", "mags_ab_zall_jwst_calzetti_dust0.5.dat"};
#else
  char *names[DUST_TYPES] = {"mags_ab_zall_jwst_nodust.dat", "mags_ab_zall_jwst.dat"};
#endif /*CALZETTI_DUST*/

  double age, redshift, Z, bands[LUM_TYPES];
  int64_t n, i;
  FILE *input, *output;
  SHORT_PARSETYPE;
  void *data[LUM_TYPES+3] = {&redshift, &age, &Z};
  enum parsetype types[LUM_TYPES+3] = {F64, F64, F64};
  for (n=0; n<LUM_TYPES; n++) {
    data[n+3] = bands+n;
    types[n+3] = F64;
  }
  assert(DUST_TYPES==2);
  check_realloc_s(tlum, sizeof(struct luminosities), DUST_TYPES);
  for (i=0; i<DUST_TYPES; i++) {
    memset(tlum+i, 0, sizeof(*tlum));
    sprintf(buffer, "%s/%s.bin", directory, names[i]);
    input = fopen(buffer, "r");
    if (input) {
      if (fread(tlum+i, sizeof(*tlum), 1, input)<1) {
	fclose(input);
	input = 0;
      } else {
	fclose(input);
      }
    }

    if (!input) {
      int64_t num_ages = 0;
      int64_t num_Zs = 0;
      output = check_fopen(buffer, "w");
      sprintf(buffer, "%s/%s", directory, names[i]);
      input = check_fopen(buffer, "r");
      for (n=0; n<L_NUM_REDSHIFTS; n++) tlum[i].redshifts[n] = n*0.05;

      while (fgets(buffer, 1024, input)) {
	if (buffer[0] == '#') continue;
	n = stringparse(buffer, data, types, 3+LUM_TYPES);
	if (n != 3+LUM_TYPES) {
	  fprintf(stderr, "Syntax error in luminosities input!\n%s", buffer);
	  exit(1);
	}
	int64_t Z_bin = 0;
	int64_t age_bin = 0;
	int64_t redshift_bin = 0;
	for (age_bin=0; age_bin<num_ages; age_bin++)
	  if (tlum[i].ages[age_bin] == age) break;
	if (age_bin==num_ages) {
	  assert(num_ages < L_NUM_AGES);
	  num_ages++;
	  tlum[i].ages[age_bin] = age;
	  if (age_bin) assert(tlum[i].ages[age_bin] > tlum[i].ages[age_bin-1]);
	}

	for (Z_bin=0; Z_bin<num_Zs; Z_bin++)
	  if (tlum[i].Zs[Z_bin] == Z) break;
	if (Z_bin==num_Zs) {
	  assert(num_Zs < L_NUM_ZS);
	  num_Zs++;
	  tlum[i].Zs[Z_bin] = Z;
	  if (Z_bin) assert(tlum[i].Zs[Z_bin] > tlum[i].Zs[Z_bin-1]);
	}
      
	redshift_bin = redshift*20+0.5; //Positive values only
	assert(fabs(redshift - tlum[i].redshifts[redshift_bin]) < 0.001);

	for (n=0; n<LUM_TYPES; n++)
	  tlum[i].lum[n][redshift_bin][Z_bin][age_bin] = pow(10, bands[n]/-2.5);
      }
      fclose(input);

      for (n=0; n<L_NUM_AGES; n++) tlum[i].ages[n] = pow(10, tlum[i].ages[n]);

      check_fwrite(tlum+i, sizeof(*tlum), 1, output);
      fclose(output);
    }
    _luminosities_init(tlum+i, i);
  }

  /*printf("UVJ Lum at Z=0.2, Age=9: %f %f %f\n", -2.5*log10f(lum_at_age_Z(tlum, L_Jn_U, 0.99e9, 1.01e9, 0.2, 0, 0)),
	 -2.5*log10f(lum_at_age_Z(tlum, L_Jn_V, 0.99e9, 1.01e9, 0.2, 0, 0)),
	 -2.5*log10f(lum_at_age_Z(tlum, L_2mass_j, 0.99e9, 1.01e9, 0.2, 0, 0)));
  printf("UVJ Lum at Z=0.2, Age=10: %f %f %f\n",  -2.5*log10f(lum_at_age_Z(tlum, L_Jn_U, 0.99e10, 1.01e10, 0.2, 0, 0)),
	 -2.5*log10f(lum_at_age_Z(tlum, L_Jn_V, 0.99e10, 1.01e10, 0.2, 0, 0)),
	 -2.5*log10f(lum_at_age_Z(tlum, L_2mass_j, 0.99e10, 1.01e10, 0.2, 0, 0)));
	 exit(0);*/
}


void _free_luminosities(int64_t dust) {
#ifdef USE_GSL
  int64_t i,j,k;
  for (i=0; i<LUM_TYPES; i++) {
    for (j=0; j<L_NUM_REDSHIFTS; j++) {
      for (k=0; k<L_NUM_ZS; k++) {
	gsl_interp_accel_free(lum_accel[dust][i][j][k]);
	gsl_interp_free(lum_interps[dust][i][j][k]);
      }
    }
  }
#endif /* USE_GSL */
}

void free_luminosities(void) {
  int64_t i;
  for (i=0; i<DUST_TYPES; i++) _free_luminosities(i);
  check_realloc_s(tlum, 0, 0);
#ifdef __linux__
  malloc_trim(0);
#endif /* __linux__ */
}

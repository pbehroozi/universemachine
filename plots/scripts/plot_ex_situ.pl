#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/ex_situ";
our $plotdir = "$dir/graphs/ex_situ";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0, 1, 2);
my @scales = get_scales_at_redshifts("$datadir", "ex_", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

my $fn = sprintf("ex_situ_sm");
$fn =~ tr/./_/;
new XmGrace(xlabel => $sm_label,
	    ylabel => "Average Fraction of M\\s*\\N from Mergers",
	    XmGrace::logscale('x', 1e9, 1e12),
	    XmGrace::logscale('y', 0.01, 1),
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    legendxy => "0.3, 0.8",
	    data => [(map {{data => "file:$datadir/ex_situ_a$scales[$_].dat", metatype => "errorstrip", transform => XmGrace::tolin(0), smooth => 1}} (0..2)),
		     (map {{data => "file:$datadir/ex_situ_a$scales[$_].dat", legend => "z = $zs[$_]", transform => XmGrace::tolin(0), smooth => 1}} (0..2)),
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

sub limit_hm {
    my $z = shift;
    return sub {
	my $mass_limit = 15.7 - $z;
	my @a = split(" ", $_[0]);
	return "" unless $a[0] < $mass_limit;
	$a[0] = 10**$a[0];
	return "@a";
    }
}

$fn = sprintf("ex_situ_hm");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => "Average Fraction of M\\s*\\N from Mergers",
	    XmGrace::logscale('x', 1e11, 1e15),
	    XmGrace::logscale('y', 0.01, 1),
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    legendxy => "0.3, 0.8",
	    data => [(map {{data => "file:$datadir/ex_situ_hm_a$scales[$_].dat", metatype => "errorstrip", transform => limit_hm($_), smooth => 1}} (0..2)),
		     (map {{data => "file:$datadir/ex_situ_hm_a$scales[$_].dat", legend => "z = $zs[$_]", transform => limit_hm($_), smooth => 1}} (0..2)),
	    ])->write_pdf("$plotdir/AGR/$fn.agr");
	    

MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/ex_situ_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}

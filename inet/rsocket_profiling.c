void set_rsocket_role(char *role) { 
  if (rsocket_role) free(rsocket_role);
  rsocket_role = strdup(role);
}

#ifdef RSOCKET_PROFILING

#include <sys/stat.h>

int64_t rsocket_t0 = 0;
int64_t rsocket_last_write_t = 0;
int64_t rsocket_poll_nsec = 0, rsocket_poll_calls = 0;

void write_rsocket_timing(void) {
  char buffer[1024];
  int64_t i, j;
  if (!rsocket_last_write_t) mkdir("rprofiles/", 0755);
  rsocket_last_write_t = rsocket_t0;


  if (rsocket_role) {
    snprintf(buffer, 1024, "rprofiles/%s", rsocket_role);
    for (i=0; i<strlen(buffer); i++)
      if (buffer[i]==' ') buffer[i] = '_';
  }
  else {
    snprintf(buffer, 1024, "rprofiles/profile.%d", getpid());
  }
  
  FILE *out = fopen(buffer, "w");
  if (!out) return;
  if (rsocket_role) fprintf(out, "Role: %s\n", rsocket_role);
  fprintf(out, "Polling: %fs, %"PRId64" calls\n", (double)rsocket_poll_nsec / 1e9, rsocket_poll_calls);
  for (i=0; i<num_rsockets; i++) {
    if (rsockets[i].flags & UNUSED_FLAG) continue;
    if (!(rsockets[i].flags & SERVER_FLAG)) continue;
    int64_t k=0;
    for (j=0; j<num_rsockets; j++) {
      if (rsockets[j].flags & UNUSED_FLAG) continue;
      if (j==i) continue;
      if (rsockets[j].server_id == i) k++;
    }
    fprintf(out, "Server: %s:%s (%"PRId64" connections)\n", rsockets[i].address, rsockets[i].port, k);
  }
  for (i=0; i<num_rsockets; i++) {
    if (rsockets[i].flags & UNUSED_FLAG) continue;
    if (rsockets[i].flags & SERVER_FLAG) continue;
    fprintf(out, "Connection: %s:%s (snd: %fs, %f MB, %"PRId64" calls; rcv: %fs, %f MB, %"PRId64" calls; r1/r2: %fs / %fs)\n", rsockets[i].address, rsockets[i].port, (double)rsockets[i].send_nsec/1e9, (double)rsockets[i].send_bytes/(1024*1024), rsockets[i].send_calls, (double)rsockets[i].rcv_nsec/1e9, (double)rsockets[i].rcv_bytes/(1024*1024), rsockets[i].rcv_calls,  (double)rsockets[i].r1_nsec/1e9, (double)(rsockets[i].r2_nsec-rsockets[i].r1_nsec)/1e9);
  }
  fclose(out);
}

#ifdef __APPLE__
#include <mach/mach.h>
#include <mach/mach_time.h>
static int64_t rsocket_mach_absolute_time(void) {
  return mach_absolute_time();
}
#else
#include <time.h>
static int64_t rsocket_mach_absolute_time(void) {
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return t.tv_sec*1e9+t.tv_nsec;
}
#endif /*__APPLE__*/


#define _rsocket_timer_start { rsocket_t0 = rsocket_mach_absolute_time(); }
#define _rsocket_timer_inter(x) (((x) += (rsocket_mach_absolute_time() - rsocket_t0))) 
#define _rsocket_timer_stop(x,y,z,b) { (x)+=(rsocket_mach_absolute_time()-rsocket_t0); \
  (y)++;  (z)+=(b); \
  if (rsocket_t0 > 1e10+rsocket_last_write_t) write_rsocket_timing(); }

#else /* !RSOCKET_PROFILING */

#define _rsocket_timer_start
#define _rsocket_timer_inter(x)
#define _rsocket_timer_stop(x,y,z,b)

#endif /* RSOCKET_PROFILING */

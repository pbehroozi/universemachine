#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "make_sf_catalog.h"
#include "inet/rsocket.h"
#include "inet/socket.h"
#include "inet/address.h"
#include "config_vars.h"
#include "config.h"
#include "check_syscalls.h"
#include "server.h"
#include "client.h"
#include "config.h"
#include "bounds.h"
#include "box_server.h"
#include "mt_rand.h"
#include "observations.h"
#include "distance.h"
#include "universe_time.h"
#include "all_luminosities.h"
#include "watchdog.h"
#include "perftimer.h"
#include "inline_analysis.h"
#include "version.h"

struct client_info *clients = NULL;
time_t time_start = 0;
int64_t server_type = LAUNCHER_TYPE;
int64_t c, s;
FILE *logfile = NULL;
FILE *datafile = NULL;
FILE *best_chi2_file = NULL;
double best_chi2 = -1;

struct observation *obsdata = NULL;
int64_t num_obs = 0;

int main(int argc, char **argv) {
  int64_t magic, type, *blocknums=NULL;
  char *hostname=NULL, *port=NULL;
  int64_t i, j;
  char buffer[1024];

  if (argc < 2) {
    fprintf(stderr, "%s", INFO_STRING);
    fprintf(stderr, "Usage: %s um.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  setup_config();
  watchdog_start("box_server");
  init_cosmology(Om, Ol, h0);
  init_time_table(Om, h0);
  load_luminosities(SPS_DIR);
  load_offsets_and_scales(0);
  gen_and_free_luminosities();
  set_rsocket_role("Box Server");
  
  //Connect to Umachine server
  srand(time(NULL));
  random_sleep(0);
  c = connect_to_addr(SERVER_ADDRESS, SERVER_PORT);
  recv_from_socket(c, &magic, sizeof(uint64_t));
  if (magic != UM_MAGIC) {
    fprintf(stderr, "[Error] Received invalid server response.  Check network connectivity.\n");
    exit(EXIT_FAILURE);
  }

  //Receive server type
  send_to_socket_noconfirm(c, &magic, sizeof(uint64_t));
  recv_from_socket(c, &type, sizeof(int64_t));
  if (type == QUIT_TYPE) return(0);

  hostname = recv_msg_nolength(c, hostname);

  //Start box server 
  if (type == BOX_SERVER_TYPE) {
    s=-1;
    check_realloc_s(port, 10, sizeof(char));
    for (i=0; i<10000 && s<0; i+=29) {
      int64_t portnum = BOX_SERVER_PORT+i;
      snprintf(port, 10, "%"PRId64, portnum);
      s = listen_at_addr(hostname, port);
    }
    if (i>=10000) {
      fprintf(stderr, "[Error] Couldn't start particle data server at %s:%d-%d!\n",
	      hostname, (int)BOX_SERVER_PORT, (int)(BOX_SERVER_PORT+i));
      exit(EXIT_FAILURE);
    }
    send_to_socket_noconfirm(c, port, strlen(port)+1);
  }

  recv_from_socket(c, &box_id, sizeof(int64_t));
  r250_init(87L);
  sprintf(buffer, "Box Server %"PRId64, box_id);
  set_rsocket_role(buffer);
  set_timer_role("Box Server", box_id, 0);

  //Receive address of box server
  port = recv_msg_nolength(c, port);
  blocknums = recv_and_alloc(c, blocknums, sizeof(int64_t)*BLOCKS_PER_NODE);
  if (type != BOX_SERVER_TYPE) close_rsocket(c);
  

  //Fork catalog clients
  for (i=0; i<BLOCKS_PER_NODE; i++) {
    if (blocknums[i] < 0) continue;
    int64_t pid = fork();
    if (pid < 0) system_error("Failed to fork process.");
    if (pid == 0) sf_catalog_client(hostname, port, blocknums[i]);
  }

  if (type != BOX_SERVER_TYPE) {
    wait(NULL); //Otherwise some clusters will kill all forked processes.
    exit(EXIT_SUCCESS);
  }
  
  char *mode = (BURN_IN_STAGE < 0) ? "w" : "r+";
  snprintf(buffer, 1024, "%s/box_server.%"PRId64".samples", OUTBASE, box_id);
  datafile = check_create_fopen(buffer, mode);
  snprintf(buffer, 1024, "%s/box_server.%"PRId64".bestfit", OUTBASE, box_id);
  best_chi2_file = check_create_fopen(buffer, mode);

  //Wait for connections
  snprintf(buffer, 1024, "%s/box_server.%"PRId64".log", OUTBASE, box_id);
  logfile = check_fopen(buffer, "w");
  time_start = time(NULL);
  fprintf(logfile, "Box server %"PRId64" started at %s", box_id, ctime(&time_start));
  syscall_logfile = logfile;

  timed_output("Accepting connections...\n");
  check_realloc_s(clients, sizeof(struct client_info), NUM_BLOCKS);
  for (i=0; i<NUM_BLOCKS; i++) {
    int client_port;
    uint64_t magic = UM_MAGIC;
    char *address=NULL;
    int64_t cs = accept_connection(s, &address, &client_port);
    if (cs < 0) { i--; continue; }
    send_to_socket_noconfirm(cs, &magic, sizeof(uint64_t));
    recv_from_socket(cs, &magic, sizeof(uint64_t));
    if (magic != UM_MAGIC) {
      fprintf(stderr, "[Error] Received invalid client responses.  Check network connectivity.\n");
      exit(EXIT_FAILURE);
    }

    recv_from_socket(cs, &num_scales, sizeof(int64_t));
    check_realloc_s(scales, sizeof(float), num_scales);
    recv_from_socket(cs, scales, sizeof(float)*num_scales);
    clients[i].address = address;
    clients[i].port = client_port;
    clients[i].cs = cs;
    clients[i].serv_port = 0;
    recv_from_socket(cs, &(clients[i].chunk), sizeof(int64_t));
    send_to_socket_noconfirm(cs, address, strlen(address)+1);
    //timed_output("Accepted connection %"PRId64".\n", i);
  }

  qsort(clients, NUM_BLOCKS, sizeof(struct client_info), sort_by_address);
  for (i=0; i<NUM_BLOCKS; i++) clients[i].id = i;
  
  //Receive boundaries
  timed_output("Accepted %"PRId64" connections; %"PRId64" timesteps in box.\n", NUM_BLOCKS, num_scales);
  timed_output("Waiting for boundaries...\n");
  int64_t blocks_loaded = 0;
  while (blocks_loaded < NUM_BLOCKS) {
    clear_rsocket_tags();
    for (i=0; i<NUM_BLOCKS; i++)
      if (!clients[i].serv_port) tag_rsocket(clients[i].cs);
    select_rsocket(RSOCKET_READ, 0);
    for (i=0; i<NUM_BLOCKS; i++) {
      if (!check_rsocket_tag(clients[i].cs)) continue;
      recv_from_socket(clients[i].cs, &clients[i].serv_port, sizeof(int64_t));
      recv_from_socket(clients[i].cs, clients[i].max_bounds, sizeof(float)*6);
      clients[i].bounds = recv_and_alloc(clients[i].cs, NULL, sizeof(float)*6*num_scales);
      blocks_loaded++;
      timed_output("Loaded block %"PRId64" (%"PRId64"/%"PRId64").\n", clients[i].chunk, blocks_loaded, NUM_BLOCKS);
    }
  }

  timed_output("Sending client lists...\n");
  for (i=0; i<NUM_BLOCKS; i++) {
    int64_t num_c = 0;
    float bounds[6];
    for (j=0; j<NUM_BLOCKS; j++) 
      if (j!=i && bounds_overlap(clients[j].max_bounds, clients[i].max_bounds, bounds, CORR_LENGTH_RP))
	num_c++;
    send_to_socket_noconfirm(clients[i].cs, &clients[i].id, sizeof(int64_t));
    send_to_socket_noconfirm(clients[i].cs, &num_c, sizeof(int64_t));
    for (j=0; j<NUM_BLOCKS; j++) {
      if (j!=i && bounds_overlap(clients[j].max_bounds, clients[i].max_bounds, bounds, CORR_LENGTH_RP)) {
	send_to_socket_noconfirm(clients[i].cs, clients+j, sizeof(struct client_info));
	send_to_socket_noconfirm(clients[i].cs, clients[j].address, strlen(clients[j].address)+1);
	send_to_socket_noconfirm(clients[i].cs, clients[j].bounds, sizeof(float)*6*num_scales);
      }
    }
    char cmd[5] = {0};
    recv_from_socket(clients[i].cs, cmd, 4);
    assert(!strcmp(cmd, "thx!"));
  }

  //Clients should fork here, response should be from calculation
  timed_output("Waiting for clients to establish connections...\n");
  for (i=0; i<NUM_BLOCKS; i++) {
    char cmd[5] = {0};
    recv_from_socket(clients[i].cs, cmd, 4);
    assert(!strcmp(cmd, "rdy!"));
  }

  allocate_shared_memory(1);
  send_to_socket(c, "rdy!", 4);
  timed_output("Loading observations...\n");
  load_observations(&obsdata, &num_obs);
  timed_output("Ready to start.\n");
  if (BURN_IN_STAGE > -1) recover_outputs();
  box_server_loop();
  fclose(logfile);
  return 0;
}


void print_time(FILE *output) {
  int64_t time_now = time(NULL);
  fprintf(output, "[%6"PRId64"s] ", time_now-time_start);
}

int sort_by_address(const void *a, const void *b) {
  const struct client_info *c = a;
  const struct client_info *d = b;
  int res = strcmp(c->address, d->address);
  if (res) return res;
  if (c->serv_port < d->serv_port) return -1;
  if (c->serv_port > d->serv_port) return 1;
  return 0;
}

int64_t mcmc_data_length(void) {
  return(sizeof(struct sf_model_allz) + (sizeof(int64_t)*3) + (sizeof(float)*num_scales) + shared_data_length + num_obs*sizeof(struct observation));
}

void mcmc_data_to_file(struct sf_model_allz m, FILE *out) {
  fwrite(&m, sizeof(struct sf_model_allz), 1, out);
  fwrite(&num_scales, sizeof(int64_t), 1, out);
  fwrite(&shared_data_length, sizeof(int64_t), 1, out);
  fwrite(&num_obs, sizeof(int64_t), 1, out);
  fwrite(scales, sizeof(float), num_scales, out);
  fwrite(shared_data, 1, shared_data_length, out);
  fwrite(obsdata, 1, num_obs*sizeof(struct observation), out);
  fflush(out);
}

void save_pp_data(struct sf_model_allz m, int64_t models) {
  char buffer[1024];
  snprintf(buffer, 1024, "%s/box_server.%"PRId64".model.%"PRId64, OUTBASE, box_id, models);
  FILE *out = check_fopen(buffer, "w");
  mcmc_data_to_file(m, out);
  fclose(out);
}

void box_server_loop(void) {
  int64_t done = 0, i, j, blocks=0, models=0;
  struct sf_model_allz m;
  memset(&m, 0, sizeof(struct sf_model_allz));
  int64_t shared_data_elements = shared_data_length / sizeof(float);
  memset(shared_data, 0, shared_data_length);
  float *input_data = check_realloc(NULL, shared_data_length, "Input data");
  add_timers("Chunk", NUM_BLOCKS);
  timer_init(wait_timer, "Waiting");
  timer_init(chi2_timer, "Chi2");
  timer_init(pp_timer, "PostProcessing");
  timer_start;
  while (!done) {
    clear_rsocket_tags();
    tag_rsocket(c);
    for (i=0; i<NUM_BLOCKS; i++) tag_rsocket(clients[i].cs);
    int64_t max_conn = select_rsocket(RSOCKET_READ, 0);
    for (i=0; i<max_conn; i++) {
      if (!check_rsocket_tag(i)) continue;
      if (i==c) {
	recv_from_socket(c, &m, sizeof(struct sf_model_allz));
	timer_stop(wait_timer);
	timer_start;
	for (j=0; j<NUM_BLOCKS; j++)
	  send_to_socket(clients[j].cs, &m, sizeof(struct sf_model_allz));
	if (INVALID(m)) done = 1;
      }
      else {
	recv_from_socket(i, input_data, shared_data_length);
	for (j=0; j<NUM_BLOCKS; j++) if (clients[j].cs==i) break;
	timer_stop(clients[j].id);
	blocks++;
	if (POSTPROCESSING_MODE)
	  timed_output("Received postprocessed data from block %"PRId64" (%"PRId64"/%"PRId64")\n", clients[j].chunk, blocks, NUM_BLOCKS);
	for (j=0; j<shared_data_elements; j++) shared_data[j] += input_data[j];
	if (blocks == NUM_BLOCKS) {
	  models++;
	  timer_start;
	  double chi2 = calc_chi2(obsdata, num_obs, m);
	  timer_stop(chi2_timer);
	  timer_start;
	  if (POSTPROCESSING_MODE) {
	    inline_postprocessing();
	    timer_stop(pp_timer);
	    timer_start;
	  }
	  CHI2(m) = chi2;
	  //Save all samples.  Post-processing req'd to remove samples not
	  //accepted in the chain.
	  if (POSTPROCESSING_MODE) save_pp_data(m, models);
	  
	  if (dr250() < SAVE_OBS_FRACTION) mcmc_data_to_file(m, datafile);
	  if (chi2 < best_chi2 || best_chi2 < 0) {
	    best_chi2 = chi2;
	    rewind(best_chi2_file);
	    mcmc_data_to_file(m, best_chi2_file);
	    int64_t k;
	    for (k=0; k<NUM_PARAMS; k++)
	      fprintf(stderr, "%.12f ", m.params[k]);
	    fprintf(stderr, "0 %.12f\n", CHI2(m));
	  }
	  send_to_socket(c, &chi2, sizeof(double));
	  blocks = 0;
	  memset(shared_data, 0, shared_data_length);
	}
      }
    }
  }
}


void recover_outputs(void) {
  int64_t sample_size = mcmc_data_length();
  struct stat st = {0};
  fstat(fileno(datafile), &st);
  int64_t records = st.st_size / sample_size;
  check_fseeko(datafile, records*sample_size, 0);
  
  struct sf_model_allz m;
  memset(&m, 0, sizeof(struct sf_model_allz));
  fstat(fileno(best_chi2_file), &st);
  rewind(best_chi2_file);
  if (st.st_size > sizeof(struct sf_model_allz)) {
    check_fread(&m, sizeof(struct sf_model_allz), 1, best_chi2_file);
    if (CHI2(m) > 0) best_chi2 = CHI2(m);
  }
}

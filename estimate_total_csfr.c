#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "sf_model.h"
#include "mf_cache.h"
#include "io_helpers.h"

static inline double doexp10(double x) {
  double a = exp(M_LN10*x);
  return a;
}

double sfr_at_vmp(double lv, struct sf_model c) {
  double vd = lv - c.v_1;
  double vd2 = vd/c.delta;
  return (c.epsilon * (1.0/(doexp10(c.alpha*vd) + doexp10(c.beta*vd)) + c.gamma*exp(-0.5*vd2*vd2)));
}

double mass_to_vmax (double m, double z) {
  double a = 1.0/(1.0+z);
  double a200 = a / 0.3782;
  double m200 = log10((1.0/0.68)*1.115e12 / ((pow(a200, -0.142441) + pow(a200, -1.78959))));
  double v200 = log10(200);
  double exp_vp = (m - m200)/3.0 + v200;
  return exp_vp;
}

double rank(double zscore) {
  return (0.5+0.5*erf(zscore/sqrt(2)));
}

double sfr_integrate(double thresh, double sfr_med, double scatter) {
  double mult = -0.5*pow(10, sfr_med)*exp(0.5*pow(scatter*log(10), 2));
  return mult*(-1.0 - erf((scatter*scatter*log(10)+sfr_med-thresh)/(sqrt(2)*scatter)));
}

int main(int argc, char **argv) {
  int64_t i,j;
  if (argc < 3) {
    fprintf(stderr, "Usage: %s mf_planck.dat param_file\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  struct sf_model_allz sfm = {{0}};
  load_mf_cache(argv[1], 2);
  read_params_from_file(argv[2], sfm.params, NUM_PARAMS);
  
  printf("#Z Csfr_Tot Csfr_(SFR>0.3) Csfr_(Bolshoi_Planck)\n");
  for (i=1; i<120; i++) {
    double z = (double) i/(double)10.0;
    struct sf_model c = calc_sf_model(sfm, 1.0/(1.0+z));
    double csfr_tot = 0, csfr_17=0, csfr_bp = 0;
    double tot_sig = sqrt(c.sig_sf*c.sig_sf + c.sig_sf_uncorr*c.sig_sf_uncorr);
    double scatter_corr = exp(pow(tot_sig*log(10),2)/2.0);
    double lthresh = log10(0.3);
    for (j=800; j<1600; j++) {
      double m = (double)j/100.0;
      double vp = mass_to_vmax(m, z);
      double nd = pow(10, mf_cache(1.0/(1.0+z), m));
      double fq = c.fq_min + (1.0-c.fq_min)*rank((vp-c.q_lvmp)/c.q_sig_lvmp);
      double fsf = 1.0-fq;
      double sfr_med = sfr_at_vmp(vp, c);
      csfr_tot += sfr_med*fsf*nd*scatter_corr/100.0;
      if (m>10) csfr_bp += sfr_med*fsf*nd*scatter_corr/100.0;
      double lmed = log10(sfr_med);
      csfr_17 += fsf*nd*sfr_integrate(lthresh, lmed, tot_sig)/100.0;
      //printf("%f %g %g %g %g %g\n", z, m, vp, nd, fsf, sfr_med);
    }
    printf("%f %e %e %e\n", z-0.05, csfr_tot, csfr_17, csfr_bp);
  }
  return 0;
}

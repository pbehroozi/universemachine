#ifndef _REGIONS_H_
#define _REGIONS_H_

struct region {
  float lims[4];
};

void load_regions(char *filename, int64_t sr);
int64_t within_r(double ra, double dec);
void rand_ra_dec(double *ra, double *dec);
void rand_within_r(double *ra, double *dec);
double area_within_r(void);

#endif /* _REGIONS_H_ */

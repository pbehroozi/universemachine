#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../universe_time.h"
#include "../all_luminosities.h"
#include "../sampler.h"
#include "../mt_rand.h"

#define NUM_FILTERS 13
#define NUM_SCALES 50
#define NUM_WALKERS 128
#define TOTAL_STEPS 100000

float scales[NUM_SCALES+1] = {0};
float stimes[NUM_SCALES+1] = {0};
float sfh[NUM_SCALES+1] = {0};
float dt[NUM_SCALES+1] = {0};

struct lum_point {
  int filter;
  double mag, err_u, err_d;
};

struct lum_point *lps = NULL;
int64_t nlps = 0;


void gen_sfh(double total_sm, double plaw) {
  int64_t i;
  double tsm=0;
  for (i=0; i<NUM_SCALES+1; i++) {
    sfh[i] = pow(stimes[i]/stimes[NUM_SCALES], plaw)*dt[i];
    tsm += sfh[i];
  }
  if (tsm > 0) tsm = pow(10, total_sm)/tsm;
  for (i=0; i<NUM_SCALES+1; i++)
    sfh[i] *= tsm;
}


void lums_of_sfh(float **lum_lookup, float *sfh, float *scales, int64_t n, float dust, float *lums) {
  int64_t i, j;
  double total_sm = 0;
  double total_l[NUM_FILTERS] = {0};
  double total_l_dust[NUM_FILTERS] = {0};
  for (i=0; i<=n; i++) {
    double new_sm = sfh[i];
    //Use instantaneous recycling fraction of 30%
    total_sm += new_sm*0.7;
    if (total_sm < 1) total_sm = 1;
    float z = 1.0/scales[i]-1.0;
    float Z = metallicity(log10f(total_sm), z);
    //    if (Z<METALLICITY_LOWER_LIMIT) Z = METALLICITY_LOWER_LIMIT;
    Z = -1;
    for (j=0; j<NUM_FILTERS; j++) {
      float avg_lum_nodust = lookup_lum(lum_lookup[j], Z, n, i, 0);
      float avg_lum_dust = lookup_lum(lum_lookup[j], Z, n, i, 1);
      total_l[j] += avg_lum_nodust*new_sm;
      total_l_dust[j] += avg_lum_dust*new_sm;
    }
  }
  if (dust < 0) dust = 0;
  //  if (dust > 2) dust = 2;
  for (j=0; j<NUM_FILTERS; j++) {
    if (!(total_l[j] > 0 && total_l_dust[j] > 0)) lums[j] = 1000;
    lums[j] = ((log10f(total_l[j]) + dust*log10f(total_l_dust[j]/total_l[j]))*-2.5);
  }
}

int main(int argc, char **argv) {
  enum lum_types lt[NUM_FILTERS] = {L_m1500, L_f435w, L_f606w, L_f775w, L_f814w, L_f850lp, L_f098m, L_f105w, L_f125w, L_f140w, L_f160w, L_irac1, L_irac2};
  if (argc < 5) {
    fprintf(stderr, "Usage: %s z total_sm plaw dust max_age/Gyr\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t i;
  double z = atof(argv[1]);
  double max_age = atof(argv[5])*1e9;

  init_time_table(0.307, 0.6777);
  scales[NUM_SCALES] = 1.0/(1.0+z);
  double t0 = scale_to_years(scales[NUM_SCALES])-scale_to_years(0);
  stimes[NUM_SCALES] = t0;
  if (max_age >= t0) scales[0] = 0;
  else scales[0] = years_to_scale(scale_to_years(scales[NUM_SCALES]) - max_age);
  stimes[0] = scale_to_years(scales[0]) - scale_to_years(0);
  stimes[NUM_SCALES] = t0;
  for (i=1; i<NUM_SCALES; i++) {
    double dtmin = 1e6;
    double dtmax = t0 - stimes[0];
    double t = scale_to_years(scales[NUM_SCALES]) - pow(dtmin/dtmax, (double)(i)/(double)(NUM_SCALES-1))*dtmax;
    scales[i] = years_to_scale(t);
    stimes[i] = scale_to_years(scales[i]) - scale_to_years(0);
  }

  for (i=0; i<NUM_SCALES+1; i++) {
    float a1 = (i>0) ? 0.5*(scales[i]+scales[i-1]) : scales[0];
    float a2 = (i<NUM_SCALES) ? 0.5*(scales[i]+scales[i+1]) : scales[i];
    dt[i] = scale_to_years(a2) - scale_to_years(a1);
  }


  float *lum_tables[NUM_FILTERS] = {0};
  load_luminosities("../fsps");
  for (i=0; i<NUM_FILTERS; i++)
    gen_lum_lookup(&(lum_tables[i]), scales, NUM_SCALES, lt[i], z);

  gen_sfh(atof(argv[2]), atof(argv[3]));
  float lums[NUM_FILTERS] = {0};

  lums_of_sfh(lum_tables, sfh, scales, NUM_SCALES, atof(argv[4]), lums);
  for (i=0; i<NUM_FILTERS; i++)
    printf("%s %f\n", lum_names[lt[i]], lums[i]);
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <assert.h>
#include <unistd.h>
#include "config.h"
#include "config_vars.h"
#include "make_sf_catalog.h"
#include "universe_time.h"
#include "smloss.h"
#include "check_syscalls.h"
#include "mt_rand.h"
#include "universal_constants.h"
#include "inet/socket.h"
#include "inet/rsocket.h"
#include "inet/pload.h"
#include "box_server.h"
#include "comm_lib.h"
#include "corr_lib.h"
#include "all_luminosities.h"
#include "io_helpers.h"
#include "inthash.h"
#include "perftimer.h"
#include "vweights.h"
#include "distance.h"
#include "watchdog.h"
#include "expcache.h"
#include "version.h"
#include "smoothing.h"
#include "muzzin_uvj.h"
#include "bitarray.h"

struct sf_model_allz the_model = {{0}};
struct sf_model *sf_models = NULL;
struct catalog_halo *halos=NULL;
struct catalog_galaxy *galaxies=NULL;

struct extra_halo_info *einfo=NULL; //Not contained in input data catalogs
struct extra_halo_tags *ehtags=NULL;
struct wl_data **wl_data=NULL;


int64_t *offsets=NULL, *num_halos=NULL, num_scales=0, max_halos = 0;
int64_t *galaxy_offsets=NULL, *descs=NULL;
char *desc_bitmask = NULL;
float *scales = NULL;
float *bounds = NULL;
float *max_bounds = NULL;
int64_t catalog_client = 0;
int64_t loaded_chunk = 0;
int64_t comm_cs = -1;
float box_center[3] = {0};
float **ll_m1500 = NULL;
float **ll_u = NULL;
float **ll_v = NULL;
float **ll_j = NULL;

float *dt = NULL, *dt2 = NULL, *rem = NULL;
float *sfh_old = NULL, *sfh_new = NULL, *icl_old = NULL, *icl_new = NULL;
float *rand_table = NULL;
int64_t shared_data_length = 0;
float *shared_data = NULL;
#define shared(a,b) float *a = NULL;
#include "shared.template.h"
#include "pshared.template.h"
#undef shared
float ssfr_thresh[SM_NBINS] = {0};
float sm_min=0, sm_max=0, hz=0, uv_sm_min=0;
int64_t uv_sm_zbin = 0;

float velocity_error = 0;
float log_quenched_ssfr = 0;

int64_t box_id = 0;
char *box_server_address = NULL, *box_server_port = NULL;

int64_t uv_count = 0;

struct extra_data *extra_data = NULL;

float smooth_smf[SM_SMOOTH_NBINS], smooth_smf_q[SM_SMOOTH_NBINS], smooth_smf_q2[SM_SMOOTH_NBINS], smooth_smf_q3[SM_SMOOTH_NBINS], smooth_ssfr[SM_SMOOTH_NBINS], smooth_uvlf[UV_SMOOTH_NBINS];
float smooth_uvsm[UVSM_REDSHIFTS*UVSM_UV_BINS*UVSM_SM_BINS], smooth_ir_excess[UV_SMOOTH_NBINS];

#ifndef NO_CATALOG_MAIN

#define random_sleep(x) 

int main(int argc, char **argv) {
  char buffer[2048];
  struct sf_model_allz model;
  if (argc<3) {
    printf("%s", INFO_STRING);
    printf("Usage: %s config.txt chunk_num output_sfh? output_shared_data?\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  r250_init(87L);
  do_config(argv[1]);
  setup_config();
  gen_erfcache();
  gen_uvj_lookups();
  init_time_table(Om, h0);
  init_cosmology(Om, Ol, h0);
  load_luminosities(SPS_DIR);

  loaded_chunk = atol(argv[2]);
  load_box(loaded_chunk);
  add_timers("Snap", num_scales);
  translate_descids();
  calc_extra_info();
  calc_volume_cache();
  set_timer_role("Catalog Maker", 0, loaded_chunk);

  if (argc > 3) SFH_OUTPUT_MODE = atol(argv[3]);
  if (argc > 4) SHARED_DATA_OUTPUT_MODE = atol(argv[4]);
  
  while(fgets(buffer, 2048, stdin)) {
    read_params(buffer, model.params, NUM_PARAMS);
    fprintf(stderr, "Calculating model: %s\n", buffer);
    model.params[NUM_PARAMS]=0;
    int64_t result = test_invalid(model);
    if (result > 0) fprintf(stderr, "[Warning] Invalid model; reason code %"PRId64"\n", result);
    calculate_model(model);
  }
  return 0;
}
#endif /*undef SF_CATALOG_LIB */


static inline double doexp10(double x) {
  double a = exp(M_LN10*x);
  return a;
}

static inline float doexp10f(float x) {
  return expf(M_LN10*x);
}


static inline double dolog10(double x) {
  if (x <= 0) return -1000;
  double a = log10(x);
  return a;
}



void calculate_model(struct sf_model_allz model) {
  int64_t n;
  the_model = model;
  clear_observations();
  for (n=0; n<num_scales; n++) {
    timer_start;
    //km/s/Comoving Mpc/h
    hz = 100.0*sqrt(Om*pow(scales[n], -3.0)+Ol)*scales[n];
    velocity_error = (scales[n] < CORR_HIGHZ_SCALE) ? REDSHIFT_ERROR_HIGHZ : REDSHIFT_ERROR;
    if (velocity_error < 1) velocity_error *= SPEED_OF_LIGHT/scales[n]; //note that 1/a = (1+z)
    calc_ssfr_threshes(n);
    calc_sf_model_catalog(model, n);
    calc_sm_histories(n);
    smooth_nds(n);
    if (!catalog_client || CATALOG_OUTPUT_MODE || POSTPROCESSING_MODE) write_catalog(n);  //For testing only
    if (n<num_scales-1) galaxy_offsets[n+2] = galaxy_offsets[n+1];
#ifdef SF_CATALOG_LIB
    if (catalog_client && ((scales[n] > CORR_MIN_SCALE) ||
			   (POSTPROCESSING_MODE && (scales[n] > POSTPROCESS_CORR_MIN_SCALE))))
      send_to_socket_noconfirm(comm_cs, &n, sizeof(int64_t));
#endif /*SF_CATALOG_LIB */
    timer_stop(n);
  }
  //printf("UV Count: %.1f M / %.1f M\n", uv_count/1e6, offsets[num_scales]/1e6);
  uv_count=0;
}


//SSFR Threshold from Moustakas et al. 2013; http://lanl.arxiv.org/abs/1301.1688
void calc_ssfr_threshes(int64_t n) {
  double c = moustakas_translate_const(scales[n]);
  int64_t i;
  for (i=0; i<SM_NBINS; i++) {
    double lsm = SM_MIN + (i+0.5)/(double)SM_BPDEX;
    double lsfr = c + 0.65*(lsm - 10.0);
    ssfr_thresh[i] = doexp10(lsfr - lsm);
  }
}



#ifdef SF_CATALOG_LIB
void sf_catalog_client(char *hostname, char *port, int64_t chunk_num)
{
  struct sf_model_allz model;
  catalog_client = 1 + chunk_num;
  char buffer[100];

  //Init
  srand(87L+chunk_num);
  r250_init(87L);
  gen_erfcache();
  gen_uvj_lookups();
  init_time_table(Om, h0);
  init_cosmology(Om, Ol, h0);
  box_server_address = check_strdup(hostname);
  box_server_port = check_strdup(port);
  sprintf(buffer, "Catalog Maker.%"PRId64".%"PRId64, box_id, chunk_num);
  set_rsocket_role(buffer);
  set_timer_role("Catalog Maker", box_id, chunk_num);
  
  num_scales = 0;
  load_box(chunk_num); //Includes forking
  translate_descids();
  calc_extra_info();
  send_to_socket(comm_cs, "rdy!", 4);
  calc_volume_cache();

  add_timers("Snap", num_scales);
  timer_init(wait_timer, "Waiting");
  while (1) {
    recv_from_socket(comm_cs, &model, sizeof(struct sf_model_allz));
    if (timer_t0) timer_stop(wait_timer);
    if (INVALID(model)) {
      int64_t step = -1;
      send_to_socket_noconfirm(comm_cs, &step, sizeof(int64_t));
      exit(EXIT_SUCCESS);
    }
    calculate_model(model);
    timer_start;
  }
}
#endif /*def SF_CATALOG_LIB*/


void allocate_shared_memory(int64_t server) {
  int64_t offset=0;
  if (!server) {
    galaxy_offsets = check_mmap_memory(sizeof(int64_t)*(num_scales+1));
    galaxies = check_mmap_memory(sizeof(struct catalog_galaxy)*offsets[num_scales]);
    bounds = check_mmap_memory(6*sizeof(float)*(num_scales+1));
    max_bounds = bounds + 6*(num_scales);
  }

  shared_data_length = 0;
#define shared(a,b) { if (b<=0) { fprintf(stderr, "Size of shared element %s is <=0 (%"PRId64")\n", #a, (int64_t)(b)); exit(EXIT_FAILURE); } \
    shared_data_length += (b)*sizeof(float); }
#include "shared.template.h"
  if (POSTPROCESSING_MODE) {
#include "pshared.template.h"
  }
#undef shared
  shared_data = check_mmap_memory(shared_data_length);
#define shared(a,b) { a = shared_data + offset; offset += (b); }
#include "shared.template.h"
  if (POSTPROCESSING_MODE) {
#include "pshared.template.h"
}
#undef shared
  sm_min = doexp10(SM_MIN);
  sm_max = doexp10(SM_MAX);
}


void allocate_nonshared_memory(void) {
  int64_t n,i;
  //Calc DT's + SM remaining
  check_realloc_s(dt, sizeof(float), num_scales);
  check_realloc_s(dt2, sizeof(float), num_scales);
  check_realloc_s(rem, sizeof(float), num_scales*num_scales);
  check_realloc_s(sf_models, sizeof(struct sf_model), num_scales);
  for (n=0; n<num_scales; n++) {
    float a1 = (n>0) ? 0.5*(scales[n]+scales[n-1]) : 0;
    float a2 = (n<num_scales-1) ? 0.5*(scales[n]+scales[n+1]) : scales[n];
    dt[n] = scale_to_years(a2) - scale_to_years(a1);
    dt2[n] = scale_to_years(scales[n]) - scale_to_years(a1);
    for (i=0; i<num_scales; i++) {
      float max_scale = a2;
      if (max_scale > scales[i]) max_scale = scales[i];
      rem[i*num_scales+n] = (i<n) ? 0.0 : calc_sm_loss_int(a1, max_scale, scales[i]);
    }
  }

  //Allocate SFH, ICL
  int64_t max_sfh_length = 0;
  check_realloc_s(num_halos, sizeof(int64_t), num_scales);
  for (n=0; n<num_scales; n++) {
    num_halos[n] = offsets[n+1]-offsets[n];
    if (max_halos < num_halos[n]) max_halos = num_halos[n];
    if (num_halos[n]*(n+1) > max_sfh_length)
      max_sfh_length = num_halos[n]*(n+1);
  }

  check_realloc_s(sfh_new, sizeof(float), max_sfh_length);
  check_realloc_s(sfh_old, sizeof(float), max_sfh_length);
  if (CALC_ICL) {
    check_realloc_s(icl_new, sizeof(float), max_sfh_length);
    check_realloc_s(icl_old, sizeof(float), max_sfh_length);
  }
    
  check_realloc_s(rand_table, sizeof(float), max_halos*HALO_NUM_RANDS);
  for (i=0; i<max_halos*HALO_NUM_RANDS; i++)
    rand_table[i] = normal_random(0,1);
  check_realloc_s(descs, sizeof(int64_t), max_halos*sizeof(int64_t));
  desc_bitmask = BIT_ALLOC(max_halos);
}

void calculate_boundaries(void) {
  int64_t n, i, j, c=0;
  memset(box_center, 0, sizeof(float)*3);
  memset(max_bounds, 0, sizeof(float)*6);
  //Compute box center at z=0 (from forests only)
  n = num_scales - 1;
  for (i=offsets[n]; i<offsets[n+1]; i++) {
    if (!(halos[i].flags & FOREST_FLAG)) continue;
    for (j=0; j<3; j++) box_center[j] += halos[i].pos[j];
    c++;
  }
  assert(c>0);
  for (j=0; j<3; j++) box_center[j] /= (double)c;
  
  //Handle wrapping
  for (n=num_scales-1; n>=0; n--) {
    float *b = bounds + n*6;
    for (i=offsets[n]; i<offsets[n+1]; i++) {
      for (j=0; j<3; j++) {
	if (box_center[j] - halos[i].pos[j] > BOX_SIZE/1.5) halos[i].pos[j] += BOX_SIZE;
	else if (halos[i].pos[j] - box_center[j] > BOX_SIZE/1.5) halos[i].pos[j] -= BOX_SIZE;
      }
      if (i>offsets[n]) {
	for (j=0; j<3; j++) {
	  if (halos[i].pos[j] < b[j]) b[j] = halos[i].pos[j];
	  if (halos[i].pos[j] > b[j+3]) b[j+3] = halos[i].pos[j];
	}
      } else {
	for (j=0; j<6; j++) b[j] = halos[i].pos[j%3];
      }
    }

    for (j=0; j<3; j++) {
      if ((b[j] < max_bounds[j]) || (n==(num_scales-1))) 
	max_bounds[j] = b[j];
      if ((b[j+3] > max_bounds[j+3]) || (n==(num_scales-1))) 
	max_bounds[j+3] = b[j+3];
    }
  }
}

#ifdef SF_CATALOG_LIB
void fork_comm_client(int64_t chunk_num) {
  int sv[2];
  if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv) == -1)
    system_error("Unable to create socket pair.");

  int64_t pid = fork();
  if (pid < 0) system_error("Unable to fork communication process.");
  if (pid==0) { 
    close(sv[0]);
    comm_client(box_server_address, box_server_port, sv[1], chunk_num);
  }
  close(sv[1]);
  int64_t length = 0, portnum;
  char *address=NULL, port[10];
  _recv_from_socket(sv[0], &length, sizeof(int64_t));
  check_realloc_s(address, sizeof(char), length);
  _recv_from_socket(sv[0], address, length);
  _recv_from_socket(sv[0], &portnum, sizeof(int64_t));
  close(sv[0]);

  snprintf(port, 10, "%d", (int)portnum);
  comm_cs = connect_to_addr(address, port);
}
#else
void fork_comm_client(int64_t chunk_num) {}
#endif /* def SF_CATALOG_LIB */

void load_offsets_and_scales(int64_t chunk_num) {
  int64_t n, snap, offset;
  float scale;
  char buffer[1024];
  snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, chunk_num);
  FILE *in = check_fopen(buffer, "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') {
      if (!strncmp(buffer, "#Total halos: ", 14)) {
	offset = atol(buffer+14);
	check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	offsets[num_scales] = offset;
      }
      continue;
    }
    n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
    if (n!=3) continue;
    check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
    check_realloc_every(scales, sizeof(float), num_scales, 100);
    scales[num_scales] = scale;
    offsets[num_scales] = offset;
    num_scales++;
  }
  fclose(in);
  if (!num_scales) {
    fprintf(stderr, "[Error] No snapshots found in %s/offsets.box%"PRId64".txt\n", INBASE, chunk_num);
    exit(EXIT_FAILURE);
  }
}


void gen_and_free_luminosities(void) {
  int64_t n;
  check_realloc_s(ll_m1500, sizeof(float *), num_scales);
  memset(ll_m1500, 0, sizeof(float *)*num_scales);
  check_realloc_s(ll_u, sizeof(float *), num_scales);
  memset(ll_u, 0, sizeof(float *)*num_scales);
  check_realloc_s(ll_v, sizeof(float *), num_scales);
  memset(ll_v, 0, sizeof(float *)*num_scales);
  check_realloc_s(ll_j, sizeof(float *), num_scales);
  memset(ll_j, 0, sizeof(float *)*num_scales);
  for (n=0; n<num_scales; n++) {
    gen_lum_lookup(ll_m1500+n, scales, n, L_m1500, 0);
    gen_lum_lookup(ll_u+n, scales, n, L_Jn_U, 0);
    gen_lum_lookup(ll_v+n, scales, n, L_Jn_V, 0);
    gen_lum_lookup(ll_j+n, scales, n, L_2mass_j, 0);
#ifdef CALZETTI_DUST
    rescale_dust(ll_m1500+n, n, 1.2);
    rescale_dust(ll_u+n, n, 0.5);
    rescale_dust(ll_v+n, n, 0.5);
    rescale_dust(ll_j+n, n, 0.5);
#endif /* CALZETTI_DUST */
  }
  free_luminosities();
}

void load_box(int64_t chunk_num) {
  char buffer[1024];

  gen_exp10cache();
  log_quenched_ssfr = log10(QUENCHED_THRESHOLD);

  if (catalog_client && box_id==0 && SEQUENTIAL_LOADING) {
    if (chunk_num>(SEQUENTIAL_LOADING-1)) {
      FILE *in=NULL;
      snprintf(buffer, 1024, "%s/ready%"PRId64".dat", OUTBASE, chunk_num);
      while (!in) {
        random_sleep(NUM_BLOCKS/10.0);
        in=fopen(buffer, "r");
      }
      fclose(in);
      unlink(buffer);
    }
  }

  
  //First load offsets from file:
  load_offsets_and_scales(chunk_num);

  allocate_shared_memory(0);
  if (catalog_client) fork_comm_client(chunk_num);
  if (!catalog_client) gen_and_free_luminosities(); //Should be called by box server otherwise
  
  //Load halos
  snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, chunk_num);

#ifdef SF_CATALOG_LIB
  if (USE_PLOAD) {
    snprintf(buffer, 1024, "cat.box%"PRId64".dat", chunk_num);
    halos = pload(buffer, INBASE, OUTBASE, box_id, TOTAL_BOXES, halos, offsets[num_scales]*sizeof(struct catalog_halo), SERVER_INTERFACE, NUM_BLOCKS);
  }
  else
#endif /*def SF_CATALOG_LIB*/
  {
    FILE *in = check_fopen(buffer, "r");
    check_realloc_s(halos, offsets[num_scales], sizeof(struct catalog_halo));
    check_fread(halos, sizeof(struct catalog_halo), offsets[num_scales], in);
    fclose(in);
  }

  if (catalog_client && box_id==0 && SEQUENTIAL_LOADING) {
    if (chunk_num < NUM_BLOCKS-SEQUENTIAL_LOADING) {
      snprintf(buffer, 1024, "%s/ready%"PRId64".dat", OUTBASE, chunk_num+SEQUENTIAL_LOADING);
      FILE *out = check_fopen(buffer, "w");
      fclose(out);
    }
  }
  
  allocate_nonshared_memory();
  calculate_boundaries();
}

//Calculates additional information from tree files in a parallel einfo structure.
void calc_extra_info(void) {
  int64_t i, j;
  check_calloc_s(einfo, sizeof(struct extra_halo_info), offsets[num_scales]);

  //Loop over scales
  for (i=0; i<num_scales; i++) {
    struct inthash *ih = new_inthash();
    for (j=offsets[i]; j<offsets[i+1]; j++) ih_setint64(ih, halos[j].id, j); //ID -> index conversion
    for (j=offsets[i]; j<offsets[i+1]; j++) {
      //Check if halo is a satellite; if so, get its host log(Vmpeak)
      if (halos[j].upid > -1) {
	int64_t parent_idx = ih_getint64(ih, halos[j].upid);
	assert(parent_idx >= offsets[i] && parent_idx < offsets[i+1]); //Requires full forests to be in catalog files
	halos[j].upid = parent_idx; //Convert UPID to Parent's index
	float parent_lvmp = halos[parent_idx].lvmp;
	if (parent_lvmp > einfo[j].max_host_lvmp)
	  einfo[j].max_host_lvmp = parent_lvmp;
      }

      //Send info to descendant
      if (halos[j].descid > -1 && halos[j].flags & MMP_FLAG) {
	assert(halos[j].descid >= offsets[i+1] && halos[j].descid < offsets[i+2]);
	einfo[halos[j].descid].max_host_lvmp = einfo[j].max_host_lvmp;
      }
    }
    free_inthash(ih);
  }
}


void translate_descids(void) {
  int64_t i, j, k, lnod; //Last non-orphan descendant
  for (i=0; i<num_scales-1; i++) {
    k=offsets[i+1];
    for (j=offsets[i]; j<offsets[i+1];) {
      int64_t last_j = j;
      assert(k<offsets[i+2]);

      //MMP is always first
      if (halos[j].descid == halos[k].id) halos[j].flags |= MMP_FLAG;
      lnod = k;
      assert(!(halos[lnod].flags & ORPHAN_FLAG)); //First halo should never be an orphan

      //Next should be orphan->orphan, non-mmp mergers, and non-mmp->orphan
      //Ordering of types is not guaranteed, but ordering of orphan descendants *is* guaranteed
      for (; j<offsets[i+1]; j++) {
	if (halos[j].descid == halos[lnod].id) { //non-mmp or orphan merger
	  halos[j].descid = lnod;
	} else {
	  if (k+1 == offsets[i+2]) break;
	  if (!(halos[k+1].flags & ORPHAN_FLAG)) break;
	  k++;
	  assert(halos[j].descid == halos[k].id); //Make sure orphan descendants are ordered
	  assert(halos[k].upid == halos[lnod].id); //Make sure orphan is attached to right descendant
	  halos[j].descid = k;
	}
      }

      if (last_j==j) halos[k].flags |= NO_PROGENITOR_FLAG;
      //else halos[j-1].flags |= LAST_PROGENITOR_FLAG; //No longer used.
      k++;
    }
    assert(j==offsets[i+1]);
    for (; k<offsets[i+2]; k++)
      halos[k].flags |= NO_PROGENITOR_FLAG;
    assert(k==offsets[i+2]);
  }
  for (j=0; j<offsets[num_scales]; j++) halos[j].weight=1; //Reset weights
  for (j=offsets[0]; j<offsets[1]; j++) halos[j].flags |= NO_PROGENITOR_FLAG; //Halos at first snap have no progenitor
}


void clear_observations(void) {
  memset(shared_data, 0, shared_data_length);
  memset(galaxy_offsets, 0, sizeof(int64_t)*(num_scales+1));
}


#define MIN_LVMP 0
#define MAX_LVMP 4
#define BPVMP 60
#define NUM_VBINS ((int)((MAX_LVMP-MIN_LVMP)*BPVMP)+2)

double lvmp[NUM_VBINS];
float lsfr_med[NUM_VBINS];


//SFR (linear units) as a function of halo's Log10(Vmax@Mpeak)
double sfr_at_vmp(double lv, struct sf_model c) {
  double vd = lv - c.v_1;
  double vd2 = vd/c.delta;
  return (c.epsilon * (1.0/(doexp10(c.alpha*vd) + doexp10(c.beta*vd)) + c.gamma*exp(-0.5*vd2*vd2)));
}

//Cache Log10(SFR) as a function of Log10(VMP) at a given snapshot.
void gen_sf_lookup(int64_t n) {
  int64_t i;
  for (i=0; i<NUM_VBINS; i++) {
    lvmp[i] = MIN_LVMP + (double)i / (double)BPVMP;
    lsfr_med[i] = dolog10(sfr_at_vmp(lvmp[i], sf_models[n]));
  }
}

//Return cached SFR
float sfr_at_vmp_cached(float lvmp) {
  float f = (lvmp - (float)MIN_LVMP)*(float)BPVMP;
  int64_t b = f;
  f -= b;
  return lsfr_med[b] + f*(lsfr_med[b+1]-lsfr_med[b]);
}

//Used to calculate dynamical time; from Bryan & Norman 1998
//Returns rho_vir / rho_matter
double vir_density(double a) {
  double x = 1.0/(1.0+a*a*a*Ol/Om)-1.0;
  return ((18*M_PI*M_PI + 82.0*x - 39*x*x)/(1.0+x));
}

//Returns 1/sqrt(4*pi*G*rho_vir/3) in years
double dynamical_time(double a) {
  double vd = vir_density(a) * Om * CRITICAL_DENSITY; //Matter density is always Om * CRITICAL_DENSITY(z=0) in comoving units
  double dt = 1.0/sqrt((4.0*M_PI*Gc/3.0)*vd)*pow(a, 1.5) / h0; //In units of Mpc / (km/s)
  dt *= HUBBLE_TIME_CONVERSION * 100.0;
  return dt;
}

void calc_sf_model_catalog(struct sf_model_allz f, int64_t n) {
  struct sf_model c = {0};
  double a = scales[n];
  c = calc_sf_model(f, a);
  c.old_ra = exp(-dt[n]/(RDECAY(f)*dynamical_time(a)));
  c.obs_min_corr_sm = (a > CORR_HIGHZ_SCALE) ? CORR_MIN_SM*pow(10,-c.obs_sm_sig) : //4 sigma
    CORR_MIN_SM_HIGHZ*pow(10, -3.0*c.obs_sm_sig);
  if (a <= CORR_MIN_SCALE) c.obs_min_corr_sm = 1e15; 
  sf_models[n] = c;
  uv_sm_min = doexp10(UV_CALC_MIN_MASS);
  uv_sm_zbin = 1.0/scales[n] - 0.5; //Round to nearest unit redshift
  memset(smooth_smf, 0, sizeof(float)*SM_SMOOTH_NBINS);
  memset(smooth_smf_q, 0, sizeof(float)*SM_SMOOTH_NBINS);
  memset(smooth_smf_q2, 0, sizeof(float)*SM_SMOOTH_NBINS);
  memset(smooth_smf_q3, 0, sizeof(float)*SM_SMOOTH_NBINS);
  memset(smooth_ssfr, 0, sizeof(float)*SM_SMOOTH_NBINS);
  memset(smooth_uvlf, 0, sizeof(float)*UV_SMOOTH_NBINS);
  memset(smooth_ir_excess, 0, sizeof(float)*UV_SMOOTH_NBINS);
  if (n==0)
    memset(smooth_uvsm, 0, sizeof(float)*UVSM_REDSHIFTS*UVSM_UV_BINS*UVSM_SM_BINS);
}

//Sum SFHs to get present-day SM and ICL mass, accounting for stellar mass loss.
void _sum_sfh(const int64_t n, const int64_t i, float const *remn) {
  int64_t k;
  float sm=0, icl=0;
  float *desc_sfh = sfh_new + (i-offsets[n])*(n+1);
  if (CALC_ICL) {
    float *desc_icl = icl_new + (i-offsets[n])*(n+1);
    for (k=0; k<n; k++) {
      sm+=desc_sfh[k]*remn[k];
      icl+=desc_icl[k]*remn[k];
    }
  }
  else {
    for (k=0; k<n; k++) sm+=desc_sfh[k]*remn[k];
  }
  halos[i].sm = sm;
  halos[i].icl = icl;

  //Hard failure if sm < 0; this indicates coding error.
  if (!(sm >= 0)) {
    int64_t chunk_num = (catalog_client) ? (catalog_client - 1) : loaded_chunk;
    fprintf(stderr, "%"PRId64" %e %e (chunk: %"PRId64"); flags: %"PRId32"\n", i, sm, halos[i].m, chunk_num, halos[i].flags);
    for (k=0; k<=NUM_PARAMS+1; k++) fprintf(stderr, "%.12g ", the_model.params[k]);
    fprintf(stderr,"\n");
    for (k=0; k<=n; k++) fprintf(stderr, "%g ", desc_sfh[k]);
    fprintf(stderr,"\n");
    trigger_watchdog();
  }
}


//Main routine for calculating galaxies' star formation rates
// as well as colors & luminosities.  Also computes summary statistics
// and flags galaxies that may need to be sent to neighboring processors for,
// e.g., calculating correlation functions.
void _calc_sf(int64_t n, int64_t i, struct sf_model c, float remnn) {
  float *rnums = rand_table + (i-offsets[n])*HALO_NUM_RANDS;
  float sfr = -10;
  if (halos[i].lvmp > MIN_LVMP && halos[i].lvmp < MAX_LVMP) {
    float r1 = c.r_min + (1.0-c.r_min)*cached_rank((c.r_cen - halos[i].lvmp)/c.r_width);
    if (r1 > 1) r1 = 1;
    else if (r1 < -1) r1 = -1;
    float rest = sqrtf(1.0-r1*r1);
    float rank = r1*halos[i].rank1 + rest*halos[i].rarank;
    float sfr_sf = sfr_at_vmp_cached(halos[i].lvmp);
    float fq = c.fq_min + (1.0-c.fq_min)*cached_rank((halos[i].lvmp-c.q_lvmp)/c.q_sig_lvmp);
    float sfr_q = (halos[i].sm > 1) ? log10fc(halos[i].sm)+c.ssfr_q : (sfr_sf-2.0);
    sfr = rank_to_sfr(rank, sfr_q, c.sig_q, sfr_sf, c.sig_sf, fq);

    float rank_uncorr = c.sig_sf_uncorr*rnums[3];
    sfr += rank_uncorr;

    halos[i].sfr = doexp10f(sfr);
    if (!(halos[i].sfr >= 0 && halos[i].sfr < 1e20)) {
      if (sfr >= 20 && sfr < 1e10) { halos[i].sfr = 1e20; } //Will max out at baryon fraction
      else {
	fprintf(stderr, "[Error] Invalid SFR at scale %f: %g %g %g %g %g %g %g %g; %g %g %g %g %g %g\n",
		scales[n], halos[i].lvmp, sfr_sf, sfr_q, rank, c.sig_q, c.sig_sf, fq, sfr, c.r_min, c.r_cen, c.r_width, r1, halos[i].rank1, halos[i].rarank);
	assert(0);
      }
    }

    float new_sm = halos[i].sm + halos[i].sfr * dt2[n] * remnn;
    if (new_sm*h0 > halos[i].mp*fb) {
      halos[i].sfr = (halos[i].mp*fb/h0 - halos[i].sm)/(dt2[n]*remnn);
      sfr = (halos[i].sfr > 0) ? log10fc(halos[i].sfr) : -10;
    }
    if (halos[i].sfr < 0) halos[i].sfr = 0;
    
    halos[i].sm += halos[i].sfr * dt2[n] * remnn;
  } else {
    halos[i].sfr = 0; //Orphans with lvmp < MIN_LVMP
  }

  float *sfhn = sfh_new+(i-offsets[n])*(n+1)+n;
  *sfhn = halos[i].sfr*dt[n];
  
  halos[i].obs_sm = halos[i].sm * doexp10f(c.obs_sm_offset + c.obs_sm_sig*rnums[0]);
  halos[i].obs_sfr = halos[i].sfr * doexp10f(c.obs_sfr_offset + c.obs_sfr_sig*rnums[1]);
  if (!isfinite(halos[i].obs_sfr) || !isfinite(halos[i].obs_sm)) return;
  double lsm = log10fc(halos[i].sm) + c.obs_sm_offset;
  double lsfr = sfr + c.obs_sfr_offset;
  if ((halos[i].obs_sm > c.obs_min_corr_sm) || 
      (POSTPROCESSING_MODE && halos[i].obs_sm > POSTPROCESS_CORR_MIN_SM &&
       scales[n] > POSTPROCESS_CORR_MIN_SCALE)) {
    struct catalog_galaxy *g = galaxies + galaxy_offsets[n+1];
    g->lsm = lsm;
    g->lsfr = lsfr;
    g->pos[0] = halos[i].pos[0];
    g->pos[1] = halos[i].pos[1];
    g->pos[2] = halos[i].pos[2] + (halos[i].pos[5])/hz;
    g->weight = halos[i].weight;
    while (g->pos[2] < 0) g->pos[2] += BOX_SIZE;
    while (g->pos[2] > BOX_SIZE) g->pos[2] -= BOX_SIZE;
    galaxy_offsets[n+1]++;
  }
  csfr[n] += halos[i].sfr*halos[i].weight;
  float uv=0,vj=0;
  if (CALC_LUMINOSITIES) {
    if (scales[n] <= UV_CALC_MAX_SCALE || (lsm > UV_CALC_MIN_MASS) || POSTPROCESSING_MODE) {
      float dust = 0;
      halos[i].obs_uv = 
	lum4_of_sfh(ll_m1500[n], ll_u[n], ll_v[n], ll_j[n],
		    sfh_new+(i-offsets[n])*(n+1), scales, n, dust, &uv, &vj)
	+ UV_MAG_CALIBRATION;
      
      
      float irx = 0;  //IR Excess
      if (halos[i].obs_uv < 0) {
	irx = expf(M_LN10*c.uv_dust_slope*(halos[i].obs_uv-c.uv_dust_thresh)/-2.5);
	float a_uv = 2.5*log1pf(irx)*(1.0/M_LN10);
	A_UV(halos[i]) = a_uv;
	halos[i].obs_uv += a_uv;
	if (POSTPROCESSING_MODE) {
	  int64_t uv_sfr_bin = ((halos[i].obs_uv - a_uv) - UV_SFR_MIN)*UV_SFR_BPMAG;
	  if (uv_sfr_bin>=0 && uv_sfr_bin < UV_SFR_NBINS) {
	    uv_sfr_bin += n*UV_SFR_NBINS;
	    uv_sfr_counts[uv_sfr_bin]+=halos[i].weight;
	    uv_sfr_tot_sfr[uv_sfr_bin]+=halos[i].weight*halos[i].sfr;
	  }
	}
      } else {
	A_UV(halos[i]) = 0;
      }
    
      if (halos[i].obs_uv < UV_MAG_THRESH)
	csfr_uv[n] += halos[i].sfr*halos[i].weight*fraction_below_thresh(halos[i].obs_uv, UV_MAG_THRESH, PHOTOMETRIC_SCATTER);
      cloud_in_cell(halos[i].obs_uv, halos[i].weight, smooth_uvlf, UV_MIN, UV_SMOOTH_BPMAG, UV_SMOOTH_NBINS);
      cloud_in_cell(halos[i].obs_uv, halos[i].weight*irx, smooth_ir_excess, UV_MIN, UV_SMOOTH_BPMAG, UV_SMOOTH_NBINS);
      
      uv_count++;
      if (uv_sm_zbin < UVSM_REDSHIFTS && uv_sm_zbin >= 0) {
	int64_t uvsm_uvbin = (halos[i].obs_uv-UVSM_MIN)*UVSM_BPMAG;
	int64_t uvbin_start = uvsm_uvbin-5.0*PHOTOMETRIC_SCATTER*UVSM_BPMAG;
	int64_t uvbin_stop = uvsm_uvbin*2 - uvbin_start+1;
	if (uvbin_start < 0) uvbin_start = 0;
	if (uvbin_stop >= UVSM_UV_BINS) uvbin_stop = UVSM_UV_BINS-1;
	float bin_edge = UVSM_MIN+(double)uvbin_start/(double)UVSM_BPMAG;
	float last_perc = fraction_below_thresh(halos[i].obs_uv, bin_edge, PHOTOMETRIC_SCATTER);
	for (uvsm_uvbin = uvbin_start; uvsm_uvbin < uvbin_stop; uvsm_uvbin++) {
	  bin_edge = UVSM_MIN+(double)(uvsm_uvbin+1)/(double)UVSM_BPMAG;
	  float new_perc = fraction_below_thresh(halos[i].obs_uv, bin_edge, PHOTOMETRIC_SCATTER);
	  float bin_weight = halos[i].weight*(new_perc - last_perc);
	  last_perc = new_perc;
	  float bin_median = ((float)uvsm_uvbin+0.5) / (float)UVSM_BPMAG + (float)UVSM_MIN;
	  float bin_floor = UVSM_SM_NORM + (bin_median - UVSM_SM_NORM_M0)*UVSM_SM_SLOPE;
	  float uvsm_lsm = lsm + (bin_median-halos[i].obs_uv)*-0.4;
	  cloud_in_cell(uvsm_lsm, bin_weight*vweights[n], smooth_uvsm+(uv_sm_zbin*UVSM_UV_BINS+uvsm_uvbin)*UVSM_SM_BINS,
			bin_floor, UVSM_SM_BPDEX, UVSM_SM_BINS);
	}
      }
    }
  }

  float osfr = halos[i].sfr*c.obs_sfr_offset_lin;
  float osm = halos[i].sm*c.obs_sm_offset_lin;
  float tssfr = osfr / osm;
  cloud_in_cell(lsm, halos[i].weight, smooth_smf, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS);
  cloud_in_cell(lsm, halos[i].weight*tssfr, smooth_ssfr, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS);
  float fq = fraction_below_thresh(lsfr-lsm, c.moustakas_constant + 0.65*(lsm - 10.0) - lsm, c.obs_ssfr_scatter);
  cloud_in_cell(lsm, halos[i].weight*fq, smooth_smf_q, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS);
  float fq11 = fraction_below_thresh(lsfr-lsm, log_quenched_ssfr, c.obs_ssfr_scatter); //Consistent only assuming that Halpha SSFRs are uncorrelated with SM...
  cloud_in_cell(lsm, halos[i].weight*fq11, smooth_smf_q2, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS);
  float qf = //muzzin_uvj_quenched(uv,vj,scales[n]);
    uvj_qf_interp(uv, vj, scales[n]);
  cloud_in_cell(lsm, halos[i].weight*qf, smooth_smf_q3, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS);
}


//For halos without progenitors.  Currently sets SFH to zero.
void init_noprog_halo(int64_t n, int64_t j, struct sf_model c, float remnn) {
  struct catalog_halo *dh = halos+j;
  float *desc_sfh = sfh_new + (j-offsets[n])*(n+1);
  float *desc_icl = icl_new + (j-offsets[n])*(n+1);
  memset(desc_sfh, 0, sizeof(float)*(n+1));
  if (CALC_ICL) memset(desc_icl, 0, sizeof(float)*(n+1));
  dh->weight = 1;
  dh->sm = dh->icl = 0;
  dh->rarank = dh->ra;
  _calc_sf(n, j, c, remnn);
}

//Helper macros to transfer star formation histories and icl histories
#define transfer_sfh(src,dest,first_test) { if (first_test) { memcpy(dest, src, sizeof(float)*n); } \
                                            else { for (k=0; k<n; k++) dest[k] += src[k]; }}

#define transfer_sfh_weighted(src,dest,first_test,weight)		\
                                          { if (first_test) { for (k=0; k<n; k++) dest[k] = (weight)*src[k]; }	\
					    else { for (k=0; k<n; k++) dest[k] += (weight)*src[k]; }}
  
#define transfer_icl_weighted(src1,src2,dest,first_test,weight1,weight2)  \
                    { if (first_test) { for (k=0; k<n; k++) dest[k] = (weight1)*src1[k] + (weight2*src2[k]); } \
		      else { for (k=0; k<n; k++) dest[k] += (weight1)*src1[k] + (weight2*src2[k]); }}

//Main routine to handle merger trees; transfers SFHs from progenitors
//to descendants, and decides when satellites are destroyed.
void calc_sm_histories(int64_t n) {
  int64_t i,i_start,j,k,desc;
  double old_ra = sf_models[n].old_ra;
  double new_ra = sqrt(1.0-sf_models[n].old_ra*sf_models[n].old_ra);
  float *remn = rem+n*num_scales; //Cache for remaining stellar mass
  gen_sf_lookup(n); //SFR(Log10(vmp)) lookup table

  struct sf_model c = sf_models[n];
  float remnn = remn[n]; //Stellar mass remaining from SF at the current snapshot.

  //Swap arrays for old SFHs and new SFHs.
  float *tmp;
  tmp = sfh_new; sfh_new = sfh_old; sfh_old = tmp;
  tmp = icl_new; icl_new = icl_old; icl_old = tmp;

  //Clear descendant bitmask (for calculating first & last progenitors)
  BIT_ALL_CLEAR(desc_bitmask, (offsets[n+1]-offsets[n]));

  //Calculate descendants and flags for all halos
  //The descendant can be different for subhalos (and flybys) that have passed below
  // the vmax destruction threshold.  When this happens, their descendants also need
  // to be marked as destroyed (i.e., ignored).
  i_start = (n>0) ? offsets[n-1] : 0;
  for (i=i_start; i<offsets[n]; i++) {
    j = halos[i].descid;
    desc = j-offsets[n];
    if (desc < 0) continue; //Happens at the last snapshot
    struct catalog_halo *dh = halos+j;
    int64_t is_mmp = (halos[i].flags & MMP_FLAG) ? 1 : 0;
    if (dh->flags & ORPHAN_FLAG)
      is_mmp = 1; //Orphans do not have MMP flag set, but are MMPs if their desc. is an orphan.
    
    //Calculate subhalo destruction threshold, which is a function of the
    // Vmax of the largest halo that the subhalo ever passed through.
    float orphan_thresh = 0;
    if (einfo[i].max_host_lvmp>0) {
      orphan_thresh = c.orphan_thresh_min +
	c.orphan_thresh_change*cached_rank((einfo[i].max_host_lvmp-ORPHAN_LVMP)/ORPHAN_LVMP_WIDTH);
    }
      
    if (is_mmp) {
      //If halo is alive and not currently merging
      if (!(halos[i].flags & IGNORE_FLAG) && (halos[i].weight==1)) {
	dh->flags -= (dh->flags & IGNORE_FLAG); //Resurrect descendant (if killed by a previous model)

	//Check if it should merge
	if (dh->v < halos[i].vmp*orphan_thresh) {
	  dh->weight = (halos[i].v-halos[i].vmp*orphan_thresh)/(halos[i].v-dh->v); //Guaranteed to be < 1.
	  //dh->weight < 0 can happen if orphan thresh changes significantly from progenitor to descendant.
	  //In that case, the code below will be skipped, and a later test will kill the descendant halo.
	  
	  //Merge some of the stellar mass into the UP halo; this will get automatically transferred to descendant
	  //If no UPID, then stellar mass is assumed to be ejected into space as halo partially disrupts...
	  if ((dh->weight >= 0) && (halos[i].upid > -1)) {
	    int64_t up_idx = halos[i].upid;
	    assert((up_idx >= i_start) && (up_idx < offsets[n]));
	    if (!(halos[up_idx].flags & IGNORE_FLAG)) {
	      float cen_frac = cached_rank((sf_models[n].icl_dist-halos[i].uparent_dist)/ICL_DIST_TOL);
	      float transfer_weight = (1.0-dh->weight)*cen_frac;
	      float *sfh_src = sfh_old + (i-i_start)*n;
	      float *sfh_dest = sfh_old + (up_idx-i_start)*n;
	      if (transfer_weight > 1e-9) {
		transfer_sfh_weighted(sfh_src, sfh_dest, 0, transfer_weight);
		//printf("%"PRId64" -> %"PRId64" %.3e -> %.3e (%.3f) %.3e\n", i, j, halos[i].sm, halos[up_idx].m, halos[i].uparent_dist, halos[i].sm*transfer_weight);
	      }
	      if (CALC_ICL) {
		transfer_weight = (1.0-dh->weight)*(1.0-cen_frac);
		float icl_transfer_weight = (1.0-dh->weight);
		float *icl_src = icl_old + (i-i_start)*n;
		float *icl_dest = icl_old + (up_idx-i_start)*n;
		transfer_icl_weighted(sfh_src, icl_src, icl_dest, 0, transfer_weight, icl_transfer_weight);
	      }
	    }
	  }
	}
	else { //Not merging
	  dh->weight = 1;
	}
      }

      //If current halo is merging (weight < 1) or already merged, ignore descendant.
      //Note that this is still within the is_mmp test.
      if ((halos[i].flags & IGNORE_FLAG) || (halos[i].weight < 1) || (dh->weight <= 0)) {
	is_mmp = 0;
	dh->flags |= IGNORE_FLAG;
	dh->weight = 0;
	CLEAR_OBSERVABLES(*dh); //Make sure people don't accidentally use...
	int64_t up_idx = dh->upid; //Switch descendant to descendant halo's upid
	if (up_idx > -1) { //Host halo exists
	  j = up_idx;
	  assert((j >= offsets[n]) && (j < offsets[n+1]));
	  dh = halos + j;
	  desc = j - offsets[n];
	}
      }
    }
    
    //Save calculated descendant and calculate flags
    descs[i-i_start] = desc;
    if (is_mmp) {
      halos[i].flags |= EFFECTIVE_MMP_FLAG;
      //Correlate descendant's random variable with progenitor's.
      dh->rarank = halos[i].rarank*old_ra + dh->ra*new_ra;
    }
    else {
      halos[i].flags -= (halos[i].flags & EFFECTIVE_MMP_FLAG);
    }
    halos[i].flags -= (halos[i].flags & (FIRST_PROGENITOR_FLAG | LAST_PROGENITOR_FLAG));
    if (!(halos[i].flags & IGNORE_FLAG) && (BIT_TST(desc_bitmask, desc)==0)) {
      BIT_SET(desc_bitmask, desc);
      halos[i].flags |= FIRST_PROGENITOR_FLAG;
    }
  }

  //Flag last progenitors
  for (i=offsets[n]-1; i>=i_start; i--) {
    desc = descs[i-i_start];
    if (!(halos[i].flags & IGNORE_FLAG) && (BIT_TST(desc_bitmask, desc))) {
      BIT_CLR(desc_bitmask, desc);
      halos[i].flags |= LAST_PROGENITOR_FLAG;
    }
  }

  //Transfer SFH and ICLH information from progenitors to descendants
  //In the common case, there are no mergers, and so just a memcpy needs to be done
  // and the sum can then be taken to compute the total stellar mass, etc.
  //Doing a memcpy for first progenitors avoids having to do a separate memory clear.
  for (i=i_start; i<offsets[n]; i++) {
    if (halos[i].flags & IGNORE_FLAG) continue;
    desc = descs[i-i_start];
    if (desc < 0) continue;
    j = offsets[n]+desc;
    float *desc_sfh = sfh_new+(desc*(n+1));
    float *desc_icl = icl_new+(desc*(n+1));
    float *prog_sfh = sfh_old+((i-i_start)*n);
    float *prog_icl = icl_old+((i-i_start)*n);
    int64_t is_first_prog = (halos[i].flags & FIRST_PROGENITOR_FLAG) ? 1 : 0;
    if (halos[i].flags & EFFECTIVE_MMP_FLAG) {
      transfer_sfh(prog_sfh, desc_sfh, is_first_prog);
      desc_sfh[n] = 0;
      if (CALC_ICL) {
	transfer_sfh(prog_icl, desc_icl, is_first_prog);
	desc_icl[n] = 0;
      }
    } else {
      float cen_frac = cached_rank((sf_models[n].icl_dist-halos[i].uparent_dist)/ICL_DIST_TOL);
      float icl_frac = (1.0-cen_frac)*halos[i].weight;
      cen_frac *= halos[i].weight;
      
      //printf("%"PRId64" -> %"PRId64" %.3e -> %.3e (%.3f) %.3e\n", i, j, halos[i].sm, halos[j].m, halos[i].uparent_dist, halos[i].sm*cen_frac);
      if (cen_frac>1e-9) {
	transfer_sfh_weighted(prog_sfh, desc_sfh, is_first_prog, cen_frac);
      } else if (is_first_prog) {
	memset(desc_sfh, 0, sizeof(float)*n); //value at n+1 is cleared by effective MMP
      }

      if (CALC_ICL) {
	transfer_icl_weighted(prog_sfh, prog_icl, desc_icl, is_first_prog, icl_frac, halos[i].weight);
      }
    }

    //If we've transferred stellar mass from the last progenitor, then we can calculate
    // the total stellar mass formed as well as the new SFR.
    //Doing the calculation here helps avoid cache misses if we've just copied to or
    // generated the SFH.
    if (halos[i].flags & LAST_PROGENITOR_FLAG) {
      _sum_sfh(n, j, remn);
      _calc_sf(n, j, c, remnn);
    }
  }
  
  //Halos without progenitors aren't affected by the code above,
  // so we should initialize them now.
  for (j=offsets[n]; j<offsets[n+1]; j++) {
    if (halos[j].flags & NO_PROGENITOR_FLAG)
      init_noprog_halo(n,j,c,remnn);

    //Resurrect halos that were incorrectly joined to a disrupted halo track
    //This is evident if their Vmax grows above their previous host's vmax.
    //Only affects 1 in a million halos or so.
    if ((halos[j].flags & IGNORE_FLAG) &&
	(halos[j].lvmp > einfo[j].max_host_lvmp+0.1)) {
      if (log10(halos[j].v) > einfo[j].max_host_lvmp+0.1) {
	halos[j].flags -= halos[j].flags & IGNORE_FLAG;
	init_noprog_halo(n,j,c,remnn); //Automatically sets weight = 1.
      }
    }
  }

  /*if (n==num_scales-1) {
    for (j=offsets[n]; j<offsets[n+1]; j++)
      printf("%"PRId64" %e %e\n", j, halos[j].sm*halos[j].weight, (halos[j].upid > -1) ? (halos[j].sm*halos[j].weight) : 0);
      }*/
}


void smooth_nds(int64_t n) {
  int64_t i, j;
  struct sf_model c = sf_models[n];
  smooth_bins(smooth_smf, smf+n*SM_NBINS, c.obs_sm_sig, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS,
	      SM_MIN, SM_BPDEX, SM_NBINS, 0, 0);
  smooth_bins(smooth_smf_q, smf_q+n*SM_NBINS, c.obs_sm_sig, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS,
	      SM_MIN, SM_BPDEX, SM_NBINS, 0, 0);
  smooth_bins(smooth_smf_q2, smf_q2+n*SM_NBINS, c.obs_sm_sig, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS,
	      SM_MIN, SM_BPDEX, SM_NBINS, 0, 0);
  smooth_bins(smooth_smf_q3, smf_q3+n*SM_NBINS, c.obs_sm_sig, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS,
	      SM_MIN, SM_BPDEX, SM_NBINS, 0, 0);
  smooth_bins(smooth_ssfr, ssfr+n*SM_NBINS, c.obs_sm_sig, SM_SMOOTH_MIN, SM_SMOOTH_BPDEX, SM_SMOOTH_NBINS,
	      SM_MIN, SM_BPDEX, SM_NBINS, 1, 0);
  smooth_bins(smooth_uvlf, uvlf+n*UV_NBINS, PHOTOMETRIC_SCATTER, UV_MIN, UV_SMOOTH_BPMAG, UV_SMOOTH_NBINS,
	      UV_MIN, UV_BPMAG, UV_NBINS, 0, 0);
  smooth_bins(smooth_ir_excess, ir_excess+n*UV_NBINS, PHOTOMETRIC_SCATTER, UV_MIN, UV_SMOOTH_BPMAG, UV_SMOOTH_NBINS,
	      UV_MIN, UV_BPMAG, UV_NBINS, 0, 0);

  csfr[n] *= c.obs_sfr_offset_lin*c.obs_sfr_scatter_corr;
  csfr_uv[n] *= c.obs_sfr_offset_lin*c.obs_sfr_scatter_corr;
  for (i=0; i<SM_NBINS; i++)
    ssfr[n*SM_NBINS+i] *= c.obs_ssfr_scatter_corr;

  if (n==num_scales-1) {
    for (i=0; i<UVSM_REDSHIFTS; i++) {
      for (j=0; j<UVSM_UV_BINS; j++) {
	int64_t offset = (i*UVSM_UV_BINS + j)*UVSM_SM_BINS;
	smooth_bins(smooth_uvsm+offset, uvsm+offset, c.obs_sm_sig, 0, UVSM_SM_BPDEX, UVSM_SM_BINS,
		    0, UVSM_SM_BPDEX, UVSM_SM_BINS, 0, 1);
      }
    }
  }
}



#include "write_catalog.c"

#define ERF_CACHE_MIN -8
#define ERF_CACHE_MAX 8
#define ERF_CACHE_BPUNIT 128
#define ERF_CACHE_NUM (((ERF_CACHE_MAX-ERF_CACHE_MIN)*ERF_CACHE_BPUNIT)+2)
float erf_cache[ERF_CACHE_NUM];
#define SIG_CACHE_MIN 0.01
#define SIG_CACHE_MAX 0.99
#define SIG_CACHE_BPUNIT 100
#define SIG_CACHE_NUM 99
float sig_cache[SIG_CACHE_NUM];

double perc_to_normal(double p) {
  double x1 = -14;
  double x2 = 14;
  while (x2-x1 > 1e-7) {
    double half = 0.5*(x1+x2);
    double perc = 0.5*(1.0+erf(half));
    if (perc > p) { x2 = half; }
    else { x1 = half; }    
  }
  return ((x1+x2)*M_SQRT1_2);
}

void gen_erfcache(void) {
  int64_t i;
  for (i=0; i<ERF_CACHE_NUM; i++) {
    double x = (double)ERF_CACHE_MIN + (double)i/(double)(ERF_CACHE_BPUNIT);
    erf_cache[i] = 0.5+0.5*erf(x/M_SQRT2);
  }

  for (i=0; i<SIG_CACHE_NUM; i++) {
    double perc = SIG_CACHE_MIN + (double)i / (double)SIG_CACHE_BPUNIT;
    sig_cache[i] = perc_to_normal(perc);
  }
}

float fraction_above_thresh(float x, float thresh, float scatter) {
  return cached_rank((x-thresh)/scatter);
}

float fraction_below_thresh(float x, float thresh, float scatter) {
  return cached_rank((thresh-x)/scatter);
}

float cached_rank(float x) {
  if (x<ERF_CACHE_MIN) return 0;
  if (x>=ERF_CACHE_MAX) return 1;
  float f = (x-(float)ERF_CACHE_MIN)*(float)ERF_CACHE_BPUNIT;
  int64_t b = f;
  f -= b;
  return (erf_cache[b]+f*(erf_cache[b+1]-erf_cache[b]));
}

float cached_perc(float x) {
  if (x < SIG_CACHE_MIN) return sig_cache[0];
  if (x >= SIG_CACHE_MAX) return sig_cache[SIG_CACHE_NUM-1];
  float f = (x-(float)SIG_CACHE_MIN)*(float)SIG_CACHE_BPUNIT;
  int64_t b = f;
  f -= b;
  return (sig_cache[b]+f*(sig_cache[b+1]-sig_cache[b]));
}

float rank_to_sfr_bisection(float rank, float sfr_q, float sig_q, float sfr_sf, float sig_sf, float fq) {
  //  sfr_sf -= sfr_q;
  //  sfr_sf /= sig_q;
  //  sig_sf /= sig_q;
  float inv_sig_sf = 1.0/sig_sf;
  float fsf = (1.0-fq); //No guarantee that either fq or fsf is nonzero
  //rank = cached_rank(rank); //Sigma -> percentile

#define SFR_RANK(x) ((cached_rank(x)*fq+cached_rank((x-sfr_sf)*inv_sig_sf)*fsf))
  float min_sfr=sfr_sf+sig_sf*ERF_CACHE_MIN;
  float max_sfr=sfr_sf+sig_sf*ERF_CACHE_MAX;
  if (min_sfr > ERF_CACHE_MIN) min_sfr = ERF_CACHE_MIN;
  if (max_sfr < ERF_CACHE_MAX) max_sfr = ERF_CACHE_MAX;
  int64_t i;
  for (i=0; i<20; i++) {
    float sfr = 0.5*(min_sfr+max_sfr);
    if (SFR_RANK(sfr) > rank) {
      max_sfr = sfr;
    } else {
      min_sfr = sfr;
    }
  }
  return (sfr_q + 0.5*(min_sfr+max_sfr)*sig_q);
#undef SFR_RANK
}

float rank_to_sfr(float rank, float sfr_q, float sig_q, float sfr_sf, float sig_sf, float fq) {
  sfr_sf -= sfr_q;
  sfr_sf /= sig_q;
  sig_sf /= sig_q;
  float inv_sig_sf = 1.0/sig_sf;
  float fsf = (1.0-fq); //No guarantee that either fq or fsf is nonzero

  if (rank < -7) { // Should never happen except for bugs in rank-ordering
    //float orank = rank;
    rank += (int) (-7 - rank); 
    //fprintf(stderr, "Reranked %g to %g\n", orank, rank);
  } else if (rank > 7) {
    rank -= (int) (rank - 7);
  }
  
  //Most things will be SF or nearly so
  float sfr_guess = sfr_sf; // + rank * sig_sf;
  rank = cached_rank(rank); //Sigma -> percentile
  if (rank > fq) {
    float rank_sf = (rank - fq) / (1.0-fq);
    rank_sf = cached_perc(rank_sf);
    sfr_guess = sfr_sf + rank_sf * sig_sf;
  } else {
    float rank_q = rank / fq;
    rank_q = cached_perc(rank_q);
    sfr_guess = rank_q;
  }

#define SFR_RANK(x) ((cached_rank(x)*fq+cached_rank((x-sfr_sf)*inv_sig_sf)*fsf))
  if ((sfr_sf > 0 && (rank < fq)) || ((sfr_sf < 0) && (rank > 1.0-fq))) sfr_guess = 0;
  float guess_rank = SFR_RANK(sfr_guess);
  float dr = 0.1;
  int64_t c = 0;
  int64_t max_iter = 30;
  while (fabs(guess_rank - rank)>0.0001 && c<max_iter) {
    c++;
    float g2 = sfr_guess + dr;
    float r2 = SFR_RANK(g2);
    if (r2 == guess_rank) return rank_to_sfr_bisection(rank, sfr_q, sig_q, sfr_sf, sig_sf, fq);
    float g3 = sfr_guess + (rank-guess_rank)*dr/(r2-guess_rank);
    if (fabs(g3-sfr_guess)>dr*40) {
      g3 = (g3 > sfr_guess) ? sfr_guess+dr*40 : sfr_guess-dr*40;
    } else {
      dr *= 0.5;
    }
    float r3 = SFR_RANK(g3);
    if (fabs(r3-rank) < fabs(guess_rank-rank)) {
      sfr_guess = g3;
      guess_rank = r3;
    }
    if (fabs(r2-rank) < fabs(guess_rank-rank)) {
      sfr_guess = g2;
      guess_rank = r2;
    }
  }
  if (c==max_iter) return rank_to_sfr_bisection(rank, sfr_q, sig_q, sfr_sf, sig_sf, fq);
  return (sfr_q + sfr_guess*sig_q);
#undef SFR_RANK 
}

void shared_data_to_file(struct sf_model_allz m, FILE *out) {
  int64_t zero = 0;
  fwrite(&m, sizeof(struct sf_model_allz), 1, out);
  fwrite(&num_scales, sizeof(int64_t), 1, out);
  fwrite(&shared_data_length, sizeof(int64_t), 1, out);
  fwrite(&zero, sizeof(int64_t), 1, out); //Normally size of obs data
  fwrite(scales, sizeof(float), num_scales, out);
  fwrite(shared_data, 1, shared_data_length, out);
  //fwrite(obsdata, 1, num_obs*sizeof(struct observation), out);
  fflush(out);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"

extern struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused

#define FAST3TREE_TYPE   struct catalog_galaxy
#define FAST3TREE_PREFIX SAMPLE_CORR
#define FAST3TREE_DIM    2
#include "fast3tree.c"


int sort_by_sm(const void *a, const void *b);
void scorr_load_box(char *filename);

int main(int argc, char **argv)
{
  int64_t i, j, k;

  if (argc < 9) {
    fprintf(stderr, "Usage: %s config.txt sm_low sm_high pi_max z_err include_sats include_backsplash sfr_catalog1.bin ...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  do_config(argv[1]);
  setup_config();

  float sm_low = atof(argv[2]);
  float sm_high = atof(argv[3]);
  if (sm_low < 15) { sm_low=pow(10, sm_low); }
  if (sm_high < 15) { sm_high=pow(10, sm_high); }
  CORR_LENGTH_PI = atof(argv[4]);
  REDSHIFT_ERROR = atof(argv[5]);
  int64_t do_sats = atol(argv[6]);
  int64_t do_backsplash = atol(argv[7]);

  r250_init(87L);

  for (i=8; i<argc; i++)
    scorr_load_box(argv[i]);

  char *scale=argv[8];
  while (*scale && (*scale < '0' || *scale > '9')) scale++;
  if (!(*scale)) {
    fprintf(stderr, "Couldn't infer scale factor from sfr catalog name! (%s)\n", argv[8]);
    exit(EXIT_FAILURE);
  }
  double a = atof(scale);

  qsort(halos, nh, sizeof(struct catalog_halo), sort_by_sm);

  for (i=0; i<nh; i++) if (halos[i].obs_sm <= sm_high) break;
  for (j=i; j<nh; j++) if (halos[j].obs_sm < sm_low) break;

  int64_t ng = j-i;
  int64_t g_start = i;
  int64_t g_stop = j;

  check_realloc_s(galaxies, sizeof(struct catalog_galaxy), ng);

  float hz = 100.0*sqrt(Om*pow(a, -3.0)+Ol)*a;

  struct catalog_galaxy *galaxies2 = NULL;
  int64_t num_g2 = 0;
  float translate_const = -0.49 + 1.07 * ((1.0/a-1.0) - 0.1);
  int64_t g_total = 0;

  for (i=g_start; i<g_stop; i++) {
    if (!do_sats && (halos[i].upid > -1)) continue;
    if (!do_backsplash && (halos[i].m < 0.9*halos[i].mp)) continue;
    if (halos[i].flags & IGNORE_FLAG) continue;
    
    struct catalog_galaxy *g = galaxies + g_total;
    g->lsm = log10(halos[i].obs_sm);
    g->lsfr = (halos[i].obs_sfr > 1e-15) ?  log10(halos[i].obs_sfr) : -15;
    translate_sfrs(g, translate_const);

    g->pos[0] = halos[i].pos[0];
    g->pos[1] = halos[i].pos[1];
    g->pos[2] = halos[i].pos[2] + (halos[i].pos[5]+normal_random(0, REDSHIFT_ERROR))/hz;
    while (g->pos[2]<0) g->pos[2] += BOX_SIZE;
    while (g->pos[2]>BOX_SIZE) g->pos[2] -= BOX_SIZE;

#define ADD_GALAXY { check_realloc_every(galaxies2, sizeof(struct catalog_galaxy), num_g2, 1000); galaxies2[num_g2] = g2; num_g2++; }
    for (j=-1; j<=1; j++) {
      for (k=-1; k<=1; k++) {
	struct catalog_galaxy g2 = *g;
	g2.pos[0] += BOX_SIZE*j;
	g2.pos[1] += BOX_SIZE*k;
	if ((g2.pos[0] > -CORR_LENGTH_RP) && (g2.pos[0] < BOX_SIZE+CORR_LENGTH_RP) &&
	    (g2.pos[1] > -CORR_LENGTH_RP) && (g2.pos[1] < BOX_SIZE+CORR_LENGTH_RP))
	  ADD_GALAXY;
      }
    }
#undef ADD_GALAXY

    g_total++;
  }

  struct fast3tree *t = fast3tree_init(g_total, galaxies);
  struct fast3tree_results *r = fast3tree_results_init();
  _fast3tree_set_minmax(t, 0, BOX_SIZE);

  init_dists();
  init_corr_tree(galaxies2, num_g2);
  int64_t *bin_counts = NULL;
  check_realloc_s(bin_counts, sizeof(int64_t), NUM_BINS*3*ng);
  for (i=0; i<g_total; i++) {
    clear_corrs();
    bin_single_galaxy(galaxies+i);
    memcpy(bin_counts+i*NUM_BINS*3,            corr,    NUM_BINS*sizeof(int64_t));
    memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS,   corr_sf, NUM_BINS*sizeof(int64_t));
    memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS*2, corr_q,  NUM_BINS*sizeof(int64_t));
  }

  //Compute correlation function
  float c[2];
  clear_corrs();
  c[0] = 0.5*BOX_SIZE;
  c[1] = 0.5*BOX_SIZE;
  fast3tree_find_sphere(t, r, c, 1.5*BOX_SIZE);
  int64_t nq=0, na=0, nsf=0;
  for (j=0; j<r->num_points; j++) {
    na++;
    if (r->points[j]->lsfr > 0) nsf++;
    else nq++;
    int64_t *gcorr = bin_counts + (r->points[j]-galaxies)*NUM_BINS*3;
    for (k=0; k<NUM_BINS; k++) {
      corr[k] += gcorr[k];
      corr_sf[k] += gcorr[k+NUM_BINS];
      corr_q[k] += gcorr[k+NUM_BINS*2];
    }
  }

  printf("#NA: %"PRId64"; NSF: %"PRId64"; NQ: %"PRId64"\n", na, nsf, nq);
  float vol = pow(BOX_SIZE, 3);
  float da = na/vol, dsf = nsf/vol, dq=nq/vol;
  printf("#Vol: %g\n", vol);
  printf("#R C(all-all) C(sf-sf) C(q-q)\n");
  if (na < 1) na = 1;
  if (nq < 1) nq = 1;
  if (nsf < 1) nsf = 1;
  for (j=0; j<NUM_BINS; j++) {
    float a1 = corr_dists[j]*M_PI;
    float a2 = corr_dists[j+1]*M_PI;
    float a = a2-a1;
    float av_r = sqrt(0.5*(a1+a2)/M_PI);
    float raa = 2.0*da*a*CORR_LENGTH_PI;
    float rasf = 2.0*dsf*a*CORR_LENGTH_PI;
    float raq = 2.0*dq*a*CORR_LENGTH_PI;
    printf("%f %e %e %e %e %g %g %g %g\n", av_r/h0, (corr[j]-na*raa)/(na*da*a)/h0, (corr_sf[j]-nsf*rasf)/(nsf*dsf*a)/h0, (corr_q[j]-nq*raq)/(nq*dq*a)/h0, (float)corr[j], da, a, raa, (float)corr_bin_of(av_r*av_r));
  }
  return 0;
}


int sort_by_sm(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if (c->obs_sm > d->obs_sm) return -1;
  if (c->obs_sm < d->obs_sm) return 1;
  return 0;
}


void scorr_load_box(char *filename) {
  int64_t length;
  void *data = check_mmap_file(filename, 'r', &length);
  assert(!(length % sizeof(struct catalog_halo)));
  int64_t to_read = length / sizeof(struct catalog_halo);
  check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
  memcpy(halos+nh, data, length);
  munmap(data, length);
  nh += to_read;
}

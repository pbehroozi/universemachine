#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "inthash.h"
#include "stringparse.h"
#include "config.h"
#include "config_vars.h"

FILE *logfile = NULL;
float *scales = NULL;
int64_t num_scales=0;

#define MASS_MIN 8
#define MASS_MAX 16
#define MASS_BPDEX 4
#define MASS_BINS ((MASS_MAX-MASS_MIN)*MASS_BPDEX+1)

#define RATIO_MIN (-5)
#define RATIO_MAX 0
#define RATIO_BPDEX 4
#define RATIO_BINS ((RATIO_MAX-RATIO_MIN)*RATIO_BPDEX+1)


void load_scales(void);
struct catalog_halo *load_catalog(float scale, int64_t *num_halos);
void print_mergers(double merger_rates[MASS_BINS][RATIO_BINS], int64_t n1, int64_t n2);

int main(int argc, char **argv) {
  int64_t n, i;
  if (argc<2) {
    printf("Usage: %s config.cfg\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  do_config(argv[1]);
  setup_config();
  load_scales();

  int64_t num_halos = 0;
  struct catalog_halo *halos = load_catalog(scales[num_scales-1], &num_halos);

  double stacked_merger_rates[MASS_BINS][RATIO_BINS] = {{0}};
  int64_t last_stack_n = num_scales-1;

  for (n=num_scales-2; n>=0; n--) {
    //Count descendants and build hash table of descendant masses
    struct inthash *desc_masses = new_inthash();
    double halo_counts[MASS_BINS] = {0};
    double merger_rates_per_halo[MASS_BINS][RATIO_BINS] = {{0}};
    for (i=0; i<num_halos; i++) {
      struct catalog_halo *tp = halos+i;
      if (tp->flags & IGNORE_FLAG) continue;
      int64_t desc_bin = MASS_BPDEX*(log10(tp->mp/h0)-MASS_MIN);
      if (desc_bin < 0 || desc_bin > MASS_BINS-1) desc_bin = -1;
      else halo_counts[desc_bin]++;
      ih_setdouble(desc_masses, tp->id, tp->mp);
    }
    munmap(halos, num_halos*sizeof(struct catalog_halo));

    //Load halos at current snapshot and identify MMPs.
    halos = load_catalog(scales[n], &num_halos);
    struct inthash *mmp_index = new_inthash();
    for (i=0; i<num_halos; i++) {
      struct catalog_halo *tp = halos+i;
      if (tp->flags & IGNORE_FLAG) continue;
      int64_t mmp = ih_getint64(mmp_index, tp->descid);
      if (mmp == IH_INVALID || halos[mmp].mp < tp->mp)
	ih_setint64(mmp_index, tp->descid, i);
    }

    //Count merger rates
    for (i=0; i<num_halos; i++) {
      struct catalog_halo *tp = halos+i;
      if (tp->flags & IGNORE_FLAG) continue;
      int64_t mmp = ih_getint64(mmp_index, tp->descid);
      assert(mmp != IH_INVALID);
      if (mmp==i) continue; //Ignore MMPs
      double desc_mass = ih_getdouble(desc_masses, tp->descid);
      if (isnan(desc_mass)) {
	fprintf(stderr, "[Warning] skipping evaporated halo %"PRId64" (-> %"PRId64") at a=%f\n", tp->id, tp->descid, scales[n]);
	continue;
      }
      double ratio = tp->mp / desc_mass;
      int64_t desc_bin = MASS_BPDEX*(log10(desc_mass/h0)-MASS_MIN);
      if (desc_bin < 0 || desc_bin > MASS_BINS-1) continue;
      int64_t ratio_bin = RATIO_BPDEX*(log10(ratio)-RATIO_MIN);
      if (ratio_bin < 0 || ratio_bin > RATIO_BINS-1) continue;
      merger_rates_per_halo[desc_bin][ratio_bin]++;      
    }

    //Print / clear stacked rates if necessary
    if (n==0 || (((int)(scales[n]*10) < (int)(scales[n+1]*10)) && scales[n+1]<1)) {
      print_mergers(stacked_merger_rates, n+1, last_stack_n);
      last_stack_n = n+1;
      for (int64_t m = 0; m<MASS_BINS; m++)
	memset(stacked_merger_rates[m], 0, sizeof(double)*RATIO_BINS);
    }

    //Normalize merger rates and stack
    for (int64_t m = 0; m<MASS_BINS; m++) {
      if (!halo_counts[m]) continue;
      for (int64_t r = 0; r<RATIO_BINS; r++) {
	merger_rates_per_halo[m][r] /= halo_counts[m];
	stacked_merger_rates[m][r] += merger_rates_per_halo[m][r];
      }
    }

    //Print merger rates at current scale factor
    print_mergers(merger_rates_per_halo, n, n+1);

    free_inthash(desc_masses);
    free_inthash(mmp_index);
  }
  munmap(halos, num_halos*sizeof(struct catalog_halo));
  return 0;
}



void print_mergers(double merger_rates[MASS_BINS][RATIO_BINS], int64_t n1, int64_t n2) {
  char buffer[1024];
  double dz = 1.0/scales[n1] - 1.0/scales[n2];
  snprintf(buffer, 1024, "%smerger_rates_a%f_a%f.dat", ((n2>n1+1)? "stacked_" : ""),
	   scales[n1], scales[n2]);
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#Merger Rates / Halo / Delta z / log10 Ratio\n");
  fprintf(out, "#a = %f - %f\n", scales[n1], scales[n2]);
  for (int64_t r = 0; r<RATIO_BINS; r++) {
    fprintf(out, "#Ratio bin %"PRId64": %g - %g\n", r,
	    RATIO_MIN+(double)r/(double)RATIO_BPDEX,
	    RATIO_MIN+(double)(r+1)/(double)RATIO_BPDEX);
  }
  fprintf(out, "#Mass_bin_min Mass_bin_max");
  for (int64_t r = 0; r<RATIO_BINS; r++) fprintf(out, " Ratio_bin_%"PRId64, r);
  fprintf(out, "\n");
  for (int64_t m=0; m<MASS_BINS; m++) {
    fprintf(out, "%g %g",
	    MASS_MIN+(double)m/(double)MASS_BPDEX,
	    MASS_MIN+(double)(m+1)/(double)MASS_BPDEX);
    for (int64_t r=0; r<RATIO_BINS; r++)
      fprintf(out, " %.5e", merger_rates[m][r]*(double)RATIO_BPDEX/(dz));
    fprintf(out, "\n");
  }
  fclose(out);
}

struct catalog_halo *load_catalog(float scale, int64_t *num_halos) {
  int64_t length;
  char buffer[2048];
  snprintf(buffer, 2048, "sfr_catalog_%f.bin", scale);
  FILE *in = fopen(buffer, "r");
  if (in) fclose(in);
  else snprintf(buffer, 2048, "%s/sfr_catalog_%f.bin", OUTBASE, scale);
  struct catalog_halo *p = check_mmap_file(buffer, 'r', &length);
  *num_halos = length / sizeof(struct catalog_halo);
  assert(length == ((*num_halos)*sizeof(struct catalog_halo)));
  return p;
}


int sort_scales(const void *a, const void *b) {
  const float *c = a;
  const float *d = b;
  if (*d > *c) return -1;
  if (*c > *d) return 1;
  return 0;
}

void load_scales(void) {
  char buffer[1024];
  int64_t snap;
  float scale;
  
  snprintf(buffer, 1024, "%s/snaps.txt", INBASE);
  FILE *input = check_fopen(buffer, "r");
  while (fgets(buffer, 1024, input)) {
    if (sscanf(buffer, "%"SCNd64" %f", &snap, &scale) != 2) continue;
    check_realloc_every(scales, sizeof(float), num_scales, 10);
    scales[num_scales] = scale;
    num_scales++;
  }
  fclose(input);

  qsort(scales, num_scales, sizeof(float), sort_scales);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include "check_syscalls.h"
#include "stringparse.h"
#include "inthash.h"
#ifdef ENABLE_HDF5
#include "io_hdf5.c"
#endif


int64_t cam_param = 0;
#define NUM_LABELS 75
#define NUM_PROPS (NUM_LABELS+1)
#define LVMP (NUM_PROPS)
#define SAT 6
#define SM (NUM_PROPS+1)
#define SM_PSC (NUM_PROPS+4)
#define VBIN (NUM_PROPS+2)
#define Q (NUM_PROPS+3)
#define VX 20
#define JX 23
#define AX 48
#define AX500 53
#define BTOA 46
#define BTOA500c 51
#define MP 60
#define X 17

struct halo_property {
  char label[50];
  int important;
  int scaling;
};


struct halo_property properties[NUM_PROPS] = {{"scale",0,0}, {"id",0,0}, {"desc_scale",0,0}, {"desc_id",0,0}, {"num_prog",0,0}, 
                                              {"pid",0,0}, {"upid",0,0}, {"desc_pid",0,0}, {"phantom",0,0}, {"sam_mvir",1,3},
                                              {"mvir",1,3}, {"rvir",0,0}, {"rs",1,1}, {"vrms",1,1}, {"mmp",0,0}, 
                                              {"scale_of_last_MM",1,0}, {"vmax",1,1}, {"x",0,0}, {"y",0,0}, {"z",0,0},
                                              {"-vabs",1,1}, {"vy",0,0}, {"vz",0,0}, {"Jabs",1,5}, {"Jy",0,0}, {"Jz",0,0},
                                              {"Spin",1,0}, {"Breadth_first_ID",0,0}, {"Depth_first_ID",0,0}, {"Tree_root_ID",0,0},
                                              {"Orig_halo_ID",0,0}, {"Snap_num",0,0}, {"Next_coprogenitor_depthfirst_ID",0,0},
                                              {"Last_progenitor_depthfirst_ID",0,0}, {"Last_mainleaf_depthfirst_ID",0,0},
                                              {"Tidal_Force", 1, 0}, {"Tidal_Force_ID", 0, 0},
                                              {"Rs_Klypin",1,1}, {"Mvir_all",1,3}, {"M200b",1,3}, {"M200c",1,3}, {"M500c",1,3},
                                              {"M2500c",1,3}, {"Xoff",1,1}, {"Voff",1,1}, {"Spin_Bullock",1,0}, {"b_to_a",1,0},
                                              {"c_to_a",1,0}, {"Aabs",1,1}, {"Ay",0,0}, {"Az",0,0}, {"b_to_a500c",1,0},
                                              {"c_to_a500c",1,0}, {"A500c_abs",1,1}, {"Ay500c",0,0}, {"Az500c",0,0}, {"TU",1,0},
                                              {"M_pe_Behroozi",1,3}, {"M_pe_Diemer",1,3}, {"Macc",1,3}, {"Mpeak",1,3}, {"Vacc",1,1},
                                              {"Vpeak",1,1}, {"Halfmass_Scale",1,0}, {"Acc_Rate_Inst",1,3}, {"Acc_Rate_100Myr",1,3},
                                              {"Acc_Rate_1Tdyn",1,3}, {"Acc_Rate_2Tdyn",1,3}, {"Acc_Rate_Mpeak",1,3},
                                              {"Mpeak_Scale",1,0}, {"Acc_Scale",1,0}, {"First_Acc_Scale",1,0}, 
                                              {"First_Acc_Mvir",1,3}, {"First_Acc_Vmax",1,1}, {"VmaxMpeak",0,0}, 
                                              {"Tidal_Force_Tdyn",1,0}};

#define DFID 28
#define LDFID 33
#define VMP 74
#define POS 17
#define VPVM 75
#define VM 16
#define VP 62
#define ID 1

struct halo {
  double properties[NUM_PROPS+5];
};
int64_t num_halos=0;

struct reduced_halo {
  int64_t id;
  int64_t num_tree_halos;
  float pos[3];
};

int sort_by_cam_param(const void *a, const void *b) {
  const struct halo *c = a;
  const struct halo *d = b;
  if (c->properties[cam_param] < d->properties[cam_param]) return -1;
  if (c->properties[cam_param] > d->properties[cam_param]) return 1;
  return 0;
}

int sort_by_position(const void *a, const void *b) {
  const struct reduced_halo *c = a;
  const struct reduced_halo *d = b;
  if (c->pos[cam_param] < d->pos[cam_param]) return -1;
  if (c->pos[cam_param] > d->pos[cam_param]) return 1;
  return 0;
}


int64_t forest_mass(struct inthash *fh, int64_t id) {
  int64_t res = ih_getint64(fh, id);
  if (res == IH_INVALID) return 0;
  return res;
}

int main(int argc, char **argv)
{
  int64_t i, j, k, l, n;
  char buffer[1024];

  if (argc < 7) {
#ifndef ENABLE_HDF5
    fprintf(stderr, "Usage: %s hlist box_size x y z /path/to/forests.list\n", argv[0]);
#else
    fprintf(stderr, "Usage: %s argument_ignored box_size x y z /path/to/forests.list hlists\n", argv[0]);
#endif /*ENABLE_HDF5*/
    exit(EXIT_FAILURE);
  }

  struct halo d;
  enum parsetype types[NUM_LABELS];
  void *data[NUM_LABELS];
  SHORT_PARSETYPE;
  for (i=0; i<NUM_LABELS; i++) {
    types[i] = F64;
    data[i] = d.properties+i;
  }
  
  //Load forests
  struct inthash *forests = new_inthash();
  struct inthash *forest_num_halos = new_inthash();
  struct inthash *id_to_index = new_inthash();
  struct inthash *next_forest_halo = new_inthash();
  FILE *in = check_fopen(argv[6], "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    int64_t tree_id, forest_id;
    void *forest_data[2] = {&tree_id, &forest_id};
    enum parsetype forest_types[2] = {D64, D64};
    n = stringparse(buffer, forest_data, forest_types, 2);
    if (n != 2) continue;
    ih_setint64(forests, tree_id, forest_id);
    if (tree_id != forest_id) {
      ih_setint64(next_forest_halo, tree_id, ih_getint64(next_forest_halo, forest_id));
      ih_setint64(next_forest_halo, forest_id, tree_id);
    }
  }
  fclose(in);

  
  struct reduced_halo *halos = NULL;
  double box_size = atof(argv[2]);

#ifndef ENABLE_HDF5
  struct reduced_halo rd;
  in = check_fopen(argv[1], "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    n = stringparse(buffer, data, types, NUM_LABELS);
    if (n<NUM_LABELS) continue;
    d.properties[X+2] += d.properties[VX+2]/100.0;
    if (d.properties[X+2] < 0) d.properties[X+2] += box_size;
    if (d.properties[X+2] > box_size) d.properties[X+2] -= box_size;
    for (i=0; i<3; i++) rd.pos[i] = d.properties[X+i];
    rd.id = d.properties[ID];
    rd.num_tree_halos = d.properties[LDFID] - d.properties[DFID] + 1;
    check_realloc_every(halos, sizeof(struct reduced_halo), num_halos, 1000);
    int64_t forest_id = ih_getint64(forests, rd.id);
    if (forest_id==IH_INVALID) {
      fprintf(stderr, "[Error] corrupt forests file: halo ID %"PRId64" not found.\n", rd.id);
      exit(EXIT_FAILURE);
    }
    ih_addint64(forest_num_halos, forest_id, rd.num_tree_halos);
    ih_setint64(id_to_index, rd.id, num_halos);
    halos[num_halos] = rd;
    num_halos++;
  }
  fclose(in);
#else
  {
    int64_t n;
    void *h5data = NULL;
    for (n=7; n<argc; n++) {
      char *filename = argv[n];
      hid_t hlist = check_H5Fopen(filename, H5F_ACC_RDONLY);
      hid_t id = check_H5Dopen(hlist, "/id", "", filename);
      hid_t id_space = check_H5Dget_space(id);
      hsize_t dataset_length = 0;
      check_H5Sget_simple_extent_dims(id_space, &dataset_length);
      H5Sclose(id_space);
      H5Dclose(id);
      check_realloc_s(halos, sizeof(struct halo), num_halos+dataset_length);
      
#define READ_HDF5(h5name, ctype, htype, location) {			\
	hid_t hid = check_H5Dopen(hlist, h5name, "", filename);		\
	check_realloc_s(h5data, sizeof(ctype), dataset_length);		\
	check_H5Dread(hid, htype, h5data, h5name, "", filename);	\
	H5Dclose(hid);							\
	ctype *h5d_type = h5data;					\
	for (i=0; i<dataset_length; i++)				\
	  halos[num_halos+i].location = h5d_type[i];			\
      }
      
      READ_HDF5("/Depth_first_ID", int64_t, H5T_NATIVE_INT64, id);
      READ_HDF5("/Last_progenitor_depthfirst_ID", int64_t, H5T_NATIVE_INT64, num_tree_halos);
      for (i=num_halos; i<num_halos+dataset_length; i++)
	halos[i].num_tree_halos = halos[i].num_tree_halos - halos[i].id + 1; //num_tree_halos = last_depthfirst_id - depthfirst_id + 1
      READ_HDF5("/id", int64_t, H5T_NATIVE_INT64, id);
      READ_HDF5("/vz", float, H5T_NATIVE_FLOAT, pos[0]);
      READ_HDF5("/z", float, H5T_NATIVE_FLOAT, pos[2]);
      for (i=num_halos; i<num_halos+dataset_length; i++) {
	halos[i].pos[2] += halos[i].pos[0]/100.0; //Redshift-space position
	if (halos[i].pos[2]<0) halos[i].pos[2] += box_size;
	if (halos[i].pos[2]>box_size) halos[i].pos[2] -= box_size;
      }
      READ_HDF5("/y", float, H5T_NATIVE_FLOAT, pos[1]);
      READ_HDF5("/x", float, H5T_NATIVE_FLOAT, pos[0]);
      H5Fclose(hlist);
      for (i=num_halos; i<num_halos+dataset_length; i++) {
	int64_t forest_id = ih_getint64(forests, halos[i].id);
	if (forest_id==IH_INVALID) {
	  fprintf(stderr, "[Error] corrupt forests file: halo ID %"PRId64" not found.\n", halos[i].id);
	  exit(EXIT_FAILURE);
	}
	ih_addint64(forest_num_halos, forest_id, halos[i].num_tree_halos);
	ih_setint64(id_to_index, halos[i].id, i);
      }
      num_halos+=dataset_length;
    }
    if (h5data) free(h5data);
  }
#undef READ_HDF5
#endif /*ENABLE_HDF5*/

  double total_mass[3], mass[3];
  int64_t halo_start[4], halo_end[4];
  int64_t divs[3];
  for (i=0; i<3; i++) divs[i] = atol(argv[i+3]);

  if (divs[2]>1) {
    fprintf(stderr, "[Warning] UniverseMachine currently assumes that box is not divided in z-direction; setting z-divs to 1.\n");
    divs[2] = 1;
  }

  cam_param = 0;
  qsort(halos, num_halos, sizeof(struct reduced_halo), sort_by_position);
  total_mass[0] = mass[0] = 0;
  halo_start[0] = 0;
  halo_end[0] = num_halos;
  int64_t box = 0;
  //forest_mass holds the number of halos in the tree (last_df_id - df_id + 1)
  for (i=0; i<num_halos; i++) total_mass[0] += halos[i].num_tree_halos;
  for (i=0; i<num_halos; i++) {
    double m = forest_mass(forest_num_halos, halos[i].id); //halos[i].properties[VPVM];
    int64_t dx = mass[0]*divs[0]/total_mass[0];
    int64_t dx2 = (mass[0]+m)*divs[0]/total_mass[0];
    mass[0] += m;
    if ((dx2 > dx) || i==halo_end[0]-1) {
      halo_start[1] = halo_start[0];
      halo_end[1] = i+1;
      cam_param = 1;
      qsort(halos+halo_start[1], halo_end[1] - halo_start[1], sizeof(struct reduced_halo), sort_by_position);
      mass[1] = total_mass[1] = 0;
      for (j=halo_start[1]; j<halo_end[1]; j++) total_mass[1] += forest_mass(forest_num_halos, halos[j].id); //halos[j].properties[VPVM];
      for (j=halo_start[1]; j<halo_end[1]; j++) {
	double m = forest_mass(forest_num_halos, halos[j].id); //halos[j].properties[VPVM];
	int64_t dy = mass[1]*divs[1]/total_mass[1];
	int64_t dy2 = (mass[1]+m)*divs[1]/total_mass[1];
	mass[1] += m;
	if ((dy2 > dy) || j==halo_end[1]-1) {
	  halo_start[2] = halo_start[1];
	  halo_end[2] = j+1;
	  cam_param = 2;
	  qsort(halos+halo_start[2], halo_end[2] - halo_start[2], sizeof(struct reduced_halo), sort_by_position);
	  mass[2] = total_mass[2] = 0;
	  double prev_mass = 0;
	  for (k=halo_start[2]; k<halo_end[2]; k++) total_mass[2] += forest_mass(forest_num_halos, halos[k].id); //halos[k].properties[VPVM];
	  for (k=halo_start[2]; k<halo_end[2]; k++) {
	    double m = forest_mass(forest_num_halos, halos[k].id); //halos[k].properties[VPVM];
	      //if (m<1e10) m=0;
	    int64_t dz = mass[2]*divs[2]/total_mass[2];
	    int64_t dz2 = (mass[2]+m)*divs[2]/total_mass[2];
	    mass[2] += m;
	    if ((dz2 > dz) || k==halo_end[2]-1) {
	      halo_start[3] = halo_start[2];
	      halo_end[3] = k+1;
	      double min[3], max[3];
	      int64_t box_num_halos = mass[2]-prev_mass;
	      if (box_num_halos) {
		for (n=0; n<3; n++) min[n] = max[n] = halos[halo_start[3]].pos[n];	      
		for (l=halo_start[3]; l<halo_end[3]; l++) {
		  for (n=0; n<3; n++) {
		    if (min[n] > halos[l].pos[n]) min[n] = halos[l].pos[n];
		    if (max[n] < halos[l].pos[n]) max[n] = halos[l].pos[n];
		  }
		}
		printf("#Box %"PRId64"\n", box);
		printf("#Bounds: %f %f %f - %f %f %f (%"PRId64" halos)\n", min[0], min[1], min[2], max[0], max[1], max[2],
		       box_num_halos);
		for (l=halo_start[3]; l<halo_end[3]; l++) {
		  double m = forest_mass(forest_num_halos, halos[l].id);
		  //printf("%"PRId64" %"PRId64"\n", (int64_t)halos[l].properties[1], box);
		  if (m > 0) {
		    printf("%"PRId64" %"PRId64" %.0lf\n", (int64_t)halos[l].id, box, m);
		    int64_t next_forest_id = ih_getint64(next_forest_halo, halos[l].id);
		    while (next_forest_id != IH_INVALID) {
		      //Note: id_to_index out of date after qsorts.
		      //  It is just being used here to check if all forests are in the hlist.
		      int64_t next_forest_idx = ih_getint64(id_to_index, next_forest_id);
		      if (next_forest_idx == IH_INVALID) {
			fprintf(stderr, "[Error] Forest file does not match hlist; ID %"PRId64" in forest not found in hlist!\n", next_forest_id);
			exit(EXIT_FAILURE);
		      }
		      printf("%"PRId64" %"PRId64" 0\n", next_forest_id, box);
		      next_forest_id = ih_getint64(next_forest_halo, next_forest_id);
		    }
		  }
		}
		prev_mass = mass[2];
		box++;
	      }
	      halo_start[2] = k+1;
	    }
	  }
	  halo_start[1] = j+1;
	}
      }
      halo_start[0] = i+1;
    }
  }
  return 0;
}


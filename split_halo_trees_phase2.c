#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <gsl/gsl_interp.h>
#include <unistd.h>
#include "inthash.h"
#include "make_sf_catalog.h"
#include "cached_io.h"
#include "mt_rand.h"
#include "universe_time.h"
#include "universal_constants.h"
#include "orphans.h"
#include "masses.h"

#define EXTRA_HALO_INFO struct halo *vpeak_halo, *mpeak_halo, *acc_halo, *first_acc_halo; float tidal_av, delta_log_vmax; int64_t orphan_count, orphan_id;
#include "read_tree/read_tree.h"
#include "read_tree/read_tree.c"
#include "check_syscalls.h"


#define MAX_SPLINE_LINESIZE 100000

double box_size = 250;
void process_tree(void);
void load_boxlist(char *fn);
int64_t num_boxes = 0;
char **boundaries = NULL;
struct inthash *boxes = NULL;
struct cached_io **outputs = NULL;
int64_t num_outputs = 0;
float *scales = NULL;
int64_t num_scales = 0;
struct inthash *forest_ids = NULL;

struct halo *orphans = NULL;
int64_t num_orphans = 0;

#define BINS_PER_SCALE 10000
int64_t *scale_to_snap = NULL;
int64_t num_scale_to_snap = 0;

double Om, Ol, h0;

struct spline {
  double scale;
  double value;
  double max_val, min_val;
  double child_low, child_high;
  double low_v, high_v;
  int64_t num_points;
  gsl_interp *spline;
  gsl_interp_accel *accel;
  double *x, *y;
  struct spline *low, *high;
};

struct spline *splines[2] = {0};
int64_t *spline_offsets[2] = {0};
int64_t num_splines[2] = {0};
double scaling[2] = {0};

void open_outputs(int64_t parallel_id);
void close_outputs(void);
void combine_outputs(int64_t combine_ids);
void load_ranks(char *path, int64_t index);
void calc_av_tidal_force(struct halo *h);
void load_scales(char *filename);
void split_halo_trees(int64_t parallel_id, int argc, char **argv, int64_t i_start, int64_t i_step);

int main(int argc, char **argv) {
  int64_t i;
  char buffer[1024];
  int64_t parallel_id = -1, combine_ids = -1;
  int64_t internal_forking = 0;
  pid_t *fork_pids = NULL;
  if (argc<9) {
    printf("Usage: %s box_size Om h path/to/scales.txt path/to/ranks path/to/boxlist parallel_id combine_ids tree.dat ...\n", argv[0]);
    printf("Set parallel_id = -1 and combine_ids = -1 for non-parallel operation.\n");
    exit(EXIT_FAILURE);
  }
  if (strlen(argv[7])>0 && argv[7][0]=='f') {
    internal_forking = 1;
    argv[7]++;
  }
  parallel_id = atol(argv[7]);
  combine_ids = atol(argv[8]);
  
  gen_ff_cache();
  box_size = atof(argv[1]);
  Om = atof(argv[2]);
  Ol = 1.0-Om;
  h0 = atof(argv[3]);
  init_time_table(Om, h0);
  load_scales(argv[4]);
  load_boxlist(argv[6]);
  sprintf(buffer, "%s/Tidal_Force_Tdyn", argv[5]);
  load_ranks(buffer, 0);
  sprintf(buffer, "%s/DeltaLogVmax", argv[5]);
  load_ranks(buffer, 1);
  
  if (combine_ids <= 0) {
    if (!internal_forking) split_halo_trees(parallel_id, argc, argv, 9, 1);
    else {
      check_realloc_s(fork_pids, sizeof(pid_t), parallel_id);
      for (i=0; i<parallel_id; i++) {
	pid_t res = check_fork();
	if (res==0) { //child
	  split_halo_trees(i, argc, argv, 9+i, parallel_id);
	  exit(EXIT_SUCCESS);
	} else {
	  fork_pids[i] = res;
	}
      }
      for (i=0; i<parallel_id; i++) check_waitpid(fork_pids[i]);
      free(fork_pids);
    }
  }
  if (parallel_id < 0) combine_outputs(combine_ids);
  return 0;
}

void split_halo_trees(int64_t parallel_id, int argc, char **argv, int64_t i_start, int64_t i_step) {
  r250_init(87L+1+parallel_id);
  
  open_outputs(parallel_id);
  for (int64_t i=i_start; i<argc; i+=i_step) {
    read_tree(argv[i]);
    process_tree();
    delete_tree();
  }
  close_outputs();
}

void load_scales(char *filename) {
  FILE *input;
  int64_t n, outnum, i;
  float scale;
  char buffer[1024];

  input = check_fopen(filename, "r");
  while (fgets(buffer, 1024, input)) {
    n = sscanf(buffer, "%"SCNd64" %f", &outnum, &scale);
    if (n<2) continue;

    check_realloc_every(scales, sizeof(float), num_scales, 1000);

    if (num_scales) {
      float last_scale = scales[num_scales-1];
      if (scale == last_scale) continue;
      if (scale < last_scale) {
        fprintf(stderr, "[Error] Scale file (%s) not sorted in ascending order!\n", filename);
        exit(EXIT_FAILURE);
      }
    }
    scales[num_scales] = scale;
    num_scales++;
  }
  fclose(input);
  check_realloc_s(scales, sizeof(float), num_scales);

  num_scale_to_snap = (scales[num_scales-1]+0.01)*BINS_PER_SCALE+1;
  check_realloc_s(scale_to_snap, sizeof(int64_t), num_scale_to_snap);
  for (n=0; n<num_scale_to_snap; n++) scale_to_snap[n] = -1;
  for (n=0; n<num_scales; n++) {
    int64_t min_i = (n>0) ? (0.5*(scales[n-1]+scales[n])*BINS_PER_SCALE) : 0;
    int64_t max_i = (n<num_scales-1) ? (0.5*(scales[n+1]+scales[n])*BINS_PER_SCALE) : num_scale_to_snap;
    for (i=min_i; i<max_i; i++) scale_to_snap[i] = n;
  }
}

int64_t snap_of_scale(float scale) {
  assert(scale >= 0 && scale <= (scales[num_scales-1]+0.01));
  int64_t snap = scale_to_snap[(int64_t)(scale*BINS_PER_SCALE)];
  assert(snap > -1);
  return snap;
}


int sort_splines(const void *a, const void *b) {
  const struct spline *c = a;
  const struct spline *d = b;
  if (c->value < d->value) return -1;
  if (c->value > d->value) return 1;
  return 0;
}

struct spline *find_spline(struct spline *s, int64_t ns, double val) {
  int64_t i;
  int64_t best_i = 0;
  for (i=1; i<ns; i++) {
    if (fabs(s[i].value - val) < fabs(s[best_i].value-val))
      best_i = i;
  }
  if (fabs(s[best_i].value-val) > 1e-8) return NULL;
  return s+best_i;
}

void link_splines(struct spline *s, int64_t ns) {
  int64_t i;
  if (!ns) return;
  assert(fabs(s[0].value) < 0.001);
  for (i=0; i<ns; i++) {
    s[i].low = find_spline(s, ns, s[i].child_low);
    s[i].high = find_spline(s, ns, s[i].child_high);
  }
}

void load_ranks(char *path, int64_t index) {
  int64_t i,j;
  char *buffer = NULL;
  check_realloc_s(buffer, sizeof(char), MAX_SPLINE_LINESIZE);
  check_realloc_s(spline_offsets[index], sizeof(int64_t), num_scales+1);
  struct spline s = {0};
  SHORT_PARSETYPE;
  void **data = NULL;
  enum parsetype *pt = NULL;
  spline_offsets[index][0] = 0;
  for (i=0; i<num_scales; i++) {
    sprintf(buffer, "%s/splines_%.5f.list", path, scales[i]);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, MAX_SPLINE_LINESIZE, in)) {
      if (buffer[0] == '\n' || buffer[0] == ' ') continue;
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#scaling =", 10))
	  sscanf(buffer+11, "%lf", scaling+index);
	continue;
      }
      if (sscanf(buffer, "%lf %"SCNd64, &s.value, &s.num_points)!=2) continue;
      s.x = check_realloc(0, sizeof(double)*s.num_points, "Allocating x");
      s.y = check_realloc(0, sizeof(double)*s.num_points, "Allocating y");
      s.scale = scales[i];
      check_realloc_s(data, sizeof(void *), s.num_points*2+8);
      check_realloc_s(pt, sizeof(enum parsetype), s.num_points*2+8);
      data[0] = &s.value;
      pt[0] = F64;
      data[1] = &s.num_points;
      pt[1] = D64;
      data[2] = &s.min_val;
      pt[2] = F64;
      data[3] = &s.max_val;
      pt[3] = F64;
      data[4] = &s.child_low;
      pt[4] = F64;
      data[5] = &s.child_high;
      pt[5] = F64;
      data[6] = &s.low_v;
      pt[6] = F64;
      data[7] = &s.high_v;
      pt[7] = F64;
      for (j=0; j<s.num_points; j++) {
	data[j*2+8] = s.x+j;
	data[j*2+9] = s.y+j;
	pt[j*2+8] = pt[j*2+9] = F64;
      }
      int64_t n = stringparse(buffer, data, pt, s.num_points*2+8);
      if (n!=(s.num_points*2+8)) continue;
      const gsl_interp_type *T = (s.num_points > 4) ? gsl_interp_akima : gsl_interp_cspline;
      s.spline = gsl_interp_alloc(T, s.num_points);
      s.accel = gsl_interp_accel_alloc();
      gsl_interp_init(s.spline, s.x, s.y, s.num_points);
      check_realloc_every(splines[index], sizeof(struct spline), num_splines[index], 1000);
      splines[index][num_splines[index]] = s;
      num_splines[index]++;
    }
    fclose(in);
    spline_offsets[index][i+1] = num_splines[index];
  }
  for (i=0; i<num_scales; i++)
    link_splines(splines[index]+spline_offsets[index][i], spline_offsets[index][i+1]-spline_offsets[index][i]);
  free(buffer);
}


int64_t mmap_copy(char *fn, FILE *out) {
  int64_t length=0;
  void *data = check_mmap_file(fn, 'r', &length);
  if (length > 0) {
    check_fwrite(data, 1, length, out);
    munmap(data, length);
  }
  return length;
}

void combine_outputs(int64_t combine_ids) {
  int64_t i,j, k;
  char buffer[1024];
  for (i=0; i<num_boxes; i++) {
    sprintf(buffer, "catalog_bin/cat.box%"PRId64".dat", i);
    FILE *out = check_fopen(buffer, "w");
    sprintf(buffer, "catalog_bin/offsets.box%"PRId64".txt", i);
    FILE *offsets = check_fopen(buffer, "w");
    fprintf(offsets, "#Output Scale Offset(halos)\n");
    int64_t offset = 0;
    for (j=0; j<num_scales; j++) {
      fprintf(offsets, "%"PRId64" %f %"PRId64"\n", j, scales[j], offset);
      if (combine_ids < 0) {
	sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
	int64_t written = mmap_copy(buffer, out);
	if (written % sizeof(struct catalog_halo)) {
	  fprintf(stderr, "Error: read %"PRId64" bytes from %s, which is not divisible by size of packed halo struct (%"PRId64" bytes)\n", written, buffer, (int64_t)sizeof(struct catalog_halo));
	  exit(EXIT_FAILURE);
	}
	offset += written / sizeof(struct catalog_halo);
      } else {
	for (k=0; k<combine_ids; k++) {
	  sprintf(buffer, "catalog_bin%"PRId64"/cat.box%"PRId64".snap%"PRId64".dat", k, i, j);
	  int64_t written = mmap_copy(buffer, out);
	  if (written % sizeof(struct catalog_halo)) {
	    fprintf(stderr, "Error: read %"PRId64" bytes from %s, which is not divisible by size of packed halo struct (%"PRId64" bytes)\n", written, buffer, (int64_t)sizeof(struct catalog_halo));
	    exit(EXIT_FAILURE);
	  }
	  offset += written / sizeof(struct catalog_halo);
	}
      }
    }
    fprintf(offsets, "#Total halos: %"PRId64"\n", offset);
    fclose(offsets);
    fclose(out);
    for (j=0; j<num_scales; j++) {
      if (combine_ids < 0) {
	sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
	unlink(buffer);
      } else {
	for (k=0; k<combine_ids; k++) {
	  sprintf(buffer, "catalog_bin%"PRId64"/cat.box%"PRId64".snap%"PRId64".dat", k, i, j);
	  unlink(buffer);
	}
      }
    }
  }
  if (combine_ids > -1) {
    for (k=0; k<combine_ids; k++) {
      sprintf(buffer, "catalog_bin%"PRId64, k);
      rmdir(buffer);
    }
  }
}


void open_outputs(int64_t parallel_id) {
  int64_t i,j;
  char buffer[1024];
  num_outputs = num_boxes*num_scales;
  check_realloc_s(outputs, sizeof(struct cached_io *), num_outputs);
  mkdir("catalog_bin", 0755);
  FILE *snaps = NULL;
  cio_set_maximum_memory((int64_t)50*1024*1024*1024);
  if (parallel_id < 1) //0 or -1 OK
    snaps = check_fopen("catalog_bin/snaps.txt", "w");
  if (parallel_id > -1) {
    snprintf(buffer, 1024, "catalog_bin%"PRId64, parallel_id);
    mkdir(buffer, 0755);
  }
  for (i=0; i<num_boxes; i++) {
    for (j=0; j<num_scales; j++) {
      if (parallel_id < 0)
	sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
      else
	sprintf(buffer, "catalog_bin%"PRId64"/cat.box%"PRId64".snap%"PRId64".dat", parallel_id, i, j);
      outputs[i*num_scales+j] = cfopen(buffer, 500000);
      outputs[i*num_scales+j]->clear = 1;
      if (i==0 && parallel_id < 1) {
	fprintf(snaps, "%"PRId64" %f\n", j, scales[j]);
      }
    }
  }
  if (parallel_id < 1) //0 or -1 OK
    fclose(snaps);
}

void close_outputs(void) {
  int64_t i;
  for (i=0; i<num_outputs; i++) cfclose(outputs[i]);
}

void load_boxlist(char *fn) {
  char buffer[1024];
  FILE *in = check_fopen(fn, "r");
  boxes = new_inthash();
  forest_ids = new_inthash();
  num_boxes = -1;
  int64_t hid, box, halo_count;
  SHORT_PARSETYPE;
  enum parsetype t[3] = {D64, D64, D64};
  void *data[3] = {&hid, &box, &halo_count};
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') {
      if (strncmp(buffer, "#Box", 4)==0) {
	num_boxes++;
	check_realloc_s(boundaries, sizeof(char *), (num_boxes+1));
      }
      if (strncmp(buffer, "#Bou", 4)==0) {
	boundaries[num_boxes] = check_strdup(buffer);
      }
      continue;
    }
    if (stringparse(buffer, data, t, 3) != 3) continue;
    ih_setint64(boxes, hid, num_boxes);
    if (halo_count > 0) ih_setint64(forest_ids, hid, 1);
  }
  fclose(in);
  num_boxes++;
}

double vir_density(double a) {
  double x = 1.0/(1.0+a*a*a*Ol/Om)-1.0;
  return ((18*M_PI*M_PI + 82.0*x - 39*x*x)/(1.0+x));
}

double dynamical_time(double a) {
  double vd = vir_density(a) * Om * CRITICAL_DENSITY;
  double dt = 1.0/sqrt((4.0*M_PI*Gc/3.0)*vd)*pow(a, 1.5) / h0; //In units of Mpc / (km/s)
  dt *= HUBBLE_TIME_CONVERSION * 100.0;
  return dt;
}

void calc_av_tidal_force(struct halo *h) {
  struct halo *orig_h = h;
  h->tidal_av = h->tidal_force;
  float av=0, ttime=0;
  float last_time = scale_to_years(h->scale);
  float min_time = last_time - dynamical_time(h->scale);
  float next_time = 0;
  do {
    next_time = (h->prog) ? (0.5*(scale_to_years(h->scale)+scale_to_years(h->prog->scale))) : scale_to_years(h->scale);
    if (next_time < min_time) next_time = min_time;
    float dt = last_time - next_time;
    if (!(dt > 0)) dt = 1;
    av += h->tidal_force*dt;
    ttime += dt;
    h = h->prog;
    last_time = next_time;
  } while (h && (next_time > min_time));
  if (ttime>0) {
    orig_h->tidal_av = av / ttime;
  }
}

void calc_delta_log_vmax(struct halo *h) {
  struct halo *orig_h = h;
  float last_time = scale_to_years(h->scale);
  float min_time = last_time - dynamical_time(h->scale);
  while (h->prog && scale_to_years(h->scale) > min_time) h = h->prog;
  if (h->scale > orig_h->mpeak_halo->scale) h = orig_h->mpeak_halo;
  orig_h->delta_log_vmax = 0;
  if (h->vmax > 0) orig_h->delta_log_vmax = log10(orig_h->vmax / h->vmax);
}


void calc_mass_vmax_peak(struct halo *h) {
  if (h->mpeak_halo) return;
  if (h->prog) {
    calc_mass_vmax_peak(h->prog);
    if (h->vmax > h->prog->vpeak_halo->vmax) h->vpeak_halo = h;
    else h->vpeak_halo = h->prog->vpeak_halo;

    if (h->mvir > h->prog->mpeak_halo->mvir) h->mpeak_halo = h;
    else h->mpeak_halo = h->prog->mpeak_halo;

    if (h->upid>-1) { // If we are a subhalo
      h->acc_halo = h->prog->acc_halo;
      h->first_acc_halo = h->prog->first_acc_halo;
    } else {
      h->acc_halo = h;
      h->first_acc_halo = h;
      struct halo *hpf = h->prog->first_acc_halo;
      if (hpf && (hpf->id != h->prog->id) && (hpf->mpeak_halo->mvir*2.0 > h->mvir))
	h->first_acc_halo = hpf;
    }
  } else {
    h->first_acc_halo = h->acc_halo = h->mpeak_halo = h->vpeak_halo = h;
  }
}

struct halo *halo_from_index(int64_t i) {
  if (i < all_halos.num_halos) return all_halos.halos+i;
  return (orphans + (i-all_halos.num_halos));
}

int sort_by_dfid_and_orphanid(const void *a, const void *b) {
  const int64_t *e = a;
  const int64_t *f = b;
  struct halo *c = halo_from_index(*e);
  struct halo *d = halo_from_index(*f);
  if (c->depth_first_id < d->depth_first_id) return -1;
  if (c->depth_first_id > d->depth_first_id) return 1;
  if (c->orphan_id < d->orphan_id) return -1;
  if (c->orphan_id > d->orphan_id) return 1;
  return 0;
}

int sort_by_dfid(const void *a, const void *b) {
  const int64_t *e = a;
  const int64_t *f = b;
  struct halo *c = halo_from_index(*e);
  struct halo *d = halo_from_index(*f);
  if (c->depth_first_id < d->depth_first_id) return -1;
  if (c->depth_first_id > d->depth_first_id) return 1;
  return 0;
}

double perc_to_normal(double p) {
  double x1 = -14;
  double x2 = 14;
  while (x2-x1 > 1e-7) {
    double half = 0.5*(x1+x2);
    double perc = 0.5*(1.0+erf(half));
    if (perc > p) { x2 = half; }
    else { x1 = half; }    
  }
  return ((x1+x2)*M_SQRT1_2);
}

double rand_rank(double value) {
  double neg_val = copysign(value, -1.0);
  double cumulative = 0.5*(1.0+erf(neg_val * M_SQRT1_2));
  double ra_perc = dr250()*cumulative;
  double ra_sd = perc_to_normal(ra_perc);
  return copysign(ra_sd, value);
}

double _spline_rank(struct spline *s, double lvmp, double val) {
#define EVAL(sp) (gsl_interp_eval(sp->spline, sp->x, sp->y, lvmp, sp->accel))
  if (!s || s->x[0] > lvmp || s->x[s->num_points-1] < lvmp) return 0;
  struct spline *s2 = s;
  int64_t low_flag = 0, high_flag = 0, is_low = 0;
  while (1) {
    val = (val - s->min_val) / (s->max_val - s->min_val);
    double res = EVAL(s);
    if (val > res) {
      s2 = s->high;
      val = (val-res)/(1.0 - res);
      is_low = 0;
      high_flag = 1;
    } else {
      s2 = s->low;
      val /= (res > 0) ? res : 1.0;
      is_low = 1;
      low_flag = 1;
    }
    if (!s2 || s2->x[0] > lvmp || s2->x[s2->num_points-1] < lvmp) break;
    s = s2;
  }

  if (!low_flag && !high_flag) return rand_rank(0);
  if (!low_flag) {
    double low_perc = 0.5*(1.0+erf(s->value*M_SQRT1_2));
    double perc = low_perc + val*(1.0-low_perc);
    return perc_to_normal(perc-1e-8);
  } else if (!high_flag) {
    double high_perc = 0.5*(1.0+erf(s->value*M_SQRT1_2));
    double perc = val*high_perc;
    return perc_to_normal(perc+1e-8);
  }
  if (is_low) return (s->low_v + val*(s->value - s->low_v));
  return (s->value + val*(s->high_v - s->value));
#undef EVAL
}

double spline_rank(int64_t snap, double lvmp, double val, int64_t index) {
  struct spline *s = splines[index]+spline_offsets[index][snap];
  return(_spline_rank(s, lvmp, val));
}

double halo_dist(struct halo *h1, struct halo *h2) {
  int64_t i;
  double ds = 0, dx;
  for (i=0; i<3; i++) {
    dx = fabs(h1->pos[i]-h2->pos[i]);
    if (dx > box_size/2.0) dx = box_size - dx;
    ds += dx*dx;
  }
  return sqrt(ds);
}

double host_dist(struct halo *h) {
  if (!h->mmp)
    if (h->desc && h->desc->prog) return (halo_dist(h, h->desc->prog)/(1e-3*h->desc->prog->rvir));

  if (h->upid < 0) return 2;
  if (!h->uparent) return 1.01;
  return (halo_dist(h, h->uparent)/(1e-3*h->uparent->rvir));
}

double calc_vmax(struct halo *h) {
  double vacc = h->first_acc_halo->vmax;
  double mratio = h->mvir / h->first_acc_halo->mvir;
  return vacc*pow(mratio, 0.3)*pow(0.5+0.5*mratio, -0.4);
}

double check_boundedness(struct halo *h, struct halo *p) {
  double hubble = 100.0*sqrt(Om*pow(h->scale, -3.0) + Ol);
  double dv2 = 0, ds, dr=0;
  int64_t i;
  for (i=0; i<3; i++) {
    ds = h->pos[i]-p->pos[i];
    if (ds > box_size/2.0) ds -= box_size;
    if (ds < -box_size/2.0) ds += box_size;
    dr += ds*ds;
    ds = h->vel[i]-p->vel[i] + hubble*h->scale*ds;
    dv2 += ds*ds;
  }
  dr = sqrt(dr);
  
  if (!(p->rvir>0 && p->rs>0)) return 1e10;
  double d = 1.0/(log1p(p->rvir / p->rs) - p->rvir / (p->rvir + p->rs));
  double pot = 0;
  if (dr*1e3 > p->rvir) {
    pot = 1.0 / dr;
  } else {
    pot = 1.0 / (p->rvir/1e3);
    pot += d*log1p(dr*1e3/p->rs)/dr;
    pot -= d*log1p(p->rvir/p->rs)/(p->rvir/1e3);
  }
  pot *= Gc*p->mvir/h->scale; //Now physical (km/s)^2
  if (!(pot > 0)) return 1e10;
  return (0.5*dv2/pot);
}

void add_orphan_track(struct halo *h) {
  int64_t i;
  if (!h->desc || !h->desc->prog) return;
  if (h->first_acc_halo == h) return;
  if (h->vmax > h->mpeak_halo->vmax) return;
  struct halo *p = h->desc->prog;
  h->mmp = 1;
  struct halo nh = *h;
  int64_t done = 0;
  double vmax_fac = h->vmax / calc_vmax(h);
  double unbound = 0, unbound2 = 0;

  while (p->desc && !done) {
    struct halo *ep = p, *ep2 = p;
    unbound = check_boundedness(&nh, ep);
    while (ep2->pid > -1 && ep2->parent) {
      unbound2 = check_boundedness(&nh, ep2->parent);
      if (unbound2 < unbound) {
	unbound = unbound2;
	ep = ep2->parent;
      }
      ep2 = ep2->parent;
    }
    if (p->upid > -1 && p->uparent) {
      unbound2 = check_boundedness(&nh, p->uparent);
      if (unbound2 < unbound) {
	unbound = unbound2;
	ep = p->uparent;
      }
    }

    evolve_orphan_halo(&nh, ep, ep->desc);
    p = p->desc;
    assert(p->id < ORPHAN_ID_OFFSET);
    nh.upid = nh.pid = p->id;
    nh.parent = nh.uparent = p;
    nh.id += ORPHAN_ID_OFFSET;
    if (p==h->desc) h->descid = nh.id;
    nh.vmax = vmax_fac*calc_vmax(&nh);
    
    if (nh.vmax < ORPHAN_VMAX_RATIO_LIMIT*nh.mpeak_halo->vmax || unbound > 2.0) {
      if (p->desc) {
	nh.descid = p->desc->id;
	nh.mmp = 0;
      } else {
	nh.descid = -1;
      }
      done = 1;
    } else {
      if (p->desc) nh.descid = nh.id + ORPHAN_ID_OFFSET;
      else nh.descid = -1;
    }
    p->orphan_count++;
    nh.orphan_id = p->orphan_count;
    nh.depth_first_id = p->depth_first_id;

    nh.prog = h; /*Ok, since mass and vmax monotonically decrease */
    calc_delta_log_vmax(&nh);

    //Wrap orphan position to be close to parent
    //Should only do so along X and Y directions
    for (i=0; i<2; i++) {
      if (nh.pos[i]-p->pos[i] > box_size/2.0) nh.pos[i] -= box_size; 
      if (nh.pos[i]-p->pos[i] < -box_size/2.0) nh.pos[i] += box_size;
    }

    check_realloc_every(orphans, sizeof(struct halo), num_orphans, 1000);
    orphans[num_orphans] = nh;
    num_orphans++;
  }
}

void process_tree(void) {
  int64_t j;
  struct halo *h;
  struct catalog_halo ch = {0};
  int64_t *process_order = NULL;

  num_orphans = 0;
  check_realloc_s(process_order, sizeof(int64_t), all_halos.num_halos);
  for (j=0; j<all_halos.num_halos; j++) {
    h = &(all_halos.halos[j]);
    calc_mass_vmax_peak(h);
    calc_av_tidal_force(h);
    calc_delta_log_vmax(h);
    process_order[j] = j;
  }

  qsort(process_order, all_halos.num_halos, sizeof(int64_t), sort_by_dfid);

  for (j=0; j<all_halos.num_halos; j++) {
    h = all_halos.halos+process_order[j];
    if (h->mmp) continue;
    if (h->mpeak_halo->vmax < ORPHAN_VMAX_LIMIT) continue;
    if (h->upid < 0) continue;
    if (h->vmax > ORPHAN_VMAX_RATIO_LIMIT*h->mpeak_halo->vmax)
      add_orphan_track(h);
  }

  check_realloc_s(process_order, sizeof(int64_t), (all_halos.num_halos+num_orphans));
  for (j=0; j<num_orphans; j++)
    process_order[j+all_halos.num_halos] = j+all_halos.num_halos;
  qsort(process_order, all_halos.num_halos+num_orphans, sizeof(int64_t), sort_by_dfid_and_orphanid);

  for (j=0; j<all_halos.num_halos+num_orphans; j++) {
    h = halo_from_index(process_order[j]);
    ch.id = h->id;
    ch.descid = h->descid;
    ch.upid = h->upid;
    memcpy(ch.pos, h->pos, sizeof(float)*3);
    memcpy(ch.pos+3, h->vel, sizeof(float)*3);
    ch.vmp = h->mpeak_halo->vmax;
    ch.lvmp = log10(ch.vmp);
    ch.mp = h->mpeak_halo->mvir;
    ch.m = h->mvir;
    ch.v = h->vmax;
    ch.r = h->rvir;
    ch.t_tdyn = h->tidal_av;
    ch.uparent_dist = host_dist(h);
    ch.ra = normal_random(0, 1);
    ch.flags = 0;
    if (h->id > ORPHAN_ID_OFFSET) ch.flags |= ORPHAN_FLAG;
    if (ih_getint64(forest_ids, ch.id) != IH_INVALID) ch.flags |= FOREST_FLAG;

    int64_t snap = snap_of_scale(h->scale);
    int64_t box = ih_getint64(boxes, h->tree_root_id);
    assert(box >= 0 && box < num_boxes);
    struct cached_io *cio = outputs[box*num_scales+snap];

    ch.rank1 = spline_rank(snap, ch.lvmp, h->delta_log_vmax, 1);
    ch.rank2 = spline_rank(snap, ch.lvmp, ch.t_tdyn / pow(ch.vmp, scaling[0]), 0);
    cfwrite(cio, &ch, sizeof(struct catalog_halo));
  }
  free(process_order);
}



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <assert.h>
#include "make_sf_catalog.h"
#include "matrices.h"
#include "distance.h"
#include "mt_rand.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "config.h"
#include "version.h"
#include "all_luminosities.h"
#include "lightcone_cache.h"
#include "inthash.h"
#include "universe_time.h"

struct inthash *ih = NULL;

int sort_by_desc(const void *a, const void *b);
struct catalog_halo *load_catalog(float scale, int64_t *num_halos);
void free_catalog(struct catalog_halo *h, int64_t num_halos);

int main(int argc, char **argv) {
  int64_t centrals_only = -1;
  double min_hm=0, max_hm=1e17, min_sm=0, max_sm=1e17, min_sfr=0, max_sfr=1e17;
  struct halo_cat *halo_cats = NULL;

  if (argc < 4) {
    printf("%s", INFO_STRING);
    printf("Usage: %s um.cfg min_hm max_hm min_sm max_sm min_sfr max_sfr central?\n", argv[0]);
    exit(EXIT_SUCCESS);
  }

  do_config(argv[1]);

  min_hm = atof(argv[2]);
  max_hm = atof(argv[3]);
  if (min_hm < 100) min_hm = pow(10, min_hm);
  if (max_hm < 100) max_hm = pow(10, max_hm);
  min_hm *= h0; //Convert to Msun/h
  max_hm *= h0;

  if (argc>5) {
    min_sm = atof(argv[4]);
    max_sm = atof(argv[5]);
    if (min_sm < 100) min_sm = pow(10, min_sm);
    if (max_sm < 100) max_sm = pow(10, max_sm);
  }

  if (argc>7) {
    min_sfr = atof(argv[6]);
    max_sfr = atof(argv[7]);
  }

  if (argc>8) centrals_only = atoll(argv[8]);

  init_time_table(Om, h0);
  
  //Load snaps.txt
  load_halos(&halo_cats);
  int64_t num_halos = 0;
  int64_t num_snaps = halo_cats[0].num_snaps;

  //Setup time table; note that halo_cat[0] is z=0
  float *dt = NULL;
  check_realloc_s(dt, sizeof(float), num_snaps);
  for (int64_t n=0; n<num_snaps; n++) {
    float a1 = (n<num_snaps-1) ? 0.5*(halo_cats[n].scale+halo_cats[n+1].scale) : 0;
    float a2 = (n>0) ? 0.5*(halo_cats[n].scale+halo_cats[n-1].scale) : halo_cats[n].scale;
    dt[n] = scale_to_years(a2) - scale_to_years(a1);
  }

  //Load first catalog and find matching halos
  struct catalog_halo *h = load_catalog(halo_cats[0].scale, &num_halos);
  struct catalog_halo *target_halos = NULL;
  int64_t num_targets = 0;
  ih = new_inthash();
  
  for (int64_t i=0; i<num_halos; i++) {
    if (h[i].mp >= min_hm && h[i].mp < max_hm &&
	h[i].sm >= min_sm && h[i].sm < max_sm &&
	h[i].sfr >= min_sfr && h[i].sfr < max_sfr) {
      if (h[i].upid>-1 && centrals_only==1) continue;
      if (h[i].upid<0 && centrals_only==0) continue;
      check_realloc_every(target_halos, sizeof(struct catalog_halo), num_targets, 1000);
      target_halos[num_targets] = h[i];
      ih_setint64(ih, h[i].id, num_targets);
      num_targets++;
    }
  }
  free_catalog(h, num_halos);
  fprintf(stderr, "%"PRId64" target halos found.\n", num_targets);

  //Start building trees
  int64_t *halo_counts = NULL; //Number of halos at each snapshot corresponding to each tree
  check_realloc_s(halo_counts, sizeof(int64_t), num_targets*num_snaps);
  memset(halo_counts, 0, sizeof(int64_t)*num_targets*num_snaps);

  struct catalog_halo *trees = NULL;
  int64_t num_halo_trees = num_targets;
  check_realloc_s(trees, sizeof(struct catalog_halo), num_halo_trees+1000);
  memcpy(trees, target_halos, sizeof(struct catalog_halo)*num_halo_trees);
  for (int64_t i=0; i<num_halo_trees; i++) halo_counts[i] = 1;

  int64_t *halo_offsets = NULL;
  check_realloc_s(halo_offsets, sizeof(int64_t), (num_snaps+1));
  halo_offsets[0] = 0;
  halo_offsets[1] = num_halo_trees;
  for (int64_t snap=1; snap<num_snaps; snap++) {
    fprintf(stderr, "Snap %"PRId64" (a=%f).\n", snap, halo_cats[snap].scale);
    struct catalog_halo *h = load_catalog(halo_cats[snap].scale, &num_halos);
    int64_t new_halos = 0;
    struct inthash *nih = new_inthash();
    for (int64_t i=0; i<num_halos; i++) {
      if (h[i].flags & IGNORE_FLAG) continue;
      int64_t res = ih_getint64(ih, h[i].descid);
      if (res >=0 && res<num_targets) {
	ih_setint64(nih, h[i].id, res);
	halo_counts[snap*num_targets+res]++;
	check_realloc_every(trees, sizeof(struct catalog_halo), num_halo_trees, 1000);
	trees[num_halo_trees] = h[i];
	num_halo_trees++;
	new_halos++;
      }
    }
    free_catalog(h, num_halos);
    if (new_halos)
      qsort(trees + halo_offsets[snap], new_halos, sizeof(struct catalog_halo), sort_by_desc);
    else break;
    halo_offsets[snap+1] = num_halo_trees;
    free_inthash(ih);
    ih = nih;
  }

  printf("#Scale SM_formed Z/Zsun Halo_Mass_Peak Galaxy_SM Galaxy_SFR Halo_UPID Halo_ID Halo_DescID\n");
  for (int64_t i=0; i<num_targets; i++) {
    printf("#Halo %"PRId64" (HM=%g; SM=%g; SFR=%g; UPID=%"PRId64")\n", trees[i].id, trees[i].mp/h0, trees[i].sm, trees[i].sfr, trees[i].upid);
    for (int64_t snap=0; snap<num_snaps; snap++) {
      for (int64_t j=0; j<halo_counts[snap*num_targets+i]; j++) {
	struct catalog_halo *th = trees + halo_offsets[snap];
	float sm = th->sfr * dt[snap];
	float Z = metallicity(log10(th->sm), 1.0/halo_cats[snap].scale-1.0);
	printf("%f %g %g %g %g %g %"PRId64" %"PRId64" %"PRId64"\n", halo_cats[snap].scale, sm, Z, th->mp/h0, th->sm, th->sfr, th->upid, th->id, th->descid);
	halo_offsets[snap]++;
	assert(halo_offsets[snap] <= halo_offsets[snap+1]);
      }
    }
    printf("\n");
  }
  return 0;
}


int sort_by_desc(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  int64_t desc_a = ih_getint64(ih, c->descid);
  int64_t desc_b = ih_getint64(ih, d->descid);
  if (desc_a < desc_b) return -1;
  if (desc_b < desc_a) return 1;
  return 0;
}

struct catalog_halo *load_catalog(float scale, int64_t *num_halos) {
  char buffer[1024];
  snprintf(buffer, 1024, "%s/sfr_catalog_%f.bin", OUTBASE, scale);
  struct catalog_halo *h = check_mmap_file(buffer, 'r', num_halos);
  if ((*num_halos) % sizeof(struct catalog_halo)) {
    fprintf(stderr, "[Error] Size of file %s (%"PRId64") not divisible by halo size (%"PRId64")\n", buffer, *num_halos,
	    (int64_t)sizeof(struct catalog_halo));
    exit(EXIT_FAILURE);
  }
  (*num_halos) /= sizeof(struct catalog_halo);
  return h;
}

void free_catalog(struct catalog_halo *h, int64_t num_halos) {
  munmap(h, num_halos*sizeof(struct catalog_halo));
}

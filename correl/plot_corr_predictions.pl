#!/usr/bin/perl -w
use lib qw(../perl);
use XmGrace;
use Common;

my @m = (10, 10.5, 11);
our $dir = "generic_pred";

for my $m (@m) {
    my %text = (10 => '10\S10\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt, 
		10.5 => '10\S10.5\N M'.$sun_txt.' < M\s*\N < 10\S11\N M'.$sun_txt,
		11 => '10\S11\N M'.$sun_txt.' < M\s*\N');
    my $md = $m;
    $md =~ s/\./_/g;
    my $mhigh = ($m > 10.7) ? 13 : $m+0.5;
    new XmGrace(xlabel => $rp_label,
		ylabel => $wp_label, 
		xmin => 0.1, xmax => 10,
		ymin => 9, ymax => 4000,
		legend2xy => "0.8, 0.31",
		legend2vg => "0.04",
		xmajortick => 10,
		xminorticks => 9,
		xscale => "Logarithmic",
		yscale => "Logarithmic",
		legendxy => "0.8, 0.78",
		ymajortick => 10,
		yminorticks => 9,
		fading_colors => 1,
		text => [{text => $text{$m}.'\n\v{-0.2}\xs\f{}\sz\N\v{-0.2} = 1000 km s\S-1\N\n\v{-0.4}\xp\f{}\smax\N\v{-0.4} = 80 Mpc h\S-1\N', 
			  pos => "0.3, 0.29",
			 },
			 {text => "Model Predictions", pos => "0.8, 0.79"},
],
		data => [{data => "file[0,1]:$dir/pred_z0.1_${m}_${mhigh}.dat",
			  lcolor => 1},
			 {data => "file[0,1]:$dir/pred_z1.0_${m}_${mhigh}.dat",
			  lcolor => 7, lstyle => 3},
			 {data => "file[0,1]:$dir/pred_z2.0_${m}_${mhigh}.dat",
			  lcolor => 13, lstyle => 2},
			 {data => "file[0,3]:$dir/pred_z0.1_${m}_${mhigh}.dat",
			   lcolor => 2},
			 {data => "file[0,3]:$dir/pred_z1.0_${m}_${mhigh}.dat",
			  lcolor => 8, lstyle => 3},
			 {data => "file[0,3]:$dir/pred_z2.0_${m}_${mhigh}.dat",
			  lcolor => 14, lstyle => 2},
			 {data => "file[0,2]:$dir/pred_z0.1_${m}_${mhigh}.dat",
			   lcolor => 3},
			 {data => "file[0,2]:$dir/pred_z1.0_${m}_${mhigh}.dat",
			  lcolor => 9, lstyle => 3},
			 {data => "file[0,2]:$dir/pred_z2.0_${m}_${mhigh}.dat",
			  lcolor => 15, lstyle => 2},
			 {data => "0 0", legend => "Quenched", lcolor => 2},
			 {data => "0 0", legend => "All", lcolor => 1},
			 {data => "0 0", legend => "Star-Forming", lcolor => 3},
			 {data => "0 0", legend2 => "z = 0", lcolor => 1},
			 {data => "0 0", legend2 => "z = 1", lcolor => 7, lstyle => 3},
			 {data => "0 0", legend2 => "z = 2", lcolor => 13, lstle => 2},
		])->write_pdf("AGR/corr_pred_m$md.agr");
}
			 
			 



for my $m (@m) {
    my %text = (10 => '10\S10\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt, 
		10.5 => '10\S10.5\N M'.$sun_txt.' < M\s*\N < 10\S11\N M'.$sun_txt,
		11 => '10\S11\N M'.$sun_txt.' < M\s*\N');
    my $md = $m;
    $md =~ s/\./_/g;
    my $mhigh = ($m > 10.7) ? 13 : $m+0.5;
    new XmGrace(xlabel => $rp_label,
		ylabel => 'w\sp\N(R\sp\N) / w\sp,All\N(R\sp\N)', 
		xmin => 0.1, xmax => 10,
		ymin => 0.09, ymax => 10,
		legend2xy => "1.0, 0.31",
		legend2vg => "0.04",
		xmajortick => 10,
		xminorticks => 9,
		xscale => "Logarithmic",
		yscale => "Logarithmic",
		legendxy => "0.8, 0.78",
		ymajortick => 10,
		yminorticks => 9,
		fading_colors => 1,
		text => [{text => $text{$m}.'\n\v{-0.2}\xs\f{}\sz\N\v{-0.2} = 1000 km s\S-1\N\n\v{-0.4}\xp\f{}\smax\N\v{-0.4} = 80 Mpc h\S-1\N', 
			  pos => "0.3, 0.29",
			 },
			 {text => "Model Predictions", pos => "0.8, 0.79"},
],
		data => [#{data => "0.1 1\n10 1", lcolor => 1},
			 {data => XmGrace::Differential("file[0,3]:$dir/pred_z0.1_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z0.1_${m}_${mhigh}.dat", "relative"), lcolor => 2},
			 {data => XmGrace::Differential("file[0,3]:$dir/pred_z1.0_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z1.0_${m}_${mhigh}.dat", "relative"), lcolor => 8, lstyle => 3},
			 {data => XmGrace::Differential("file[0,3]:$dir/pred_z2.0_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z2.0_${m}_${mhigh}.dat", "relative"), lcolor => 14, lstyle => 2},
			 {data => XmGrace::Differential("file[0,2]:$dir/pred_z0.1_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z0.1_${m}_${mhigh}.dat", "relative"), lcolor => 3},
			 {data => XmGrace::Differential("file[0,2]:$dir/pred_z1.0_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z1.0_${m}_${mhigh}.dat", "relative"), lcolor => 9, lstyle => 3},
			 {data => XmGrace::Differential("file[0,2]:$dir/pred_z2.0_${m}_${mhigh}.dat", "file[0,1]:$dir/pred_z2.0_${m}_${mhigh}.dat", "relative"), lcolor => 15, lstyle => 2},
			 {data => "0 0", legend => "Quenched", lcolor => 2},
			 {data => "0 0", legend => "Star-Forming", lcolor => 3},
			 {data => "0 0", legend2 => "z = 0", lcolor => 1},
			 {data => "0 0", legend2 => "z = 1", lcolor => 7, lstyle => 3},
			 {data => "0 0", legend2 => "z = 2", lcolor => 13, lstle => 2},
		])->write_pdf("AGR/corr_pred_diff_m$md.agr");
}
			 
			 

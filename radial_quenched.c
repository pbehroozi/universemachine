#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "inthash.h"

#define HR_MIN -1.4
#define HR_MAX 0
#define HR_BINS 7
#define HR_BPDEX 5

double BOX_SIZE = 250;

double projected_distance2(struct catalog_halo *h1, struct catalog_halo *h2) {
  int64_t i;
  double dx=0, ds=0;
  for (i=0; i<2; i++) {
    dx = fabs(h1->pos[i]-h2->pos[i]);
    if (dx*2 > BOX_SIZE) dx = BOX_SIZE-dx;
    ds += dx*dx;
  }
  return ds;
}

int main(int argc, char **argv) {
  int64_t i, j, new_length;
  if (argc<2) {
    printf("Usage: %s sfr_list.bin\n", argv[0]);
    exit(1);
  }
  
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);


  double total_counts[3][HR_BINS] = {0};
  double q_counts[3][HR_BINS] = {0};
  double m_min = pow(10, 9.7);
  double m_max = pow(10, 10.5);
  double r_min = pow(10, HR_MIN*2.0);
  double r_max = pow(10, HR_MAX*2.0);
  double hbin_1 = pow(10, 12.3)*0.68;
  double hbin_2 = pow(10, 13.2)*0.68;
  double hbin_3 = pow(10, 14.1)*0.68;
  
  struct inthash *ih=new_inthash();
  for (i=0; i<num_p; i++)
    ih_setval(ih, p[i].id, p+i);
  
  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    float ssfr = 0;
    if (tp->flags & IGNORE_FLAG) continue;
    if (tp->upid < 0) continue;
    if (tp->obs_sm > 0)
      ssfr = tp->obs_sfr / tp->obs_sm;
    if (tp->obs_sm < m_min || tp->obs_sm > m_max) continue;
    struct catalog_halo *host = ih_getval(ih, tp->upid);
    assert(host);
    double distance = projected_distance2(host, tp)/(host->r*host->r*1e-6);
    if (distance < r_min || distance > r_max ) continue;
    int64_t rbin= log10(distance/r_min)*((double)HR_BPDEX/2.0);
    if (rbin < 0) continue;
    if (rbin >= HR_BINS) continue;
    if (host->m < hbin_1) continue;
    int64_t hbin = 0;
    if (host->m > hbin_3) hbin = 2;
    else if (host->m > hbin_2) hbin = 1;

    total_counts[hbin][rbin]++;
    if (ssfr < 1e-11)
      q_counts[hbin][rbin]++;
  }
  munmap(p, new_length);

  printf("#R/Rvir 12.3-13.2 13.2-14.1 14.1-15.0\n");
  for (i=0; i<HR_BINS; i++) {
    double r1 = pow(10, HR_MIN+(double)i/(double)HR_BPDEX);
    double r2 = pow(10, HR_MIN+(double)(i+1)/(double)HR_BPDEX);
    double r = sqrt(r1*r1+r2*r2);
    printf("%e", r);
    for (j=0; j<3; j++) {
      double qf = (total_counts[j][i] > 0) ? q_counts[j][i]/total_counts[j][i] : 0;
      printf(" %e", qf);
    }
    printf("\n");
  }
  return 0;
}

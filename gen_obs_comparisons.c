#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include <sys/stat.h>
#include "check_syscalls.h"
#include "observations.h"
#include "sf_model.h"
#include "make_sf_catalog.h"
#include "corr_lib.h"
#include "config.h"
#include "config_vars.h"
#include "inline_analysis.h"
#include "distance.h"
#include "universe_time.h"
#include "mt_rand.h"
#include <sys/mman.h>

float *shared_data;
#define shared(a,b) float *a = NULL;
#include "shared.template.h"
#include "pshared.template.h"
#undef shared
int64_t num_scales, shared_data_length, *galaxy_offsets, *offsets;
float *scales, *bounds, *max_bounds;
struct catalog_galaxy *galaxies;
FILE *logfile = NULL;

char *obs_data_types_short[NUM_TYPES] = {"smf", "uvlf", "qf", "qf11", "qf_uvj", "ssfr", "csfr", "csfr_(uv)", "correlation", "cross_correlation", "conformity", "lensing", "cdens_fsf", "cdens_ssfr_sf", "uvsm", "irx"};

int main(int argc, char **argv) {
  int64_t i,j, length;
  char subtypes[4] = {'a', 's', 'q', 'c'};
  if (argc < 3) {
    fprintf(stderr, "Usage: %s um.cfg file <redo postprocessing?> <recalc_chi2>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  struct sf_model_allz *m = NULL;
  float *sd = NULL;
  struct observation *obsdata = NULL;
  int64_t num_obs = 0, ns =0, sds = 0;
  int64_t redo_post = 0;
  int64_t recalc_chi2 = 0;
  logfile = stderr;
  
  do_config(argv[1]);
  r250_init(87L);
  init_cosmology(Om, Ol, h0);
  init_time_table(Om, h0);

  if (argc > 3) redo_post = atol(argv[3]);
  if (argc > 4) recalc_chi2 = atol(argv[4]);
  
  void *data = check_mmap_file(argv[2], 'c', &length);
  m = data;
  int64_t offset = sizeof(struct sf_model_allz);
  memcpy(&ns, data+offset, sizeof(int64_t));
  offset += sizeof(int64_t);  
  memcpy(&sds, data+offset, sizeof(int64_t));
  offset += sizeof(int64_t);  
  memcpy(&num_obs, data+offset, sizeof(int64_t));
  offset += sizeof(int64_t);
  
  scales = data+offset;
  offset += sizeof(float)*ns;
  sd = data+offset;
  offset += sds;
  obsdata = data+offset;

  printf("#Model:");
  for (i=0; i<NUM_PARAMS; i++)
    printf(" %.12f", m->params[i]);
  printf(" 0 %.12f\n", CHI2(*m));
  printf("#Type Subtype Z1 Z2 step1 step2 SM1 SM2 smb1 smb2 R1 R2 Val Err_h Err_l Model_Val Chi2\n");
  for (i=0; i<num_obs; i++) {
    struct observation *o = obsdata+i;
    printf("%s %c %f %f %"PRId64" %"PRId64" %f %f %"PRId64" %"PRId64" %f %f %e %f %f %e %e\n", obs_data_types_short[o->type], subtypes[(int)o->subtype], o->z_low, o->z_high, o->step1, o->step2, o->sm_low, o->sm_high, o->smb1, o->smb2, o->r_low, o->r_high, o->val, o->err_h, o->err_l, o->model_val, o->chi2);
  }

  num_scales = ns;
  shared_data_length = offset = 0;
#define shared(a,b) shared_data_length += (b)*sizeof(float);
#include "shared.template.h"
  if (POSTPROCESSING_MODE) {
#include "pshared.template.h"
  }
#undef shared
  shared_data = sd;
#define shared(a,b) { a = shared_data + offset; offset += (b); }
#include "shared.template.h"
  if (POSTPROCESSING_MODE) {
#include "pshared.template.h"
  }
#undef shared

  //Testing the Chi2 calculation code
  struct observation *test_obs = NULL;
  int64_t num_test_obs = 0;
  if (recalc_chi2) {
    load_observations(&test_obs, &num_test_obs);
    printf("chi2: %f\n", calc_chi2(test_obs, num_test_obs, *m));
  }
  

  
  if (POSTPROCESSING_MODE && redo_post) {
    inline_postprocessing();
    if (redo_post == 2) {
      char buffer[2048];
      snprintf(buffer, 2048, "%s.rp", argv[2]);
      FILE *out = check_fopen(buffer, "w");
      fwrite(data, 1, length, out);
      fclose(out);
    }
  }

#define NUM_OUTPUTS SMHM_NUM_ZS
  int64_t snaps[NUM_OUTPUTS] = {0}, wl_snaps[NUM_WL_ZS] = {0}, wl_j, smhm_j;
  double zs[NUM_OUTPUTS] = SMHM_ZS; 
  double wl_zs[NUM_WL_ZS] = WL_ZS;
  
  for (i=0; i<num_scales; i++) {
    for (j=0; j<NUM_OUTPUTS; j++)
      if (fabs(scales[i]-1.0/(1.0+zs[j])) < fabs(scales[snaps[j]]-1.0/(1.0+zs[j])))
	snaps[j] = i;
    for (j=0; j<NUM_WL_ZS; j++)
      if (fabs(scales[i]-1.0/(1.0+wl_zs[j])) < fabs(scales[wl_snaps[j]]-1.0/(1.0+wl_zs[j])))
	wl_snaps[j] = i;
  }
  
  if (POSTPROCESSING_MODE) {
    printf("#SMHM Scales:");
    for (j=0; j<SMHM_NUM_ZS; j++) printf(" %f", smhm_scales[j]);
    printf("\n#WeakLensing Scales:");
    for (j=0; j<NUM_WL_ZS; j++) printf(" %f", wl_scales[j]);
    printf("\n");
  }
  
  

  printf("#Raw Model Data\n");
  printf("#Median UV-SM relations\n");
  printf("#z_min z_max uv_min uv_max Log10(Median_SM)\n");
  for (i=0; i<UVSM_REDSHIFTS; i++) {
    for (j=0; j<UVSM_UV_BINS; j++) {
      double z_min = i-0.5;
      double z_max = i+0.5;
      double uv_min = UVSM_MIN + (double)j/(double)UVSM_BPMAG;
      double uv_max = uv_min + 1.0/(double)UVSM_BPMAG;
      if (z_min < 0) z_min = 0;
      double med_sm = median_uvsm[i*UVSM_UV_BINS+j];
      if (med_sm > 1) med_sm = log10(med_sm);
      else med_sm = 0;
      printf("%f %f %f %f %f\n", z_min, z_max, uv_min, uv_max, med_sm);
    }
  }
  
  for (i=0; i<ns; i++) {
    for (smhm_j=0; smhm_j<SMHM_NUM_ZS; smhm_j++) if (snaps[smhm_j] == i) break;
    for (wl_j=0; wl_j<NUM_WL_ZS; wl_j++) if (wl_snaps[wl_j] == i) break;

    if (POSTPROCESSING_MODE) {
      if (smhm_j < SMHM_NUM_ZS) {
	printf("#SMHM_Relations\n");
	printf("#HM Med_All S_All Med_Cen S_Cen Med_Sat S_Sat Med_SF S_SF Med_Q S_Q\n");
	offset = smhm_j*SMHM_FIT_NBINS;
	for (j=0; j<SMHM_FIT_NBINS; j++) {
	  printf("%f %f %f %f %f %f %f %f %f %f %f\n", SMHM_HM_MIN+(j+0.5)/(double)SMHM_FIT_BPDEX,
		 smhm_med[offset+j], smhm_scatter[offset+j], smhm_med_cen[offset+j], smhm_scatter_cen[offset+j],
		 smhm_med_sat[offset+j], smhm_scatter_sat[offset+j], smhm_med_sf[offset+j], smhm_scatter_sf[offset+j],
		 smhm_med_q[offset+j], smhm_scatter_q[offset+j]);
	}
      }
    }

    
    printf("#Scale: %f\n", scales[i]);
    printf("#Csfr: %e\n", csfr[i]);
    printf("#Csfr_uv: %e\n", csfr_uv[i]);
    printf("#SM SMF FQ(SSFR<PRIMUS) FQ(SSFR<%f) FQ_UVJ SSFR\n", log10(QUENCHED_THRESHOLD));
    
    offset = SM_NBINS*i;
    for (j=0; j<SM_NBINS; j++) {
      double counts = smf[j+offset];
      if (!counts) counts = 1;
      printf("%f %e %f %f %f %e\n", SM_MIN+(j+0.5)/(double)SM_BPDEX,
	     smf[j+offset], smf_q[j+offset]/counts, smf_q2[j+offset]/counts, smf_q3[j+offset]/counts, ssfr[j+offset]/counts);
    }

    if (POSTPROCESSING_MODE) {
      printf("#(Postprocessed)SM SMF FQ(SSFR<PRIMUS) FQ(SSFR<%f) FQ_UVJ SSFR Ex_situ_frac Rejuv_frac SMF_Cen SMF_Sat FQ_Cen FQ_Sat SM_Sat_MW SM_Sat_GR SM_Sat_CL SM_Sat_MW_FQ SM_Sat_GR_FQ SM_Sat_CL_FQ SM_Sat_MW_FQ_Infall SM_Sat_GR_FQ_Infall SM_Sat_CL_FQ_Infall\n", log10(QUENCHED_THRESHOLD));
      offset = SM_RED_NBINS*i;
      for (j=0; j<SM_RED_NBINS; j++) {
	printf("%f %e %f %f %f %e", SM_MIN+(j+0.5)/(double)SM_RED_BPDEX,
	       sm_red[j+offset], sm_fq[j+offset], sm_fq2[j+offset], sm_fq3[j+offset], sm_ssfr[j+offset]);
	printf(" %f %f %e %e %f %f %e %e %e %f %f %f %f %f %f\n", sm_ex_situ_ratio[j+offset], sm_rejuv_ratio[j+offset], sm_cen[j+offset], sm_red[j+offset]-sm_cen[j+offset], sm_q_cen_ratio[j+offset], sm_q_sat_ratio[j+offset], sm_sat_mw[j+offset], sm_sat_gr[j+offset], sm_sat_cl[j+offset], sm_q_sat_mw_ratio[j+offset], sm_q_sat_gr_ratio[j+offset], sm_q_sat_cl_ratio[j+offset], sm_fq_sat_mw_infall_ratio[j+offset], sm_fq_sat_gr_infall_ratio[j+offset], sm_fq_sat_cl_infall_ratio[j+offset]);
      }

      printf("#SM Counts_All Counts_SF Counts_Q SFR(a) SFR_SF(a) SFR_Q(a)\n");
      printf("#");
      int64_t k;
      for (k=0; k<num_scales; k++)
	printf("%f ", scales[k]);
      printf("\n");
      offset = SM_RED_NBINS*num_scales*i;
      for (j=0; j<SM_RED_NBINS; j++) {
	printf("%f %e %e %e", SM_MIN+(j+0.5)/(double)SM_RED_BPDEX, sm_red[j+SM_RED_NBINS*i], sm_red[j+SM_RED_NBINS*i]-sm_red_q2[j+SM_RED_NBINS*i], sm_red_q2[j+SM_RED_NBINS*i]);
	for (k=0; k<num_scales; k++) printf(" %g", sm_sfh_ratio[offset+j*num_scales+k]);
	for (k=0; k<num_scales; k++) printf(" %g", sm_sfh_sf_ratio[offset+j*num_scales+k]);
	for (k=0; k<num_scales; k++) printf(" %g", sm_sfh_q_ratio[offset+j*num_scales+k]);
	printf("\n");
      }
    }

    if (POSTPROCESSING_MODE) {
      printf("#HM HMF FQ(SSFR<%g) Ex_situ_frac Rejuv_frac HMF_Cen HMF_Sat FQ_Cen FQ_Sat\n", log10(QUENCHED_THRESHOLD));
      offset = HM_M_NBINS*i;
      for (j=0; j<HM_M_NBINS; j++) {
	double counts = hm_dist[j+offset];
	if (!counts) counts = 1;
	double counts_cen = hm_cen_dist[j+offset];
	double counts_sat = hm_dist[j+offset] - hm_cen_dist[j+offset];
	if (!counts_cen) counts_cen = 1;
	if (!counts_sat) counts_sat = 1;

	printf("%f %e %f %f %f %e %e %f %f\n", HM_M_MIN+(j+0.5)/(double)HM_M_BPDEX,
	       hm_dist[j+offset], hm_q[j+offset]/counts, hm_ex_situ_ratio[j+offset], hm_rejuv_ratio[j+offset], hm_cen_dist[j+offset], hm_dist[j+offset]-hm_cen_dist[j+offset], hm_q_cen_ratio[j+offset], hm_q_sat_ratio[j+offset]);
      }
    }
    
    if (POSTPROCESSING_MODE && wl_j < NUM_WL_ZS) {
      int64_t k, l;
      printf("#Weak Lensing\n");
      printf("#WL Scale Min_SM Max_SM Type Delta_Sigma Avg.Bins\n");
      for (j=0; j<NUM_WL_THRESHES; j++) {
	double max_sm = 13.0;
	double min_sm = log10(WL_MIN_SM);
	if (j==1) min_sm = log10(WL_MIN_SM2);
	if (j==2) min_sm = log10(WL_MIN_SM3);
	for (k=0; k<NUM_WL_TYPES; k++) {
	  int64_t offset =  NUM_WL_BINS*(k + NUM_WL_TYPES*(j + NUM_WL_THRESHES*wl_j));
	  char *type = "all";
	  if (k==1) type = "sf";
	  else if (k==2) type = "q";
	  printf("WL %f %.1f %.1f %s X", scales[i], min_sm, max_sm, type);
	  for (l=0; l<NUM_WL_BINS; l++) printf(" %g", wl_post[offset+l]);
	  printf("\n");
	}
      }
    }

    
    if (scales[i] <= UV_CALC_MAX_SCALE) {
      printf("#M_1500 ND <IRX>\n");
      offset = UV_NBINS*i;
      for (j=0; j<UV_NBINS; j++) {
	double uv_nd = uvlf[j+offset];
	if (!uv_nd) uv_nd = 1;
	printf("%f %e %e\n", UV_MIN+(j+0.5)/(double)UV_BPMAG,
	       uvlf[j+offset], ir_excess[j+offset]/uv_nd);
      }

      if (POSTPROCESSING_MODE) {
	printf("#SFR_at_Muv=-17: %f\n", uv_sfr_17[i]);
	printf("#UV_SFR(M_1500) <SFR> Counts\n");
	offset = UV_SFR_NBINS*i;
	for (j=0; j<UV_SFR_NBINS; j++) {
	  double sfr = uv_sfr_av_sfr[j+offset];
	  if (!sfr) sfr = 0;
	  printf("%f %e %e\n", UV_SFR_MIN+(j+0.5)/(double)UV_SFR_BPMAG,
		 sfr, uv_sfr_counts[j+offset]);
	}
      }
    }
    
    float sms[5] = {CORR_MIN_SM, CORR_MIN_SM2, CORR_MIN_SM3, CORR_MIN_SM4, 1e13};
    if ((scales[i] >= CORR_MIN_SCALE) || (POSTPROCESSING_MODE && scales[i] > POSTPROCESS_CORR_MIN_SCALE)) {
      printf("#SM1 SM2 R1 R2 All SF Q SF_Q (Raw: All SF Q SF_Q)\n");
      for (j=0; j<NUM_CORR_THRESHES; j++) {
	int64_t k;
	float sm1 = log10(sms[j]);
	float sm2 = log10(sms[j+1]);
	int64_t l;
	printf("#Counts:");
	for (l=0; l<NUM_CORR_TYPES; l++)
	  printf(" %g", fccounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + j*NUM_CORR_TYPES + l]);
	printf("\n");
	for (k=0; k<NUM_BINS; k++) {
	  float *corr_loc = fcorrs + i*NUM_CORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS;
	  float r1 = MIN_DIST + k*INV_DIST_BPDEX;
	  float r2 = MIN_DIST + (k+1)*INV_DIST_BPDEX;
	  printf("%.1f %.1f % .1f % .1f", sm1, sm2, r1, r2);
	  int64_t l;
	  for (l=0; l<NUM_CORR_TYPES; l++)
	    printf(" % 10.3f", _pp_calc_corr(i, j, l, k, scales[i], 0));
	  for (l=0; l<NUM_CORR_TYPES; l++)
	    printf(" %e", corr_loc[k+l*NUM_BINS]);
	  printf("\n");
	}
      }
      printf("#SM1 SM2 SM3 SM4 R1 R2 AllxAll AllxQ QxQ SFxSF (Raw: All SF Q SF_Q)\n");
      for (j=0; j<NUM_XCORR_THRESHES; j++) {
	int64_t k;
	float sm1 = log10(sms[XCORR_BIN1[j]]);
	float sm2 = log10(sms[XCORR_BIN1[j]+1]);
	float sm3 = log10(sms[XCORR_BIN2[j]]);
	float sm4 = log10(sms[XCORR_BIN2[j]+1]);
	int64_t l;
	printf("#Counts:");
	for (l=0; l<NUM_CORR_TYPES; l++)
	  printf(" %g", fccounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + XCORR_BIN1[j]*NUM_CORR_TYPES + XCORR_COUNTS1[l]]);
	printf("\n");
	for (k=0; k<NUM_BINS; k++) {
	  float *corr_loc = fxcorrs + i*NUM_XCORR_BINS_PER_STEP + j*NUM_CORR_TYPES*NUM_BINS;
	  float r1 = MIN_DIST + k*INV_DIST_BPDEX;
	  float r2 = MIN_DIST + (k+1)*INV_DIST_BPDEX;
	  printf("%.1f %.1f %.1f %.1f % .1f % .1f", sm1, sm2, sm3, sm4, r1, r2);
	  int64_t l;
	  for (l=0; l<NUM_CORR_TYPES; l++)
	    printf(" % 10.3f", _pp_calc_xcorr(i, j, l, k, scales[i], 0));
	  for (l=0; l<NUM_CORR_TYPES; l++)
	    printf(" %e", corr_loc[k+l*NUM_BINS]);
	  printf("\n");
	}
      }
    }

    if (scales[i] >= CONFORMITY_MIN_SCALE) {
      printf("#Conformity (SM: %e - %e)\n", CONFORMITY_MIN_SM, CONFORMITY_MAX_SM);
      printf("#R1 R2 Corr. Counts <SFR1> <SFR2> <SFR1^2> <SFR2^2> <SFR1*SFR2>\n");
      float *conf = fconfs + i*NUM_CONF_BINS_PER_STEP;
      for (int64_t k=0; k<NUM_CONF_BINS; k++) {
	if (!conf[k]) continue;
	float cc = conf[k];
	float sfr1 = conf[k+NUM_CONF_BINS]/cc;
	float sfr2 = conf[k+NUM_CONF_BINS*2]/cc;
	float sfr11 = conf[k+NUM_CONF_BINS*3]/cc;
	float sfr22 = conf[k+NUM_CONF_BINS*4]/cc;
	float sfr12 = conf[k+NUM_CONF_BINS*5]/cc;
	float sig1 = sfr22 - sfr2*sfr2;
	float sig2 = sfr11 - sfr1*sfr1;
	float corr = -1;
	if (sig1>0 && sig2 > 0) {
	  corr = (sfr12 - sfr1*sfr2)/sqrt(sig1*sig2);
	}
	float r1 = pow(10, (MIN_CONF_DIST+(double)k/(double)DIST_CONF_BPDEX));
	float r2 = pow(10, (MIN_CONF_DIST+(double)(k+1)/(double)DIST_CONF_BPDEX));
	printf("%f %f %f %g %e %e %e %e %e\n", r1, r2, corr, cc, sfr1, sfr2, sfr11, sfr22, sfr12);
      }
    }

    if (scales[i] >= DENS_MIN_SCALE) {
      printf("#Centrals Density Dep. (SM: %e - %e)\n", DENS_MIN_SM, DENS_MAX_SM);
      printf("#N1 N2 f_sf <SSFR>_sf (Counts SF_Counts)\n");
      for (int64_t k=0; k<NUM_DENS_BINS; k++) {
        float dc = fdens_counts[k+i*NUM_DENS_BINS];
        float dsf = fdens_sf[k+i*NUM_DENS_BINS];
        float dssfr = fdens_sf_ssfr[k+i*NUM_DENS_BINS];
        float ssfr_av = dssfr;
        if (dsf) ssfr_av /= dsf;
        float fsf = dsf;
        if (dc) fsf/=dc;
        int64_t n1 = dens_bin_edge(k);
        int64_t n2 = dens_bin_edge(k+1)-1;
        printf("%"PRId64" %"PRId64" %f %f %g %g\n", n1, n2, fsf, ssfr_av, dc, dsf);
      }
    }

  }

  munmap(data, length);
  return 0;
}


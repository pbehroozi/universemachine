#ifndef _WATCHDOG_H_
#define _WATCHDOG_H_
#include <assert.h>

void watchdog_clear(void); 
void watchdog_start(char *name);
void watchdog_killall(void);
void watchdog_failexit(void);

void _trigger_watchdog(void);
#define trigger_watchdog() { _trigger_watchdog(); assert(0); }

#endif /* _WATCHDOG_H_ */

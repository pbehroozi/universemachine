#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stats.h"

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<2) {
    printf("Usage: %s sfr_list.bin\n", argv[0]);
    exit(1);
  }
  
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  struct binstats *sfr_uv = init_binstats(-3, 3, 10, 0, 0);
  struct binstats *sfr_uv_med = init_binstats(-3, 3, 10, 1, 0);
  struct binstats *sfr_hm = init_binstats(-3, 3, 10, 0, 0);
  struct binstats *uv_sfr_med = init_binstats(-24, -10, 5, 1, 0);
  struct binstats *uv_hm = init_binstats(-24, -10, 5, 0, 0);
  
  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    if (tp->flags & IGNORE_FLAG) continue;
    if (tp->obs_sfr <= 0) continue;
    double lsfr = log10(tp->obs_sfr);
    add_to_binstats(sfr_uv, lsfr, pow(10, (tp->obs_uv - A_UV(*tp))/-2.5));
    add_to_binstats(sfr_uv_med, lsfr, tp->obs_uv - A_UV(*tp));
    add_to_binstats(sfr_hm, lsfr, log10(tp->mp/0.677));
    add_to_binstats(uv_sfr_med, (tp->obs_uv - A_UV(*tp)), tp->sfr);
    add_to_binstats(uv_hm, (tp->obs_uv - A_UV(*tp)), log10(tp->mp/0.677));
  }
  munmap(p, new_length);

  calc_median(sfr_uv_med);
  printf("#Log10(SFR) <M_1500> Median_M1500 1sig+ 1sig- <Log10(HM)> counts\n");
  for (i=0; i<sfr_uv->num_bins; i++) {
    if (!(sfr_uv->counts[i]>0)) continue;
    printf("%g %g %g %g %g %g %"PRId64"\n", sfr_uv->bin_start+(i+0.5)/sfr_uv->bpunit, -2.5*log10(sfr_uv->avg[i]), sfr_uv_med->med[i], sfr_uv_med->stdup[i], sfr_uv_med->stddn[i], sfr_hm->avg[i], sfr_uv->counts[i]);
  }

  calc_median(uv_sfr_med);
  printf("#M_1500 <SFR> Median_SFR 1sig+ 1sig- <Log10(HM)> counts\n");
  for (i=0; i<uv_sfr_med->num_bins; i++) {
    if (!(uv_sfr_med->counts[i]>0)) continue;
    printf("%g %g %g %g %g %g %"PRId64"\n", uv_sfr_med->bin_start+(i+0.5)/uv_sfr_med->bpunit, uv_sfr_med->avg[i], uv_sfr_med->med[i], uv_sfr_med->stdup[i], uv_sfr_med->stddn[i], uv_hm->avg[i], uv_hm->counts[i]);
  }

  return 0;
}


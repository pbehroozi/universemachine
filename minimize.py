import os, sys
import subprocess as sp
import numpy as np
from scipy.optimize import minimize
import python.ReadConfig as rc

if len(sys.argv) < 2:
    print("Usage:",sys.argv[0],"um.cfg")
    sys.exit()

np.set_printoptions(linewidth=100000)

p = sp.Popen("./umachine_server "+sys.argv[1], shell=True, bufsize=100,
             stdin=sp.PIPE, stdout=sp.PIPE, close_fds=True)

#steps = np.array([0.1, 0.2, 0.2, 0.2, 0.1, 0.1, 0.1, 0.3, 0.3, 1.0, 1.0, 1.0, 1.0, 0.02, 0.1, 0.2, 0.2, 1, 1, 1, 0.1, 0.02, 0.05, 0.02, 0.03, 0.01, 0.02, 0.003, 0.002, 1, 1, 1, 1, 1])
steps = np.array([0.1, 0.5, 0.5, 1, 0.05, 0.1, 0.1, 0.2, 1, 2, 0.5, 2, 1, 2, 1, 0.01, 0.5, 1, 0.5, 0.1, 0.5, 0.2, 0.5, 0.5, 1, 0.2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.2, 0.05, 0.01, 0.02, 0.1, 0.03, 0.2, 0.1, 0.2, 0.1, 0.1])
chi2_dict = {}

def get_chi2(x):
#    global chi2_dict
#    dkey = str(x)
#    if (dkey in chi2_dict):
#        return chi2_dict[dkey]
    x = np.multiply(x, steps)
    x = np.concatenate((x[0:26], np.array([1]), x[26:36], np.array([0.07]), x[36:45]))
    y = str(str(x)[1:-1])
    p.stdin.write(str.encode(y+" 0\n"))
    p.stdin.flush()
    chi2 = float(p.stdout.readline())
#    if (chi2>1e20):
#        chi2 = -1.
    print(y," ",chi2)
    sys.stdout.flush()
#    chi2_dict[dkey] = chi2
    return chi2


x = np.array([0.111566383751, 1.851165306043, -0.262498814697, 2.453342000000, 2.060578524077, 0.322385485956, -0.095760500000, 0.706293003179, -6.678835123879, -0.742246653101, -0.268311073596, 2.412981052222, -1.103871743392, 0.382719417322, 0.871704118926, 0.080322851043, -1.313790240545, 3.342866290000, -0.972355917846, -0.169145118576, 1.674300061812, 0.290904124799, 2.827757777301, -0.569456960882, 1.896251348213, -0.080875527084, 2.322659214735, -0.144645720025, 0.210928240000, 0.466701980000, -1.126892182934, 1.080337094572, 0.081656800292, -0.125073476035, 0.322800227535, 0.059910483272, -0.170076796082, -0.108142107758, 0.315259793596, 9.103332360805, -0.040651324704, 0.442313450000, 0.560758442785, 0.472760328977])
#x = np.array([0.103966668408056,1.83063455621321,-0.277654271380385,2.48415368092332,2.06070261079063,0.321276981182992,-0.099259307971709,0.698611015909492,-6.78203027787717,-0.739069324146649,-0.242721698327971,2.35968078709715,-0.769998322065594,-0.116483508267006,0.714846466734106,0.0782099967020499,-1.30935957816679,3.34888593019753,-0.968024308196845,-0.350872870921099,1.62602563488957,0.422315986805749,3.05486114746315,0.362931710371528,2.95574055440488,-0.109709699148675,2.32098601411566,-0.155812123549582,0.208789379382599,0.451639335282312,-1.13797216523246,1.06671891715612,0.102255261540046,-0.230386628725287,0.302581799836008,0.0393551448374071,-0.16244070603523,-0.125857572293462,0.330001386022831,8.97192979696666,-0.0440697344531944,0.431915895223722,0.600482034644315, 0.600482034644315])

x = np.divide(x, steps)
res = minimize(get_chi2, x, method='COBYLA', options={'rhobeg':1e-1, 'maxiter': 10000}) #, constraints=({'type':'ineq', 'fun':get_chi2}))
chi2 = get_chi2(res.x)
x = np.multiply(res.x, steps)
x = np.concatenate((x[0:26], np.array([1]), x[26:36], np.array([0.07]), x[36:45]))
print("Final answer:")
print(str(str(x)[1:-1])," ",chi2)

p.terminate()

cfg = rc.ReadConfig(sys.argv[1])
if "OUTBASE" in cfg.data:
    file = open(cfg.data["OUTBASE"]+"/"+"done", "w")
    file.close()



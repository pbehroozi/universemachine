//string(a,b) real(a,b) integer(a,b)
//Sets strings, floats, and integers; 
//     "a" is the variable name; 
//     "b" is the default.

string(INBASE, ".");
string(OUTBASE, ".");
string(OBSERVATIONS_DIR, "obs");
string(SPS_DIR, "fsps");
real(BOX_SIZE, 250); //In Mpc/h
real(Om, 0.307);
real(Ol, 0.693);
real(h0, 0.68);
real(fb, 0.158);

integer(NO_SYSTEMATICS, 0);

real(CALC_ICL, 1);
real(SAVE_OBS_FRACTION, 1e-3);
string(INITIAL_POSITIONS, "initial_positions.dat");
integer(POSTPROCESSING_MODE, 0);
integer(POSTPROCESSING_BESTFIT, -1);
integer(PROFILING_MODE, 0);
integer(CALC_LUMINOSITIES, 1);
integer(FITTING_MODE, 0);
integer(EXTERNAL_FITTING_MODE, 0);
integer(CATALOG_OUTPUT_MODE, 0);
integer(SFH_OUTPUT_MODE, 0);
integer(SHARED_DATA_OUTPUT_MODE, 0);

real(UV_MAG_CALIBRATION, -0.06);

real(SIM_RES_LIMIT_SM0, 7.5);
real(SIM_RES_LIMIT_SM1, 0.125);
real(SIM_RES_LIMIT_HM, 10.5);
real(WL_PARTICLE_MASS, 1.33e10); //In Msun/h

real(QUENCHED_THRESHOLD, 1e-11);
real(UV_MAG_THRESH, -17);
real(PHOTOMETRIC_SCATTER, 0.2); //In mags
real(UVJ_SCATTER, 0.05); //In mags
real(UV_CALC_MAX_SCALE, 0.24);
real(UV_CALC_MIN_MASS, 8.35);
real(CORR_MIN_SCALE, 0.57);
real(POSTPROCESS_CORR_MIN_SCALE, 0.1);
real(POSTPROCESS_CORR_MIN_SM, 1e10);
real(POSTPROCESS_CORR_MIN_SM2, 3.16227e10);
real(POSTPROCESS_CORR_MIN_SM3, 1e11);
real(POSTPROCESS_CORR_LENGTH_PI, 80);
real(LIGHTCONE_MIN_UV, 0);
real(LIGHTCONE_MIN_HM, 0);


real(CORR_MIN_SM, 3e9);
real(CORR_MIN_SM2, 1.99526e10);
real(CORR_MIN_SM3, 3.16227e10);
real(CORR_MIN_SM4, 1e11); 
real(CORR_LENGTH_PI, 13.554); // Mpc/h
real(CORR_LENGTH_RP, 10); // Mpc/h
real(REDSHIFT_ERROR, 30); // km/s.  If <1, assumed to be Delta z / (1+z)

real(WL_MIN_SM, 6.309573e9);
real(WL_MIN_SM2, 1.5848931e10);
real(WL_MIN_SM3, 3.9810717e10);

real(CORR_HIGHZ_SCALE, 0.84);
real(CORR_MIN_SM_HIGHZ, 2.5118e10);
real(CORR_MAX_SM_HIGHZ, 8.9125e10);
real(CORR_LENGTH_PI_HIGHZ, 40); // Mpc/h
real(REDSHIFT_ERROR_HIGHZ, 0.0033); 


real(CONFORMITY_MIN_SCALE, 0.93);
real(CONFORMITY_NEIGHBOR_DV, 500);
real(CONFORMITY_EXCLUSION_DV, 1000);
real(CONFORMITY_EXCLUSION_DX, 0.5);
real(CONFORMITY_NEIGHBOR_DSM, 0.5);
real(CONFORMITY_MIN_SM, 1e10);
real(CONFORMITY_MAX_SM, 3.16227e10);
real(CONFORMITY_MAX_DX, 4);

real(DENS_MIN_SCALE, 0.93);
real(DENS_EXCLUSION_DV, 1000);
real(DENS_EXCLUSION_DX, 0.5);
real(DENS_NEIGHBOR_MIN_DSM, -0.25);
real(DENS_NEIGHBOR_MAX_DSM, 0.25);
real(DENS_MIN_SM, 1e10);
real(DENS_MAX_SM, 3.16227e10);
real(DENS_MAX_DX, 4);
real(DENS_MIN_DX, 0.3);
real(DENS_COMPLETENESS, 1.0);

integer(NUM_NODES, 1);
integer(NUM_BLOCKS, 1);
integer(BLOCKS_PER_NODE, 1);
integer(PERIODIC, 1);
integer(FORK_BOX_FROM_UMACHINE_SERVER, 0);
integer(TOTAL_BOXES, 0);
integer(USE_PLOAD, 1);

string(SERVER_ADDRESS, "auto");
string(SERVER_PORT, "auto");
string(SERVER_INTERFACE, "");
integer(BOX_SERVER_PORT, 32001);
string(CONFIG_FILENAME, "um_runvals.cfg");

integer(NUM_WALKERS, 100);
real(STRETCH_STEP, 1.4);
integer(MCMC_STEP_TYPE, 1);
integer(BURN_IN_LENGTH, 50000);
integer(MCMC_LENGTH, 20000000);
integer(BURN_IN_STAGE, -1);
integer(RECALC_EIGENVALUES, 0);
integer(SEQUENTIAL_LOADING,0);

real(FIT_TOLERANCE, 0.02);
real(QF_TOLERANCE, 0.02);
real(CF_TOLERANCE, 0.04);
real(CALCULATION_TOLERANCE, 0.01);

integer(PERFORMANCE_TEST, 0);


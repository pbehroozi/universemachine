#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "expcache.h"
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"
#include "distance.h"
#include "io_helpers.h"
#include "bin_definitions.h"

extern struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused
double SCALE_NOW = 1;

#define FAST3TREE_TYPE   struct catalog_galaxy
#define FAST3TREE_PREFIX SAMPLE_CORR_SM
#define FAST3TREE_DIM    2
#include "fast3tree.c"

void load_vbins(char *filename);
void scorr_load_boxes(float target, float orphan_thresh);
double calc_sm(double lvmp);

struct vbin {
  double v, nd, sm, qf;
};
struct vbin *vbins = NULL;
int64_t nvb = 0;

double sample_pp_calc_corr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post);

int main(int argc, char **argv)
{
  int64_t i, j, k;

  if (argc < 7) {
    fprintf(stderr, "Usage: %s config.cfg scale sm_vmp.dat params.dat f_nr sig_sm (reps) (all=0/cen=1/sat=2)\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  struct sf_model_allz sfm = {{0}};

  do_config(argv[1]);
  setup_config();
  float scale = atof(argv[2]);
  load_vbins(argv[3]);
  read_params_from_file(argv[4], sfm.params, NUM_PARAMS);
  gen_erfcache();
  init_dists();
  gen_exp10cache();

  int64_t reps = 1;
  if (argc>7) reps = atol(argv[7]);

  int64_t central_type = 0; //all galaxies
  if (argc>8) central_type = atol(argv[8]);

  R_MIN(sfm) = atof(argv[5]);
  R_CENTER(sfm) = 2.5;
  R_WIDTH(sfm) = 10;
  struct sf_model c = calc_sf_model(sfm, SCALE_NOW);
  float sig_sm = atof(argv[6]);
  //c.obs_sm_sig = sig_sm;
  c.obs_sm_sig = 0.07;
  float intrinsic_scatter = (sig_sm > 0.07) ? sqrt(sig_sm*sig_sm-0.07*0.07) : 0;
  c.obs_sfr_sig = 0.3;
  
  r250_init(87L);
  scorr_load_boxes(scale, c.orphan_thresh_min);
  double min_sm = log10(CORR_MIN_SM2)-4.0*c.obs_sm_sig; //log10(DENS_MIN_SM) + DENS_NEIGHBOR_MIN_DSM - 4.0*c.obs_sm_sig;
  printf("Min sm: %f\n", min_sm);
  for (i=0; i<nh; i++)
    halos[i].sm = calc_sm(halos[i].lvmp) + normal_random(0, intrinsic_scatter);

  for (i=0; i<nh; i++) {
    if ((halos[i].sm < min_sm) || (halos[i].flags & IGNORE_FLAG)) {
      halos[i] = halos[nh-1];
      nh--;
      i--;
      continue;
    }
  }
  int64_t ng = nh;
  check_realloc_s(galaxies, sizeof(struct catalog_galaxy), ng);

  float hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;

  struct catalog_galaxy *galaxies2 = NULL;
  int64_t num_g2 = 0;

  //double translate_const = -0.49 + 1.07 * ((1.0/SCALE_NOW-1.0) - 0.1);
  for (i=0; i<nh; i++) {
    float ra = c.r_min*halos[i].rank1 + c.ra*halos[i].ra;
    //float rank = 0.5*(1.0+erf(ra*M_SQRT1_2));
    struct catalog_galaxy *g = galaxies + i;
    float fq = c.fq_min + (1.0-c.fq_min)*0.5*(1.0+erf((halos[i].lvmp-c.q_lvmp)*M_SQRT1_2/(c.q_sig_lvmp)));
    float sfr_sf = log10(sfr_at_vmp(halos[i].lvmp, c));
    float sfr_q = (halos[i].sm > 1) ? halos[i].sm+c.ssfr_q : (sfr_sf-2.0);
    g->lsfr = rank_to_sfr(ra, sfr_q, c.sig_q, sfr_sf, c.sig_sf, fq); // + normal_random(0, c.obs_sfr_sig);
    g->lsm = halos[i].sm;
    //translate_sfrs(g, translate_const);

    memcpy(g->opos, halos[i].pos, sizeof(float)*6);
    g->pos[0] = halos[i].pos[0];
    g->pos[1] = halos[i].pos[1];
    g->pos[2] = halos[i].pos[2] + halos[i].pos[5]/hz;
    g->weight = halos[i].weight;
    while (g->pos[2]<0) g->pos[2] += BOX_SIZE;
    while (g->pos[2]>BOX_SIZE) g->pos[2] -= BOX_SIZE;

    for (j=-1; j<=1; j++) {
      for (k=-1; k<=1; k++) {
	struct catalog_galaxy g2 = *g;
	g2.pos[0] += BOX_SIZE*j;
	g2.pos[1] += BOX_SIZE*k;
	if ((g2.pos[0] > -CORR_LENGTH_RP) && (g2.pos[0] < BOX_SIZE+CORR_LENGTH_RP) &&
	    (g2.pos[1] > -CORR_LENGTH_RP) && (g2.pos[1] < BOX_SIZE+CORR_LENGTH_RP)) {
	    check_realloc_every(galaxies2, sizeof(struct catalog_galaxy), num_g2, 1000);
	    galaxies2[num_g2] = g2; 
	    num_g2++;
	}
      }
    }
  }
  
  float min_threshes[NUM_CORR_THRESHES];
  assert(NUM_CORR_THRESHES==4);
  min_threshes[0] = log10(CORR_MIN_SM);
  min_threshes[1] = log10(CORR_MIN_SM2);
  min_threshes[2] = log10(CORR_MIN_SM3);
  min_threshes[3] = log10(CORR_MIN_SM4);

  for (i=0; i<reps; i++)
    bin_galaxies(SCALE_NOW, galaxies, ng, galaxies2, num_g2, -1, c, min_threshes, 0);

  float sms[5] = {CORR_MIN_SM, CORR_MIN_SM2, CORR_MIN_SM3, CORR_MIN_SM4, 1e13};
  printf("#SM1 SM2 R1 R2 All SF Q SF_Q (Raw: All SF Q SF_Q)\n");
  for (j=0; j<NUM_CORR_THRESHES; j++) {
    int64_t k;
    float sm1 = log10(sms[j]);
    float sm2 = log10(sms[j+1]);
    int64_t l;
    printf("#Counts:");
    for (l=0; l<NUM_CORR_TYPES; l++)
      printf(" %g", ca[j*NUM_CORR_TYPES + l]);
    printf("\n");
    for (k=0; k<NUM_BINS; k++) {
      double *corr_loc = wcorr + j*NUM_CORR_TYPES*NUM_BINS;
      float r1 = MIN_DIST + k*INV_DIST_BPDEX;
      float r2 = MIN_DIST + (k+1)*INV_DIST_BPDEX;
      printf("%.1f %.1f % .1f % .1f", sm1, sm2, r1, r2);
      int64_t l;
      for (l=0; l<NUM_CORR_TYPES; l++)
	printf(" % 10.3f", sample_pp_calc_corr(0, j, l, k, SCALE_NOW, 0));
      for (l=0; l<NUM_CORR_TYPES; l++)
	printf(" %e", corr_loc[k+l*NUM_BINS]);
      printf("\n");
    }
  }

  return 0;
}


double sample_pp_calc_corr(int64_t i, int64_t thresh, int64_t subtype, int64_t rbin, double scale, int64_t post) {
  double s=0, c=0;
  double *mcorrs = wcorr;
  double *mcounts = ca;
  s = mcorrs[i*NUM_CORR_BINS_PER_STEP + thresh*NUM_CORR_TYPES*NUM_BINS + subtype*NUM_BINS + rbin];
  c = mcounts[i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES + subtype];
  if (!c) return 0;
  s /= c;

  double d = c-1; //Density
  if (subtype == 3) { //Cross-correlation: average pair count density: (fsf*nq + fq*nsf)/box_size
    double *counts = mcounts + i*(NUM_CORR_TYPES*NUM_CORR_THRESHES) + thresh*NUM_CORR_TYPES;
    d = 2.0*(counts[1]*counts[2]);
    d /= c;
  }
  
  if (!d) return 0;
  double cd1 = pow(10, 2.0*(MIN_DIST+rbin*INV_DIST_BPDEX));
  double cd2 = pow(10, 2.0*(MIN_DIST+(rbin+1)*INV_DIST_BPDEX));
  double bin_area = M_PI*(cd2-cd1);
  double counts_per_length = bin_area * d / (BOX_SIZE*BOX_SIZE*BOX_SIZE);
  double pi_max = CORR_LENGTH_PI;
  if (scale < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  if (post) pi_max = POSTPROCESS_CORR_LENGTH_PI;
  double expected_counts = counts_per_length * 2.0 * pi_max;
  return ((s - expected_counts)/counts_per_length);
}

void scorr_load_boxes(float target, float orphan_thresh) {
  char buffer[1024];
  int64_t i, box_num, min_snap, offset, snap, n;
  float scale;

  for (box_num=0; box_num<NUM_BLOCKS; box_num++) {
    num_scales = 0;
    //First load offsets from file:
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, box_num);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    min_snap=0;
    for (i=1; i<num_scales; i++)
      if (fabs(target-scales[min_snap])>fabs(target-scales[i])) min_snap = i;

    int64_t to_read = offsets[min_snap+1]-offsets[min_snap];
    SCALE_NOW = scales[min_snap];

    //Then load halos
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, box_num);
    in = check_fopen(buffer, "r");
    check_fseeko(in, offsets[min_snap]*sizeof(struct catalog_halo), 0);
    check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
    check_fread(halos+nh, sizeof(struct catalog_halo), to_read, in);
    fclose(in);
    nh += to_read;
  }
  for (i=0; i<nh; i++) {
    halos[i].weight=1;
    if ((halos[i].flags & ORPHAN_FLAG) && (halos[i].v < orphan_thresh*halos[i].vmp)) {
      halos[i].weight=0;
      halos[i].flags |= IGNORE_FLAG;
    }
  }
}


int sort_vbins(const void *a, const void *b) {
  const struct vbin *c = a;
  const struct vbin *d = b;
  if (c->v < d->v) return -1;
  if (c->v > d->v) return 1;
  return 0;
}

void load_vbins(char *filename) {
  FILE *in = check_fopen(filename, "r");
  char buffer[1024];
  struct vbin v = {0};
  while (fgets(buffer, 1024, in)) {
    if (sscanf(buffer, "%lf %lf %lf", &v.v, &v.nd, &v.sm)<3) continue;
    if (!v.nd) continue;
    check_realloc_every(vbins,sizeof(struct vbin), nvb, 10);
    vbins[nvb] = v;
    nvb++;
  }
  fclose(in);
  qsort(vbins, nvb, sizeof(struct vbin), sort_vbins);
}

double calc_sm(double lvmp) {
  int64_t i;
  if (lvmp <= vbins[0].v) return vbins[0].sm;
  for (i=1; i<nvb; i++)
    if (vbins[i].v > lvmp) break;
  if (i==nvb) return vbins[nvb-1].sm;
  double sm = vbins[i-1].sm;
  sm += (lvmp - vbins[i-1].v)*(vbins[i].sm-vbins[i-1].sm)/(vbins[i].v - vbins[i-1].v);
  return sm;
}

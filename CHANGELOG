Updates since DR1:
*) Fix to umachine_server.c to allow a number of blocks that is not evenly
    divisible by the number of nodes (Thanks to Richie Wang for finding).
*) Changed uncorrelated random contribution for SFR to use RA halo field instead
    of internal random number.

DR1 (v1.0):
*) Significant updates to smooth chi^2 error surface.
*) Fixed small errors with chi^2 calculation for correlation functions.
*) Additional parameter to adjust orphan threshold for massive galaxies.
*) All satellites below orphan threshold are now considered disrupted.
*) RDECAY fixed to 1 to prevent unphysically long periods off the main sequence.
*) Additional IRX data to constrain dust parameters at high z.
*) Additional cross-correlation data to constrain new orphan parameter.
*) Removal of z=0 Mousatakas quenched fractions in favor of GAMA-based quenched fractions.
*) Recalculation of correlation functions at z=0 to take into account different quenched fractions.
*) Initial fitting routines now more functional.

EDR (v0.99):
*) Initial code release.

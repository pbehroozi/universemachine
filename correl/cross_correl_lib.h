#ifndef _CORREL_LIB_H_
#define _CORREL_LIB_H_
#include <inttypes.h>

struct correl_galaxy {
  double ra, dec, z, sm, sfr, sfr_fib, specsfr, sm_fib, specsfr_fib;
  double zweight, tweight;
  int32_t within_r, eligible, bptclass, bptclassp;
  int32_t N2;
  double d4000[4], pmag, amag;
  float rp, r2, dsm, zp, ssfrp, smp;
};

double cross_correl_library_init(double mi_sm, double ma_sm, double ma_pi, double ma_r, double mi_z, double ma_z, int64_t nr, char *regions_file, double **output_corr);
int64_t set_hg(struct correl_galaxy *hgals, int64_t ngals);
int64_t set_hg2(struct correl_galaxy *hgals, int64_t ngals);
void obs_calc_cross_corr(double *output_corr);
void switch_trees(void);
void set_sm_limits(double mi_sm, double ma_sm);

#endif /* _CORREL_LIB_H_ */

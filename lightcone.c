#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <inttypes.h>
#include "make_sf_catalog.h"
#include "matrices.h"
#include "distance.h"
#include "mt_rand.h"
#include "lightcone_cache.h"
#include "trapezoid_prism.h"
#include "lightcone.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "config.h"
#include "version.h"

struct halo_cat *halo_cats = NULL;
void lightcone_cb(int x, int y, int z, void *extra_data);
void calculate_trapezoid_points(float *x, float *y, float *z, float *o,
				float redshift, float inv_rot[3][3],
				float ytan, float ztan, int c);
int collision = 0;
char collision_test[COL_SIZE][COL_SIZE][COL_SIZE];
int extra_rotation = 0;
float extra_rot[3][3] = {{0}};


int main(int argc, char **argv) {
  float z1, z2, a1, zl, zh;
  float x[8], y[8], z[8];
  int samples, i, j, trial_counts=0;
  float rot[3][3];
  char buffer[200];
  int first_cat = 1;
  int do_collisions = 0;
  float e_ra=0, e_dec=0, e_theta=0;
  char *tag = 0;
  struct lightcone_data ldata;
  struct lightcone_data *ldata_list = NULL;
  struct halo_cat *cur_cat, *last_cat = NULL;
  if (argc < 7) {
    printf("%s", INFO_STRING);
    printf("Usage: %s um.cfg z_low z_high x_arcmin y_arcmin samples [id_tag do_collision_test ra dec theta rseed]\n",
	   argv[0]);
    exit(EXIT_SUCCESS);
  }

  do_config(argv[1]);

  if (argc > 7) tag = argv[7];
  if (argc > 8) do_collisions = atoi(argv[8]);
  if (argc > 9) e_ra = atof(argv[9]);
  if (argc > 10) e_dec = atof(argv[10]);
  if (argc > 11) e_theta = atof(argv[11]);
  if (e_ra || e_dec || e_theta) {
    extra_rotation = 1;
    rotate3(-e_dec*M_PI/180.0, -e_ra*M_PI/180.0, e_theta*M_PI/180.0, extra_rot);
  }
  int64_t rseed = time(NULL);
  if (argc > 12) rseed = atoll(argv[12]);
  
  
  init_cosmology(Om, Ol, h0);
  
  samples = atoi(argv[6]);
  ldata.ytan = tan(fabs(atof(argv[4])/2.0)*M_PI / (180.0*60.0));
  ldata.ztan = tan(fabs(atof(argv[5])/2.0)*M_PI / (180.0*60.0));
  if (!ldata.ytan || !ldata.ztan) {
    fprintf(stderr, "[Error] Zero survey size!\n");
    exit(EXIT_FAILURE);
  }

  check_calloc_s(ldata_list, sizeof(struct lightcone_data), samples);
  load_halos(&halo_cats);
  r250_init(rseed);

  z1 = atof(argv[2]);
  z2 = atof(argv[3]);
  if (z1 > z2) { zl = z1; z1 = z2; z2 = zl; }
  a1 = 1.0/(z1+1.0);

  cur_cat = halo_cats;
  while (cur_cat &&  a1 < cur_cat->scale) {
    last_cat = cur_cat;
    cur_cat = cur_cat->next;
  }
  zl = z1;

  do {
    load_cat_data(cur_cat);

    if (first_cat) {
      for (i=0; i<samples; i++) {
	trial_counts = 0;
	do {
	  clear_collision_test();
	  for (j=0; j<3; j++) ldata.origin[j] = BOX_SIZE*dr250();
	  rand_rotation(rot);
	  inv_rot_matrix(rot, ldata.inv_rot);
	  
	  calculate_trapezoid_points(x,y,z,ldata.origin,z1,ldata.inv_rot,
				     ldata.ytan,ldata.ztan,1);
	  calculate_trapezoid_points(x+4,y+4,z+4,ldata.origin,z2,ldata.inv_rot,
				     ldata.ytan,ldata.ztan,0);
	  trapezoid_prism(x,y,z,BOX_SIZE/((float)COL_SIZE), collision_cb, NULL);
	  trial_counts++;
	} while (collision && (trial_counts < 1e6) && do_collisions);
	if (tag)
	  fprintf(stderr, "#Origin: %f %f %f (%s)\n", ldata.origin[0], 
		  ldata.origin[1], ldata.origin[2], tag);
	else 
	  fprintf(stderr, "#Origin: %f %f %f\n", ldata.origin[0], 
		  ldata.origin[1], ldata.origin[2]);
	ldata_list[i] = ldata;
	if (trial_counts == 1e6) {
	  fprintf(stderr, "[Error] Couldn't fit lightcone inside of box!\n");
	  exit(EXIT_FAILURE);
	}
      }
    }

    for (i=0; i<samples; i++) {
      ldata = ldata_list[i];
      if (tag)
	sprintf(buffer, "survey_%s_z%.2f-%.2f_x%.2f_y%.2f_%d.dat", 
		tag, z1, z2, atof(argv[4]), atof(argv[5]), i);
      else
	sprintf(buffer, "survey_z%.2f-%.2f_x%.2f_y%.2f_%d.dat", 
		z1, z2, atof(argv[4]), atof(argv[5]), i);
      ldata.output = check_fopen(buffer, first_cat ? "w" : "a");

      if (first_cat) {
	fprintf(ldata.output, "#RA Dec Z(los) Z(cosmo) Scale ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV A_UV\n");
	fprintf(ldata.output, "#Command line: ");
	for (j=0; j<argc; j++) fprintf(ldata.output, "%s ", argv[j]);
	fprintf(ldata.output, "\n");
	fprintf(ldata.output, "#Random seed: %"PRId64"\n", rseed);
	fprintf(ldata.output, "#Origin (O): %f %f %f\n", ldata.origin[0], 
		ldata.origin[1], ldata.origin[2]);
	fprintf(ldata.output, "#Cosmology: Om: %.3f; Ol: %.3f; h: %.3f\n", Om, Ol, h0);
	fprintf(ldata.output, "#Original box size: %f Mpc/h\n", BOX_SIZE);
	fprintf(ldata.output, 
		"#Ra, Dec: RA and Dec in degrees.\n"
		"#Z(los): Redshift including redshift-space distortions.\n"
		"#Z(cosmo): Cosmological redshift---i.e., without redshift-space distortions.\n"
		"#Scale: Scale factor of the timestep from which the halo was taken\n"
		"#ID: Halo ID (unique across all timesteps)\n"
		"#DescID: ID of descendant halo (or -1 at z=0).\n"
		"#UPID: -1 for central halos, otherwise, ID of largest parent halo\n"
		"#Flags: Ignore\n"
		"#Uparent_Dist: Ignore\n"
		"#XYZ: Position of halo relative to axes of lightcone (X = distance along lightcone, Y,Z = distance perpendicular to lightcone X-axis) in comoving Mpc / h.\n"
                "#VX,VY,VZ: Peculiar velocity in physical (i.e., non-comoving) km/s, relative to lightcone axes above\n"
		"#M: Halo mass (Bryan & Norman 1998 virial mass, Msun)\n"
		"#V: Halo vmax (physical km/s)\n"
		"#MP: Halo peak historical mass (BN98 vir, Msun)\n"
		"#VMP: Halo vmax at the time when peak mass was reached.\n"
		"#R: Halo radius (BN98 vir, comoving kpc/h)\n"
		"#Rank1: halo rank in Delta_vmax (see UniverseMachine paper)\n"
		"#Rank2, RA, RARank: Ignore\n"
		"#SM: True stellar mass (Msun)\n"
		"#ICL: True intracluster stellar mass (Msun)\n"
		"#SFR: True star formation rate (Msun/yr)\n"
		"#Obs_SM: observed stellar mass, including random & systematic errors (Msun)\n"
		"#Obs_SFR: observed SFR, including random & systematic errors (Msun/yr)\n"
		"#SSFR: observed SSFR\n"
		"#SMHM: SM/HM ratio\n"
		"#Obs_UV: Observed UV Magnitude (M_1500 AB)\n"
		"#A_UV: UV attenuation from dust (AB mag)\n");

	fprintf(ldata.output, "#Rotation Matrix (R):\n");
	fprintf(ldata.output, "#  [[%f %f %f],\n", ldata.inv_rot[0][0], 
		ldata.inv_rot[1][0], ldata.inv_rot[2][0]);
	fprintf(ldata.output, "#   [%f %f %f],\n", ldata.inv_rot[0][1], 
		ldata.inv_rot[1][1], ldata.inv_rot[2][1]);
	fprintf(ldata.output, "#   [%f %f %f]],\n", ldata.inv_rot[0][2], 
		ldata.inv_rot[1][2], ldata.inv_rot[2][2]);
	if (extra_rotation) {
	  fprintf(ldata.output, "#Additional Rotation Matrix (R2):\n");
	  fprintf(ldata.output, "#  [[%f %f %f],\n", extra_rot[0][0], 
		  extra_rot[1][0], extra_rot[2][0]);
	  fprintf(ldata.output, "#   [%f %f %f],\n", extra_rot[0][1], 
		  extra_rot[1][1], extra_rot[2][1]);
	  fprintf(ldata.output, "#   [%f %f %f]],\n", extra_rot[0][2], 
		  extra_rot[1][2], extra_rot[2][2]);
	  fprintf(ldata.output, "#Location conversion procedure: X_lc = R*(X_sim - O) \n");
	  fprintf(ldata.output, "#Location conversion procedure: X_sky = R2*R*(X_sim - O) = R2*X_lc\n");
	}
	else {
	  fprintf(ldata.output, "#Location conversion procedure: X_sky = X_lc = R*(X_sim - O) \n");
	}
	fprintf(ldata.output, "#RA Dec Z(los) Z(cosmo) Scale ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV A_UV\n");
      }
      
      calculate_trapezoid_points(x,y,z,ldata.origin,zl,ldata.inv_rot,
				 ldata.ytan,ldata.ztan,1);
      zh = (cur_cat->next) ? (2.0/(cur_cat->scale + cur_cat->next->scale) - 1.0)
	: z2;
      if (zh > z2) zh = z2;
      calculate_trapezoid_points(x+4,y+4,z+4,ldata.origin,zh,ldata.inv_rot,
				 ldata.ytan,ldata.ztan,0);
      ldata.d2min = Dch(zl)*Dch(zl);
      ldata.d2max = Dch(zh)*Dch(zh);
      ldata.cat = cur_cat;
      trapezoid_prism(x,y,z,BOX_SIZE/((float)CACHE_SIZE), lightcone_cb, &ldata);
      fclose(ldata.output);
    }
    clear_halos();
    last_cat = cur_cat;
    cur_cat = cur_cat->next;
    if (cur_cat) { zl = 2.0/(last_cat->scale + cur_cat->scale) - 1.0; }
    fprintf(stderr, ".");
    first_cat = 0;
  } while (cur_cat && zl < z2);
  return 0;
}


void clear_collision_test(void) {
  int i,j,k;
  collision = 0;
  for (i=0; i<COL_SIZE; i++)
    for (j=0; j<COL_SIZE; j++)
      for (k=0; k<COL_SIZE; k++)
	collision_test[i][j][k] = 0;
}

void collision_cb(int x, int y, int z, void *extra_data) {
  int wrap_x = floor((float)x/COL_SIZE);
  int wrap_y = floor((float)y/COL_SIZE);
  int wrap_z = floor((float)z/COL_SIZE);
  x -= wrap_x*COL_SIZE;
  y -= wrap_y*COL_SIZE;
  z -= wrap_z*COL_SIZE;
  if (collision_test[x][y][z]++)
    collision = 1;
}

void calculate_trapezoid_points(float *x, float *y, float *z, float *o,
				float redshift, float inv_rot[3][3],
				float ytan, float ztan, int c)
{
  float xcorr = 1.0/sqrt(1 + ztan*ztan + ytan*ytan);
  float dx = c ? Dch(redshift)*xcorr : Dch(redshift);
  float dy = ytan*dx;
  float dz = ztan*dx;
  int i;
  float xc[3], yc[3], zc[3];

  for (i=0; i<3; i++) {
    xc[i] = o[i] + inv_rot[i][0]*dx;
    yc[i] = inv_rot[i][1]*dy;
    zc[i] = inv_rot[i][2]*dz;
  }

#define setpoint(a,b,c) \
  x[a] = xc[0] b yc[0] c zc[0]; \
  y[a] = xc[1] b yc[1] c zc[1]; \
  z[a] = xc[2] b yc[2] c zc[2];

  setpoint(0,+,+);
  setpoint(1,+,-);
  setpoint(2,-,-);
  setpoint(3,-,+);
#undef setpoint
}


void lightcone_cb(int x, int y, int z, void *extra_data) {
  int wrap_x = floor((float)x/CACHE_SIZE);
  int wrap_y = floor((float)y/CACHE_SIZE);
  int wrap_z = floor((float)z/CACHE_SIZE);
  float d[3];
  float p[3];
  float p2[3];
  float p3[3];
  float v[3];
  const float lightspeed = 299792.458; // in km/s
  float ra, dec, redshift, r;
  int i;
  struct lightcone_data *ldata = (struct lightcone_data *)extra_data;
  struct catalog_halo *halo;

  x -= wrap_x*CACHE_SIZE;
  y -= wrap_y*CACHE_SIZE;
  z -= wrap_z*CACHE_SIZE;
  d[0] = wrap_x*BOX_SIZE - ldata->origin[0];
  d[1] = wrap_y*BOX_SIZE - ldata->origin[1];
  d[2] = wrap_z*BOX_SIZE - ldata->origin[2];

  if (!ldata->cat->ncache[x][y][z]) return;
  for (halo = ldata->cat->loc[x][y][z]; halo<ldata->cat->loc[x][y][z]+ldata->cat->ncache[x][y][z]; halo++) {
    if (halo->flags & IGNORE_FLAG) continue;
    if ((halo->obs_uv > LIGHTCONE_MIN_UV) && (halo->mp < LIGHTCONE_MIN_HM)) continue;
    for (i=0; i<3; i++) p[i] = halo->pos[i] + d[i];
    if (lightcone_test(p, ldata->inv_rot, ldata->ytan, ldata->ztan,
		       ldata->d2min, ldata->d2max)) {
      float ssfr = 0;
      if (halo->obs_sm > 0)
	ssfr = halo->obs_sfr / halo->obs_sm;
      float smhm = halo->sm*h0 / halo->mp;
      mult_vector(ldata->inv_rot, p, p3);
      mult_vector(ldata->inv_rot, halo->pos+3, v);
      if (extra_rotation) mult_vector(extra_rot, p3, p2);
      else memcpy(p2, p3, sizeof(float)*3);
      ra = atan2(-p2[1], p2[0])*(180.0/M_PI);
      r = sqrtf(p[0]*p[0] + p[1]*p[1] + p[2]*p[2]);
      dec = (r>0) ? ((float)(asinf(p2[2]/r)*180.0/M_PI)) : ((float)0.0);
      redshift = comoving_distance_to_redshift(r/h0);
      if (ra < 0) ra += 360;
      //      fprintf(ldata.output, "#RA Dec Z(los) Z(cosmo) Scale ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV A_UV\n");	      
      fprintf(ldata->output, "%f %f %f %f %f %"PRId64" %"PRId64" %"PRId64" %"PRId32" %f %f %f %f %f %f %f %.3e %f %.3e %f %f %f %f %f %f %.3e %.3e %.3e %.3e %.3e %.3e %.3e %.3f %.3f\n",
	      ra, dec, (1.0+redshift)*(1.0 + v[0]/lightspeed)-1.0, redshift, ldata->cat->scale,
	      halo->id, halo->descid, halo->upid, halo->flags, halo->uparent_dist, p3[0], p3[1], p3[2], v[0], v[1], v[2], halo->m/h0, halo->v, halo->mp/h0, halo->vmp, halo->r, halo->rank1, halo->rank2, halo->ra, halo->rarank, halo->sm, halo->icl, halo->sfr, halo->obs_sm, halo->obs_sfr, ssfr, smhm, halo->obs_uv, A_UV((*halo)));
    }
  }
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include <unistd.h>
#include "make_sf_catalog.h"
#include "check_syscalls.h"

struct catalog_halo **halos=NULL;
int64_t **h_offsets=NULL, num_scales=0, num_boxes = 0;
float *scales = NULL;

#define MIN_VMP 1.0
#define MAX_VMP 3.5
#define VMP_BPDEX 4
#define VMP_BINS 12

double av1[VMP_BINS], av2[VMP_BINS], av12[VMP_BINS], av11[VMP_BINS], av22[VMP_BINS], weight[VMP_BINS];
int64_t counts[VMP_BINS];

void clear_correlations(void);
void load_box_mmap(char *directory, int64_t box_num);
void print_correlations(char *directory, int64_t n);
void calc_averages(void);
void correlate_ranks(int64_t n, int64_t i);
void correlate_ranks_p2(int64_t n, int64_t i);


int main(int argc, char **argv) {
  int64_t n, i;
  if (argc<3) {
    printf("Usage: %s directory num_boxes\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  num_boxes = atol(argv[2]);
  check_realloc_s(halos, sizeof(struct catalog_halo *), num_boxes);
  check_realloc_s(h_offsets, sizeof(int64_t *), num_boxes);
  memset(h_offsets, 0, sizeof(int64_t *)*num_boxes);
  for (i=0; i<num_boxes; i++)
    load_box_mmap(argv[1], i);

  for (n=0; n<num_scales; n++) {
    clear_correlations();
    for (i=0; i<num_boxes; i++) correlate_ranks(n, i);
    calc_averages();
    for (i=0; i<num_boxes; i++) correlate_ranks_p2(n, i);
    print_correlations(argv[1], n);
  }
  return 0;
}


void clear_correlations(void) {
  memset(av1, 0, sizeof(double)*VMP_BINS);
  memset(av2, 0, sizeof(double)*VMP_BINS);
  memset(av12, 0, sizeof(double)*VMP_BINS);
  memset(av11, 0, sizeof(double)*VMP_BINS);
  memset(av22, 0, sizeof(double)*VMP_BINS);
  memset(weight, 0, sizeof(double)*VMP_BINS);
  memset(counts, 0, sizeof(int64_t)*VMP_BINS);
}

void correlate_ranks(int64_t n, int64_t i) {
  int64_t j;
  for (j=h_offsets[i][n]; j<h_offsets[i][n+1]; j++) {
    int64_t bin = (halos[i][j].lvmp-MIN_VMP)*VMP_BPDEX;
    if ((bin < 0) || (bin >= VMP_BINS)) continue;
    counts[bin]++;
    float v = halos[i][j].vmp;
    float w = v*v*v;
    weight[bin] += w;
    float r1 = halos[i][j].rank1;
    float r2 = halos[i][j].rank2;
    av1[bin] += r1*w;
    av2[bin] += r2*w;
  }  
}

void calc_averages(void) {
  int64_t i;
  for (i=0; i<VMP_BINS; i++) {
    if (counts[i]>0 && weight[i]>0) {
      av1[i] /= weight[i];
      av2[i] /= weight[i];
    }
  }
}

void correlate_ranks_p2(int64_t n, int64_t i) {
  int64_t j;
  for (j=h_offsets[i][n]; j<h_offsets[i][n+1]; j++) {
    int64_t bin = (halos[i][j].lvmp-MIN_VMP)*VMP_BPDEX;
    if ((bin < 0) || (bin >= VMP_BINS)) continue;
    float v = halos[i][j].vmp;
    float w = v*v*v;
    float r1 = halos[i][j].rank1-av1[bin];
    float r2 = halos[i][j].rank2-av2[bin];
    av12[bin] += r1*r2*w;
    av11[bin] += r1*r1*w;
    av22[bin] += r2*r2*w;
  }  
}

void print_correlations(char *directory, int64_t n) {
  char buffer[1024];
  snprintf(buffer, 1024, "%s/rank_corrs.scale%"PRId64".txt", directory, n);
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#a = %f\n", scales[n]);
  fprintf(out, "#VMP(min) VMP(max) Counts Av1 Av2 SD1 SD2 Corr.12\n");
  int64_t i;
  for (i=0; i<VMP_BINS; i++) {
    double vmp_min = MIN_VMP+(double)i/(double)VMP_BPDEX;
    double vmp_max = MIN_VMP+(double)(i+1)/(double)VMP_BPDEX;
    double corr = 0;
    if (counts[i] && weight[i]) {
      av12[i] /= weight[i];
      av11[i] /= weight[i];
      av22[i] /= weight[i];
      av11[i] = sqrt(av11[i]);
      av22[i] = sqrt(av22[i]);
      if (av11[i]*av22[i] > 0)
	corr = av12[i] / (av11[i]*av22[i]);
    }
    fprintf(out, "%f %f %"PRId64" %f %f %f %f %g\n", vmp_min, vmp_max, counts[i], av1[i], av2[i], av11[i], av22[i], corr);
  }
  fclose(out);
}

void load_box_mmap(char *directory, int64_t box_num) {
  int64_t n, snap, offset;
  float scale;
  char buffer[1024];

  //First load h_offsets from file:
  int64_t scales_read = 0;
  snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", directory, box_num);
  FILE *in = check_fopen(buffer, "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') {
      if (!strncmp(buffer, "#Total halos: ", 14)) {
	offset = atol(buffer+14);
	check_realloc_every(h_offsets[box_num], sizeof(int64_t), num_scales, 100);
	h_offsets[box_num][scales_read] = offset;
      }
      continue;
    }
    n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
    if (n!=3) continue;
    check_realloc_every(h_offsets[box_num], sizeof(int64_t), scales_read, 100);
    h_offsets[box_num][scales_read] = offset;
    scales_read++;
    if (!box_num) {
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      num_scales++;
    }
  }
  fclose(in);

  //Load halos
  snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", directory, box_num);
  halos[box_num] = check_mmap_file(buffer, 'r', NULL);
}

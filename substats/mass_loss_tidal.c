#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include "../check_syscalls.h"
#include "../stringparse.h"
#include "../inthash.h"

struct halo {
  int64_t id, tid;
  float pos[6];
  float mar[4];
  float vmp;
  float m, r, v, tf;
  int32_t mmp;
};

int64_t num_halos=0;
double BOX_SIZE = 250;

float calc_rvir_ratio(struct halo *h1, struct halo *h2);

int main(int argc, char **argv)
{
  int64_t i, n;
  char buffer[1024];

  if (argc < 3) {
    fprintf(stderr, "Usage: %s hlist box_size\n", argv[0]);
    exit(1);
  }

  struct halo d = {0};
#define NUM_FIELDS 18

  SHORT_PARSETYPE;
  struct parse_format pf[NUM_FIELDS] = 
    {{1, D64, &d.id}, {10, F, &d.m}, {11, F, &d.r}, {14, D, &d.mmp}, {16, F, &d.v}, {17, F, d.pos}, {18, F, d.pos+1}, {19, F, d.pos+2}, {20, F, d.pos+3}, {21, F, d.pos+4}, {22, F, d.pos+5}, {35, F, &d.tf}, {36, D64, &d.tid}, {64, F, d.mar}, {66, F, d.mar+1}, {69, F, d.mar+2}, {70, F, d.mar+3}, {76, F, &d.vmp}};

  struct halo *halos = NULL;
  FILE *in = check_fopen(argv[1], "r");
  BOX_SIZE = atof(argv[2]);
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    n = stringparse_format(buffer, pf, NUM_FIELDS);
    if (n<NUM_FIELDS) continue;
    check_realloc_every(halos, sizeof(struct halo), num_halos, 1000);
    halos[num_halos] = d;
    num_halos++;
  }
  fclose(in);

  struct inthash *ih;
  ih = new_inthash();
  ih_prealloc(ih, num_halos);
  for (i=0; i<num_halos; i++)
    ih_setval(ih, halos[i].id, halos+i);

  printf("#ID TID MMP? VMP Host_M Host_R Host_V M V Dist/Host_R M/Host_M V/Host_V TF dM/dt dM/dt(dyn) dlogV/dt dlogV/dt(dyn)\n");
  for (i=0; i<num_halos; i++) {
    struct halo *h = halos+i;
    if (h->tid < 0 || h->vmp < 50) continue;
    struct halo *p = ih_getval(ih, h->tid);
    assert(p > 0);
    float rvir_ratio = calc_rvir_ratio(p, h);
    float mass_ratio = (p->m > 0) ? h->m/p->m : 0;
    float vmax_ratio = (p->v > 0) ? h->v/p->v : 0;
    printf("%"PRId64" %"PRId64" %d %.5f %.3e %.3f %.5f %.3e %.3f %.4e %.4e %.4e %.4e %.4e %.4e %.4e %.4e\n", h->id, h->tid, h->mmp, h->vmp, p->m, p->r, p->v, h->m, h->v, rvir_ratio, mass_ratio, vmax_ratio, h->tf, h->mar[0], h->mar[1], h->mar[2], h->mar[3]);
  }
  return 0;
}


float calc_rvir_ratio(struct halo *h1, struct halo *h2) {
  double ds=0, dx;
  int64_t i;
  for (i=0; i<3; i++) {
    dx = fabs(h1->pos[i]-h2->pos[i]);
    if (dx > BOX_SIZE/2.0)
      dx = BOX_SIZE - dx;
    ds += dx*dx;
  }
  ds = sqrt(ds);
  return (ds / (h1->r/1.0e3));
}
